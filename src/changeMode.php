<?php declare(strict_types = 1);

include_once 'private/page.php';
include_once 'private/jsFunctions.php';

class ChangeModePage extends Page
{
    function __construct()
    {
        parent::__construct();

        if ($this->m_access->hasAccess(AccessRight::ChangeSetting))
        {
            if (isset($_POST['newMode']))
            {
                $newMode = $_POST['newMode'];
            }
            else
            {
                $newMode = ProgramMode::PreRegistration;
            }

            $javaScript = getJSFunction_post();
            $style = "";
            $this->outputHeader($javaScript, $style);

            echo "<p class='big'>Programm Modus ändern</p>";


            $this->changeMode($newMode);
            echo "<p class='yellow'>Der aktuelle Programm Modus wurde geändert.</p>";

            $this->beginFooter();
            $this->outputBackButton("admin.php");
            $this->endFooter();
        }
        else
        {
            $this->outputWrongPasswordErrorMessage();
        }
    }

    function changeMode(string $newMode)
    {
        $setting = array();
        $setting['program']['mode'] = $newMode;
        Config::GetInstance()->writeSettings($setting);
    }
}

new ChangeModePage();

?>
