<?php declare(strict_types = 1);

include_once 'private/page.php';
include_once 'private/entry.php';
include_once 'private/entrySorter.php';
include_once 'private/database.php';
include_once 'private/entries.php';
include_once 'private/addDraftOverlay.php';
include_once 'private/PDFCreator/Table.php';
include_once 'private/getRegistrationLink.php';
include_once 'private/post.php';

class DownloadResultsPage extends Page
{
    function __construct()
    {
        parent::__construct();

        if ($this->m_access->hasAccess(AccessRight::Read))
        {
            $this->initWidths();
            $this->createLists();
        }
        else
        {
            $this->outputWrongPasswordErrorMessage();
        }
    }

    function getTableHead(int $attribute): string
    {
        return Entry::GetAttributeShortName($attribute);
    }

    function initWidths()
    {
        $listsWidthConfig = Config::Get()['resultlists']['width'];
        $this->m_width[EntryAttribute::Placing] = floatval($listsWidthConfig['placing']);
        $this->m_width[EntryAttribute::PlacingMW] = floatval($listsWidthConfig['placingMW']);
        $this->m_width[EntryAttribute::PlacingAgeGroup] = floatval($listsWidthConfig['placingAgeGroup']);
        $this->m_width[EntryAttribute::AgeGroup] = floatval($listsWidthConfig['ageGroup']);
        $this->m_width[EntryAttribute::Startnr] = floatval($listsWidthConfig['startnr']);
        $this->m_width[EntryAttribute::Year] = floatval($listsWidthConfig['year']);
        $this->m_width[EntryAttribute::Time] = floatval($listsWidthConfig['time']);
        $this->m_width[EntryAttribute::Run] = floatval($listsWidthConfig['run']);
    }

    function getWidth(int $sortOrder): float
    {
        return $this->m_width[$sortOrder];
    }

    function createLists()
    {
        $overview = Post::GetBoolValueFromPost("overview");

        $runs = Runs::GetInstance()->getRuns();
        $numberOfRuns = count($runs);
        $runList = array();
        for ($runIndex = 0; $runIndex < $numberOfRuns; $runIndex++)
        {
            $name = "run" . $runIndex . "List";
            $runList[$runIndex] = Post::GetBoolValueFromPost($name);
        }

        $eventName = Config::Get()['event']['name'];
        $registrationLink = getRegistrationLink();

        $this->m_database = new Database();
        $this->m_database->close();

        $timeOfLastModification = $this->m_database->getTimeOfLastModificationAsString();
        $filename = "ergebnisliste_$timeOfLastModification";
        if ($overview)
        {
            $filename .= "_u";
        }

        for ($runIndex = 0; $runIndex < $numberOfRuns; $runIndex++)
        {
            if ($runList[$runIndex])
            {
                $filename .= "_r". ($runIndex + 1);
            }
        }

        $filename .=".pdf";

        $this->m_entries = $this->m_database->getEntries();
        Entries::RemoveEntriesWithInvalidTime($this->m_entries);

        $this->m_pdfCreator = new PDFCreator();

        $fontSize = 8;
        $this->m_pdfCreator->m_pdf->SetFont('dejavusans', '', $fontSize, '');

        $headerTitle = "Ergebnisse $eventName";
        $headerString = "Version: $timeOfLastModification\n$registrationLink";

        $this->m_pdfCreator->m_pdf->setHeaderData('../../res/ltlogo.svg', 20, $headerTitle, $headerString);
        $this->m_pdfCreator->m_pdf->setHeaderFont(Array(
            'dejavusans',
            '',
            10
        ));
        $this->m_pdfCreator->m_pdf->setFooterFont(Array(
            'dejavusans',
            '',
            8
        ));

        $this->m_fontName1 = new Font('dejavusansb', $fontSize + 4);
        $this->m_fontName2 = new Font('dejavusansb', $fontSize + 2);
        $this->m_fontHead = new Font('dejavusansb', $fontSize);
        $this->m_fontData = new Font('dejavusans', $fontSize);

        if ($overview)
        {
            $this->createOverview();
        }

        $this->createRunLists($runList);

        addDraftOverlay($this->m_pdfCreator->m_pdf);

        $this->m_pdfCreator->m_pdf->Output($filename, "D");
    }

    function outputLineFeed(float $lf)
    {
        $textObject = new TextObject($this->m_pdfCreator, "");
        $textObject->setFont($this->m_fontData);
        $textObject->setLineFeed($lf);

        if ($this->m_pdfCreator->fitsOnPage($textObject->getHeight()))
        {
            $textObject->generate();
        }
        else
        {
            $this->m_pdfCreator->doPageBreak();
        }
    }

    function createOverview()
    {
        $this->m_pdfCreator->doPageBreak();

        $this->createCompetitionTable();
        $this->createTeamTable();
        $this->createWinnerTables();
    }

    function createCompetitionTable()
    {
        $table = new Table($this->m_pdfCreator, "Beteiligung Wettbewerbe", $this->m_fontName1, $this->m_fontHead, $this->m_fontData);
        $registrationsWidth = 15.0;
        $sumWidth = 2 * $registrationsWidth;
        $longWidth = 100 - $sumWidth;

        $table->addTableHead("Wettbewerb", $longWidth, 'L');
        $table->addTableHead("Meldungen", $registrationsWidth, 'R');
        $table->addTableHead("Finisher", $registrationsWidth, 'R');

        $runs = Runs::GetInstance()->getRuns();

        $numberOfFinisherPerRun = $this->m_database->getNumberOfFinisherPerRun();
        $numberOfRegistrationsPerRun = $this->m_database->getNumberOfRegistrationsPerRun();
        foreach ($runs as $run)
        {
            $table->addTableData($run->getName());
            $table->addTableData(strval($numberOfRegistrationsPerRun[$run->getIndex()]));
            $table->addTableData(strval($numberOfFinisherPerRun[$run->getIndex()]));
        }

        $table->end();
        $this->outputLineFeed(1.0);
    }

    function createTeamTable()
    {
        $table = new Table($this->m_pdfCreator, "Beteiligung Vereine", $this->m_fontName1, $this->m_fontHead, $this->m_fontData);
        $registrationsWidth = 15.0;
        $sumWidth = 2 * $registrationsWidth;
        $longWidth = 100 - $sumWidth;

        $table->addTableHead($this->getTableHead(EntryAttribute::Team), $longWidth, 'L');
        $table->addTableHead("Meldungen", $registrationsWidth, 'R');
        $table->addTableHead("Finisher", $registrationsWidth, 'R');

        $teamList = $this->m_database->getRegistrationsAndFinisherTeamList();
        foreach ($teamList as $team => $count)
        {
            if ($team == "")
            {
                $team = "---";
            }
            $table->addTableData($team);
            $table->addTableData(strval($count->registrations));
            $table->addTableData(strval($count->finisher));
        }

        $table->end();
    }

    function createWinnerTables()
    {
        $this->m_pdfCreator->doPageBreak();
        foreach (["m","w"] as $gender)
        {
            $table = new Table($this->m_pdfCreator, "Sieger (" . strtoupper($gender) . ")", $this->m_fontName1, $this->m_fontHead, $this->m_fontData);
            $sumWidth = $this->getWidth(EntryAttribute::Run) + $this->getWidth(EntryAttribute::Time);

            $longWidth = (100 - $sumWidth) / 2;

            $runs = Runs::GetInstance()->getRuns();

            $winnerTable = $this->getWinnerTable();
            $table->addTableHead("Wettbewerb", $this->getWidth(EntryAttribute::Run), 'L');
            $table->addTableHead(self::$TableHead_Name, $longWidth, 'L', true);
            $table->addTableHead($this->getTableHead(EntryAttribute::Team), $longWidth, 'L');
            $table->addTableHead($this->getTableHead(EntryAttribute::Time), $this->getWidth(EntryAttribute::Time), 'R');

            foreach ($runs as $run)
            {
                $table->addTableData($run->getName());

                $entries = $winnerTable[$gender][$run->getIndex()];

                if (!empty($entries))
                {
                    $names = "";
                    $teams = "";
                    foreach ($entries as $entry)
                    {
                        if ($names != "")
                        {
                            $names .= " / ";
                            $teams .= " / ";
                        }
                        $names .= $entry->getLastnameFirstname();
                        $teams .= $entry->getTeam();
                    }
                    $time = $entries[0]->getTime()->getAsString(true, false, false);
                }
                else
                {
                    $names = "---";
                    $teams = "---";
                    $time = "---";
                }

                $table->addTableData($names);
                $table->addTableData($teams);
                $table->addTableData($time);
            }
            $table->end();
            $this->outputLineFeed(1.0);
        }
    }

    function getWinnerTable(): array
    {
        $numberOfRuns = Runs::GetInstance()->getNumberOfRuns();
        $winnerTable = [];
        for ($i = 0; $i < $numberOfRuns; $i++)
        {
            $winnerTable['m'][$i] = array();
            $winnerTable['w'][$i] = array();
        }

        foreach ($this->m_entries as $entry)
        {
            if ($entry->getPlacingMW() == 1)
            {
                $winnerTable[$entry->getGender()][$entry->getRun()][] = $entry;
            }
        }
        return $winnerTable;
    }

    function createRunLists(array $runList)
    {
        $groups = Entries::SplitEntriesByRun($this->m_entries);
        foreach ($groups as $groupEntries)
        {
            assert(!empty($groupEntries));
            $runIndex = $groupEntries[0]->getRun();
            if ($runList[$runIndex])
            {
                $this->createRunList($groupEntries, Runs::GetInstance()->getRun($runIndex));
            }
        }
    }

    function createRunList(array $entries, Run $run)
    {
        $this->createIncomingList($entries, $run);
        $this->createAgeGroupList($entries, $run);

        if ($run->getTeamPlacing())
        {
            $this->createTeamList($run);
        }
    }

    function createIncomingList(array $entries, Run $run)
    {
        $this->m_pdfCreator->doPageBreak();
        EntrySorter::Sort($entries, EntrySorter::OrderTimeLastnameIndex);

        $table = new Table($this->m_pdfCreator, "Einlaufliste • " . $run->getName(), $this->m_fontName1, $this->m_fontHead, $this->m_fontData);

        $sumWidth = $this->getWidth(EntryAttribute::Placing) + $this->getWidth(EntryAttribute::PlacingMW) + $this->getWidth(EntryAttribute::PlacingAgeGroup) +
            $this->getWidth(EntryAttribute::AgeGroup) + $this->getWidth(EntryAttribute::Startnr) + $this->getWidth(EntryAttribute::Year) +
            $this->getWidth(EntryAttribute::Time);

        $longWidth = (100 - $sumWidth) / 2;

        $table->addTableHead($this->getTableHead(EntryAttribute::Placing), $this->getWidth(EntryAttribute::Placing), 'R', true);
        $table->addTableHead($this->getTableHead(EntryAttribute::PlacingMW), $this->getWidth(EntryAttribute::PlacingMW), 'R');
        $table->addTableHead($this->getTableHead(EntryAttribute::PlacingAgeGroup), $this->getWidth(EntryAttribute::PlacingAgeGroup), 'R');
        $table->addTableHead($this->getTableHead(EntryAttribute::AgeGroup), $this->getWidth(EntryAttribute::AgeGroup), 'C');
        $table->addTableHead($this->getTableHead(EntryAttribute::Startnr), $this->getWidth(EntryAttribute::Startnr), 'R');
        $table->addTableHead(self::$TableHead_Name, $longWidth, 'L');
        $table->addTableHead($this->getTableHead(EntryAttribute::Year), $this->getWidth(EntryAttribute::Year), 'C');
        $table->addTableHead($this->getTableHead(EntryAttribute::Team), $longWidth, 'L');
        $table->addTableHead($this->getTableHead(EntryAttribute::Time), $this->getWidth(EntryAttribute::Time), 'R');

        foreach ($entries as $entry)
        {
            $table->addTableData($entry->getPlacingAsString());
            $table->addTableData($entry->getPlacingMWAsString());
            $table->addTableData($entry->getPlacingAgeGroupAsString());
            $table->addTableData($entry->getAgeGroupAsString());
            $table->addTableData($entry->getStartnrAsString());
            $table->addTableData($entry->getLastnameFirstname());
            $table->addTableData($entry->getYearAsString());
            $table->addTableData($entry->getTeamAsString());
            $table->addTableData($entry->getTime()->getAsString(true, false, false));
        }

        $table->end();
    }

    function createAgeGroupList(array $entries, Run $run)
    {
        $this->m_pdfCreator->doPageBreak();
        $groups = Entries::SplitEntriesByAgeGroupAndGender($entries);

        foreach ($groups as $groupEntries)
        {
            $firstEntry = $groupEntries[0];
            $ageGroupString = $firstEntry->getAgeGroupAsString();

            EntrySorter::Sort($groupEntries, EntrySorter::OrderTimeLastnameIndex);

            $table = new Table($this->m_pdfCreator, $ageGroupString . " • " . $run->getName(), $this->m_fontName1, $this->m_fontHead, $this->m_fontData);

            $sumWidth = $this->getWidth(EntryAttribute::PlacingAgeGroup) +
            $this->getWidth(EntryAttribute::Placing) +
            $this->getWidth(EntryAttribute::PlacingMW) +
            $this->getWidth(EntryAttribute::Startnr) +
            $this->getWidth(EntryAttribute::Year) +
            $this->getWidth(EntryAttribute::Time);

            $longWidth = (100 - $sumWidth) / 2;

            $table->addTableHead($this->getTableHead(EntryAttribute::PlacingAgeGroup), $this->getWidth(EntryAttribute::PlacingAgeGroup), 'R', true);
            $table->addTableHead($this->getTableHead(EntryAttribute::Placing), $this->getWidth(EntryAttribute::Placing), 'R');
            $table->addTableHead($this->getTableHead(EntryAttribute::PlacingMW), $this->getWidth(EntryAttribute::PlacingMW), 'R');
            $table->addTableHead($this->getTableHead(EntryAttribute::Startnr), $this->getWidth(EntryAttribute::Startnr), 'R');
            $table->addTableHead(self::$TableHead_Name, $longWidth, 'L');
            $table->addTableHead($this->getTableHead(EntryAttribute::Year), $this->getWidth(EntryAttribute::Year), 'C');
            $table->addTableHead($this->getTableHead(EntryAttribute::Team), $longWidth, 'L');
            $table->addTableHead($this->getTableHead(EntryAttribute::Time), $this->getWidth(EntryAttribute::Time), 'R');

            foreach ($groupEntries as $entry)
            {
                $table->addTableData($entry->getPlacingAgeGroupAsString());
                $table->addTableData($entry->getPlacingAsString());
                $table->addTableData($entry->getPlacingMWAsString());
                $table->addTableData($entry->getStartnrAsString());
                $table->addTableData($entry->getLastnameFirstname());
                $table->addTableData($entry->getYearAsString());
                $table->addTableData($entry->getTeamAsString());
                $table->addTableData($entry->getTime()->getAsString(true, false, false));
            }
            $table->end();
            $this->outputLineFeed(1.0);
        }
    }

    function createTeamList(Run $run)
    {
        $this->m_pdfCreator->doPageBreak();
        $this->createTeamListForGender($run, "m");
        $this->createTeamListForGender($run, "w");
    }

    function createTeamListForGender(Run $run, string $gender)
    {
        $teams = $this->m_database->getTeamPlacing($run->getIndex(), $gender);

        $title = "Mannschaften (" . strtoupper($gender) . ") • " . $run->getName();

        $sumWidth = $this->getWidth(EntryAttribute::Placing) +
        $this->getWidth(EntryAttribute::Startnr) +
        $this->getWidth(EntryAttribute::Year) +
        $this->getWidth(EntryAttribute::Time);

        $longWidth = 100 - $sumWidth;

        $table = new Table($this->m_pdfCreator, $title, $this->m_fontName1, $this->m_fontHead, $this->m_fontData);
        $table->addTableHead($this->getTableHead(EntryAttribute::Placing), $this->getWidth(EntryAttribute::Placing), 'R', true);
        $table->addTableHead($this->getTableHead(EntryAttribute::Startnr), $this->getWidth(EntryAttribute::Startnr), 'R');
        $table->addTableHead("Mannschaft", $longWidth, 'L');
        $table->addTableHead($this->getTableHead(EntryAttribute::Year), $this->getWidth(EntryAttribute::Year), 'C');
        $table->addTableHead($this->getTableHead(EntryAttribute::Time), $this->getWidth(EntryAttribute::Time), 'R');

        foreach ($teams as $team)
        {
            $this->createTeamItem($team, $table);
        }
        $table->end();
        $this->outputLineFeed(1.0);
    }

    function createTeamItem(Team $team, Table $table)
    {
        $teamName = $team->getTeamName();

        $table->addTableData(strval($team->getPlacing()));

        $startNrArray = array(
            "",
            $team->getEntry(0)->getStartnrAsString(),
            $team->getEntry(1)->getStartnrAsString(),
            $team->getEntry(2)->getStartnrAsString()
        );
        $table->addTableDataArray($startNrArray);

        $teamArray = array(
            $teamName,
            $team->getEntry(0)->getLastnameFirstname(),
            $team->getEntry(1)->getLastnameFirstname(),
            $team->getEntry(2)->getLastnameFirstname()
        );
        $table->addTableDataArray($teamArray);

        $yearArray = array(
            "",
            $team->getEntry(0)->getYearAsString(),
            $team->getEntry(1)->getYearAsString(),
            $team->getEntry(2)->getYearAsString()
        );
        $table->addTableDataArray($yearArray);

        $timeArray = array(
            $team->getTime()->getAsString(false, false, false),
            $team->getEntry(0)
                ->getTime()
                ->getAsString(false, false, false),
            $team->getEntry(1)
                ->getTime()
                ->getAsString(false, false, false),
            $team->getEntry(2)
                ->getTime()
                ->getAsString(false, false, false)
        );
        $table->addTableDataArray($timeArray);
    }

    private $m_database;
    private $m_entries = array();
    private $m_align = array();

    private $m_pdfCreator;

    private $m_fontName1;
    private $m_fontName2;
    private $m_fontHead;
    private $m_fontData;

    private $m_width;

    private static $TableHead_Name = "Name";
}

new DownloadResultsPage();

?>