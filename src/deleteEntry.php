<?php declare(strict_types = 1);

include_once 'private/page.php';
include_once 'private/entry.php';
include_once 'private/database.php';
include_once 'private/writeDeleteEntry.php';
include_once 'private/convertStringToHTML.php';
include_once 'private/getRegistrationLink.php';
include_once 'private/mail.php';
include_once 'private/jsFunctions.php';

class DeleteEntryPage extends Page
{
    function __construct()
    {
        parent::__construct();

        $javaScript = getJSFunction_post();
        if ($this->outputHeader($javaScript) > 0)
        {
            if (isset($_POST["key"]))
            {
                $key = $_POST["key"];
            }
            else
            {
                $key = "";
            }
            $database = new Database();
            $oldEntry = new Entry();

            $writeOk = writeDeleteEntry($database, $key, $oldEntry, $this->m_access->getUsername());
            if (!$database->close())
            {
                $writeOk = false;
            }

            if ($writeOk)
            {
                $mailSend = $this->sendMail($oldEntry);
                $this->successfulWrittenMessage($oldEntry, $mailSend);
            }

            $this->beginFooter();
            $this->outputBackButton("index.php");
            $this->endFooter();
        }
        else
        {
            $this->beginFooter();
            $this->outputBackButton("index.php");
            $this->endFooter();
        }
    }

    function sendMail(Entry $entry): bool
    {
        $sendMailAddress = Config::Get()['mail']['sendAddress'];

        if ($sendMailAddress == "")
        {
            return false;
        }

        $eventName = Config::Get()['event']['name'];

        $firstname = $entry->getFirstname();
        $lastname = $entry->getLastname();
        $subject = "Anmeldung $eventName / $firstname $lastname";

        $firstname = convertStringToHTML($firstname);
        $lastname = convertStringToHTML($lastname);
        $registrationLink = getRegistrationLink();
        $confirmationMailAddress = Config::Get()['mail']['confirmationAddress'];
        $logoURL = "$registrationLink/res/racetoolLogoMonochrome.png";

        switch (Config::Get()['event']['nameGenus'])
        {
            case "n":
                $article = "das";
                break;
            case "m":
                $article = "den";
                break;
            case "f":
                $article = "die";
                break;
        }

        $message = "<p>Sehr geehrte Damen und Herren!</p>\n";
        $message .= "<p>Die Löschung Ihrer Online-Anmeldung für $article $eventName für <b>$firstname $lastname</b> ist bei uns eingegangen.</p>\n";

        $message .= "<p>Mit freundlichem Gruß<br>\n";
        $message .= "Lauftreff Unterkirnach</p>\n";

        $message .= "<img src='$logoURL' width='150' style='vertical-align: middle;'>\n";

        $headerFields = array(
            "From: {$sendMailAddress}",
            "MIME-Version: 1.0",
            "Content-Type: text/html;charset=utf-8"
        );
        if ($confirmationMailAddress != "")
        {
            array_push($headerFields, "Bcc: $confirmationMailAddress");
        }

        if (!Mail::SendMail($entry->getEmail(), $subject, $message, implode("\n", $headerFields)))
        {
            echo "<p class='yellow'>Fehler: Bestätigungs E-Mail konnte nicht an \"" . $entry->getEmail() . "\" versendet werden !</p>";
            return false;
        }
        return true;
    }

    function filterInput(string &$input)
    {
        $input = str_replace(';', '', $input);
    }

    function successfulWrittenMessage(Entry $entry, bool $mailSend)
    {
        $eventName = Config::Get()['event']['name'];
        $address = convertStringToHTML($entry->getFirstname()) . " " . convertStringToHTML($entry->getLastname());

        switch (Config::Get()['event']['nameGenus'])
        {
            case "n":
                $article = "das";
                break;
            case "m":
                $article = "den";
                break;
            case "f":
                $article = "die";
                break;
        }

        echo "<p>Vielen Dank!</p>";
        echo "<p>Sie haben hiermit Ihre Anmeldung für $article $eventName für $address gelöscht.";

        if ($mailSend)
        {
            echo " Sie erhalten in Kürze eine Bestätigung per E-Mail an Ihre Adresse '" . convertStringToHTML($entry->getEmail()) . "'.</p>";
        }
    }
}

new DeleteEntryPage();

?>
