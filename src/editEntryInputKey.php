<?php declare(strict_types = 1);

include_once 'private/page.php';
include_once 'private/jsFunctions.php';

class EditEntryInputKeyPage extends Page
{
    function __construct()
    {
        parent::__construct();

        $javaScript = "";
        $javaScript .= getJSFunction_post();
        $style = "";

        $style .= <<<EOD
            .inputField {width: 8em;}
EOD;

        if ($this->outputHeader($javaScript, $style) > 0)
        {
            echo "<p  class='big'>Anmeldung ändern</p>";

            echo "<p class='yellow'>Hier können Sie Ihre Anmeldedaten nachträglich ändern.</br>";
            echo "Bitte geben Sie den sechsstelligen Code aus der Bestätigungs E-Mail ein.</p>";

            $this->beginForm("editEntryInput.php");

            echo "<p>";
            echo "<span class='marginRight'>Code</span>";
            echo "<input id='code' class='marginRight inputField' maxlength='6' name='key'>";
            echo "<button class='button noMarginBottom' type='submit'>Ok <div class='fas fa-check'></div></button>";
            echo "</p>";

            $this->endForm();
        }

        $this->beginFooter();
        $this->outputBackButton("index.php");
        $this->endFooter();
    }
}

new EditEntryInputKeyPage();

?>