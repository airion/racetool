<?php declare(strict_types = 1);

include_once 'private/page.php';
include_once 'private/database.php';
include_once 'private/entries.php';
include_once 'private/jsFunctions.php';

class GenerateStartAndChipNumbersPage extends Page
{
    function __construct()
    {
        parent::__construct();

        if ($this->m_access->hasAccess(AccessRight::ChangeEntry))
        {
            $javaScript = getJSFunction_post();
            $style = "";
            $this->outputHeader($javaScript, $style);

            echo "<p class='big'>Start- und Chipnummern vergeben</p>";

            $numberOfGeneratedStartNumbers = 0;
            $numberOfGeneratedChipNumbers = 0;

            $database = new Database();
            $database->generateStartAndChipNumbers($numberOfGeneratedStartNumbers, $numberOfGeneratedChipNumbers);
            $writingOk = $database->close();

            if ($writingOk)
            {
                switch ($numberOfGeneratedStartNumbers)
                {
                    case 0:
                        echo "<p class='yellow'>Alle Einträge haben schon Startnummern.</p>";
                        break;
                    case 1:
                        echo "<p class='yellow'>Startnummer wurde vergeben für $numberOfGeneratedStartNumbers Eintrag.</p>";
                        break;
                    default:
                        echo "<p class='yellow'>Startnummern wurden vergeben für $numberOfGeneratedStartNumbers Einträge.</p>";
                        break;
                }

                switch ($numberOfGeneratedChipNumbers)
                {
                    case 0:
                        echo "<p class='yellow'>Alle Einträge haben schon Chipnummern.</p>";
                        break;
                    case 1:
                        echo "<p class='yellow'>Chipnummer wurde vergeben für $numberOfGeneratedChipNumbers Eintrag.</p>";
                        break;
                    default:
                        echo "<p class='yellow'>Chipnummern wurden vergeben für $numberOfGeneratedChipNumbers Einträge.</p>";
                        break;
                }
            }
            else
            {
                echo "<p class='yellow'>Einträge konnten nicht geschrieben werden!</p>";
            }

            $this->beginFooter();
            $this->outputBackButton("admin.php");
            $this->endFooter();
        }
        else
        {
            $this->outputWrongPasswordErrorMessage();
        }
    }
}

new GenerateStartAndChipNumbersPage();

?>
