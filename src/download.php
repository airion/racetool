<?php declare(strict_types = 1);

include_once 'private/page.php';
include_once 'private/entry.php';
include_once 'private/database.php';
include_once 'private/getPriceFromEntry.php';
include_once 'private/getPriceAsString.php';

class DownloadPage extends Page
{
    function __construct()
    {
        parent::__construct();

        if ($this->m_access->hasAccess(AccessRight::Read))
        {
            $this->printList();
        }
        else
        {
            $this->outputWrongPasswordErrorMessage();
        }
    }

    function outputLine(string $text)
    {
        $text = preg_replace('/&nbsp;/', ' ', $text);
        echo mb_convert_encoding($text . "\r\n", "UTF-8");
    }

    function printList()
    {
        $database = new Database();
        $database->close();

        $entries = $database->getEntries();

        $cupFlag = Config::Get()['features']['cupFlag'];

        $timeOfLastModifcation = $database->getTimeOfLastModificationAsString();
        $filename = "teilnehmerliste_" . $timeOfLastModifcation . ".csv";

        header("Content-Type: text/csv; charset=UTF-8");
        header("Content-Disposition: attachment; filename=$filename");

        $header = Entry::GetAttributeName(EntryAttribute::Startnr);
        $header .= ";" . Entry::GetAttributeName(EntryAttribute::Chipnr);
        $header .= ";" . Entry::GetAttributeName(EntryAttribute::Lastname);
        $header .= ";" . Entry::GetAttributeName(EntryAttribute::Firstname);
        $header .= ";" . Entry::GetAttributeName(EntryAttribute::Year);
        $header .= ";" . Entry::GetAttributeName(EntryAttribute::AgeGroup);
        $header .= ";" . Entry::GetAttributeName(EntryAttribute::Gender);
        $header .= ";" . Entry::GetAttributeName(EntryAttribute::Team);
        $header .= ";" . Entry::GetAttributeName(EntryAttribute::Run);
        if ($cupFlag)
        {
            $header .= ";" . Entry::GetAttributeName(EntryAttribute::Cup);
        }
        $header .= ";" . Entry::GetAttributeName(EntryAttribute::Email);
        $header .= ";" . Entry::GetAttributeName(EntryAttribute::Date);
        $header .= ";" . Entry::GetAttributeName(EntryAttribute::PreRegistration);
        $header .= ";" . "Preis";
        $header .= ";" . Entry::GetAttributeName(EntryAttribute::Time);
        $header .= ";" . Entry::GetAttributeName(EntryAttribute::PlacingMW);
        $header .= ";" . Entry::GetAttributeName(EntryAttribute::PlacingAgeGroup);
        $header .= ";" . Entry::GetAttributeName(EntryAttribute::PlacingTeam);

        $this->outputLine($header);
        foreach ($entries as $entry)
        {
            $startnrString = $entry->getStartnrAsString();
            $chipnrString = $entry->getChipnrAsString();
            $name = $entry->getLastname();
            $firstname = $entry->getFirstname();
            $year = $entry->getYearAsString();
            $ageGroup = $entry->getAgeGroupAsString();
            $gender = $entry->getGender();
            $team = $entry->getTeamAsString();
            $run = $entry->getRunAsString();
            $preRegistrationString = $entry->getPreRegistrationAsString();
            if ($cupFlag)
            {
                $cup = $entry->getCupAsString();
            }
            $email = $entry->getEmail();
            $date = $entry->getDate();
            $price = getPriceAsString(getPriceFromEntry($entry));
            $time = $entry->getTime()->getAsString();
            $placingMW = $entry->getPlacingMWAsString();
            $placingAgeGroup = $entry->getPlacingAgeGroupAsString();
            $placingTeam = $entry->getPlacingTeamAsString();

            $line = "$startnrString;$chipnrString;$name;$firstname;$year;$ageGroup;$gender;$team;$run";

            if ($cupFlag)
            {
                $line .= ";$cup";
            }
            $line .= ";$email;$date;$preRegistrationString;$price;$time;$placingMW;$placingAgeGroup;$placingTeam";

            $this->outputLine($line);
        }
    }
}

new DownloadPage();

?>