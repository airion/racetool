<?php declare(strict_types = 1);

include_once 'private/page.php';
include_once 'private/entry.php';
include_once 'private/entrySorter.php';
include_once 'private/database.php';
include_once 'private/checkCosaCollidingTeamNames.php';

class DownloadCosaPage extends Page
{

    function __construct()
    {
        parent::__construct();

        if ($this->m_access->hasAccess(AccessRight::Read))
        {
            $this->m_database = new Database();
            $this->m_database->close();
            $this->m_entries = $this->m_database->getEntries();
            Entries::RemoveEntriesWithInvalidTime($this->m_entries);

            $collidingTeamNames = CheckCosaCollidingTeamNames::GetCollidingTeamNames($this->m_database);
            if (!empty($collidingTeamNames))
            {
                $javaScript = getJSFunction_post();
                $this->outputHeader($javaScript);
                CheckCosaCollidingTeamNames::OutputCollidingTeamNames($collidingTeamNames);

                $this->beginFooter();
                $this->outputBackButton("admin.php");
                $this->endFooter();
            }
            else
            {
                $this->printList();
            }
        }
        else
        {
            $this->outputWrongPasswordErrorMessage();
        }
    }

    function outputText(string $text)
    {
        echo mb_convert_encoding(str_replace("\n", "\r\n", $text), "Windows-1252");
    }

    function outputLine(string $text)
    {
        echo mb_convert_encoding($text. "\r\n", "Windows-1252");
    }

    function outputHTMLHeader()
    {
        $output = <<<EOD
        <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
        <html><head><title>Ergebnisliste</title>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <!-- 3.1.0 / 23.04.2018 / 23.04.2018 -->
        <style type="text/css">
        body{ width: 770px;}
        body{ font-family: Arial, Helvetica, sans-serif; font-style: normal;}
        table.body{ width: 770px; border: 0px;}
        td.blGrund{ BACKGROUND: white; FONT-FAMILY: Arial, Helvetia, Sans-Serif;
        FONT-SIZE: 13px; PADDING-LEFT: 0px; COLOR: black; }
        td.KopfZ1{ FONT-FAMILY: Arial, Helvetica, sans-serif; FONT-SIZE: 18px;
        FONT-STYLE: normal; FONT-WEIGHT: bold; TEXT-ALIGN: center;
        Border-TOP: 1px solid black; BORDER-BOTTOM: 0; COLOR: black; PADDING: 0px; }
        td.KopfZ11{ FONT-FAMILY: Arial, Helvetica, sans-serif; FONT-SIZE: 18px;
        FONT-STYLE: normal; FONT-WEIGHT: bold; TEXT-ALIGN: center;
        Border-TOP: 0; BORDER-BOTTOM: 0; COLOR: black; PADDING: 0px; }
        td.KopfZ12{ FONT-FAMILY: Arial, Helvetica, sans-serif; FONT-SIZE: 18px;
        FONT-STYLE: normal; FONT-WEIGHT: bold; TEXT-ALIGN: center;
        Border-TOP: 0; BORDER-BOTTOM: 1px solid black; COLOR: black; PADDING: 0px; }
        td.KopfZ2{ font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-style: normal;
        font-weight: bold; text-align: left; border-top: 1px solid black; border-bottom: 0px solid black;
        color: black; padding-top: 7px; padding-bottom: 7px; width: 600px; }
        td.KopfZ21{ font-family: Arial, Helvetica, sans-serif; font-size: 19px; font-style: normal;
        font-weight: bold; text-align: left; border-top: 0px solid black; border-bottom: 0px solid black;
        color: black; padding-top:12px; padding-bottom: 0px; width: 600px; }
        td.Stand{ font-family: Arial, Helvetica, sans-serif; font-size: 15px; font-style: normal;
        font-weight: bold; text-align: right; border-top: 1px solid black; border-bottom: 1px solid black;
        color: black; padding-top: 0px; padding-bottom: 0px; width: 150px; }
        td.FussZ{ font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal;
        font-weight: bold; TEXT-ALIGN: center; border-top: 1px solid black;
        border-bottom: 1px solid black; padding-top: 5px; width: 770px; }
        td.Erstellt{ font-size:13px;
        font-weight: bold; text-align: left;
        color: black; padding-top: 9px; padding-bottom: 0px; width: 50px; }
        td.AklZ{ font-family: Arial, Helvetia, sans-serif; font-size: 18px; font-style: normal;
        font-weight: bold; text-align: left; border-top: 1px solid black; border-bottom: 0px solid black;
        color: black; padding-top: 5px; padding-bottom: 8px; height: 10px; witdh: 600px; }
        p.Wettb{ font-family: Arial, Helvetica, sans-serif; font-size: 14px;
        font-style: normal; font-weight: bold; text-align: left; padding-top: 0px; padding-bottom: 0px;
        color: black; width: 230px; }
        .blWettb{ font-family: Arial, Helvetica, sans-serif; font-size: 14px;
        font-style: normal; text-align: left; padding-top: 0px; padding-bottom: 0px;
        color: black; width: 330px; }
        .blAKl{ font-family: Arial, Helvetica, sans-serif; font-size: 14px;
        font-style: normal; text-align: left; padding-top: 0px; padding-bottom: 0px;
        color: black; width: 230px; }
        .blBru{ font-family: Arial, Helvetica, sans-serif; font-size: 10px;
        font-style: normal; text-align: right; padding-top: 0px; padding-bottom: 0px;
        color: black; width: 702px; }
        .blNet{ font-family: Arial, Helvetica, sans-serif; font-size: 10px;
        font-style: normal; text-align: right; padding-top: 0px; padding-bottom: 0px;
        color: black; width:  68px; }
        .blZwifrei1w{ font-family: Arial, Helvetica, sans-serif; font-size:11px;
        font-style: normal; TEXT-ALIGN: left; width: 106px; }
        .blZwiTextw{ font-family: Arial, Helvetica, sans-serif; font-size:11px;
        font-style: normal; TEXT-ALIGN: right; width: 42px; }
        .blZwifreiEw{ font-family: Arial, Helvetica, sans-serif; font-size:11px;
        font-style: normal; TEXT-ALIGN: left; width: 300px; }
        .blZwifrei1g{ font-family: Arial, Helvetica, sans-serif; font-size:11px;
        font-style: normal; TEXT-ALIGN: left; width: 106px; }
        td.blZwifrei1g{ background-color: #DDDDDD; }
        .blZwiTextg{ font-family: Arial, Helvetica, sans-serif; font-size:11px;
        font-style: normal; TEXT-ALIGN: right; width: 42px; }
        td.blZwiTextg{ background-color: #DDDDDD; }
        .blZwifreiEg{ font-family: Arial, Helvetica, sans-serif; font-size:11px;
        font-style: normal; TEXT-ALIGN: left; width: 300px; }
        td.blZwifreiEg{ background-color: #DDDDDD; }
        .blRangw{ font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-style: normal;
        TEXT-ALIGN: right; padding: 0px; width: 30px; }
        .blRangBw{ font-family: Arial, Helvetica, sans-serif; font-size: 13px;
        font-style: normal; TEXT-ALIGN: right; WIDTH: 60px; }
        .blStNrw{ font-family: Arial, Helvetica, sans-serif; font-size: 13px;
        font-style: normal; TEXT-ALIGN: right; WIDTH: 42px; }
        .blNamew{ font-family: Arial, Helvetica, sans-serif; font-size: 13px;
        font-style: normal; TEXT-ALIGN: left; padding-left: 5px; width: 233px; }
        .blJGw{ font-family: Arial, Helvetica, sans-serif; font-size: 13px;
        font-style: normal; TEXT-ALIGN: center; padding-left: 0px; width: 30px; }
        .blVereinw{ font-family: Arial, Helvetica, sans-serif; font-size: 13px;
        font-style: normal; TEXT-ALIGN: left; padding-left: 4px; width: 225px; }
        .blLeistw{ font-family: Arial, Helvetica, sans-serif; font-size: 13px;
        font-style: normal; TEXT-ALIGN: right; padding-left: 0px; width: 65px; }
        .blVereinMaw{ font-family: Arial, Helvetica, sans-serif; font-size: 13px;
        font-style: normal; TEXT-ALIGN: left; padding-left: 4px; width: 525px; }
        .blMaFreiw{ font-family: Arial, Helvetica, sans-serif; font-size: 13px;
        font-style: normal; TEXT-ALIGN: left; width: 140px; }
        .blMaFrei1w{ font-family: Arial, Helvetica, sans-serif; font-size: 13px;
        font-style: normal; TEXT-ALIGN: left; width: 86px; }
        .blMaFrei2w{ font-family: Arial, Helvetica, sans-serif; font-size: 13px;
        font-style: normal; TEXT-ALIGN: right; width:  20x; }
        .blMaFrei3w{ font-family: Arial, Helvetica, sans-serif; font-size: 13px;
        font-style: normal; TEXT-ALIGN: right; width: 350x; }
        .blNameMaw{ font-family: Arial, Helvetica, sans-serif; font-size: 13px;
        font-style: normal; TEXT-ALIGN: left; padding-left: 8px; width: 270px; }
        .blLeistMaw{ font-family: Arial, Helvetica, sans-serif; font-size: 13px;
        font-style: normal; TEXT-ALIGN: right; padding-left: 0px; width: 90px; }
        .blVerBer{ font-family: Curier New; font-size: 14px;
        font-style: normal; TEXT-ALIGN: left; padding-left: 0px; width:770px; }
        .blRangg{ font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-style: normal;
        TEXT-ALIGN: right; padding: 0px; width: 30px; }
        td.blRangg{ background-color: #DDDDDD; }
        .blRangBg{ font-family: Arial, Helvetica, sans-serif; font-size: 13px;
        font-style: normal; TEXT-ALIGN: right; WIDTH: 60px; }
        td.blRangBg{ background-color: #DDDDDD; }
        .blStNrg{ font-family: Arial, Helvetica, sans-serif; font-size: 13px;
        font-style: normal; TEXT-ALIGN: right; WIDTH: 42px; }
        td.blStNrg{ background-color: #DDDDDD; }
        .blNameg{ font-family: Arial, Helvetica, sans-serif; font-size: 13px;
        font-style: normal; TEXT-ALIGN: left; padding-left: 5px; width: 233px; }
        td.blNameg{ background-color: #DDDDDD; }
        .blJGg{ font-family: Arial, Helvetica, sans-serif; font-size: 13px;
        font-style: normal; TEXT-ALIGN: center; padding-left: 0px; width: 30px; }
        td.blJGg{ background-color: #DDDDDD; }
        .blVereing{ font-family: Arial, Helvetica, sans-serif; font-size: 13px;
        font-style: normal; TEXT-ALIGN: left; padding-left: 4px; width: 225px; }
        td.blVereing{ background-color: #DDDDDD; }
        .blLeistg{ font-family: Arial, Helvetica, sans-serif; font-size: 13px;
        font-style: normal; TEXT-ALIGN: right; padding-left: 0px; width: 65px; }
        td.blLeistg{ background-color: #DDDDDD; }
        .blVereinMag{ font-family: Arial, Helvetica, sans-serif; font-size: 13px;
        font-style: normal; TEXT-ALIGN: left; padding-left: 4px; width: 525px; }
        td.blVereinMag{ background-color: #DDDDDD; }
        .blMaFreig{ font-family: Arial, Helvetica, sans-serif; font-size: 13px;
        font-style: normal; TEXT-ALIGN: left; width: 140px; }
        td.blMaFreig{ background-color: #DDDDDD; }
        .blMaFrei1g{ font-family: Arial, Helvetica, sans-serif; font-size: 13px;
        font-style: normal; TEXT-ALIGN: left; width: 86px; }
        td.blMaFrei1g{ background-color: #DDDDDD; }
        .blMaFrei2g{ font-family: Arial, Helvetica, sans-serif; font-size: 13px;
        font-style: normal; TEXT-ALIGN: left; width:  20x; }
        td.blMaFrei2g{ background-color: #DDDDDD; }
        .blMaFrei3g{ font-family: Arial, Helvetica, sans-serif; font-size: 13px;
        font-style: normal; TEXT-ALIGN: right; width:  195x; }
        td.blMaFrei3g{ background-color: #DDDDDD; }
        .blNameMag{ font-family: Arial, Helvetica, sans-serif; font-size: 13px;
        font-style: normal; TEXT-ALIGN: left; padding-left: 8px; width: 270px; }
        td.blNameMag{ background-color: #DDDDDD; }
        .blLeistMag{ font-family: Arial, Helvetica, sans-serif; font-size: 13px;
        font-style: normal; TEXT-ALIGN: right; padding-left: 0px; width: 90px; }
        td.blLeistMag{ background-color: #DDDDDD; }
        .blStMaTnw{ font-family: Arial, Helvetica, sans-serif; font-size: 11px;
        font-style: normal; TEXT-ALIGN: left; padding-left: 0px; width: 602px; }
        .blStMaTng{ font-family: Arial, Helvetica, sans-serif; font-size: 11px;
        font-style: normal; TEXT-ALIGN: left; padding-left: 0px; width: 602px; }
        td.blStMaTng{ background-color: #DDDDDD; }
        .blStMaTnFreiw{ font-family: Arial, Helvetica, sans-serif; font-size: 11px;
        font-style: normal; TEXT-ALIGN: left; width: 95px; }
        .blStMaTnFreig{ font-family: Arial, Helvetica, sans-serif; font-size: 11px;
        font-style: normal; TEXT-ALIGN: left; width: 95px; }
        td.blStMaTnFreig{ background-color: #DDDDDD; }
        .blDatumMkT2w{ font-family: Arial, Helvetica, sans-serif; font-size: 13px;
        font-style: normal; TEXT-ALIGN: left; width: 40px; }
        .blDatumMkT2g{ font-family: Arial, Helvetica, sans-serif; font-size: 13px;
        font-style: normal; TEXT-ALIGN: left; width: 40px; }
        td.blDatumMkT2g{ background-color: #DDDDDD; }
        .blVorjRekFrei{ font-family: Arial, Helvetica, sans-serif; font-size: 11px;
        font-style: normal; TEXT-ALIGN: left; width: 85px; }
        .blVorjRekBez{ font-family: Arial, Helvetica, sans-serif; font-size: 11px;
        font-style: normal; TEXT-ALIGN: left; width: 130px; }
        .blVorjRekLeist{ font-family: Arial, Helvetica, sans-serif; font-size: 11px; font-style: normal;
        TEXT-ALIGN: left; padding: 0px; width: 50px; }
        .blVorjRekDaten{ font-family: Arial, Helvetica, sans-serif; font-size: 11px;
        font-style: normal; TEXT-ALIGN: left; padding-left: 0px; width: 400px; }
        .blRekordDatum{ font-family: Arial, Helvetica, sans-serif; font-size: 11px;
        font-style: normal; TEXT-ALIGN: left; padding-left: 0px; width:  50px; }
        td.links { border-left-width: 1px; border-left-style:solid; text-vertical-align : top; }
        td.linksZentriert { border-left-width: 1px; border-left-style:solid; text-align : center; text-vertical-align : top; }
        td.linksOben { border-left-width: 1px; border-left-style:solid; border-top-width: 1px; border-top-style:solid; text-vertical-align : top; }
        td.linksObenZentriert { border-left-width: 1px; border-left-style:solid; border-top-width: 1px; border-top-style:solid; text-align : center; text-vertical-align : top; }
        td.linksObenRechts { border-left-width: 1px; border-left-style:solid; border-top-width: 1px; border-top-style:solid; border-right-width: 1px; border-right-style:solid; text-vertical-align : top; }
        td.linksObenRechtsRechts { border-left-width: 1px; border-left-style:solid; border-top-width: 1px; border-top-style:solid; border-right-width: 1px; border-right-style:solid; text-align : right; text-vertical-align : top; }
        td.rechts { border-right-width: 1px; border-right-style:solid; text-vertical-align : top; }
        td.rechtsZentriert { border-right-width: 1px; border-right-style:solid; text-align : center; text-vertical-align : top; }
        td.linksRechts { border-left-width: 1px; border-left-style:solid; border-right-width: 1px; border-right-style:solid; text-vertical-align : top; }
        td.linksRechtsRechts { border-left-width: 1px; border-left-style:solid; border-right-width: 1px; border-right-style:solid; text-align : right; text-vertical-align : top; }
        td.Oben { border-top-width: 1px; border-top-style:solid; text-vertical-align : top; }
        </style>
        </head>
EOD;

        $this->outputText($output);
    }


    function createRunLists()
    {
        $groups = Entries::SplitEntriesByRun($this->m_entries);
        foreach ($groups as $groupEntries)
        {
            assert(!empty($groupEntries));
            $runIndex = $groupEntries[0]->getRun();
            $this->createRunList($groupEntries, Runs::GetInstance()->getRun($runIndex));
        }
    }

    function createRunList(array $entries, Run $run)
    {
        $this->outputLine('<table class="body" cellspacing="0"><tbody><tr>');
        $link = 'run'.strval($run->getIndex());
        $this->outputLine('<td class="AklZ">'.$run->getCosaName().'<a name="'.$link.'"></a></td></tr></tbody></table>');

        $this->createAgeGroupList($entries, $run);

        if ($run->getTeamPlacing())
        {
            $this->createTeamList($run);
        }
    }

    function createAgeGroupList(array $entries, Run $run)
    {
        $groups = Entries::SplitEntriesByAgeGroupAndGender($entries);

        foreach ($groups as $groupEntries)
        {
            $firstEntry = $groupEntries[0];
            $ageGroupString = $firstEntry->getAgeGroupAsString();

            EntrySorter::Sort($groupEntries, EntrySorter::OrderTimeLastnameIndex);

            $this->outputLine('<table class="body" cellspacing="0" cellpadding="0">');
            $this->outputLine('<tbody><tr><td class="blWettb">'.$run->getCosaName().'</td>');
            $this->outputLine('<td class="blAKl">'.$ageGroupString.'</td>');
            $this->outputLine('</tr></tbody></table><br><table class="body" cellspacing="0" cellpadding="0">');
            $this->outputLine('<tbody>');

            $oddLine = true;
            foreach ($groupEntries as $entry)
            {
                $placingAgeGroup = $entry->getPlacingAgeGroup();
                $placingMW = $entry->getPlacingMW();
                $startNumber = $entry->getStartnr();
                $name = $entry->getLastname() . ", " . $entry->getFirstname();
                $year = $entry->getYear();
                $team = $entry->getTeam();
                $time = $entry->getTime()->getAsString(true, false, false);

                $classPostfix = $oddLine ? 'g' : 'w';
                $this->outputLine('<tr><td class="blRang'.$classPostfix.'">'.$placingAgeGroup.'.</td>');
                $this->outputLine('<td class="blRangB'.$classPostfix.'">/'.$placingMW.'.</td>');
                $this->outputLine('<td class="blStNr'.$classPostfix.'">'.$startNumber.'</td>');
                $this->outputLine('<td class="blName'.$classPostfix.'">'.$name.'</td>');
                $this->outputLine('<td class="blJG'.$classPostfix.'">'.$year.'</td>');
                $this->outputLine('<td class="blVerein'.$classPostfix.'">'.$team.'</td>');
                $this->outputLine('<td class="blLeist'.$classPostfix.'">'.$time.'</td>');
                $this->outputLine('</tr>');

                $oddLine = !$oddLine;
            }

            $this->outputLine('</tbody></table><br>');
        }
    }

    function createTeamList(Run $run)
    {
        $this->createTeamListForGender($run, "m");
        $this->createTeamListForGender($run, "w");
    }

    function createTeamListForGender(Run $run, string $gender)
    {
        $teams = $this->m_database->getTeamPlacing($run->getIndex(), $gender);

        $config = Config::Get()['cosahtmlexport'];

        if (!empty($teams))
        {
            $this->outputLine('<table class="body" cellspacing="0" cellpadding="0">');
            $this->outputLine('<tbody><tr><td class="blWettb">'.$run->getCosaName().'</td>');
            $runMWString = $config['runMWName'][$run->getIndex()][$gender];
            $this->outputLine('<td class="blAKl">'.$runMWString.'</td>');
            $this->outputLine('</tr></tbody></table><br>');


            $oddLine = true;
            foreach ($teams as $team)
            {
                $classPostfix = $oddLine ? 'g' : 'w';

                $placing = $team->getPlacing();
                $teamName = $team->getTeamName();
                $time = $team->getTime()->getAsString(true, false, false);

                $this->outputLine('<table class="body" cellspacing="0" cellpadding="0">');
                $this->outputLine('<tbody><tr><td class="blRang'.$classPostfix.'">'.$placing.'.</td>');
                $this->outputLine('<td class="blMaFrei1'.$classPostfix.'"></td>');
                $this->outputLine('<td class="blVereinMa'.$classPostfix.'">'.$teamName.'</td>');
                $this->outputLine('<td class="blMaFrei2'.$classPostfix.'"></td>');
                $this->outputLine('<td class="blLeist'.$classPostfix.'">'.$time.'</td></tr>');
                $this->outputLine('</tbody></table>');

                $this->outputLine('<table class="body" cellspacing="0" cellpadding="0">');

                for ($i = 0; $i < 3; $i++)
                {
                    $entry = $team->getEntry($i);
                    $startNr = $entry->getStartnr();
                    $name = $entry->getLastname() . ", " . $entry->getFirstname();
                    $year = $entry->getYear();
                    $time = $entry->getTime()->getAsString(true, false, false);

                    $this->outputLine('<tbody><tr><td class="blMaFrei'.$classPostfix.'"></td>');
                    $this->outputLine('<td class="blStNr'.$classPostfix.'">'.$startNr.'</td>');
                    $this->outputLine('<td class="blNameMa'.$classPostfix.'">'.$name.'</td>');
                    $this->outputLine('<td class="blJG'.$classPostfix.'">'.$year.'</td>');
                    $this->outputLine('<td class="blLeistMa'.$classPostfix.'">'.$time.'</td>');
                    $this->outputLine('<td class="blMaFrei3'.$classPostfix.'">.</td></tr>');
                }

                $this->outputLine('</tbody></table>');

                $oddLine = !$oddLine;
            }

            $this->outputLine('<br>');

        }
    }

    function printList()
    {
        $entries = $this->m_database->getEntries();
        EntrySorter::Sort($entries, EntrySorter::OrderStartnrIndex);

        $timeOfLastModificationString = $this->m_database->getTimeOfLastModificationAsString();
        $filename = "cosa_ergebnisdaten_html_" . $timeOfLastModificationString . ".htm";

        $chipIds = ChipIds::GetInstance();
        if ($chipIds->isOk())
        {
            header("Content-Type: text/html; charset=Windows-1252");
            header("Content-Disposition: attachment; filename=$filename");

            $config = Config::Get()['cosahtmlexport'];

            $this->outputHTMLHeader();

            $this->outputLine('<body><table class="body" cellspacing="0">');
            $this->outputLine('<tbody><tr><td class="KopfZ1"></td></tr>');
            $this->outputLine('<tr><td class="KopfZ11">'.$config['headline1'].'</td></tr>');
            $this->outputLine('<tr><td class="KopfZ12">'.$config['headline2'].'</td></tr>');
            $this->outputLine('</tbody></table>');

            $this->outputLine('<table class="body" cellspacing="0">');
            $this->outputLine('<tbody><tr><td class="KopfZ21">Ergebnisliste</td></tr></tbody></table>');
            $this->outputLine('<table class="body" cellspacing="0">');

            $timeOfLastModification = $this->m_database->getTimeOfLastModification();
            $date = date("d.m.Y", $timeOfLastModification) . " / " . date("H", $timeOfLastModification) . ":" . date("i", $timeOfLastModification);

            $this->outputLine('<tbody><tr><td class="Erstellt">'.$date.'</td></tr></tbody></table><br>');
            $this->outputLine('<table class="body" cellspacing="0"><tbody>');
            $this->outputLine('<tr><td class="KopfZ2">Wettbewerbübersicht</td></tr>');

            $runs = Runs::GetInstance()->getRuns();
            foreach($runs as $run)
            {
                $runName = $run->getCosaName();
                $link = '#run'.strval($run->getIndex());
                $this->outputLine('<tr><td class="blGrund"><a href="'.$link.'">'.$runName.'</a></td></tr>');
            }

            $this->outputLine('</tbody></table>');
            $this->outputLine('<br>');

            $this->createRunLists();
            $this->outputLine('</body></html>');
        }
    }

    private $m_database;
    private $m_entries = array();

}

new DownloadCosaPage();
?>