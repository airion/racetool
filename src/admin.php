<?php declare(strict_types=1);

include_once 'private/page.php';
include_once 'private/database.php';
include_once 'private/jsFunctions.php';
include_once 'private/parameters.php';
include_once 'private/entryTable.php';
include_once 'private/post.php';

class AdminPage extends Page
{
    function __construct()
    {
        parent::__construct();

        if ($this->m_access->hasAccess(AccessRight::Read))
        {
            $parameters = new Parameters();
            $parameters->addJS("orderBy", "document.settings.orderBy");
            $parameters->addJS("hiddenColumns", "document.settings.hiddenColumns");
            $parameters->addJS("filterText", "document.settings.filterText");
            $parameters->addJS("filterRun", "document.settings.filterRun");

            $javaScript = getJSFunction_post();

            $style = "";
            $style .= EntryTable::getStyle();

            $scrollable = false;
            $this->outputHeader($javaScript, $style, $scrollable);

            echo "<p class='big'>Anmeldeliste</p>";

            $database = new Database();
            $databaseEmpty = $database->isEmpty();
            $database->close();

            $onClickPage = "adminChangeEntry.php";
            $allowShowingDeleted = true;

            $enabledColumns = 0;
            EntryTable::AddColumn($enabledColumns, EntryAttribute::Lastname);
            EntryTable::AddColumn($enabledColumns, EntryAttribute::Firstname);
            EntryTable::AddColumn($enabledColumns, EntryAttribute::Year);
            EntryTable::AddColumn($enabledColumns, EntryAttribute::Team);
            EntryTable::AddColumn($enabledColumns, EntryAttribute::Run);
            EntryTable::AddColumn($enabledColumns, EntryAttribute::Email);
            EntryTable::AddColumn($enabledColumns, EntryAttribute::Startnr);
            EntryTable::AddColumn($enabledColumns, EntryAttribute::Chipnr);
            EntryTable::AddColumn($enabledColumns, EntryAttribute::Revision);
            EntryTable::AddColumn($enabledColumns, EntryAttribute::Date);
            EntryTable::AddColumn($enabledColumns, EntryAttribute::Username);
            if (Config::Get()['features']['cupFlag'])
            {
                EntryTable::AddColumn($enabledColumns, EntryAttribute::Cup);
            }
            EntryTable::AddColumn($enabledColumns, EntryAttribute::AgeGroup);
            EntryTable::AddColumn($enabledColumns, EntryAttribute::PreRegistration);
            EntryTable::AddColumn($enabledColumns, EntryAttribute::Time);
            EntryTable::AddColumn($enabledColumns, EntryAttribute::Placing);
            EntryTable::AddColumn($enabledColumns, EntryAttribute::PlacingMW);
            EntryTable::AddColumn($enabledColumns, EntryAttribute::PlacingAgeGroup);
            EntryTable::AddColumn($enabledColumns, EntryAttribute::PlacingTeam);
            $shortColumnNames = true;
            $enableShowColumnButtons = true;
            $orderBy = Post::GetIntValueFromPost("orderBy", EntryAttribute::Lastname);
            $hiddenColumns = Post::GetIntValueFromPost("hiddenColumns");
            $filterText = Post::GetStringValueFromPost("filterText");
            $filterRun = Post::GetIntValueFromPost("filterRun", -1);

            new EntryTable($this, $database, $onClickPage, $allowShowingDeleted, $enabledColumns, $shortColumnNames, $enableShowColumnButtons, $orderBy, $hiddenColumns, $filterText, $filterRun);

            $this->beginFooter();

            $programMode = Config::Get()['program']['mode'];

            $this->outputBackButton("index.php", $parameters);

            $menuEntries = [
                new MenuEntry("Startnummern Aufkleber (PDF)", $this->getPostString("downloadStickerSettings.php", $parameters)),
                new MenuEntry("Teilnehmerlisten (PDF)", $this->getPostString("downloadListsSettings.php", $parameters)),
                new MenuEntry("Ergebnislisten (PDF)", $this->getPostString("downloadResultsSettings.php", $parameters)),
                new MenuEntry("Urkunden (PDF)", $this->getPostString("downloadCertificatesSettings.php", $parameters))
            ];
            Menu::OutputButtonWithMenu("fa-file-pdf", "Dokumente Download", $menuEntries);

            Menu::OutputButton("fa-stopwatch", "Zeitnahme Datensätze", $this->getPostString("timeDatasetPage.php", $parameters), "timeDatasets");

            $postUpload = $this->outputUploadForm("button-file", ".txt", "uploadDatabaseReceiveFile.php", $parameters);
            $enabled = $this->m_access->hasAccess(AccessRight::DeleteImportExportDatabase) && ($programMode != ProgramMode::PreRegistrationValidatedAndFinished);
            $importEnabled = $enabled && $databaseEmpty;
            $deleteEnabled = $enabled && !$databaseEmpty;

            $clearDatabasePost = $this->getPostFunction("clearDatabase.php", $parameters);

            $menuEntries = [
                new MenuEntry("Import", $postUpload, "databaseImport", $importEnabled),
                new MenuEntry("Export", $this->getPostString("downloadDatabase.php", $parameters), "databaseExport", $enabled),
                new MenuEntry("Löschen", "openMessageBox('Datenbank wirklich löschen?', MessageBoxType_Question, $clearDatabasePost)", "databaseClear", $deleteEnabled)
            ];
            Menu::OutputButtonWithMenu("fa-database", "Datenbank", $menuEntries, "database");

            $menuEntries = [
                new MenuEntry("Teilnehmerliste (CSV)", $this->getPostString("download.php", $parameters), "downloadStartList"),
                new MenuEntry("COSA Teilnehmer (CSV)", $this->getPostString("downloadCosa.php", $parameters)),
                new MenuEntry("COSA Ergebnisdaten (CSV)", $this->getPostString("downloadCosaResults.php", $parameters), "downloadResults"),
                new MenuEntry("COSA Ergebnisdaten (HTML)", $this->getPostString("downloadCosaHTMLResults.php", $parameters))
            ];
            Menu::OutputButtonWithMenu("fa-download", "Daten Download", $menuEntries, "download");

            $postCupImport = $this->outputUploadForm("button-cup-file", ".csv", "importCupDataReceiveFile.php", $parameters);
            $enabled = Config::Get()['features']['cupFlag'] && $this->m_access->hasAccess(AccessRight::ImportCupData) && ($programMode != ProgramMode::PreRegistrationValidatedAndFinished);

            $hoverText = "Format (Windows-1252)\n";
            $hoverText .= Entry::GetAttributeShortName(EntryAttribute::Startnr) . ';';
            $hoverText .= Entry::GetAttributeShortName(EntryAttribute::Lastname) . ';';
            $hoverText .= Entry::GetAttributeShortName(EntryAttribute::Firstname) . ';';
            $hoverText .= Entry::GetAttributeShortName(EntryAttribute::Gender) . ';';
            $hoverText .= Entry::GetAttributeShortName(EntryAttribute::Team) . ';';
            $hoverText .= Entry::GetAttributeShortName(EntryAttribute::Year);

            $menuEntries = [
                new MenuEntry("Cup Meldungen (CSV)", $postCupImport, "importCupDataChooseFile", $enabled, $hoverText)
            ];
            Menu::OutputButtonWithMenu("fa-upload", "Daten Upload", $menuEntries, "upload");

            $enabledGenerateNumbers = $this->m_access->hasAccess(AccessRight::ChangeEntry) && ($programMode != ProgramMode::PreRegistrationValidatedAndFinished);
            $enabledBlockerlist = $this->m_access->hasAccess(AccessRight::ChangeBlockerlist) && ($programMode != ProgramMode::PreRegistrationValidatedAndFinished);

            $generateStartAndChipNumbersPost = $this->getPostFunction("generateStartAndChipNumbers.php", $parameters);

            $menuEntries = [
                new MenuEntry("Nummern vergeben", "openMessageBox('Start- und Chipnummern wirklich vergeben?', MessageBoxType_Question, $generateStartAndChipNumbersPost)", "generateStartAndChipNumbers", $enabledGenerateNumbers),
                new MenuEntry("Blockerliste", $this->getPostString("blockerList.php", $parameters), "blockerList", $enabledBlockerlist),
            ];
            Menu::OutputButtonWithMenu("fa-list-ol", "Start-/Chipnummern", $menuEntries, "numbers");

            $enabled = $this->m_access->hasAccess(AccessRight::ChangeSetting);
            Menu::OutputButton("fa-wrench", "Einstellungen", $this->getPostString("settingsPage.php", $parameters), "settings", $enabled);

            // TODO: Auswahl bearbeiten - fa-edit
            // TODO: Auswahl Menu - fa-bars
            $this->endFooter();
        }
        else
        {
            $this->outputWrongPasswordErrorMessage();
        }
    }
}

new AdminPage();

?>
