<?php declare(strict_types=1);

include_once 'private/page.php';
include_once 'private/entry.php';
include_once 'private/entries.php';
include_once 'private/entrySorter.php';
include_once 'private/database.php';
include_once 'private/entries.php';
include_once 'private/addDraftOverlay.php';
include_once 'private/PDFCreator/Table.php';
include_once 'private/getRegistrationLink.php';
include_once 'private/getPriceFromEntry.php';
include_once 'private/getPriceAsString.php';
include_once 'private/post.php';

class DownloadListsPage extends Page
{
    function __construct()
    {
        parent::__construct();
        if ($this->m_access->hasAccess(AccessRight::Read))
        {
            $this->initWidths();
            //$startTime = microtime(true);
            $this->createLists();
            //$consumedTime = (microtime(true) - $startTime);
            //echo "Consumed time: $consumedTime";
        }
        else
        {
            $this->outputWrongPasswordErrorMessage();
        }
    }

    private function getTableHead(int $attribute): string
    {
        return Entry::GetAttributeShortName($attribute);
    }

    private function initWidths()
    {
        $listsWidthConfig = Config::Get()['lists']['width'];
        $this->m_width[EntryAttribute::Startnr] = floatval($listsWidthConfig['startnr']);
        $this->m_width[EntryAttribute::Chipnr] = floatval($listsWidthConfig['chipnr']);
        $this->m_width[EntryAttribute::Year] = floatval($listsWidthConfig['year']);
        $this->m_width[EntryAttribute::AgeGroup] = floatval($listsWidthConfig['ageGroup']);
        $this->m_width[EntryAttribute::Run] = floatval($listsWidthConfig['run']);
        $this->m_widthPrice = floatval($listsWidthConfig['price']);

        if (Config::Get()['features']['cupFlag'])
        {
            $this->m_width[EntryAttribute::Cup] = floatval($listsWidthConfig['cup']);
        }
    }

    private function getWidth(int $sortOrder): float
    {
        return $this->m_width[$sortOrder];
    }

    private function createLists()
    {
        $nameList = Post::GetBoolValueFromPost("nameList");
        $startNumberList = Post::GetBoolValueFromPost("startNumberList");
        $teamLists = Post::GetBoolValueFromPost("teamLists");
        $newPagePerTeam = Post::GetBoolValueFromPost("newPagePerTeam");

        $runs = Runs::GetInstance()->getRuns();
        $numberOfRuns = count($runs);
        $runList = array();
        for ($runIndex = 0; $runIndex < $numberOfRuns; $runIndex++)
        {
            $name = "run".$runIndex."List";
            $runList[$runIndex] = Post::GetBoolValueFromPost($name);
        }

        $cupList = Post::GetBoolValueFromPost("cupList");

        //echo "memory_get_peak_usage #1:".memory_get_peak_usage()."<br>";
        $eventName = Config::Get()['event']['name'];
        $registrationLink = getRegistrationLink();

        $database = new Database();
        $database->close();

        $timeOfLastModification = $database->getTimeOfLastModificationAsString();
        $filename = "teilnehmerliste_" . $timeOfLastModification;
        if ($nameList)
        {
            $filename .= "_n";
        }
        if ($startNumberList)
        {
            $filename .= "_s";
        }
        if ($teamLists)
        {
            $filename .= "_v";
            if ($newPagePerTeam)
            {
                $filename .= "s";
            }
        }

        for ($runIndex = 0; $runIndex < $numberOfRuns; $runIndex++)
        {
            if ($runList[$runIndex])
            {
                $filename .= "_r". ($runIndex + 1);
            }
        }

        if ($cupList)
        {
            $filename .= "_c";
        }

        $filename .= ".pdf";

        $this->m_entries = $database->getEntries();

        $this->m_pdfCreator = new PDFCreator();

        $fontSize = 8;
        $this->m_pdfCreator->m_pdf->SetFont('dejavusans', '', $fontSize, '');

        $headerTitle = "Teilnehmerlisten $eventName";
        $headerString = "Version: $timeOfLastModification\n$registrationLink";

        $this->m_pdfCreator->m_pdf->setHeaderData('../../res/ltlogo.svg', 20, $headerTitle, $headerString);
        $this->m_pdfCreator->m_pdf->setHeaderFont(Array(
            'dejavusans',
            '',
            10
        ));
        $this->m_pdfCreator->m_pdf->setFooterFont(Array(
            'dejavusans',
            '',
            8
        ));

        $this->m_fontName1 = new Font('dejavusansb', $fontSize + 4);
        $this->m_fontName2 = new Font('dejavusansb', $fontSize + 2);
        $this->m_fontHead = new Font('dejavusansb', $fontSize);
        $this->m_fontData = new Font('dejavusans', $fontSize);

        if ($nameList)
        {
            $this->createNameList();
        }

        if ($startNumberList)
        {
            $this->createStartnrList();
        }

        if ($teamLists)
        {
            $this->createTeamList($newPagePerTeam);
        }

        $this->createRunList($runList);

        if (Config::Get()['features']['cupFlag'] && $cupList)
        {
            $this->createCupList();
        }

        addDraftOverlay($this->m_pdfCreator->m_pdf);

        //echo "memory_get_peak_usage #2:".memory_get_peak_usage()."<br>";
        $this->m_pdfCreator->m_pdf->Output($filename, "D");
    }

    private function createNameList()
    {
        $this->m_pdfCreator->doPageBreak();
        EntrySorter::Sort($this->m_entries, EntrySorter::OrderLastnameIndex);
        $table = new Table($this->m_pdfCreator, "Namensliste", $this->m_fontName1, $this->m_fontHead, $this->m_fontData);
        $cupFlag = Config::Get()['features']['cupFlag'];

        $sumWidth = $this->getWidth(EntryAttribute::Startnr) +
            $this->getWidth(EntryAttribute::Year) +
            $this->getWidth(EntryAttribute::AgeGroup) +
            $this->getWidth(EntryAttribute::Run) +
            $this->m_widthPrice;
        if ($cupFlag)
        {
            $sumWidth += $this->getWidth(EntryAttribute::Cup);
        }

        $longWidth = (100 - $sumWidth) / 2;

        $table->addTableHead($this->getTableHead(EntryAttribute::Startnr), $this->getWidth(EntryAttribute::Startnr), 'R');
        $table->addTableHead($this->getTableHead(EntryAttribute::Chipnr), $this->getWidth(EntryAttribute::Chipnr), 'R');
        $table->addTableHead(self::$TableHead_Name, $longWidth, 'L', true);
        $table->addTableHead($this->getTableHead(EntryAttribute::Year), $this->getWidth(EntryAttribute::Year), 'C');
        $table->addTableHead($this->getTableHead(EntryAttribute::AgeGroup), $this->getWidth(EntryAttribute::AgeGroup), 'C');
        $table->addTableHead($this->getTableHead(EntryAttribute::Team), $longWidth, 'L');
        $table->addTableHead($this->getTableHead(EntryAttribute::Run), $this->getWidth(EntryAttribute::Run), 'L');
        $table->addTableHead("Preis", $this->m_widthPrice, 'L');
        if ($cupFlag)
        {
            $table->addTableHead($this->getTableHead(EntryAttribute::Cup), $this->getWidth(EntryAttribute::Cup), 'C');
        }

        foreach ($this->m_entries as $entry)
        {
            $price = getPriceAsString(getPriceFromEntry($entry));

            $table->addTableData($entry->getStartnrAsString());
            $table->addTableData($entry->getChipnrAsString());
            $table->addTableData($entry->getLastnameFirstname());
            $table->addTableData($entry->getYearAsString());
            $table->addTableData($entry->getAgeGroupAsString());
            $table->addTableData($entry->getTeamAsString());
            $table->addTableData($entry->getRunAsStringShort());
            $table->addTableData($price);
            if ($cupFlag)
            {
                $table->addTableData($entry->getCupAsString());
            }
        }

        $table->end();
    }

    private function createStartnrList()
    {
        $this->m_pdfCreator->doPageBreak();
        EntrySorter::Sort($this->m_entries, EntrySorter::OrderStartnrIndex);

        $table = new Table($this->m_pdfCreator, "Startnummernliste", $this->m_fontName1, $this->m_fontHead, $this->m_fontData);
        $cupFlag = Config::Get()['features']['cupFlag'];

        $sumWidth = $this->getWidth(EntryAttribute::Startnr) +
        $this->getWidth(EntryAttribute::Year) +
        $this->getWidth(EntryAttribute::AgeGroup) +
        $this->getWidth(EntryAttribute::Run) +
        $this->m_widthPrice;
        if ($cupFlag)
        {
            $sumWidth += $this->getWidth(EntryAttribute::Cup);
        }

        $longWidth = (100 - $sumWidth) / 2;

        $table->addTableHead($this->getTableHead(EntryAttribute::Startnr), $this->getWidth(EntryAttribute::Startnr), 'R', true);
        $table->addTableHead($this->getTableHead(EntryAttribute::Chipnr), $this->getWidth(EntryAttribute::Chipnr), 'R');
        $table->addTableHead(self::$TableHead_Name, $longWidth, 'L');
        $table->addTableHead($this->getTableHead(EntryAttribute::Year), $this->getWidth(EntryAttribute::Year), 'C');
        $table->addTableHead($this->getTableHead(EntryAttribute::AgeGroup), $this->getWidth(EntryAttribute::AgeGroup), 'C');
        $table->addTableHead($this->getTableHead(EntryAttribute::Team), $longWidth, 'L');
        $table->addTableHead($this->getTableHead(EntryAttribute::Run), $this->getWidth(EntryAttribute::Run), 'L');
        $table->addTableHead("Preis", $this->m_widthPrice, 'L');
        if ($cupFlag)
        {
            $table->addTableHead($this->getTableHead(EntryAttribute::Cup), $this->getWidth(EntryAttribute::Cup), 'C');
        }

        foreach ($this->m_entries as $entry)
        {
            $price = getPriceAsString(getPriceFromEntry($entry));

            $table->addTableData($entry->getStartnrAsString());
            $table->addTableData($entry->getChipnrAsString());
            $table->addTableData($entry->getLastnameFirstname());
            $table->addTableData($entry->getYearAsString());
            $table->addTableData($entry->getAgeGroupAsString());
            $table->addTableData($entry->getTeamAsString());
            $table->addTableData($entry->getRunAsStringShort());
            $table->addTableData($price);
            if ($cupFlag)
            {
                $table->addTableData($entry->getCupAsString());
            }
        }

        $table->end();
    }

    private function createTeamList(bool $newPagePerTeam)
    {
        if (!$newPagePerTeam)
        {
            $this->m_pdfCreator->doPageBreak();
            $textObject = new TextObject($this->m_pdfCreator, "Vereinslisten");
            $textObject->setFont($this->m_fontName1);
            $textObject->setLineFeed(0.5);
            $textObject->generate();
        }

        EntrySorter::Sort($this->m_entries, EntrySorter::OrderTeamLastnameIndex);

        $cupFlag = Config::Get()['features']['cupFlag'];

        $lastTeam = "";
        $table = null;
        $priceSum = 0.0;
        foreach ($this->m_entries as $entry)
        {
            if ($entry->getTeam() != $lastTeam || !isset($table))
            {
                if (isset($table))
                {
                    $table->end();
                    $this->outputPriceSum($priceSum, $lastTeam);
                    $priceSum  = 0.0;

                    $this->outputLineFeed(2.0);
                }
                $lastTeam = $entry->getTeam();

                if ($newPagePerTeam)
                {
                    $this->m_pdfCreator->doPageBreak();
                }
                $table = new Table($this->m_pdfCreator, $entry->getTeamAsString(), $this->m_fontName2, $this->m_fontHead, $this->m_fontData);

                $sumWidth = $this->getWidth(EntryAttribute::Startnr) +
                $this->getWidth(EntryAttribute::Year) +
                $this->getWidth(EntryAttribute::AgeGroup) +
                $this->getWidth(EntryAttribute::Run) +
                $this->m_widthPrice;
                if ($cupFlag)
                {
                    $sumWidth += $this->getWidth(EntryAttribute::Cup);
                }

                $longWidth = (100 - $sumWidth);

                $table->addTableHead($this->getTableHead(EntryAttribute::Startnr), $this->getWidth(EntryAttribute::Startnr), 'R');
                $table->addTableHead($this->getTableHead(EntryAttribute::Chipnr), $this->getWidth(EntryAttribute::Chipnr), 'R');
                $table->addTableHead(self::$TableHead_Name, $longWidth, 'L', true);
                $table->addTableHead($this->getTableHead(EntryAttribute::Year), $this->getWidth(EntryAttribute::Year), 'C');
                $table->addTableHead($this->getTableHead(EntryAttribute::AgeGroup), $this->getWidth(EntryAttribute::AgeGroup), 'C');
                $table->addTableHead($this->getTableHead(EntryAttribute::Run), $this->getWidth(EntryAttribute::Run), 'L');
                $table->addTableHead("Preis", $this->m_widthPrice, 'L');
                if ($cupFlag)
                {
                    $table->addTableHead($this->getTableHead(EntryAttribute::Cup), $this->getWidth(EntryAttribute::Cup), 'C');
                }
            }

            $price = getPriceFromEntry($entry);
            $priceSum += $price;

            $table->addTableData($entry->getStartnrAsString());
            $table->addTableData($entry->getChipnrAsString());
            $table->addTableData($entry->getLastnameFirstname());
            $table->addTableData($entry->getYearAsString());
            $table->addTableData($entry->getAgeGroupAsString());
            $table->addTableData($entry->getRunAsStringShort());
            $table->addTableData(getPriceAsString($price));
            if ($cupFlag)
            {
                $table->addTableData($entry->getCupAsString());
            }
        }
        if (isset($table))
        {
            $table->end();
            $this->outputPriceSum($priceSum, $lastTeam);
        }
    }

    private function outputLineFeed(float $lf)
    {
        $textObject = new TextObject($this->m_pdfCreator, "");
        $textObject->setFont($this->m_fontData);
        $textObject->setLineFeed($lf);

        if ($this->m_pdfCreator->fitsOnPage($textObject->getHeight()))
        {
            $textObject->generate();
        }
        else
        {
            $this->m_pdfCreator->doPageBreak();
        }
    }

    private function outputPriceSum(float $priceSum, string $teamName)
    {
        $this->outputLineFeed(1.0);
        $priceString = getPriceAsString($priceSum);
        $textObject = new TextObject($this->m_pdfCreator, "Gesamtsumme $teamName: $priceString");
        $textObject->setFont($this->m_fontData);
        if (!$this->m_pdfCreator->fitsOnPage($textObject->getHeight()))
        {
            $this->m_pdfCreator->doPageBreak();
        }
        $textObject->generate();
    }

    private function outputNumberOfEntries(int $number)
    {
        $this->outputLineFeed(1.0);
        $textObject = new TextObject($this->m_pdfCreator, "Anzahl Meldungen: $number");
        $textObject->setFont($this->m_fontData);
        if (!$this->m_pdfCreator->fitsOnPage($textObject->getHeight()))
        {
            $this->m_pdfCreator->doPageBreak();
        }
        $textObject->generate();
    }

    private function createRunList(array $runList)
    {
        $runGroups = Entries::SplitEntriesByRunAndGender($this->m_entries);
        $cupFlag = Config::Get()['features']['cupFlag'];
        foreach ($runGroups as $runGroup)
        {
            $run = $runGroup[0]->getRun();
            if ($runList[$run])
            {
                EntrySorter::Sort($runGroup, EntrySorter::OrderStartnrIndex);

                $this->m_pdfCreator->doPageBreak();
                $tableName = $runGroup[0]->getRunAsString() ." • " . strtoupper($runGroup[0]->getGender());
                $table = new Table($this->m_pdfCreator, $tableName, $this->m_fontName1, $this->m_fontHead, $this->m_fontData);

                $sumWidth = $this->getWidth(EntryAttribute::Startnr) + $this->getWidth(EntryAttribute::Year) + $this->getWidth(EntryAttribute::AgeGroup);
                if ($cupFlag)
                {
                    $sumWidth += $this->getWidth(EntryAttribute::Cup);
                }

                $longWidth = (100 - $sumWidth) / 2;

                $table->addTableHead($this->getTableHead(EntryAttribute::Startnr), $this->getWidth(EntryAttribute::Startnr), 'R', true);
                $table->addTableHead($this->getTableHead(EntryAttribute::Chipnr), $this->getWidth(EntryAttribute::Chipnr), 'R');
                $table->addTableHead(self::$TableHead_Name, $longWidth, 'L');
                $table->addTableHead($this->getTableHead(EntryAttribute::Year), $this->getWidth(EntryAttribute::Year), 'C');
                $table->addTableHead($this->getTableHead(EntryAttribute::AgeGroup), $this->getWidth(EntryAttribute::AgeGroup), 'C');
                $table->addTableHead($this->getTableHead(EntryAttribute::Team), $longWidth, 'L');
                if ($cupFlag)
                {
                    $table->addTableHead($this->getTableHead(EntryAttribute::Cup), $this->getWidth(EntryAttribute::Cup), 'C');
                }

                foreach ($runGroup as $entry)
                {
                    $table->addTableData($entry->getStartnrAsString());
                    $table->addTableData($entry->getChipnrAsString());
                    $table->addTableData($entry->getLastnameFirstname());
                    $table->addTableData($entry->getYearAsString());
                    $table->addTableData($entry->getAgeGroupAsString());
                    $table->addTableData($entry->getTeamAsString());
                    if ($cupFlag)
                    {
                        $table->addTableData($entry->getCupAsString());
                    }
                }
                $table->end();

                $this->outputNumberOfEntries(count($runGroup));
            }
        }
    }

    private function createCupList()
    {
        $this->m_pdfCreator->doPageBreak();
        EntrySorter::Sort($this->m_entries, EntrySorter::OrderLastnameIndex);

        $table = new Table($this->m_pdfCreator, "Cup-Liste", $this->m_fontName1, $this->m_fontHead, $this->m_fontData);

        $sumWidth = $this->getWidth(EntryAttribute::Startnr) +
        $this->getWidth(EntryAttribute::Year) +
        $this->getWidth(EntryAttribute::AgeGroup) +
        $this->getWidth(EntryAttribute::Run);

        $longWidth = (100 - $sumWidth) / 2;

        $table->addTableHead($this->getTableHead(EntryAttribute::Startnr), $this->getWidth(EntryAttribute::Startnr), 'R');
        $table->addTableHead($this->getTableHead(EntryAttribute::Chipnr), $this->getWidth(EntryAttribute::Chipnr), 'R');
        $table->addTableHead(self::$TableHead_Name, $longWidth, 'L', true);
        $table->addTableHead($this->getTableHead(EntryAttribute::Year), $this->getWidth(EntryAttribute::Year), 'C');
        $table->addTableHead($this->getTableHead(EntryAttribute::AgeGroup), $this->getWidth(EntryAttribute::AgeGroup), 'C');
        $table->addTableHead($this->getTableHead(EntryAttribute::Team), $longWidth, 'L');
        $table->addTableHead($this->getTableHead(EntryAttribute::Run), $this->getWidth(EntryAttribute::Run), 'L');

        foreach ($this->m_entries as $entry)
        {
            if ($entry->getCup())
            {
                $table->addTableData($entry->getStartnrAsString());
                $table->addTableData($entry->getChipnrAsString());
                $table->addTableData($entry->getLastnameFirstname());
                $table->addTableData($entry->getYearAsString());
                $table->addTableData($entry->getAgeGroupAsString());
                $table->addTableData($entry->getTeamAsString());
                $table->addTableData($entry->getRunAsStringShort());
            }
        }

        $table->end();
    }

    private $m_entries = array();
    private $m_align = array();
    private $m_pdfCreator;
    private $m_fontName1;
    private $m_fontName2;
    private $m_fontHead;
    private $m_fontData;
    private $m_width;
    private $m_widthPrice;
    private static $TableHead_Name = "Name";
}

new DownloadListsPage();

?>