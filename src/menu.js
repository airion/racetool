"use strict";

var g_openDropdown = null;

function buttonClicked(button)
{
    var dropDown = button.nextElementSibling;
    var open = dropDown != g_openDropdown;

    hideOpenDropdown();

    if (open)
    {
        openDropdown(button, dropDown);
    }
}

function hideOpenDropdown()
{
    if (g_openDropdown != null)
    {
        var menuBackground = document.getElementById("menuBackground");
        menuBackground.style.display = "none";
        g_openDropdown.style.display = "none";
        g_openDropdown = null;
    }
}

function openDropdown(button, dropDown)
{
    var menuBackground = document.getElementById("menuBackground");
    menuBackground.style.display = "block";
    dropDown.style.display = "block";

    var pos = button.getBoundingClientRect();
    var windowWidth = document.body.clientWidth;

    if (hasClass(button, "footerRightAligned"))
    {
        dropDown.style.right = windowWidth - pos.right + "px";
    } else
    {
        var x = Math.min(pos.left, windowWidth - dropDown.clientWidth);
        dropDown.style.left = x + "px";
    }

    dropDown.style.bottom = window.innerHeight - pos.top + "px";

    g_openDropdown = dropDown;
}

var footer = document.getElementById("footer");
footer.addEventListener("scroll", hideOpenDropdown);
