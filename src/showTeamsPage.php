<?php declare(strict_types = 1);

include_once 'private/showRegistrationsPage.php';
include_once 'private/database.php';
include_once 'private/convertStringToHTML.php';
include_once 'private/entry.php';

class ShowTeamsPage extends ShowRegistrationsPage
{
    function __construct()
    {
        $style = "";
        $style .= <<<EOD
    .tableStatistics
    {
        font-size: 0.7em;
        border-spacing:0px;
        color: Black;
    }

    .tableStatistics td
    {
        padding: 0.5em;
        box-shadow: inset -1px 0px Black;
        padding-right: 1.0em;
    }

    .tableStatistics td:first-child
    {
        box-shadow: inset 1px 0 Black,
                    inset -1px 0px Black;
    }

    .tableStatistics td:not(:first-child)
    {
        text-align: right;
    }

    .tableStatistics th
    {
        background: #505050;
        color: White;
        text-align: left;
        padding: 0.5em;
        padding-right: 1.0em;
        white-space: nowrap;
        box-shadow: inset -1px 1px Black,
                    inset 0 -1px Black,
                    0px 2px 2px rgba(0, 0, 0, 0.5);
        z-index: 1;
    }

    .tableStatistics th:first-child
    {
        box-shadow: inset -1px 1px Black,
                    inset +1px -1px Black,
                    0px 2px 2px rgba(0, 0, 0, 0.5);
    }

    .tableStatistics tr:nth-child(even)
    {
        background: #ffffff;
    }

    .tableStatistics tr:nth-child(odd)
    {
        background: #dfdfdf;
    }

    .tableStatistics tr:last-child td
    {
        box-shadow: inset -1px -1px Black;
    }

    .tableStatistics tr:last-child td:first-child
    {
        box-shadow: inset 1px -1px Black,
                    inset -1px 0px Black;
    }

EOD;

        $parameters = new Parameters();
        $activeTab = 2;
        parent::__construct($style, $parameters, $activeTab);
    }

    function outputContent()
    {
        $database = new Database();
        $database->close();

        $this->beginScrollable();

        echo "<div class='overflowXAuto'>";
        echo "<table class='tableStatistics'>";
        echo "<tr>";
        echo "<th class='positionStickyTop'>" . Entry::GetAttributeShortName(EntryAttribute::Team) . "</th>";
        echo "<th class='positionStickyTop'>Meldungen</th>";
        echo "<th class='positionStickyTop'>Finisher</th>";
        echo "</tr>";
        $teamList = $database->getRegistrationsAndFinisherTeamList();
        foreach ($teamList as $team => $count)
        {
            if ($team == "")
            {
                $team = "---";
            }
            $teamHTML = convertStringToHTML($team);

            echo "<tr>";
            echo "<td>";
            echo $teamHTML;
            echo "</td>";
            echo "<td>";
            echo $count->registrations;
            echo "</td>";
            echo "<td>";
            echo $count->finisher;
            echo "</td>";
            echo "</tr>";
        }

        echo "</table>";
        echo "</div>";
    }
}

new ShowTeamsPage();

?>
