<?php declare(strict_types = 1);

include_once 'private/showRegistrationsPage.php';
include_once 'private/database.php';
include_once 'private/entryTable.php';
include_once 'private/post.php';

class ShowListPage extends ShowRegistrationsPage
{
    function __construct()
    {
        $style = "";
        $style .= EntryTable::getStyle();

        $parameters = new Parameters();
        $parameters->addJS("orderByShowList", "document.settings.orderBy");
        $parameters->addJS("hiddenColumnsShowList", "document.settings.hiddenColumns");
        $parameters->addJS("filterTextShowList", "document.settings.filterText");
        $parameters->addJS("filterRunShowList", "document.settings.filterRun");

        $activeTab = 0;
        parent::__construct($style, $parameters, $activeTab);
    }

    function hasTeamPlacing(): bool
    {
        $runs = Runs::GetInstance()->getRuns();
        foreach ($runs as $run)
        {
            if ($run->getTeamPlacing())
                return true;
        }
        return false;
    }

    function outputContent()
    {
        $database = new Database();
        $database->close();

        $programMode = Config::Get()['program']['mode'];

        $onClickPage = "";
        $allowShowingDeleted = false;
        $enabledColumns = 0;
        EntryTable::AddColumn($enabledColumns, EntryAttribute::Lastname);
        EntryTable::AddColumn($enabledColumns, EntryAttribute::Firstname);
        EntryTable::AddColumn($enabledColumns, EntryAttribute::Year);
        EntryTable::AddColumn($enabledColumns, EntryAttribute::AgeGroup);
        EntryTable::AddColumn($enabledColumns, EntryAttribute::Team);
        EntryTable::AddColumn($enabledColumns, EntryAttribute::Run);

        if ($programMode == ProgramMode::Registration || $programMode == ProgramMode::Finished)
        {

            EntryTable::AddColumn($enabledColumns, EntryAttribute::Time);
            EntryTable::AddColumn($enabledColumns, EntryAttribute::Placing);
            EntryTable::AddColumn($enabledColumns, EntryAttribute::PlacingMW);
            EntryTable::AddColumn($enabledColumns, EntryAttribute::PlacingAgeGroup);

            if ($this->hasTeamPlacing())
            {
                EntryTable::AddColumn($enabledColumns, EntryAttribute::PlacingTeam);
            }
        }

        $shortColumnNames = false;
        $enableShowColumnButtons = false;
        $orderBy = Post::GetIntValueFromPost("orderByShowList", EntryAttribute::Lastname);
        $hiddenColumns = Post::GetIntValueFromPost("hiddenColumnsShowList");
        $filterText = Post::GetStringValueFromPost("filterTextShowList");
        $filterRun = Post::GetIntValueFromPost("filterRunShowList", -1);

        new EntryTable($this, $database, $onClickPage, $allowShowingDeleted, $enabledColumns, $shortColumnNames, $enableShowColumnButtons, $orderBy, $hiddenColumns, $filterText, $filterRun);
    }
}

new ShowListPage();

?>
