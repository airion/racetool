<?php declare(strict_types = 1);

class MessageBox
{
    static function OutputMessage(string $message)
    {
        echo "<script>openMessageBox('$message', MessageBoxType_Message)</script>";
    }

    static function GetStyle(): string
    {
        $style = <<<EOD
        .messageBox
        {
            position: fixed;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            background-color: rgba(0,0,0,0.4);
            z-index: 101;
        }
        .messageBox > div
        {
            background-color: #ddd;
            box-shadow: 10px 10px 6px rgba(0,0,0,0.4);
            padding: 1.0em;
            color: #000;
            border-style: solid;
            border-width: 0.1em;
            border-radius: 0.5em;
        }
        .messageBox > div > div:nth-child(1)
        {
            margin-bottom: 1em;
        }
        .messageBox > div > div > span
        {
            color: blue;
        }
        .messageBox > div > div > p
        {
            margin-left: 1em;
        }
        .messageBox > div > div:nth-child(2)
        {
        }
        .messageBox button:last-child
        {
            margin-right: 0em;
            margin-bottom: 0em;
        }
        .messageBox button:not(:last-child)
        {
            margin-right: 2em;
            margin-bottom: 0em;
        }

        .messageBoxGreen
        {
            color: green;
        }

        .messageBoxRed
        {
            color: red;
        }
EOD;

        return $style;
    }
}
