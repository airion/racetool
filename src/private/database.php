<?php declare(strict_types=1);
include_once 'private/entry.php';
include_once 'private/entrySorter.php';
include_once 'private/config.php';
include_once 'private/filelock.php';
include_once 'private/filefunctions.php';
include_once 'private/runs.php';
include_once 'private/timeDataset.php';
include_once 'private/evaluation.php';
include_once 'private/filePathRegistry.php';

class Database
{
    function __construct()
    {
        $numberRuns = Runs::GetInstance()->getNumberOfRuns();
        for ($i = 0; $i < $numberRuns; $i++)
        {
            $this->m_timeDatasets[] = new TimeDataset();
        }
        $currentTimeDatasetIndex = 0;

        $dbPath = FilePathRegistry::GetDBPath();
        $this->m_locked = lockFile($dbPath);

        if ($this->m_locked)
        {
            $readingFailed = false;

            $fh = @fopen($dbPath, "c+");

            if ($fh)
            {
                while (!feof($fh))
                {
                    $line = fgets($fh);
                    if ($line == true)
                    {
                        $line = str_replace(array(
                            "\r",
                            "\n"
                        ), '', $line);
                        $fields = explode(";", $line);

                        if (count($fields) > 0)
                        {
                            $type = array_shift($fields);
                            if ($type == Entry::GetTypeString())
                            {
                                $entry = new Entry();
                                if ($entry->readFromFile($fields))
                                {
                                    $this->addEntry($entry);
                                }
                                else
                                {
                                    $readingFailed = true;
                                }
                            } else
                            if ($type == Database::$m_blockedStartNumbersTypeString)
                            {
                                if (!$this->readBlockedStartNumbers($fields))
                                {
                                    $readingFailed = true;
                                }
                            } else
                            if ($type == Database::$m_blockedChipNumbersTypeString)
                            {
                                if (!$this->readBlockedChipNumbers($fields))
                                {
                                    $readingFailed = true;
                                }
                            } else
                            if ($type == TimeDataset::GetTypeString())
                            {
                                if ($currentTimeDatasetIndex < $numberRuns)
                                {
                                    if (!$this->m_timeDatasets[$currentTimeDatasetIndex++]->readFromFile($fields))
                                    {
                                        $readingFailed = true;
                                    }
                                }
                                else
                                {
                                    $readingFailed = true;
                                    echo "<p class='yellow'>Mehr Zeitdatensätze als Wettbewerbe!</p>";
                                }
                            } else
                            if ($type == Database::$m_lastModificationTimeTypeString)
                            {
                                if (!$this->readLastModificationTime($fields))
                                {
                                    $readingFailed = true;
                                }
                            }
                            else
                            {
                                $readingFailed = true;
                                $numberFields = count($fields);
                                echo "<p class='yellow'>Ungültiger Eintrag mit Typ $type, Anzahl Felder: $numberFields !</p>";
                            }
                        }
                        else
                        {
                            $readingFailed = true;
                            echo "<p class='yellow'>Ungültiger leerer Eintrag !</p>";
                        }
                    }
                }
                $retval = @fclose($fh);
                if (!$retval)
                {
                    echo "<p class='yellow'>Fehler: Datei $dbPath konnte nicht geschlossen werden!</p>";
                    $readingFailed = true;
                }
            }
            else
            {
                echo "<p class='yellow'>Fehler: Datei $dbPath konnte nicht geöffnet werden!</p>";
                $readingFailed = true;
            }

            if ($readingFailed)
            {
                $this->close();
            }
        }
        else
        {
            echo "<p class='yellow'>Fehler: Datenbank konnte nicht gelockt werden!</p>";
        }

        $this->doCompetitionEvaluation();
    }

    function __destruct()
    {
        if ($this->m_locked)
        {
            $this->close();
        }
    }

    function close(): bool
    {
        if ($this->m_locked)
        {
            $success = true;

            if ($this->m_changed)
            {
                if (!$this->write())
                {
                    $success = false;
                }
                $this->m_changed = false;
            }

            unlockFile(FilePathRegistry::GetDBPath());

            $this->m_locked = false;
            return $success;
        }
        else
        {
            return false;
        }
    }

    function writeEntry(Entry $entry): bool
    {
        if ($this->m_locked)
        {
            $this->addEntry($entry);
            $this->m_changed = true;
            return true;
        }
        return false;
    }

    function generateStartAndChipNumbers(int &$numberOfGeneratedStartNumbers, int &$numberOfGeneratedChipNumbers)
    {
        $entries = $this->getEntriesNoClone();
        EntrySorter::Sort($entries, EntrySorter::OrderCupTeamLastnameIndex);

        $numberOfGeneratedStartNumbers = 0;
        $numberOfGeneratedChipNumbers = 0;

        $usedStartnr = $this->getUsedStartNumbers();
        $usedChipnr = $this->getUsedChipNumbers();

        foreach ($entries as $entry)
        {
            if ($entry->getStartnr() == Entry::INVALID_STARTNR)
            {
                $startNr = $this->GetNewStartNumber($entry, $usedStartnr);
                if ($startNr != Entry::INVALID_STARTNR)
                {
                    $entry->setStartnr($startNr);

                    $usedStartnr[$startNr] = 1;
                    $numberOfGeneratedStartNumbers++;
                    $this->m_changed = true;
                }
            }

            if ($entry->getChipnr() == Entry::INVALID_CHIPNR)
            {
                $chipNr = $this->GetNewChipNumber($entry, $usedChipnr);
                if ($chipNr != Entry::INVALID_CHIPNR)
                {
                    $entry->setChipnr($chipNr);

                    $usedChipnr[$chipNr] = 1;
                    $numberOfGeneratedChipNumbers++;
                    $this->m_changed = true;
                }
            }
        }
    }

    function clear()
    {
        $this->m_allEntries = array();
        $this->m_revisionMap = array();
        $this->m_blockedStartNumbers = array();
        $this->m_blockedChipNumbers = array();

        foreach($this->m_timeDatasets as $timeDataset)
        {
            $timeDataset->clear();
        }

        $this->m_changed = true;
    }

    function isEmpty(): bool
    {
        if (!$this->m_locked)
        {
            return false;
        }

        foreach($this->m_timeDatasets as $timeDataset)
        {
            if (!empty($timeDataset->getTimeData()))
            {
                return false;
            }
        }

        return
            empty($this->m_allEntries) &&
            empty($this->m_blockedStartNumbers) &&
            empty($this->m_blockedChipNumbers);
    }

    function backup(string &$backupPath): bool
    {
        $date = date("c");
        $dbPath = FilePathRegistry::GetDBPath();
        $backupPath = $dbPath . "_backup_" . $date;

        if ($this->m_locked && !$this->m_changed)
        {
            if (file_exists($dbPath))
            {
                if (copy($dbPath, $backupPath))
                {
                    return true;
                }
            }
        }
        return false;
    }

    function restoreAndClose(string $filePath): bool
    {
        if ($this->m_locked && !$this->m_changed)
        {
            if (file_exists($filePath))
            {
                if (copy($filePath, FilePathRegistry::GetDBPath()))
                {
                    return $this->close();
                }
            }
        }
        return false;
    }

    function addBlockedStartNumber(int $number)
    {
        $this->m_blockedStartNumbers[$number] = 1;
        $this->m_changed = true;
    }

    function removeBlockedStartNumber(int $number)
    {
        unset($this->m_blockedStartNumbers[$number]);
        $this->m_changed = true;
    }

    function addBlockedChipNumber(int $number)
    {
        $this->m_blockedChipNumbers[$number] = 1;
        $this->m_changed = true;
    }

    function removeBlockedChipNumber(int $number)
    {
        unset($this->m_blockedChipNumbers[$number]);
        $this->m_changed = true;
    }

    function searchEntryByKey(string $key): Entry
    {
        $foundEntry = new Entry();
        foreach ($this->m_allEntries as $entry)
        {
            if ($entry->getKey() == $key)
            {
                $foundEntry = $entry;
            }
        }

        return $foundEntry;
    }

    function generateNewKey(): string
    {
        $allKeys = array();
        foreach ($this->m_allEntries as $entry)
        {
            $allKeys[$entry->getKey()] = 1;
        }

        while (true)
        {
            $key = $this->randomStr(6);
            if (!isset($allKeys[$key]))
            {
                return $key;
            }
        }
    }

    function getEntries(): array
    {
        $entryByKey = array();
        foreach ($this->m_allEntries as $entry)
        {
            if ($entry->isValid())
            {
                $entryByKey[$entry->getKey()] = clone $entry;
            }
            else
            {
                unset($entryByKey[$entry->getKey()]);
            }
        }
        return $entryByKey;
    }

    function hasEqualEntryWithOtherKey(Entry $other): bool
    {
        $entryByKey = $this->getEntries();
        foreach ($entryByKey as $entry)
        {
            if ($entry->isEqual($other) && $other->getKey() != $entry->getKey())
            {
                return true;
            }
        }
        return false;
    }

    function getEntriesWithDeleted(): array
    {
        $entryByKey = array();
        foreach ($this->m_allEntries as $entry)
        {
            if ($entry->isValid())
            {
                $entryByKey[$entry->getKey()] = clone $entry;
            }
            else
            {
                $entryByKey[$entry->getKey()]->setDeleted();
                $entryByKey[$entry->getKey()]->setDate($entry->getDate());
                $entryByKey[$entry->getKey()]->setUsername($entry->getUsername());
                $entryByKey[$entry->getKey()]->setIndex($entry->getIndex());
                $entryByKey[$entry->getKey()]->setRevision($entry->getRevision());
            }
        }
        return $entryByKey;
    }

    function getEntriesForKeyWithDeleted(string $key): array
    {
        $entries = array();
        $lastValidEntry = new Entry();
        foreach ($this->m_allEntries as $entry)
        {
            if ($entry->getKey() == $key)
            {
                if ($entry->isValid())
                {
                    $lastValidEntry = clone $entry;
                    array_push($entries, $lastValidEntry);
                }
                else
                {
                    $deleteEntry = clone $lastValidEntry;
                    $deleteEntry->setDeleted();
                    $deleteEntry->setDate($entry->getDate());
                    $deleteEntry->setUsername($entry->getUsername());
                    $deleteEntry->setIndex($entry->getIndex());
                    $deleteEntry->setRevision($entry->getRevision());

                    array_push($entries, $deleteEntry);
                }

            }
        }
        return $entries;
    }

    function getNumberOfRegistrationsPerRun(): array
    {
        $entries = $this->getEntries();
        $registrationsPerRun = array();
        $numberOfRuns = Runs::GetInstance()->getNumberOfRuns();
        for ($i = 0; $i < $numberOfRuns; $i++)
        {
            $registrationsPerRun[$i] = 0;
        }
        foreach ($entries as $entry)
        {
            $runIndex = $entry->getRun();
            if ($runIndex >= 0 && $runIndex < $numberOfRuns)
            {
                $registrationsPerRun[$runIndex]++;
            }
        }
        return $registrationsPerRun;
    }

    function getNumberOfFinisherPerRun(): array
    {
        $entries = $this->getEntries();
        $finisherPerRun = array();
        $numberOfRuns = Runs::GetInstance()->getNumberOfRuns();
        for ($i = 0; $i < $numberOfRuns; $i++)
        {
            $finisherPerRun[$i] = 0;
        }
        foreach ($entries as $entry)
        {
            $runIndex = $entry->getRun();
            if ($runIndex >= 0 && $runIndex < $numberOfRuns && $entry->getPlacing() != Entry::INVALID_PLACING)
            {
                $finisherPerRun[$runIndex]++;
            }
        }
        return $finisherPerRun;
    }

    function getRegistrationsAndFinisherTeamList(): array
    {
        $entries = $this->getEntries();
        $teams = array();
        foreach($entries as $entry)
        {
            $team = $entry->getTeam();
            if (!isset($teams[$team]))
            {
                $count = new StdClass();
                $count->{"registrations"} = 0;
                $count->{"finisher"} = 0;
                $teams[$team] = $count;
            }

            $teams[$team]->registrations++;
            if ($entry->getTime()->isValid())
            {
                $teams[$team]->finisher++;
            }
        }
        uasort($teams, "Database::CmpRegistrationsAndFinisherTeamList");
        return $teams;
    }

    function getTimeOfLastModificationAsString(): string
    {
        return date("Y-m-d_H-i-s", $this->m_lastModificationTime);
    }

    function getTimeOfLastModification():int
    {
        return $this->m_lastModificationTime;
    }

    function generateNewStartNumber(Entry $entry): int
    {
        $usedStartNumbers = $this->getUsedStartNumbers($entry->getKey());
        return Database::GetNewStartNumber($entry, $usedStartNumbers);
    }

    function generateNewChipNumber(Entry $entry): int
    {
        $usedChipNumbers = $this->getUsedChipNumbers($entry->getKey());
        return Database::GetNewChipNumber($entry, $usedChipNumbers);
    }

    function getUsedStartNumbers(string $keyToExclude = ""): array
    {
        $entries = $this->getEntries();

        $usedStartnr = $this->m_blockedStartNumbers;
        foreach ($entries as $entry)
        {
            if (($entry->getStartnr() != Entry::INVALID_STARTNR) && ($entry->getKey() != $keyToExclude))
            {
                $usedStartnr[$entry->getStartnr()] = 1;
            }
        }
        return $usedStartnr;
    }

    function getUsedChipNumbers(string $keyToExclude = ""): array
    {
        $entries = $this->getEntries();

        $usedChipnr = $this->m_blockedChipNumbers;
        foreach ($entries as $entry)
        {
            if (($entry->getChipnr() != Entry::INVALID_CHIPNR) && ($entry->getKey() != $keyToExclude))
            {
                $usedChipnr[$entry->getChipnr()] = 1;
            }
        }
        return $usedChipnr;
    }

    static function GetNewStartNumber(Entry $entry, array $usedStartnr): int
    {
        if (Config::Get()['features']['cupFlag'])
        {
            if ($entry->getCup())
            {
                $startNr = Config::Get()['startNumbers']['min'];
            }
            else
            {
                $startNr = Config::Get()['startNumbers']['cupReserved'] + 1;
            }
        }
        else
        {
            $startNr = Config::Get()['startNumbers']['min'];
        }

        $startNrMax = Config::Get()['startNumbers']['max'];
        do
        {
            if (!isset($usedStartnr[$startNr]))
            {
                return $startNr;
            }
            $startNr++;
        } while ($startNr <= $startNrMax);

        return Entry::INVALID_STARTNR;
    }

    static function GetNewChipNumber(Entry $entry, array $usedChipnr): int
    {
        $startNr = $entry->getStartnr();
        $chipNrMin = Config::Get()['chipNumbers']['min'];
        $chipNrMax = Config::Get()['chipNumbers']['max'];
        if ($startNr != Entry::INVALID_STARTNR)
        {
            $startNrMin = Config::Get()['startNumbers']['min'];

            $chipNr = $startNr - $startNrMin + $chipNrMin;
            if (!isset($usedChipnr[$chipNr]) && $chipNr <= $chipNrMax)
            {
                return $chipNr;
            }
        }

        $chipNr = $chipNrMax;

        do
        {
            if (!isset($usedChipnr[$chipNr]))
            {
                return $chipNr;
            }
            $chipNr--;
        } while ($chipNr >= $chipNrMin);

        return Entry::INVALID_CHIPNR;
    }


    function isStartnrFree(Entry $entry): bool
    {
        $startNr = $entry->getStartnr();

        if ($startNr != Entry::INVALID_STARTNR)
        {
            if ($this->isStartnrBlocked($startNr))
            {
                return false;
            }

            $entries = $this->getEntries();
            foreach ($entries as $existingEntry)
            {
                if (($existingEntry->getKey() != $entry->getKey()) && ($existingEntry->getStartnr() == $startNr))
                {
                    return false;
                }
            }
        }
        return true;
    }

    function isChipnrFree(Entry $entry): bool
    {
        $chipNr = $entry->getChipnr();

        if ($chipNr != Entry::INVALID_CHIPNR)
        {
            if ($this->isChipnrBlocked($chipNr))
            {
                return false;
            }

            $entries = $this->getEntries();
            foreach ($entries as $existingEntry)
            {
                if (($existingEntry->getKey() != $entry->getKey()) && ($existingEntry->getChipnr() == $chipNr))
                {
                    return false;
                }
            }
        }
        return true;
    }

    function getBlockedStartNumbers(): array
    {
        $numbers = array_keys($this->m_blockedStartNumbers);
        sort($numbers);
        return $numbers;
    }

    function getBlockedChipNumbers(): array
    {
        $numbers = array_keys($this->m_blockedChipNumbers);
        sort($numbers);
        return $numbers;
    }

    function isStartnrBlocked(int $startNr): bool
    {
        return isset($this->m_blockedStartNumbers[$startNr]);
    }

    function isChipnrBlocked(int $startNr): bool
    {
        return isset($this->m_blockedChipNumbers[$startNr]);
    }

    function getTimeDataset(int $runIndex): TimeDataset
    {
        assert($runIndex < count($this->m_timeDatasets));
        return clone $this->m_timeDatasets[$runIndex];
    }

    function clearTimeData(int $runIndex)
    {
        assert($runIndex < count($this->m_timeDatasets));
        $this->m_timeDatasets[$runIndex]->clear();
        $this->m_changed = true;
    }

    function addTimeData(int $runIndex, TimeData $timeData)
    {
        assert($runIndex < count($this->m_timeDatasets));
        $this->m_timeDatasets[$runIndex]->addTimeData($timeData);
        $this->m_changed = true;
    }

    function getChipNumberToEntryMap(): array
    {
        $entriesByChipNumber = array();
        $entries = $this->getEntriesNoClone();
        foreach ($entries as $entry)
        {
            $chipNumber = $entry->getChipnr();
            if ($chipNumber != Entry::INVALID_CHIPNR)
            {
                $entriesByChipNumber[$chipNumber] = clone $entry;
            }
        }
        return $entriesByChipNumber;
    }

    function getTeamPlacing(int $run, string $gender): array
    {
        return $this->m_evaluation->getTeamPlacing($run, $gender);
    }

    function readfile(): bool
    {
        if ($this->m_locked && !$this->m_changed)
        {
            $dbPath = FilePathRegistry::GetDBPath();
            if (file_exists($dbPath))
            {
                readfile($dbPath);
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    private function addEntry(Entry $entry)
    {
        $revision = 1;
        if (isset($this->m_revisionMap[$entry->getKey()]))
        {
            $revision = $this->m_revisionMap[$entry->getKey()] + 1;
        }
        $this->m_revisionMap[$entry->getKey()] = $revision;
        $entry->setRevision($revision);

        $index = count($this->m_allEntries);
        $entry->setIndex($index);

        array_push($this->m_allEntries, $entry);
    }

    private function randomStr(int $length, string $keyspace = '123456789ABCDEFGHIJKLMNPQRSTUVWXYZ'): string
    {
        $str = '';
        $max = mb_strlen($keyspace, '8bit') - 1;
        for ($i = 0; $i < $length; ++$i)
        {
            $str .= $keyspace[rand(0, $max)];
        }
        return $str;
    }

    private function write(): bool
    {
        $this->m_lastModificationTime = time();

        $dbPath = FilePathRegistry::GetDBPath();
        $newFilePath = $dbPath . ".new";

        $fh = @fopen($newFilePath, "w");
        if (!$fh)
        {
            return false;
        }

        $writingOk = $this->writeToFile($fh);

        $closeOk = @fclose($fh);
        if (!$closeOk)
        {
            echo "<p class='yellow'>Fehler: Datei $newFilePath konnte nicht geschlossen werden!</p>";
            assert(false);
            return false;
        }

        if (!$writingOk)
        {
            return false;
        }

        $this->writeBackup();

        if (!rename($newFilePath, $dbPath))
        {
            echo "<p class='yellow'>Fehler: Datei $newFilePath konnte nicht umbenannt werden!</p>";
            assert(false);
            return false;
        }
        return true;
    }

    private function writeToFile($fh): bool
    {
        if (!$this->writeEntriesToFile($fh))
        {
            return false;
        }
        if (!$this->writeBlockerToFile($fh))
        {
            return false;
        }
        if (!$this->writeTimeDatasets($fh))
        {
            return false;
        }
        if (!$this->writeLastModificationTime($fh))
        {
            return false;
        }
        return true;
    }

    private function writeEntriesToFile($fh): bool
    {
        foreach($this->m_allEntries as $entry)
        {
            if (!$entry->writeToFile($fh))
            {
                echo "<p class='yellow'>Eintrag konnte nicht in Datenbank geschrieben werden !</p>";
                assert(false);
                return false;
            }
        }
        return true;
    }

    private function writeBlockerToFile($fh): bool
    {
        if (!$this->writeBlockedStartNumbersToFile($fh))
        {
            return false;
        }

        if (!$this->writeBlockedChipNumbersToFile($fh))
        {
            return false;
        }
        return true;
    }

    private function writeBlockedStartNumbersToFile($fh): bool
    {
        $blockerString = Database::$m_blockedStartNumbersTypeString;
        foreach ($this->m_blockedStartNumbers as $number => $value)
        {
            assert($value == 1);
            $blockerString .= ";$number";
        }
        $blockerString.="\n";
        return writeStringToFile($fh, $blockerString);
    }

    private function writeBlockedChipNumbersToFile($fh): bool
    {
        $blockerString = Database::$m_blockedChipNumbersTypeString;
        foreach ($this->m_blockedChipNumbers as $number => $value)
        {
            assert($value == 1);
            $blockerString .= ";$number";
        }
        $blockerString.="\n";
        return writeStringToFile($fh, $blockerString);
    }

    private function writeTimeDatasets($fh): bool
    {
        foreach ($this->m_timeDatasets as $timeDataset)
        {
            if (!$timeDataset->writeToFile($fh))
            {
                return false;
            }
        }
        return true;
    }

    private function writeLastModificationTime($fh): bool
    {
        $string = Database::$m_lastModificationTimeTypeString.";".$this->m_lastModificationTime."\n";
        return writeStringToFile($fh, $string);
    }

    private function getEntriesNoClone(): array
    {
        $entryByKey = array();
        foreach ($this->m_allEntries as $entry)
        {
            if ($entry->isValid())
            {
                $entryByKey[$entry->getKey()] = $entry;
            }
            else
            {
                unset($entryByKey[$entry->getKey()]);
            }
        }
        return $entryByKey;
    }

    private function readBlockedStartNumbers(array $fields): bool
    {
        foreach($fields as $numberAsString)
        {
            if (is_numeric($numberAsString))
            {
                $startNumber = intval($numberAsString);
                $this->m_blockedStartNumbers[$startNumber] = 1;
            }
            else
            {
                assert(false);
                return false;
            }
        }
        return true;
    }

    private function readLastModificationTime(array $fields): bool
    {
        if (count($fields) != 1)
        {
            return false;
        }
        $this->m_lastModificationTime = intval($fields[0]);
        return true;
    }

    private function readBlockedChipNumbers(array $fields): bool
    {
        foreach($fields as $numberAsString)
        {
            if (is_numeric($numberAsString))
            {
                $startNumber = intval($numberAsString);
                $this->m_blockedChipNumbers[$startNumber] = 1;
            }
            else
            {
                assert(false);
                return false;
            }
        }
        return true;
    }

    private function writeBackup()
    {
        if (Config::Get()['program']['mode'] == ProgramMode::Registration)
        {
            $dbBackupPath = FilePathRegistry::GetDBBackupPath();
            if ($dbBackupPath != "")
            {
                $fh = @fopen($dbBackupPath, "w");
                if ($fh)
                {
                    if (!$this->writeToFile($fh))
                    {
                        echo "<p class='yellow'>Backup File $dbBackupPath konnte nicht geschrieben werden!</p>";
                    }
                    if (!@fclose($fh))
                    {
                        echo "<p class='yellow'>Backup File $dbBackupPath konnte nicht geschlossen werden!</p>";
                    }
                    @exec("sync $dbBackupPath > /dev/null &");
                }
                else
                {
                    echo "<p class='yellow'> Backup File $dbBackupPath konnte nicht zum schreiben geöffnet werden! </p>";
                }

                $this->deleteOldBackupFiles(dirname($dbBackupPath));
            }
        }
    }

    private function deleteOldBackupFiles(string $dbBackupDir)
    {
        $pathParts = pathinfo(Config::Get()['files']['dbName']);
        $pattern = "/^".$pathParts['filename']."_backup_.*\.".$pathParts['extension']."$/";
        $files = @scandir($dbBackupDir);
        if ($files)
        {
            $matchedFiles = array();
            foreach ($files as $file)
            {
                if (preg_match($pattern, $file))
                {
                    $matchedFiles[] = $file;
                }
            }
            $versionsToKeep = Config::Get()['files']['dbBackupVersionsToKeep'];
            $count = count($matchedFiles);
            $toDelete = $count - $versionsToKeep;
            for ($i = 0; $i < $toDelete; $i++)
            {
                $file = $matchedFiles[$i];
                if (!@unlink($dbBackupDir . "/" . $file))
                {
                    echo "<p class='yellow'>Could not remove expired backup file! </p>";
                }
            }
        }
    }

    private function doCompetitionEvaluation()
    {
        $this->checkTimeDatasetsAndSetToEntries();
        $this->m_evaluation = new Evaluation($this->getEntriesNoClone());
    }

    private function checkTimeDatasetsAndSetToEntries()
    {
        $chipNumberToEntryMap = $this->getChipNumberToEntryMapNoClone();

        foreach ($this->m_timeDatasets as $run => $timeDataset)
        {
            $this->checkTimeDatasetAndSetToEntries($run, $timeDataset, $chipNumberToEntryMap);
        }
    }

    private function getChipNumberToEntryMapNoClone(): array
    {
        $entriesByChipNumber = array();
        $entries = $this->getEntriesNoClone();
        foreach ($entries as $entry)
        {
            $chipNumber = $entry->getChipnr();
            if ($chipNumber != Entry::INVALID_CHIPNR)
            {
                $entriesByChipNumber[$chipNumber] = $entry;
            }
        }
        return $entriesByChipNumber;
    }

    private function checkTimeDatasetAndSetToEntries(int $run, TimeDataSet $timeDataset, array $chipNumberToEntryMap)
    {
        $timeDataArray = $timeDataset->getTimeData();
        $chipNumberToTimeDataMap = array();

        foreach ($timeDataArray as $timeData)
        {
            $chipNumber = $timeData->getChipNumber();
            if ($chipNumber != Entry::INVALID_CHIPNR)
            {
                if (array_key_exists($chipNumber, $chipNumberToTimeDataMap))
                {
                    $timeData->setDuplicateChipId();
                }
                else
                {
                    $chipNumberToTimeDataMap[$chipNumber] = $timeData;
                }

                if (array_key_exists($chipNumber, $chipNumberToEntryMap))
                {
                    $entry = $chipNumberToEntryMap[$chipNumber];
                    if ($entry->getRun() == $run)
                    {
                        $timeData->setChipNumberFitsRun();
                    }
                }
            }
        }

        foreach ($this->m_allEntries as $entry)
        {
            if ($entry->getRun() == $run)
            {
                $chipNumber = $entry->getChipnr();
                if (array_key_exists($chipNumber, $chipNumberToTimeDataMap))
                {
                    $timeData = $chipNumberToTimeDataMap[$chipNumber];
                    $entry->setDatasetTime($timeData->getTime());
                }
            }
        }
    }

    private static function CmpRegistrationsAndFinisherTeamList($a, $b)
    {
        $diff = $b->finisher - $a->finisher;
        if ($diff != 0)
        {
            return $diff;
        }
        return $b->registrations - $a->registrations;
    }

    private $m_allEntries = array();
    private $m_locked = false;
    private $m_changed = false;
    private $m_revisionMap = array();
    private $m_blockedStartNumbers = array();
    private $m_blockedChipNumbers = array();
    private $m_timeDatasets = array();

    private static $m_blockedStartNumbersTypeString = "Blocked StartNumbers";
    private static $m_blockedChipNumbersTypeString = "Blocked ChipNumbers";
    private static $m_lastModificationTimeTypeString = "DatabaseLastModificationTime";

    private $m_evaluation = null;
    private $m_lastModificationTime = 0;
}
