<?php declare(strict_types=1);

include_once 'private/config.php';

function addDraftOverlay(TCPDF $pdf)
{
    if (Config::Get()['program']['mode'] == ProgramMode::PreRegistration)
    {
        $pdf->SetAutoPageBreak(false);
        $pdf->SetTextColor(255, 0, 0);
        $numPages = $pdf->getNumPages();
        $angle = rad2deg(atan($pdf->GetPageHeight() / $pdf->GetPageWidth()));
        for ($page = 1; $page <= $numPages; $page++)
        {
            $pdf->setPage($page);
            $pdf->setAlpha(1, 'Normal', 0.3);
            $pdf->SetFont('dejavusans', 'B', 140, '');
            $pdf->SetLeftMargin(0);
            $pdf->SetRightMargin(0);

            $pdf->StartTransform();

            $pdf->setXY(0, $pdf->GetPageHeight() / 2.0);
            $pdf->Rotate($angle, $pdf->GetPageWidth() / 2.0, $pdf->GetPageHeight() / 2.0);
            $pdf->Cell(0, 0, "ENTWURF", 0, 0, 'C', false, '', 0, false, 'M', 'M');

            $pdf->StopTransform();
            $pdf->setAlpha(1, 'Normal', 1.0);
        }
    }
}
