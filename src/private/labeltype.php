<?php declare(strict_types=1);

class LabelType
{

    public function __construct(float $pageWidth, float $pageHeight, int $numX, int $numY, float $margin)
    {
        $this->m_pageWidth = $pageWidth;
        $this->m_pageHeight = $pageHeight;
        $this->m_numX = $numX;
        $this->m_numY = $numY;
        $this->m_margin = $margin;

        $this->m_labelWidth = $this->m_pageWidth / $this->m_numX;
        $this->m_labelHeight = $this->m_pageHeight / $this->m_numY;
    }

    public function getPageWidth(): float
    {
        return $this->m_pageWidth;
    }

    public function getPageHeight(): float
    {
        return $this->m_pageHeight;
    }

    public function getLabelWidth(): float
    {
        return $this->m_labelWidth;
    }

    public function getLabelHeight(): float
    {
        return $this->m_labelHeight;
    }

    public function getNumX(): int
    {
        return $this->m_numX;
    }

    public function getNumY(): int
    {
        return $this->m_numY;
    }

    public function getMargin(): float
    {
        return $this->m_margin;
    }

    private $m_pageWidth = 0.0;
    private $m_pageHeight = 0.0;
    private $m_numX = 0;
    private $m_numY = 0;
    private $m_margin = 0;

    private $m_labelWidth = 0.0;
    private $m_labelHeight = 0.0;
}
