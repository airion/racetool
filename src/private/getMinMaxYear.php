<?php

declare(strict_types=1);
include_once 'private/config.php';
include_once 'private/runs.php';

function getMinYear(bool $checkYearRestrictive): int
{
    if ($checkYearRestrictive)
    {
        return Runs::GetInstance()->getMinYear();
    }
    else
    {
        return Config::Get()['event']['year'] - 200;
    }
}

function getMaxYear(bool $checkYearRestrictive): int
{
    if ($checkYearRestrictive)
    {
        return Runs::GetInstance()->getMaxYear();
    }
    else
    {
        return Config::Get()['event']['year'];
    }
}
