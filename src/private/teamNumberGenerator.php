<?php declare(strict_types = 1);

include_once 'private/database.php';

class TeamNumberGenerator
{
    function __construct(Database $database)
    {
        $entries = $database->getEntries();
        EntrySorter::Sort($entries, EntrySorter::OrderTeamLastnameIndex);

        $newTeamNumber = 1;
        foreach ($entries as $entry)
        {
            $team = $entry->getTeam();
            if ($team != "")
            {
                if (!array_key_exists($team, $this->m_teamNameToNumberArray))
                {
                    $this->m_teamNameToNumberArray[$team] = strval($newTeamNumber++);
                }
            }
        }
    }

    function getNumberAsString(Entry $entry): string
    {
        $team = $entry->getTeam();
        if (array_key_exists($team, $this->m_teamNameToNumberArray))
        {
            return $this->m_teamNameToNumberArray[$team];
        }
        else
        {
            return "";
        }
    }

    private $m_teamNameToNumberArray = array();

}
