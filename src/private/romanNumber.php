<?php declare(strict_types=1);

class RomanNumber
{
    static function GetAsRomanNumber(int $value): string
    {
        $roman = array( "M","CM","D","CD","C","XC","L","XL","X","IX","V","IV","I");
        $arabic = array(1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1);
        $romanNumber = "";
        foreach ($arabic as $index => $arabicValue)
        {
            while ($value >= $arabicValue)
            {
                $romanNumber .= $roman[$index];
                $value -= $arabicValue;
            }
        }
        return $romanNumber;
    }
}
