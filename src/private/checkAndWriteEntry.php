<?php declare(strict_types=1);
include_once 'private/config.php';
include_once 'private/entry.php';
include_once 'private/database.php';
include_once 'private/getMinMaxYear.php';

function checkAndWriteEntry(Database $database, Entry $entry, bool $allowAddWhenNoOldEntry, bool $allowEmptyEmail, bool $checkYear): bool
{
    if (!checkInput($entry, $allowEmptyEmail, $checkYear))
    {
        return false;
    }

    return writeData($database, $entry, $allowAddWhenNoOldEntry);
}

function checkInput(Entry $entry, bool $allowEmptyEmail, bool $checkYearRestrictive): bool
{
    $minRegistrationYear = getMinYear($checkYearRestrictive);
    $maxRegistrationYear = getMaxYear($checkYearRestrictive);

    $firstname_ok = $entry->isFirstnameOk();
    $lastname_ok = $entry->isLastnameOk();

    $year_ok = $entry->getYear() >= $minRegistrationYear && $entry->getYear() <= $maxRegistrationYear;

    $gender_ok = $entry->isGenderOk();
    $run_ok = $entry->isRunOk();
    $email_ok = $entry->isEmailOk($allowEmptyEmail);
    $startnr_ok = $entry->isStartnrOk();
    $chipnr_ok = $entry->isChipnrOk();
    $time_ok = $entry->isManualTimeOk();

    if (!($firstname_ok && $lastname_ok && $year_ok && $gender_ok && $run_ok && $email_ok && $startnr_ok && $chipnr_ok && $time_ok))
    {
        echo "<p class='yellow'>Fehlerhafte Eingabe! Folgende Felder wurden nicht korrekt ausgefüllt:</p>";
        echo "<ul>";

        if (!$firstname_ok)
        {
            echo "<li>Vorname (darf nicht leer sein)</li>";
        }
        if (!$lastname_ok)
        {
            echo "<li>Nachname (darf nicht leer sein)</li>";
        }
        if (!$year_ok)
        {
            echo "<li>Jahrgang ($minRegistrationYear - $maxRegistrationYear)</li>";
        }
        if (!$gender_ok)
        {
            echo "<li>Geschlecht (muss ausgewählt sein)</li>";
        }
        if (!$run_ok)
        {
            echo "<li>Gewählter Wettbewerb für Altersklasse nicht möglich</li>";
        }
        if (!$email_ok)
        {
            echo "<li>E-Mail Adresse (z.B. max.mustermann@web.de)</li>";
        }
        if (!$startnr_ok)
        {
            $minStartnr = Config::Get()['startNumbers']['min'];
            $maxStartnr = Config::Get()['startNumbers']['max'];
            echo "<li>Startnummer ($minStartnr - $maxStartnr)</li>";
        }
        if (!$chipnr_ok)
        {
            $minChipnr = Config::Get()['chipNumbers']['min'];
            $maxChipnr = Config::Get()['chipNumbers']['max'];
            echo "<li>Chipnummer ($minChipnr - $maxChipnr)</li>";
        }
        if (!$time_ok)
        {
            echo "<li>Zeit (z.B. 01:23:45, 1:11:22, 23:45a, 01:22, 1:22, 15b, ---, oder leer)</li>";
        }
        echo "</ul>";
        return false;
    }

    return true;
}

function writeData(Database $database, Entry $entry, bool $allowAddWhenNoOldEntry): bool
{
    $isChange = $entry->getKey() != "";

    if ($entry->getStartnr() != Entry::INVALID_STARTNR)
    {
        if (!$database->isStartnrFree($entry))
        {
            echo "<p class='yellow'>Startnummer bereits vergeben!</p>";
            return false;
        }
    }

    if ($entry->getChipnr() != Entry::INVALID_CHIPNR)
    {
        if (!$database->isChipnrFree($entry))
        {
            echo "<p class='yellow'>Chipnummer bereits vergeben!</p>";
            return false;
        }
    }

    if ($isChange)
    {
        $oldEntry = $database->searchEntryByKey($entry->getKey());
        if (!$allowAddWhenNoOldEntry)
        {
            if (!$oldEntry->isValid())
            {
                echo "<p class='yellow'>Fehler: Keine Anmeldung mit Code \"" . $entry->getKey() . "\" vorhanden!</p>";
                return false;
            }
        }

        if ($entry->isEqualForAllEditableAttributes($oldEntry))
        {
            echo "<p class='yellow'>Es wurde kein Feld der Anmeldung geändert!</p>";
            return false;
        }
        $entry->setPreRegistration($oldEntry->getPreRegistration());
    }
    else
    {
        $key = $database->generateNewKey();
        $entry->setKey($key);
    }

    if ($database->hasEqualEntryWithOtherKey($entry))
    {
        echo "<p class='yellow'>Fehlerhafte Eingabe!</p>";
        echo "<p>Es existiert bereits eine Anmeldung mit identischen Daten.</p>";
        return false;
    }

    $writeOk = $database->writeEntry($entry);

    if (!$writeOk)
    {
        echo "<p class='yellow'>Fehler: Eintrag konnte nicht in Datenbank geschrieben werden !!!</p>";
        return false;
    }

    return true;
}
