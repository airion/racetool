<?php declare(strict_types=1);

include_once 'private/PDFCreator/TableRow.php';

class TextObject extends TableRow
{
    public function __construct(PDFCreator $creator, string $text)
    {
        parent::__construct($creator);
        $this->setRelativeColumnWidth(0, 1.0);
        $this->setColumnData(0, array($text));
        $this->setColumnBold(0, false);
        $this->setColumnAlign(0, 'L');
    }
}
