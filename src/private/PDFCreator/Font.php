<?php declare(strict_types=1);

class Font
{

    public function __construct(string $family, float $size)
    {
        $this->m_family = $family;
        $this->m_size = $size;
    }

    public function activate(TCPDF $pdf, bool $out)
    {
        $pdf->SetFont($this->m_family, '', $this->m_size, '', 'default', $out);
    }

    public function getFamily(): string
    {
        return $this->m_family;
    }

    public function getSize(): float
    {
        return $this->m_size;
    }

    private $m_family;
    private $m_size;
}
