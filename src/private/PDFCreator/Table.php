<?php declare(strict_types = 1);

include_once 'private/PDFCreator/TableRow.php';
include_once 'private/PDFCreator/TextObject.php';

class Table
{

    public function __construct(PDFCreator $creator, string $name, Font $fontName, Font $fontHead, Font $fontData)
    {
        $this->m_creator = $creator;

        $this->m_headRow = new TableRow($creator);
        $this->m_headRow->setFont($fontHead);
        $this->m_headRow->setFontBold($fontHead);

        $this->m_dataRow = new TableRow($creator);
        $this->m_dataRow->setFont($fontData);
        $this->m_dataRow->setFontBold($fontHead);

        $this->m_nameTextObject = new TextObject($creator, $name);
        $this->m_nameTextObject->setFont($fontName);
        $this->m_nameTextObject->setLineFeed(0.5);

        $this->m_nameTextObjectContinued = new TextObject($creator, $name . " (Fortsetzung)");
        $this->m_nameTextObjectContinued->setFont($fontName);
        $this->m_nameTextObjectContinued->setLineFeed(0.5);

        $creator->m_pdf->SetLineWidth(0.05);
    }

    public function addTableHead(string $text, float $relativeWidth, string $align, bool $bold = false)
    {
        $i = $this->m_headRow->getNumColumns();
        $this->m_headRow->setColumnData($i, array($text));
        $this->m_headRow->setRelativeColumnWidth($i, $relativeWidth);
        $this->m_headRow->setColumnAlign($i, 'C');
        $this->m_headRow->setColumnBold($i, false);

        $this->m_dataRow->setRelativeColumnWidth($i, $relativeWidth);
        $this->m_dataRow->setColumnAlign($i, $align);
        $this->m_dataRow->setColumnBold($i, $bold);
    }

    public function addTableData(string $data)
    {
        $this->addTableDataArray(array($data));
    }

    public function addTableDataArray(array $data)
    {
        $this->m_dataRow->setColumnData($this->m_col++, $data);

        if ($this->m_col == $this->m_headRow->getNumColumns())
        {
            $this->m_col = 0;

            $this->outputData();
            $this->m_row++;
        }
    }

    private function generateHead()
    {
        $this->m_columnLineStartY = $this->m_creator->m_pdf->GetY();
        $this->m_headRow->generate();
    }

    private function drawColumnLines()
    {
        if (isset($this->m_columnLineStartY))
        {
            $endY = $this->m_creator->m_pdf->GetY();
            $cols = $this->m_headRow->getNumColumns();

            $x = $this->m_creator->getLeftMargin() + $this->m_headRow->getColumnWidth(0);
            $pdf = $this->m_creator->m_pdf;
            for ($col = 1; $col < $cols; $col++)
            {
                $pdf->Line($x, $this->m_columnLineStartY, $x, $endY);
                $x += $this->m_headRow->getColumnWidth($col);
            }
        }
    }

    function outputData()
    {
        $height = $this->m_dataRow->getHeight();
        if ($this->m_row == 0)
        {
            $height += $this->m_nameTextObject->getHeight() + $this->m_headRow->getHeight();
        }

        if ($this->m_creator->fitsOnPage($height))
        {
            if ($this->m_row == 0)
            {
                $this->m_nameTextObject->generate();
                $this->generateHead();
            }
        }
        else
        {
            $this->drawColumnLines();
            $this->m_creator->doPageBreak();
            if ($this->m_row == 0)
            {
                $this->m_nameTextObject->generate();
            }
            else
            {
                $this->m_nameTextObjectContinued->generate();
            }
            $this->generateHead();
            $this->m_darkRowColor = true;
        }
        if ($this->m_darkRowColor)
        {
            $this->m_dataRow->setBackgroundColor(205);
        }
        else
        {
            $this->m_dataRow->setBackgroundColor(255);
        }

        $this->m_dataRow->generate();
        $this->m_darkRowColor = !$this->m_darkRowColor;
    }

    public function end()
    {
        if ($this->m_row == 0)
        {
            $height = $this->m_nameTextObject->getHeight() + $this->m_headRow->getHeight();

            if (!$this->m_creator->fitsOnPage($height))
            {
                $this->m_creator->doPageBreak();
            }
            $this->m_nameTextObject->generate();
            $this->generateHead();
        }

        $this->drawColumnLines();
    }

    private $m_creator;
    private $m_nameTextObject;
    private $m_nameTextObjectContinued;

    private $m_headRow;
    private $m_dataRow;
    private $m_col = 0;
    private $m_row = 0;
    private $m_darkRowColor = true;
    private $m_columnLineStartY;
}
