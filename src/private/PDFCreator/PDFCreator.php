<?php declare(strict_types=1);

include_once 'private/PDFCreator/mytcpdf.php';
include_once 'private/PDFCreator/Font.php';

class PDFCreator
{
    public function __construct($marginLeft = 15, $marginTop = 27, $marginRight = 15, $marginBottom = 20, $format='A4')
    {
        $this->m_pdf = new MYTCPDF($format);
        $this->m_pdf->SetMargins($marginLeft, $marginTop, $marginRight);

        $this->m_marginLeft = $marginLeft;
        $this->m_marginRight = $marginRight;
        $this->m_marginTop = $marginTop;
        $this->m_marginBottom = $marginBottom;

        $this->m_pageBreakTrigger = $this->m_pdf->getPageHeight() - $this->m_marginBottom;
        $this->m_width = $this->m_pdf->getPageWidth() - $this->m_marginLeft - $this->m_marginRight;
        $this->m_height = $this->m_pdf->getPageHeight() - $this->m_marginTop - $this->m_marginBottom;

        $this->m_pdf->SetCellPadding(0);

        $this->m_pdf->SetHeaderMargin(5);
        $this->m_pdf->SetFooterMargin(10);

        $this->m_pdf->setFontSubsetting(true);

        $this->m_pdf->SetAutoPageBreak(false);
        $this->m_pdf->SetDrawColor(0, 0, 0);
    }

    public function getWidth(): float
    {
        return $this->m_width;
    }

    public function getHeight(): float
    {
        return $this->m_height;
    }

    public function getLeftMargin(): float
    {
        return $this->m_marginLeft;
    }

    public function getRightMargin(): float
    {
        return $this->m_marginRight;
    }

    public function getTopMargin(): float
    {
        return $this->m_marginTop;
    }

    public function getBottomMargin(): float
    {
        return $this->m_marginBottom;
    }

    public function fitsOnPage(float $height): bool
    {
        return ($this->m_pdf->GetY() + $height) <=  $this->m_pageBreakTrigger;
    }

    public function doPageBreak()
    {
        $this->m_pdf->AddPage();
        $this->m_pdf->SetY($this->m_marginTop);
    }

    public function activateFont(Font $font, bool $out)
    {
        if (($font != $this->m_lastFont) || (!$this->m_lastFontOut && $out))
        {
            $font->activate($this->m_pdf, $out);

            $this->m_lastFont = $font;
            $this->m_lastFontOut = $out;
        }
    }

    public function getCellHeight()
    {
        return $this->m_pdf->getCellHeight($this->m_pdf->getFontSize());
    }

    public function getOrCreateFont(string $family, float $size): Font
    {
        foreach($this->m_fonts as $font)
        {
            if ($font->getFamily() == $family && $font->getSize() == $size)
            {
                return $font;
            }
        }
        $font = new Font($family, $size);
        $this->m_fonts[] = $font;
        return $font;
    }

    public function outputTextInBox(float $x, float $y, float $w, float $h, string $text, string $border = '', string $align = 'L', string $valign = 'T')
    {
        if ($border != '')
        {
            $borderArray = [$border => 1];
            $this->m_pdf->Rect($x, $y, $w, $h, 'S', $borderArray);
        }

        $lines = array();
        $textHeight = $this->getTextHeight($w, $text, $lines);

        if ($textHeight > $h)
        {
            $restoreFontSize = $this->m_pdf->getFontSizePt();
            $textHeight = $this->chooseFittingFontSize($w, $h, $text, $lines);
        }

        if ($valign == 'M')
        {
            $y += ($h - $textHeight) / 2;
        }
        else
        {
            if ($valign == 'B')
            {
                $y += ($h - $textHeight);
            }
        }

        $this->m_pdf->SetLeftMargin($x);
        $this->m_pdf->SetY($y);
        $lineHeight = $this->getCellHeight();

        $border = 0;
        $ln = 1;
        $fill = false;
        $stretch = 0;
        $ignoreMinHeight = true;
        $valign = "T";

        foreach ($lines as $lineText)
        {
            $this->Cell($w, $lineHeight, $lineText, $border, $ln, $align, $fill, '', $stretch, $ignoreMinHeight, "T", $valign);
        }

        if (isset($restoreFontSize))
        {
            $this->m_pdf->SetFontSize($restoreFontSize);
        }
    }

    public function outputTextInColumn(float $x, float $y, float $w, string $text, string $border = '', string $align = 'L'): float
    {
        $lines = array();
        $textHeight = $this->getTextHeight($w, $text, $lines);

        if ($border != '')
        {
            $borderArray = [$border => 1];
            $this->m_pdf->Rect($x, $y, $w, $textHeight, 'S', $borderArray);
        }

        $this->m_pdf->SetLeftMargin($x);
        $this->m_pdf->SetY($y);
        $lineHeight = $this->getCellHeight();

        $border = 0;
        $ln = 1;
        $fill = false;
        $stretch = 0;
        $ignoreMinHeight = true;
        $valign = "T";

        foreach ($lines as $lineText)
        {
            $this->Cell($w, $lineHeight, $lineText, $border, $ln, $align, $fill, '', $stretch, $ignoreMinHeight, "T", $valign);
        }
        return $y + $textHeight;
    }

    public function Cell($w, $h = 0, $txt = '', $border = 0, $ln = 0, $align = '', $fill = false, $link = '', $stretch = 0, $ignore_min_height = false, $calign = 'T', $valign = 'M')
    {
        $this->m_pdf->Cell($w, $h, $txt, $border, $ln, $align, $fill, $link, $stretch, $ignore_min_height, $calign, $valign);

        // Workaround for Cell() changing the linewidth and not restoring it:
        $this->m_pdf->SetLineWidth($this->m_pdf->GetLineWidth());
    }

    private function chooseFittingFontSize(float $w, float $h, string $text, array &$lines): float
    {
        $fmin = 1;
        $fmax = $this->m_pdf->getFontSizePt();
        $diff_epsilon = (1 / $this->m_pdf->getScaleFactor());
        $maxIterations = (2 * min(100, max(10, intval($fmax))));
        while ($maxIterations >= 0)
        {
            $fmid = (($fmax + $fmin) / 2);
            $this->m_pdf->SetFontSize($fmid, false);
            $textHeight = $this->getTextHeight($w, $text, $lines);
            $diff = ($h - $textHeight);
            if ($diff >= 0)
            {
                if ($diff <= $diff_epsilon)
                {
                    $this->m_pdf->SetFontSize($fmid);
                    return $textHeight;
                }
                $fmin = $fmid;
            }
            else
            {
                $fmax = $fmid;
            }
            --$maxIterations;
        }

        $this->m_pdf->SetFontSize($fmin);
        $textHeight = $this->getTextHeight($w, $text, $lines);
        return $textHeight;
    }

    private function getTextHeight(float $w, string $text, array &$lines): float
    {
        $lines = $this->m_pdf->wrapText($text, $w);
        $lineHeight = $this->getCellHeight();
        $textHeight = count($lines) * $lineHeight;
        return $textHeight;
    }

    public $m_pdf;
    private $m_width;
    private $m_height;
    private $m_marginLeft;
    private $m_marginRight;
    private $m_marginBottom;
    private $m_marginTop;
    private $m_pageBreakTrigger;
    private $m_lastFont = null;
    private $m_lastFontOut = false;
    private $m_fonts = array();
}
