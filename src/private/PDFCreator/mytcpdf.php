<?php declare(strict_types=1);
include_once 'private/TCPDF/tcpdf.php';

class MYTCPDF extends TCPDF
{

    function __construct($format)
    {
        parent::__construct('P', 'mm', $format);
    }

    public function wrapText(string $txt, float $w)
    {
        $textLines = array();
        if ($txt === NULL)
        {
            return $textLines;
        }
        if ($txt === '')
        {
            // empty string
            return $textLines;
        }

        $sum = 0;
        $chars = TCPDF_FONTS::UTF8StringToArray($txt, $this->isunicode, $this->CurrentFont);

        $uchars = TCPDF_FONTS::UTF8ArrayToUniArray($chars, $this->isunicode);

        $charsWidth = $this->GetArrStringWidth($chars, '', '', 0, true);
        $length = count($chars);
        $lastSeparator = -1;

        $start = 0;

        for ($i = 0; $i < $length; ++$i)
        {
            $c = $chars[$i];
            $charWidth = $charsWidth[$i];
            if (($c == 32) && ($i > $start))
            {
                // Space as separator
                $lastSeparator = $i;
            }
            if ((($sum + $charWidth) > $w) or ($c == 10))
            {
                if ($c == 10)
                {
                    $lastSeparator = -1;
                    $sum = 0;

                    $tmpstr = TCPDF_FONTS::UniArrSubString($uchars, $start, $i);
                    $start = $i + 1;
                }
                elseif ($lastSeparator != -1)
                {
                    $end = $lastSeparator;
                    if ($chars[$lastSeparator] != 32)
                    {
                        // Separator is no space, also show it.
                        $end++;
                    }
                    $tmpstr = TCPDF_FONTS::UniArrSubString($uchars, $start, $end);
                    $start = $lastSeparator + 1;

                    $i = $lastSeparator;
                    $lastSeparator = -1;
                    $sum = 0;
                }
                else
                {
                    $tmpstr = TCPDF_FONTS::UniArrSubString($uchars, $start, $i);
                    $start = $i;

                    $sum = $charWidth;
                }
                array_push($textLines, $tmpstr);
            }
            else
            {
                $sum += $charWidth;

                if (($c == 45) && ($i > $start))
                {
                    // Hypen-minus
                    $lastSeparator = $i;
                }
            }
        }
        $tmpstr = TCPDF_FONTS::UniArrSubString($uchars, $start, $length);
        array_push($textLines, $tmpstr);

        return $textLines;
    }
}
