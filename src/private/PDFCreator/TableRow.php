<?php declare(strict_types = 1);

include_once 'private/PDFCreator/LayoutableObject.php';

class TableRow extends LayoutableObject
{

    public function __construct($creator)
    {
        parent::__construct($creator);
    }

    public function setColumnData(int $i, array $data)
    {
        $this->m_columnData[$i] = $data;
        $this->invalidateHeight();
    }

    public function setRelativeColumnWidth(int $i, float $width)
    {
        $this->m_relativeColumnWidth[$i] = $width;
        $this->invalidateHeight();
        $this->invalidateWidths();
    }

    public function setColumnAlign(int $i, string $align)
    {
        $this->m_columnAlign[$i] = $align;
    }

    public function setColumnBold(int $i, bool $bold)
    {
        $this->m_columnBold[$i] = $bold;
    }

    public function setLineFeed(float $lf)
    {
        $this->m_lineFeed = $lf;
        $this->invalidateHeight();
    }

    public function setBackgroundColor(int $backgroundColor)
    {
        $this->m_backgroundColor = $backgroundColor;
    }

    public function getNumColumns(): int
    {
        return count($this->m_relativeColumnWidth);
    }

    function getColumnWidth($col): float
    {
        return $this->m_columnWidths[$col];
    }

    function activateFont(bool $bold, bool $out)
    {
        $font = null;

        if ($bold)
        {
            $font = $this->m_fontBold;
        }
        else
        {
            $font = $this->m_font;
        }

        $this->m_creator->activateFont($font, $out);
    }

    public function generate()
    {
        $height = $this->getHeight();

        $x = $this->m_creator->getLeftMargin();
        $y = $this->pdf()->GetY();

        if ($this->m_backgroundColor != 255)
        {
            $this->pdf()->Rect($x, $y, $this->m_creator->getWidth(), $height, 'F', array(), array($this->m_backgroundColor));
        }

        $numColumns = $this->getNumColumns();

        for ($col = 0; $col < $numColumns; $col++)
        {
            $bold = (bool) $this->m_columnBold[$col];
            $this->activateFont($bold, true);
            $this->pdf()->SetLeftMargin($x + $this->m_padding);
            $this->pdf()->SetY($y + $this->m_padding);

            $textLines = $this->m_columnLines[$col];
            $w = $this->m_columnWidths[$col];
            foreach ($textLines as $line)
            {
                $this->m_creator->Cell($w - 2.0 * $this->m_padding, 0, $line, 0, 1, $this->m_columnAlign[$col]);
            }

            $x += $w;
        }
        $y += $height;
        $this->pdf()->SetY($y);
    }

    public function computeHeight(): float
    {
        if (!isset($this->m_columnWidths))
        {
            $this->computeColumnWidths();
        }

        $numColumns = $this->getNumColumns();

        $maxColumnFields = 0;
        for ($col = 0; $col < $numColumns; $col++)
        {
            $columnFields = count($this->m_columnData[$col]);
            if ($columnFields > $maxColumnFields)
            {
                $maxColumnFields = $columnFields;
            }
            $this->m_columnLines[$col] = array();
        }
        $maxNumLines = 0;
        for ($field = 0; $field < $maxColumnFields; $field++)
        {
            for ($col = 0; $col < $numColumns; $col++)
            {
                $bold = (bool) $this->m_columnBold[$col];
                $this->activateFont($bold, false);
                if ($field < count($this->m_columnData[$col]))
                {
                    $str = $this->m_columnData[$col][$field];
                }
                else
                {
                    $str = "";
                }
                $w = $this->m_columnWidths[$col] - 2 * $this->m_padding;
                $textLines = $this->pdf()->wrapText($str, $w);
                $this->m_columnLines[$col] = array_merge($this->m_columnLines[$col], $textLines);

                $numLines = count($this->m_columnLines[$col]);
                if ($numLines > $maxNumLines)
                {
                    $maxNumLines = $numLines;
                }
            }
            for ($col = 0; $col < $numColumns; $col++)
            {
                $numberEmptyLinesToAdd = $maxNumLines - count($this->m_columnLines[$col]);
                for ($i = 0; $i < $numberEmptyLinesToAdd; $i++)
                {
                    array_push($this->m_columnLines[$col], "");
                }
            }
        }
        $lineHeight = $this->pdf()->getCellHeight($this->pdf()->getFontSize());
        return ($maxNumLines + $this->m_lineFeed) * $lineHeight + 2.0 * $this->m_padding;
    }

    public function setFont(Font $font)
    {
        $this->m_font = $font;
    }

    public function setFontBold(Font $font)
    {
        $this->m_fontBold = $font;
    }

    private function invalidateWidths()
    {
        unset($this->m_columnWidths);
    }

    private function computeColumnWidths()
    {
        $sum = 0;
        foreach ($this->m_relativeColumnWidth as $w)
        {
            $sum += $w;
        }

        $f = $this->m_creator->getWidth() / $sum;

        $cols = $this->getNumColumns();

        for ($i = 0; $i < $cols; $i++)
        {
            $this->m_columnWidths[$i] = $this->m_relativeColumnWidth[$i] * $f;
        }
    }

    private $m_backgroundColor = 255;
    private $m_relativeColumnWidth = array();
    private $m_columnWidths;
    private $m_columnAlign;
    private $m_columnBold;
    private $m_columnData = array();
    private $m_columnLines = array();
    private $m_padding = 0.5;
    private $m_lineFeed = 0.0;
    private $m_font;
    private $m_fontBold;
}
