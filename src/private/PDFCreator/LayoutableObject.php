<?php declare(strict_types=1);

include_once 'private/PDFCreator/PDFCreator.php';
include_once 'private/PDFCreator/Font.php';

abstract class LayoutableObject
{
    public function __construct(PDFCreator $creator)
    {
        $this->m_creator = $creator;
    }
    
    abstract public function generate();
    abstract public function computeHeight();
    
    public function getHeight(): float
    {
        if (!isset($this->m_height))
        {
            $this->m_height = $this->computeHeight();
        }
        return $this->m_height;
    }
    
    public function invalidateHeight()
    {
        unset($this->m_height);
    }
    
    public function pdf(): MYTCPDF
    {
        return $this->m_creator->m_pdf;
    }
    
    protected $m_creator;
    private $m_height;
}
