<?php declare(strict_types = 1);

class ErrorHandler
{
    function __construct()
    {
        set_error_handler("ErrorHandler::HandleError");
        set_exception_handler("ErrorHandler::HandleException");
        ini_set("display_errors", "off");
        error_reporting(E_ALL);
    }

    static function HandleError(int $errno, string $errstr, string $errfile, int $errline): bool
    {
        if (error_reporting() & $errno)
        {
            self::HandleException(new ErrorException($errstr, 0, $errno, $errfile, $errline));
        }
        return true;
    }

    static function HandleException(Throwable $e): void
    {
        $dir = dirname(__DIR__) . "/";

        $trace = $e->getTraceAsString();
        $trace = nl2br(str_replace($dir, "", $trace));
        $file = str_replace($dir, "", $e->getFile());

        ob_end_clean();

        $class = get_class($e);
        $line = $e->getLine();
        $message = $e->getMessage();

        echo <<<EOT
        <!doctype html>
        <html>
            <head>
                <style>
                    h2
                    {
                        color: rgb(190, 50, 50);
                    }
                    table
                    {
                        text-align: left;
                    }
                    tr:nth-child(odd)
                    {
                        background-color:rgb(220,220,220);
                    }
                    tr:nth-child(even)
                    {
                        background-color:rgb(240,240,240);
                    }
                    th
                    {
                        padding-right: 5em;
                    }
                </style>
                <title>Exception Occured</title>
            </head>
            <body>
                <h2>Exception Occured</h2>
                <table>
                    <tr><th>Type</th><td>$class</td></tr>
                    <tr><th>Message</th><td>$message</td></tr>
                    <tr><th>File</th><td>$file</td></tr>
                    <tr><th>Line</th><td>$line</td></tr>
                    <tr><th>Backtrace</th><td>$trace</td></tr>
                </table>
            </body>
        </html>
EOT;

        exit();
    }
};

new ErrorHandler();
