<?php declare(strict_types=1);

include_once 'private/getPriceAsString.php';

function outputCompetitionTable()
{
    echo "<table class='infoTable alignTableHeadLeft' style='margin: 0 4em 1em 0;'>";
    echo "<tr>";
    echo "<th>Wettbewerb</th>";
    echo "<th>Jahrgang</th>";
    echo "<th class='noPaddingRight'>Startzeit</th>";
    echo "</tr>";

    $runs = Runs::GetInstance()->getRuns();
    foreach ($runs as $run)
    {
        $runName = $run->getName();
        $runStartTime = $run->getStartTime();
        echo "<tr>";
        echo "<td style='white-space: nowrap;'>$runName</td>";
        $ageGroups = $run->getAgeGroups();

        $minYear = $ageGroups->getMinYear();
        $maxYear = $ageGroups->getMaxYear();

        echo "<td>$minYear - $maxYear</td>";
        echo "<td class='noPaddingRight'>$runStartTime</td>";
        echo "</tr>";
    }

    echo "</table>";
}

function outputPriceTable(bool $preRegistration)
{
    $eventYear = Config::Get()['event']['year'];
    echo "<table class='infoTable alignTableHeadLeft' style='margin: 0 2em 1em 0;'>";
    echo "<tr>";
    echo "<th>Jahrgang</th>";
    echo "<th class='noPaddingRight'>Startgebühr</th>";
    echo "</tr>";

    if ($preRegistration)
    {
        $prices = Config::Get()['prices']['preRegistration'];
    }
    else
    {
        $prices = Config::Get()['prices']['registration'];
    }

    foreach ($prices as $price)
    {
        $priceMinYear = $eventYear - $price['maxAge'];
        $priceMaxYear = $eventYear - $price['minAge'];
        echo "<tr>";
        echo "<td>$priceMinYear - $priceMaxYear</td>";
        $priceString = getPriceAsString(floatval($price['price']));
        echo "<td class='noPaddingRight'>$priceString</td>";
        echo "</tr>";
    }

    if (Config::Get()['features']['cupFlag'] && !$preRegistration)
    {
        $prices = Config::Get()['prices']['registrationCup'];

        foreach ($prices as $price)
        {
            if ($price['price'] != 0)
            {
                $priceMinYear = $eventYear - $price['maxAge'];
                $priceMaxYear = $eventYear - $price['minAge'];
                echo "<tr>";
                echo "<td>$priceMinYear - $priceMaxYear</td>";
                $priceString = getPriceAsString(floatval($price['price'])) . " (Cup Serie)";
                echo "<td class='noPaddingRight'>$priceString</td>";
                echo "</tr>";
            }
        }
    }

    echo "</table>";
}
