<?php declare(strict_types=1);

function microtime_float(): float
{
    list ($usec, $sec) = explode(' ', microtime());
    return ((float) $usec + (float) $sec);
}

function lockFile(string $filename, float $timeLimit = 10, int $staleAge = 30): bool
{
    ignore_user_abort(true);
    set_time_limit(0);

    $lockDir = "$filename.lock";
    if (is_dir($lockDir))
    {
        if ((time() - filemtime($lockDir)) > $staleAge)
        {
            rmdir($lockDir);
            echo "<p class='yellow'>lockFile - removed stale lock !!!</p>";
        }
    }

    $timeStart = microtime_float();

    do
    {
        if (@mkdir($lockDir, 0777))
        {
            return true;
        }
        $timedelta = microtime_float() - $timeStart;
        usleep(10000);
    }
    while ($timedelta < $timeLimit);

    return false;
}

function unlockFile(string $filename)
{
    $lockDir = "$filename.lock";
    if (is_dir($lockDir))
    {
        rmdir($lockDir);
    }
    else
    {
        echo "<p class='yellow'>unlockFile - no lockfile !!!</p>";
        assert(false);
    }
}
