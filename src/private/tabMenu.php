<?php declare(strict_types = 1);

class TabMenu
{
    function __construct($selectedTab)
    {
        $this->m_selectedTab = $selectedTab;
    }

    function begin()
    {
        echo "<div class='tabMenu'>";
        echo "<nav>";
        echo "<ul>";
    }

    function addEntry(string $name, string $post, string $id)
    {
        if ($this->m_selectedTab == $this->m_currentTab)
        {
            echo "<li class='selected'><span id='$id'>$name</span></li>";
        }
        else
        {
            $zIndex = $this->m_currentTab;
            echo "<li style='z-index:$zIndex'><span id='$id' onclick=\"$post\">$name</span></li>";
        }

        $this->m_currentTab++;
    }

    function end()
    {
        echo "</ul>";
        echo "</nav>";

        echo "<div class='horizontalLine'></div>";
        echo "</div>";
    }


    static function GetStyle(): string
    {
        $style = <<<EOD
        .tabMenu nav ul
        {
        	list-style: none;
        	padding: 0 0 0 1em;
        }

    	.tabMenu nav li
        {
    		float: left;
    		position: relative;
    		border-radius: 0.75em 2em 0 0;
    		margin-left: -0.5em;
    		box-shadow: 0 -0.2em 0.2em rgba(0,0,0,0.5);
    		transition: .2s;
            background: #93070B;
    	}

        .tabMenu nav .selected, .tabMenu nav .selected:hover
        {
            background: #D20B12;
            z-index: 100;
        }

    	.tabMenu nav span
        {
            display:inline-block;
            padding: 0.25em 1.25em 0.25em 1.0em;
    		color: #ddd;
    	}

    	.tabMenu nav li:hover
        {
    		z-index: 99 !important;
    		background: #444;
    	}

        .tabMenu nav li:hover span
        {
            color: White;
            cursor: pointer;
        }

    	.tabMenu nav .selected span, .tabMenu nav .selected:hover span
        {
            color: White;
            cursor: default;
    	}

    	.tabMenu .horizontalLine
        {
    		position: relative;
    	    clear: both;
    		padding: 0.3em;
            border-radius: 0.75em;
    		box-shadow: 0 -0.25em 0.2em rgba(0,0,0,0.5);
            margin: 0px -0.5em 0px -0.5em;
    	}
EOD;

        return $style;
    }

    private $m_currentTab = 0;
    private $m_selectedTab = 0;
}
