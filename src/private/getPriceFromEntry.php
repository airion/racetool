<?php declare(strict_types=1);

include_once 'private/config.php';
include_once 'private/entry.php';

function getPriceFromEntry(Entry $entry): float
{
    if ($entry->getCup())
    {
        if ($entry->getPreRegistration())
        {
            $prices = Config::Get()['prices']['preRegistrationCup'];
        }
        else
        {
            $prices = Config::Get()['prices']['registrationCup'];
        }
    }
    else
    {
        if ($entry->getPreRegistration())
        {
            $prices = Config::Get()['prices']['preRegistration'];
        }
        else
        {
            $prices = Config::Get()['prices']['registration'];
        }
    }
    $age = $entry->getAgeGroup()->getMinAge();

    foreach ($prices as $price)
    {
        if ($age >= $price['minAge'] && $age <= $price['maxAge'])
        {
            return floatval($price['price']);
        }
    }
    return 0.0;
}
