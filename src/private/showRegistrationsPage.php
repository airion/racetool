<?php declare(strict_types = 1);

include_once 'private/page.php';
include_once 'private/jsFunctions.php';
include_once 'private/tabMenu.php';

class ShowRegistrationsPage extends Page
{
    function __construct(string $style, Parameters $parameters, int $selectedTab)
    {
        parent::__construct();
        $this->enableLoginButton();

        $javaScript = "";
        $javaScript .= getJSFunction_post();
        $style .= TabMenu::GetStyle();

        $scrollable = false;
        $this->outputHeader($javaScript, $style, $scrollable);

        echo "<p class='big'>Teilnehmerstand</p>";

        $this->outputTabs($parameters, $selectedTab);
        $this->outputContent();
        $this->outputFooter($parameters);
    }

    function outputFooter(Parameters $parameters)
    {
        $this->beginFooter();
        $this->outputBackButton("index.php", $parameters);
        $this->endFooter();
    }

    function outputTabs(Parameters $parameters, int $selectedTab)
    {
        $tabMenu = new TabMenu($selectedTab);
        $tabMenu->begin();
        $post = $this->getPostString("showlist.php", $parameters);
        $tabMenu->addEntry("Teilnehmer", $post, "listTab");
        $post = $this->getPostString("showCompetitionsPage.php", $parameters);
        $tabMenu->addEntry("Wettbewerbe", $post, "competitionTab");
        $post = $this->getPostString("showTeamsPage.php", $parameters);
        $tabMenu->addEntry("Vereine", $post, "teamTab");
        $tabMenu->end();
    }
}
