<?php declare(strict_types=1);

include_once 'private/entry.php';
include_once 'private/time.php';
include_once 'private/romanNumber.php';

class Team
{
    function __construct(Entry $entry0, Entry $entry1, Entry $entry2, int $teamNumber)
    {
        $this->m_entries = [$entry0, $entry1, $entry2];
        $this->m_teamNumber = $teamNumber;
        $timeSeconds = 0;
        foreach($this->m_entries as $entry)
        {
            $timeSeconds += $entry->getTime()->getSeconds();
        }
        $this->m_time = new Time($timeSeconds * 100);
    }

    function getTime()
    {
        return $this->m_time;
    }

    function setPlacing(int $placing)
    {
        $this->m_placing = $placing;

        foreach($this->m_entries as $entry)
        {
            $entry->setPlacingTeam($placing);
        }
    }

    function getEntry(int $i): Entry
    {
        return $this->m_entries[$i];
    }

    function getPlacing(): int
    {
        return $this->m_placing;
    }

    function getTeamNumber(): int
    {
        return $this->m_teamNumber;
    }

    function getTeamName(): string
    {
        $teamName = $this->m_entries[0]->getTeam();
        if ($this->m_teamNumber > 0)
        {
            $teamName .= " " . RomanNumber::GetAsRomanNumber($this->m_teamNumber);
        }
        return $teamName;
    }

    function getTeamNameWithoutNumber(): string
    {
        return $this->m_entries[0]->getTeam();
    }

    private $m_entries = array();
    private $m_teamNumber = 0;
    private $m_time = null;
    private $m_placing = Entry::INVALID_PLACING;
}
