<?php declare(strict_types=1);

class EntryAttribute
{
    const Index = 0;
    const Lastname = 1;
    const Firstname = 2;
    const Year = 3;
    const Gender = 4;
    const Team = 5;
    const Run = 6;
    const Email = 7;
    const Startnr = 8;
    const Chipnr = 9;
    const Code = 10;
    const Revision = 11;
    const Date = 12;
    const Username = 13;
    const Cup = 14;
    const AgeGroup = 15;
    const PreRegistration = 16;
    const Time = 17;
    const Placing = 18;
    const PlacingMW = 19;
    const PlacingAgeGroup = 20;
    const PlacingTeam = 21;

    const Last = 22;
}
// Caution: Has to match with table in entryTable.js !
