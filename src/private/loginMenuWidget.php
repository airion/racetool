<?php declare(strict_types = 1);

include_once 'private/page.php';
include_once 'private/menu.php';

class LoginMenuWidget implements IMenuWidget
{
    function __construct(Page $page, Parameters $parameters)
    {
        $this->m_page = $page;
        $this->m_parameters = $parameters;
    }

    function output()
    {
        echo "<div class='loginDialog'>";
        $this->m_parameters->add("userLogin", "1");
        $currentPage = basename($_SERVER["SCRIPT_FILENAME"]);
        $this->m_page->beginForm($currentPage, $this->m_parameters);

        echo "<table>";
        echo "<tr>";
        echo "<th>Benutzer</th>";
        echo "<td> <input name='username' id='username' type='text' value=''> </td>";
        echo "</tr>";

        echo "<tr>";
        echo "<th>Passwort</th>";
        echo "<td> <input name='password' id='password' type='password' required> </td>";
        echo "</tr>";

        echo "<tr>";
        echo "<td colspan='2'>";
        echo "<button id='loginSubmit' class='button noMarginRight noMarginBottom' type='submit' style='float:right;'>Ok <span class='fas fa-check'></span></button>";
        echo "</td>";
        echo "</tr>";

        echo "</table>";

        $this->m_page->endForm();

        echo "</div>";
    }

    static function GetStyle(): string
    {
        $style = <<<EOD
        .loginDialog
        {
          display: none;
          position: fixed;
          background-color: #702020;
          box-shadow: 0px 0px 6px 2px rgba(0,0,0,0.9);
          z-index: 100;
          padding: 1.0em;
        }

        .loginDialog table tr th
        {
            text-align: left;
            padding-right: 1em;
            padding-bottom: 0.5em;
            padding-left: 0;
            font-size: 0.9em;
            font-weight: normal;
        }

        .loginDialog table tr td
        {
            padding-bottom: 0.5em;
        }

        .loginDialog table
        {
            border-spacing: 0em;
        }
EOD;

        return $style;
    }

    private $m_page = null;
    private $m_parameters = null;
}
