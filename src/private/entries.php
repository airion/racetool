<?php declare(strict_types=1);

include_once 'private/entry.php';

class Entries
{
    static function SplitEntriesByGender(array $entries): array
    {
        $groups = array();
        foreach ($entries as $entry)
        {
            if ($entry->getGender() == "m")
            {
                $groups[0][] = $entry;
            }
            else
            {
                $groups[1][] = $entry;
            }
        }
        ksort($groups);
        return $groups;
    }

    static function SplitEntriesByRun(array $entries): array
    {
        $groups = array();
        foreach ($entries as $entry)
        {
            $run = $entry->getRun();
            $groups[$run][] = $entry;
        }
        ksort($groups);
        return $groups;
    }

    static function SplitEntriesByRunAndGender(array $entries): array
    {
        $runGroups = self::SplitEntriesByRun($entries);
        $groups = array();
        foreach ($runGroups as $runGroup)
        {
            $runGenderGroups = self::SplitEntriesByGender($runGroup);
            $groups = array_merge($groups, $runGenderGroups);
        }
        return $groups;
    }

    static function SplitEntriesByAgeGroups(array $entries): array
    {
        $groups = array();
        foreach ($entries as $entry)
        {
            $ageGroup = $entry->getAgeGroup();
            if ($ageGroup != null)
            {
                $ageGroupIndex = $ageGroup->getIndex();
                $groups[$ageGroupIndex][] = $entry;
            }
        }
        ksort($groups);
        return $groups;
    }

    static function SplitEntriesByTeam(array $entries): array
    {
        $groups = array();
        foreach ($entries as $entry)
        {
            $team = $entry->getTeam();
            $groups[$team][] = $entry;
        }
        ksort($groups);
        return $groups;
    }

    static function SplitEntriesByAgeGroupAndGender(array $entries): array
    {
        $ageGroupGroups = self::SplitEntriesByAgeGroups($entries);
        $groups = array();
        foreach ($ageGroupGroups as $ageGroupGroup)
        {
            $ageGroupGenderGroups = self::SplitEntriesByGender($ageGroupGroup);
            $groups = array_merge($groups, $ageGroupGenderGroups);
        }
        return $groups;
    }

    static function RemoveEntriesWithInvalidTime(array &$entries)
    {
        $entries = array_filter($entries, "Entries::HasValidTime");
    }

    static function ConvertPlacingForSorting(int $placing): int
    {
        if ($placing == Entry::INVALID_PLACING)
        {
            return PHP_INT_MAX;
        }
        else
        {
            return $placing;
        }
    }

    private static function HasValidTime(Entry $entry): bool
    {
        return $entry->getTime()->isValid();
    }
}
