<?php declare(strict_types = 1);

include_once 'private/page.php';
include_once 'private/registrationFields.php';
include_once 'private/entry.php';
include_once 'private/entrySorter.php';
include_once 'private/jsFunctions.php';
include_once 'private/database.php';
include_once 'private/entries.php';
include_once 'private/checkAndWriteEntry.php';
include_once 'private/convertStringToHTML.php';
include_once 'private/printLabel.php';
include_once 'private/outputCompetitionTable.php';

class RegistrationPage extends Page
{
    function __construct()
    {
        parent::__construct();
        $this->enableLoginButton();
        $this->outputContent();
    }

    function outputContent()
    {
        $javaScript = "";
        $javaScript .= getJSFunction_post();
        $javaScript .= getJSFunction_getElementValue();

        $style = RegistrationPage::GetStyle();
        $this->outputHeader($javaScript, $style);

        $database = new Database();
        if (isset($_POST["newRegistration"]))
        {
            if ($this->m_access->hasAccess(AccessRight::AddRegistration))
            {
                $entry = new Entry();
                $entry->setFromPost();
                $entry->setUsername($this->m_access->getUsername());

                $startNr = $database->generateNewStartNumber($entry);
                $entry->setStartnr($startNr);

                $chipNr = $database->generateNewChipNumber($entry);
                $entry->setChipnr($chipNr);

                $allowAddWhenNoOldEntry = false;
                $allowEmptyEmail = true;
                $checkYearRestrictive = true;
                $writeOk = checkAndWriteEntry($database, $entry, $allowAddWhenNoOldEntry, $allowEmptyEmail, $checkYearRestrictive);
                if (!$database->close())
                {
                    $writeOk = false;
                }

                if (!$writeOk)
                {
                    $this->beginFooter();
                    $parameters = new Parameters();
                    Entry::PassThroughParameters($parameters);
                    $this->outputBackButton("index.php", $parameters);
                    $this->endFooter();
                    return;
                }
            }
        }

        $database->close();

        $this->registrationForm($database);

        if (isset($entry))
        {
            echo "<p style='font-size: 2.5em; border: 1px solid White; margin-top: 0.5em; margin-bottom: 0.5em;'>";

            $run = Runs::GetInstance()->getRun($entry->getRun());

            $string = $entry->getFirstname() . " " . $entry->getLastname() .
                " • " . $entry->getTeamAsString() .
                " • " . $entry->getYearAsString() .
                " • " . $entry->getAgeGroupAsString() .
                " • " . $entry->getRunAsStringShort() .
                " • " . $run->getStartTime();

            echo convertStringToHTML($string);

            $string = "#" . $entry->getStartnrAsString() .
                " • C" . $entry->getChipnrAsString() .
                " • " . getPriceAsString(getPriceFromEntry($entry));
            echo "</br><span style='font-weight: bold;' class='yellow'>";
            echo convertStringToHTML($string);
            echo "</span>";

            echo "</p>";

            printLabel($entry);
        }

        $this->beginFooter();
        Menu::OutputButton("fa-users", "Teilnehmerstand", $this->getPostString("showlist.php"), "showList");

        if ($this->m_access->hasAccess(AccessRight::Read))
        {
            Menu::OutputSpacer();
            Menu::OutputButton("fa-door-open", "Administrativer Bereich", $this->getPostString("admin.php"), "admin");
        }

        $this->endFooter();
    }

    static function GetStyle(): string
    {
        $style = <<<EOD
        .registrationTable
        {
            border-spacing:0em;
            /*border: 1px Black solid;*/
            width: 100%;
            padding: 0em;
        }
        .registrationTable td
        {
            vertical-align:top;
            padding: 0em;
            /*border: 1px Black solid;*/
        }
        .registrationTable input
        {
            font-size: 1.5em;
        }
        .registrationTable select
        {
            font-size: 1.5em;
        }
        .registrationTable input[type="radio"]
        {
            transform: scale(1.5);
        }
        .registrationTable input[type="checkbox"]
        {
            transform: scale(1.5);
        }
        .registrationTable label
        {
            font-size: 1.25em;
        }

        .registrationTable th
        {
            font-size: 1.25em;
        }

        .registrationTable tr
        {
            padding: 0em;
        }

        .listTable
        {
            font-size: 1.0em; border-collapse: collapse;
        }
        .listTable th
        {
            color: White; text-align: center; padding: 0.5em;
            padding-left: 0.0em;
            font-size: 1.0em;
        }
        .listTable td:not(:last-child)
        {
            border-right: 1px solid #D20B12;
        }
        .listTable td
        {
            padding: 0.5em;
            color: White;
        }
        .oddRow
        {
            background: #c00000;
        }

        .evenRow
        {
            background: #a00000;
        }

        .infoTable
        {
            border-spacing:0em;
        }

        .infoTable tr td
        {
            padding-left: 0em;
            font-size: 0.9em;
        }

        .infoTable tr th
        {
            padding-left: 0em;
            font-size: 1.1em;
        }

        .infoTable tr td,
        .tableWithPadding tr th
        {
            padding-right: 2em;
        }
EOD;
        $style .= getRegistrationFormStyle();
        return $style;
    }

    function registrationForm(Database $database)
    {
        $entry = new Entry();

        echo "<div class='displayFlex flexWrap' style='align-items: flex-start;'>";

        outputCompetitionTable();
        outputPriceTable(false);

        $anouncementLink = Config::Get()['event']['anouncementLink'];
        $announcementName = Config::Get()['event']['announcementName'];
        echo "<a href='$anouncementLink'>$announcementName</a>";
        echo "</div>";
        echo "<br>";

        echo '<table class="registrationTable" >';
        echo '<tr>';

        echo '<th style="width: 40%;color: yellow; padding-bottom: 1em; padding-right: 0.5em; text-align:left;">';
        echo 'Neuer Teilnehmer';
        echo '</th>';
        echo '<th style="width: 60%;color: yellow; padding-bottom: 1em; text-align:left;">';
        echo 'Letzte Meldungen';
        echo '</th>';
        echo '</tr>';

        echo '<tr>';
        echo '<td style="padding-right: 0.5em;">';

        $registrationFieldsEnabled = $this->m_access->hasAccess(AccessRight::AddRegistration);

        $parameters = new Parameters();
        $parameters->add("newRegistration", 1);

        $showCup = Config::Get()['features']['cupFlag'];
        $showEmail = false;
        $showStartnr = false;
        $showChipnr = false;
        $showAgeGroups = false;
        $showTime = false;
        $checkYearRestrictive = true;
        outputRegistrationFields($this, $entry, $parameters, "index.php", $showCup, $showEmail, $showStartnr, $showChipnr, $showAgeGroups, $showTime,
            $registrationFieldsEnabled, $checkYearRestrictive, $database);
        echo '</td>';
        echo '<td>';
        $this->outputLastRegistrations($database);
        echo '</td>';
        echo '</tr>';
        echo '</table>';
    }

    function outputTableHeader(int $attribute, string $size)
    {
        echo "<th style='width: $size'>";
        echo Entry::GetAttributeShortName($attribute);
        echo "</th>";
    }

    function outputLastRegistrations(Database $database)
    {
        echo "<table class='listTable'>";

        echo "<thead>";
        echo "<tr>";
        $this->outputTableHeader(EntryAttribute::Firstname, "20%");
        $this->outputTableHeader(EntryAttribute::Lastname, "20%");
        $this->outputTableHeader(EntryAttribute::Team, "20%");
        $this->outputTableHeader(EntryAttribute::Year, "5%");
        $this->outputTableHeader(EntryAttribute::AgeGroup, "8%");
        $this->outputTableHeader(EntryAttribute::Run, "15%");
        $this->outputTableHeader(EntryAttribute::Startnr, "5%");
        $this->outputTableHeader(EntryAttribute::Chipnr, "5%");

        if (Config::Get()['features']['cupFlag'])
        {
            $this->outputTableHeader(EntryAttribute::Cup, "5%");
        }
        echo "</tr>";
        echo "</thead>";

        echo "<tbody>";

        $entries = $database->getEntries();
        EntrySorter::Sort($entries, EntrySorter::OrderIndex);

        $maxNumberOfRegistrationsToShow = 6;

        $i = 0;
        $n = min($maxNumberOfRegistrationsToShow, count($entries));

        for ($i = 0; $i < $n; $i++)
        {
            $entry = $entries[count($entries) - 1 - $i];

            $id = ($i & 1) ? 'oddRow' : 'evenRow';
            echo "<tr class='$id'>";
            echo "<td>" . convertStringToHTML($entry->getFirstname()) . "</td>";
            echo "<td>" . convertStringToHTML($entry->getLastname()) . "</td>";
            echo "<td>" . convertStringToHTML($entry->getTeamAsString()) . "</td>";
            echo "<td>" . $entry->getYearAsString() . "</td>";
            echo "<td>" . convertStringToHTML($entry->getAgeGroupAsString()) . "</td>";
            echo "<td>" . convertStringToHTML($entry->getRunAsStringShort()) . "</td>";
            echo "<td>" . $entry->getStartnrAsString() . "</td>";
            echo "<td>" . $entry->getChipnrAsString() . "</td>";

            if (Config::Get()['features']['cupFlag'])
            {
                echo "<td>" . convertStringToHTML($entry->getCupAsString()) . "</td>";
            }
            echo "</tr>";
        }
        echo "</tbody>";
        echo "</table>";
    }
}
