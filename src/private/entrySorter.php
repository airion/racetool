<?php declare(strict_types = 1);

include_once 'private/entry.php';

class EntrySorter
{
    const OrderIndex = 0;
    const OrderLastnameIndex = 1;
    const OrderTeamLastnameIndex = 2;
    const OrderStartnrIndex = 3;
    const OrderTimeLastnameIndex = 4;
    const OrderCupTeamLastnameIndex = 5;

    static function Sort(array &$entries, int $orderBy)
    {
        $sortFunctions = array(
            self::OrderIndex => "EntrySorter::CmpIndex",
            self::OrderLastnameIndex => "EntrySorter::CmpLastnameIndex",
            self::OrderTeamLastnameIndex => "EntrySorter::CmpTeamLastnameIndex",
            self::OrderStartnrIndex => "EntrySorter::CmpStartnrIndex",
            self::OrderTimeLastnameIndex => "EntrySorter::CmpTimeLastnameIndex",
            self::OrderCupTeamLastnameIndex => "EntrySorter::CmpCupTeamLastnameIndex"
        );
        setlocale(LC_COLLATE, 'de_DE.utf8');
        usort($entries, $sortFunctions[$orderBy]);
    }

    private static function CmpIndex(Entry $a, Entry $b): int
    {
        return $a->getIndex() - $b->getIndex();
    }

    private static function CmpLastnameIndex(Entry $a, Entry $b): int
    {
        $res = strcoll($a->getLastname(), $b->getLastname());
        if ($res != 0)
        {
            return $res;
        }
        return self::CmpIndex($a, $b);
    }

    private static function CmpTeamLastnameIndex(Entry $a, Entry $b): int
    {
        $res = strcoll($a->getTeam(), $b->getTeam());
        if ($res != 0)
        {
            if ($a->getTeam() == "")
            {
                return 1;
            }
            else if ($b->getTeam() == "")
            {
                return -1;
            }
            else
            {
                return $res;
            }
        }
        return self::CmpLastnameIndex($a, $b);
    }

    private static function CmpStartnrIndex(Entry $a, Entry $b): int
    {
        $res = $a->getStartnr() - $b->getStartnr();
        if ($res != 0)
        {
            return $res;
        }
        return self::CmpIndex($a, $b);
    }

    private static function CmpTimeLastnameIndex(Entry $a, Entry $b): int
    {
        $res = $a->getTime()->get() - $b->getTime()->get();
        if ($res != 0)
        {
            return $res;
        }
        return self::CmpLastnameIndex($a, $b);
    }

    private static function CmpCupTeamLastnameIndex(Entry $a, Entry $b): int
    {
        $res = (int) $b->getCup() - (int) $a->getCup();
        if ($res != 0)
        {
            return $res;
        }
        return self::CmpTeamLastnameIndex($a, $b);
    }
}
