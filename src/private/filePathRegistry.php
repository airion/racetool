<?php declare(strict_types = 1);

include_once "private/config.php";

class FilePathRegistry
{

    static function GetDBPath(): string
    {
        return self::ResolveFilename(self::GetBaseDirectory() . "/" . Config::Get()['files']['dbDir'] . "/" . Config::Get()['files']['dbName']);
    }

    static function GetDBBackupPath(): string
    {
        $dbBackupDir = Config::Get()['files']['dbBackupDir'];
        if ($dbBackupDir != "")
        {
            $pathParts = pathinfo(Config::Get()['files']['dbName']);
            $date = date("c");
            $dbBackupPath = self::ResolveFilename(self::GetBaseDirectory() . "/" . $dbBackupDir."/".$pathParts['filename']."_backup_".$date.".".$pathParts['extension']);
            return $dbBackupPath;
        }
        else
        {
            return "";
        }
    }

    static function GetTmpDir(): string
    {
        return self::ResolveFilename(self::GetBaseDirectory() . "/" . Config::Get()['files']['tmpDir']);
    }

    private static function GetBaseDirectory(): string
    {
        // Absolute path to the directory above this file, is taken.
        $dir = dirname(__DIR__);
        return $dir;
    }

    private static function ResolveFilename($filename)
    {
        $filename = str_replace('//', '/', $filename);
        $parts = explode('/', $filename);
        $out = array();
        foreach ($parts as $part)
        {
            if ($part == '.')
                continue;
            if ($part == '..')
            {
                array_pop($out);
                continue;
            }
            $out[] = $part;
        }
        return implode('/', $out);
    }
}
