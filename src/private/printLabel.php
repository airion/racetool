<?php declare(strict_types=1);

include_once 'private/PDFCreator/PDFCreator.php';
include_once 'private/filePathRegistry.php';
include_once 'private/entry.php';
include_once 'private/getPriceFromEntry.php';
include_once 'private/getPriceAsString.php';

function printLabel(Entry $entry)
{
    $pageWidth = 54.0;
    $margin = 3.0;
    $marginTop = 3.0;

    $pageHeight = 100.0;
    $format = [$pageWidth, $pageHeight];
    $pdfCreator = new PDFCreator(0, 0, 0, 0, $format);
    $pdfCreator->m_pdf->setPrintHeader(false);
    $pdfCreator->m_pdf->setPrintFooter(false);

    $fontBig = new Font('dejavusansb', 12);
    $fontSmall = new Font('dejavusans', 10);

    $pdfCreator->doPageBreak();
    $pdfCreator->m_pdf->SetLineStyle(['dash' => '1', 'width' => 0.1]);

    $width = $pageWidth - 2 * $margin;
    $x = $margin;
    $y = $marginTop;
    $align = "C";

    $pdfCreator->activateFont($fontSmall, true);
    $string = $entry->getFirstname() . " " . $entry->getLastname();
    $y =$pdfCreator->outputTextInColumn($x, $y, $width, $string, 'LRTB', $align);

    $string = $entry->getTeamAsString();
    $y =$pdfCreator->outputTextInColumn($x, $y, $width, $string, 'LRB', $align);

    $pdfCreator->activateFont($fontSmall, true);
    $run = Runs::GetInstance()->getRun($entry->getRun());
    $string = $entry->getYearAsString() .
        " • " . $entry->getAgeGroupAsString() .
        " • " . $entry->getRunAsStringShort() .
        " • " . $run->getStartTime();
    $y =$pdfCreator->outputTextInColumn($x, $y, $width, $string, 'LRB', $align);

    $string = "#" . $entry->getStartnrAsString() .
        " • C" . $entry->getChipnrAsString() .
        " • " . getPriceAsString(getPriceFromEntry($entry));
    $pdfCreator->activateFont($fontBig, true);
    $y =$pdfCreator->outputTextInColumn($x, $y, $width, $string, 'LRB', $align);


    $tmpdir = FilePathRegistry::GetTmpDir();
    $fileName = tempnam($tmpdir, "label");

    $pdfCreator->m_pdf->Output($fileName, "F");
    chmod($fileName, 0640);

    $printCommand = "lp -d QL-700 $fileName";

    @exec($printCommand);

    unlink($fileName);
}
