<?php declare(strict_types=1);

include_once 'private/database.php';

function getTeamList(Database $database): array
{
    $entries = $database->getEntries();
    $teams = array();
    foreach($entries as $entry)
    {
        $teams[$entry->getTeam()] = 1;
    }
    $teams = array_keys($teams);
    sort($teams);
    return $teams;
}
