<?php declare(strict_types=1);

function convertStringToHTML(string $string): string
{
    return htmlentities($string, ENT_QUOTES);
}
