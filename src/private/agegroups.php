<?php declare(strict_types=1);

include_once 'private/config.php';

class AgeGroup
{
    function __construct(string $name, string $cosaNameM, string $cosaNameW, int $minAge, int $maxAge, int $index)
    {
        $this->m_name = $name;
        $this->m_cosaNameM = $cosaNameM;
        $this->m_cosaNameW = $cosaNameW;
        $this->m_minAge = $minAge;
        $this->m_maxAge = $maxAge;
        $this->m_index = $index;
    }

    function contains(int $year): bool
    {
        return ($year >= $this->m_minAge) && ($year <= $this->m_maxAge);
    }

    function getName(): string
    {
        return $this->m_name;
    }

    function getCosaName(string $gender): string
    {
        assert($gender == "m" || $gender == "w");
        if ($gender == "m")
        {
            return $this->m_cosaNameM;
        }
        else
        {
            return $this->m_cosaNameW;
        }
    }

    function getMinAge(): int
    {
        return $this->m_minAge;
    }

    function getMaxAge(): int
    {
        return $this->m_maxAge;
    }

    function getIndex(): int
    {
        return $this->m_index;
    }

    private $m_name;
    private $m_cosaNameM;
    private $m_cosaNameW;
    private $m_minAge;
    private $m_maxAge;
    private $m_index;
}

class AgeGroups
{

    function __construct(array $config)
    {
        $this->m_eventYear = Config::Get()['event']['year'];

        $lastMaxAge = 0;
        foreach ($config as $ageGroupIndex => $ageGroup)
        {
            assert($ageGroupIndex == count($this->m_ageGroups));

            $ageGroupName = strval($ageGroup['name']);
            $ageGroupCosaNameM = strval($ageGroup['cosaNameM']);
            $ageGroupCosaNameW = strval($ageGroup['cosaNameW']);
            $ageGroupMinAge = $ageGroup['minAge'];
            $ageGroupMaxAge = $ageGroup['maxAge'];

            $this->add($ageGroupName, $ageGroupCosaNameM, $ageGroupCosaNameW, $ageGroupMinAge, $ageGroupMaxAge, $ageGroupIndex);
            if ($ageGroupMaxAge < $ageGroupMinAge)
            {
                echo "<p class='yellow'>Fehler: Altersklassendefinition Maximum < Minimum!</p>";
                assert(false);
            }

            if ($ageGroupIndex > 0)
            {
                if ($ageGroupMinAge <= $lastMaxAge)
                {
                    echo "<p class='yellow'>Fehler: Altersklassendefinition Überschneidung zweier Klassen name: $ageGroupName minAge: $ageGroupMinAge maxAge: $ageGroupMaxAge!</p>";
                    assert(false);
                }
            }
            $lastMaxAge = $ageGroupMaxAge;
        }
    }

    private function add(string $name, string $cosaNameM, string $cosaNameW, int $fromAge, int $toAge, int $ageGroupIndex)
    {
        $this->m_ageGroups[] = new AgeGroup($name, $cosaNameM, $cosaNameW, $fromAge, $toAge, $ageGroupIndex);
    }

    function getAgeGroup(int $index): AgeGroup
    {
        return $this->m_ageGroups[$index];
    }

    function getAgeGroups(): array
    {
        return $this->m_ageGroups;
    }

    function getAgeGroupFromYear(int $year): ?AgeGroup
    {
        $age = $this->m_eventYear - $year;
        foreach ($this->m_ageGroups as $ageGroup)
        {
            if ($ageGroup->contains($age))
            {
                return $ageGroup;
            }
        }
        return null;
    }

    function getAgeGroupByName(string $name): ?AgeGroup
    {
        foreach ($this->m_ageGroups as $ageGroup)
        {
            if ($ageGroup->getName() == $name)
            {
                return $ageGroup;
            }
        }
        return null;
    }

    function getMinYear(): int
    {
        $lastAgeGroup = end($this->m_ageGroups);
        if ($lastAgeGroup === false)
        {
            return 0;
        }
        else
        {
            return $this->m_eventYear - $lastAgeGroup->getMaxAge();
        }
    }

    function getMaxYear(): int
    {
        $firstAgeGroup = reset($this->m_ageGroups);
        if ($firstAgeGroup === false)
        {
            return 0;
        }
        else
        {
            return $this->m_eventYear - $firstAgeGroup->getMinAge();
        }
    }

    private $m_ageGroups = array();
    private $m_eventYear;
}
