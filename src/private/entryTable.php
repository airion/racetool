<?php declare(strict_types = 1);

include_once 'private/config.php';
include_once 'private/parameters.php';
include_once 'private/entries.php';
include_once 'private/database.php';

class EntryTable
{
    function __construct(Page $page, Database $database, string $onClickPage, bool $allowShowingDeleted, int $enabledColumns, bool $shortColumnNames, bool $enableShowColumnButtons, $orderBy, $hiddenColumns, $filterText, $filterRun)
    {
        $programMode = Config::Get()['program']['mode'];

        $runNames = array();

        $runs = Runs::GetInstance()->getRuns();
        foreach ($runs as $run)
        {
            $runNames[] = $run->getName();
        }

        $columnNames = array();
        for ($i = 0; $i < EntryAttribute::Last; $i++)
        {
            if ($shortColumnNames)
            {
                $columnNames[] = Entry::GetAttributeShortName($i);
            }
            else
            {
                $columnNames[] = Entry::GetAttributeName($i);
            }
        }

        $data = array("entries" => array(), "settings" => array("orderBy" => $orderBy, "programMode" => $programMode, "runs" => $runNames,
            "columnNames" => $columnNames, "enabledColumns" => $enabledColumns, "hiddenColumns" => $hiddenColumns, "filterText" => $filterText, "filterRun" => $filterRun
        ));

        if ($allowShowingDeleted)
        {
            $entries = $database->getEntriesWithDeleted();
        }
        else
        {
            $entries = $database->getEntries();
        }
        foreach ($entries as $entry)
        {
            $sendCode = self::IsEnabled($enabledColumns, EntryAttribute::Code);
            if ($onClickPage != "")
            {
                $parameters = new Parameters();
                $parameters->add("key", $entry->getKey());
                $parameters->addJS("orderBy", "document.settings.orderBy");
                $parameters->addJS("hiddenColumns", "document.settings.hiddenColumns");
                $parameters->addJS("filterText", "document.settings.filterText");
                $parameters->addJS("filterRun", "document.settings.filterRun");

                $post = $page->getPostString($onClickPage, $parameters);
                $sendCode = true; // Code needed for tests to click on an entry
            }
            else
            {
                $post = "";
            }

            $sendAgeGroup = self::IsEnabled($enabledColumns, EntryAttribute::AgeGroup) ||
                            self::IsEnabled($enabledColumns, EntryAttribute::PlacingAgeGroup);

            $sendTime = self::IsEnabled($enabledColumns, EntryAttribute::Time) ||
                        self::IsEnabled($enabledColumns, EntryAttribute::PlacingTeam);

            // Lastname needed as secondary sorting key
            // Gender and run needed for splitting for placing sorting orders
            $data["entries"][] = array(
                $entry->getLastname(),
                self::IsEnabled($enabledColumns, EntryAttribute::Firstname) ? $entry->getFirstname() : "",
                self::IsEnabled($enabledColumns, EntryAttribute::Year) ? $entry->getYear() : 0,
                $entry->getGender(),
                self::IsEnabled($enabledColumns, EntryAttribute::AgeGroup) ? $entry->getAgeGroupAsString() : "",
                self::IsEnabled($enabledColumns, EntryAttribute::Team) ? $entry->getTeam() : "",
                $entry->getRun(),
                self::IsEnabled($enabledColumns, EntryAttribute::Cup) ? $entry->getCup() : false,
                self::IsEnabled($enabledColumns, EntryAttribute::Startnr) ? $entry->getStartnr() : 0,
                self::IsEnabled($enabledColumns, EntryAttribute::Chipnr) ? $entry->getChipnr() : 0,
                self::IsEnabled($enabledColumns, EntryAttribute::Email) ? $entry->getEmail() : "",
                self::IsEnabled($enabledColumns, EntryAttribute::Revision) ? $entry->getRevision() : 0,
                self::IsEnabled($enabledColumns, EntryAttribute::Date) ? $entry->getDate() : "",
                self::IsEnabled($enabledColumns, EntryAttribute::Username) ? $entry->getUsername() : "",
                self::IsEnabled($enabledColumns, EntryAttribute::PreRegistration) ? $entry->getPreRegistration() : false,
                self::IsEnabled($enabledColumns, EntryAttribute::Time) ? $entry->getTimeAsString() : "",
                self::IsEnabled($enabledColumns, EntryAttribute::Placing) ? $entry->getPlacing() : 0,
                self::IsEnabled($enabledColumns, EntryAttribute::PlacingMW) ? $entry->getPlacingMW() : 0,
                self::IsEnabled($enabledColumns, EntryAttribute::PlacingAgeGroup) ? $entry->getPlacingAgeGroup() : 0,
                self::IsEnabled($enabledColumns, EntryAttribute::PlacingTeam) ? $entry->getPlacingTeam() : 0,
                $sendCode ? $entry->getKey() : "",

                $entry->getIndex(),
                $entry->isDeleted(),
                $entry->getAgeGroupIsGenerated(),

                $sendAgeGroup ? $entry->getAgeGroup()->getMinAge() : 0,
                $sendTime ? $entry->getTime()->get() : 0,

                $post
            );
        }

        echo "<script>function getData(){";
        echo "return ".json_encode($data, JSON_HEX_TAG).";";
        echo "}</script>";


        $this->createFilterElements();

        if ($enableShowColumnButtons)
        {
            $this->createTableShowColumnButtons($enabledColumns, $columnNames);
        }

        $page->beginScrollable();
        echo "<div id='entryTable' class='overflowXAuto'></div>";

        echo "<script src='entryTable.js'></script>";
    }

    function createTableShowColumnButtons(int $enabledColumns, array $columnNames)
    {
        echo "<div style='margin-bottom: 0.5em;'>";

        $columns = array(
            EntryAttribute::Lastname,
            EntryAttribute::Firstname,
            EntryAttribute::Year,
            EntryAttribute::AgeGroup,
            EntryAttribute::Team,
            EntryAttribute::Run,
            EntryAttribute::Cup,
            EntryAttribute::Startnr,
            EntryAttribute::Chipnr,
            EntryAttribute::Email,
            EntryAttribute::Revision,
            EntryAttribute::Date,
            EntryAttribute::Username,
            EntryAttribute::PreRegistration,
            EntryAttribute::Time,
            EntryAttribute::Placing,
            EntryAttribute::PlacingMW,
            EntryAttribute::PlacingAgeGroup,
            EntryAttribute::PlacingTeam
        );

        foreach($columns as $column)
        {
            if (self::IsEnabled($enabledColumns, $column))
            {
                $columnName = $columnNames[$column];
                echo "<button onclick='toggleColumnVisibility($column);this.blur();' id='showColumnButton$column'>$columnName</button>";
            }
        }

        echo "</div>";
    }

    function createFilterElements()
    {
        echo "<div>";
        echo "<div style='display: inline-block;margin-bottom: 0.5em;'><span>Filter: </span>";
        echo "<input id='filterTextInput' type='text' value='' style='margin-right: 3.0em;' oninput='filterTextInputChanged()'></div>";
        echo "<div style='display: inline-block;margin-bottom: 0.5em;'><span>Wettbewerb: </span>";

        echo "<select id='filterRun' onchange='filterRunChanged()'>";
        $runs = Runs::GetInstance()->getRuns();
        $runValue = Runs::INVALID_RUN;

        echo "<option value='" . Runs::INVALID_RUN . "'";
        if ($runValue == Runs::INVALID_RUN)
        {
            echo " selected";
        }
        echo "> Alle </option>";
        foreach ($runs as $index => $run)
        {
            echo "<option value=\"$index\"";
            if ($index == $runValue)
            {
                echo " selected";
            }
            $runName = $run->getName();
            echo ">$runName</option>";
        }
        echo "</select></div>";
        echo "</div>";
    }

    static function GetStyle(): string
    {
        $style = <<<EOD
        .entryTable
        {
            font-size: 0.7em;
            border-spacing: 0px;
        }
        .entryTable td:not(.splitter)
        {
            padding: 0.5em;
            box-shadow: inset -1px 0px Black;
            padding-right: 1.0em;
        }

        .entryTable td:first-child:not(.splitter)
        {
            box-shadow: inset 1px 0 Black,
                        inset -1px 0px Black;
        }

        .entryTable tr:last-child td
        {
            box-shadow: inset -1px -1px Black;
        }

        .entryTable tr:last-child td:first-child
        {
            box-shadow: inset 1px -1px Black,
                        inset -1px 0px Black;
        }

        .entryTable .oddRow
        {
            background: #dfdfdf;
            color: Black;
        }
        .entryTable .evenRow
        {
            background: #ffffff;
            color: Black;
        }

        .entryTable .oddRow .sortColumn
        {
            background: #bfbfbf;
            color: Black;
        }
        .entryTable .evenRow .sortColumn
        {
            background: #efefef;
            color: Black;
        }

        .entryTable .clickableRow:hover, .entryTable .clickableRow:hover .sortColumn
        {
            background: #0000ff;
            color: White;
            cursor: pointer;
        }

        .entryTable th
        {
            background: #505050;
            color: White;
            text-align: left;
            padding: 0.5em;
            padding-right: 1.0em;
            white-space: nowrap;

            box-shadow: inset -1px 1px Black,
                        inset 0 -1px Black,
                        0px 2px 2px rgba(0, 0, 0, 0.5);
            z-index: 1;
        }

        .entryTable th:first-child
        {
            box-shadow: inset -1px 1px Black,
                        inset +1px -1px Black,
                        0px 2px 2px rgba(0, 0, 0, 0.5);
        }

        .entryTable .nonSelectableHeader
        {
            cursor: default;
        }
        .entryTable .selectableHeader
        {
            cursor: pointer;
        }
        .entryTable .selectableHeader:hover
        {
            background-color: Black;
            box-shadow: 0 0 6px 3px Black;
            z-index: 2;
        }
        .entryTable .splitter
        {
            box-shadow: inset 0 -1px Black,
                        inset 0 1px Black;
            height:0px;
            padding:0em;
            margin:0px;
        }

        .entryTableShowColumnButton
        {
            font-size: 0.75em;
            background: transparent;
            color: White;
            border: 1px solid White;
            margin-right: 0.25em;
            margin-bottom: 0.25em;
            -webkit-border-radius: 8px; -moz-border-radius: 8px; border-radius: 8px;
        }
        .entryTableShowColumnButtonHighLighted
        {
            background: #505050;
            border: 1px solid #505050;
            box-shadow: 2px 2px 2px Black;
        }
        .entryTableShowColumnButtonClickable:hover
        {
            cursor: pointer;
        }
EOD;

        return $style;
    }

    static function AddColumn(int& $columns, int $column)
    {
        $columns |= 1 << $column;
    }

    private static function IsEnabled(int $columns, int $column): bool
    {
        return ($columns & (1 << $column)) != 0;
    }
}
