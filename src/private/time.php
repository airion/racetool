<?php declare(strict_types=1);

class Time
{
    function __construct(int $timeCS)
    {
        $this->m_timeCS = $timeCS;
    }

    function setFromString(string $timeString): bool
    {
        $matches = array();
        if (!preg_match('/^([0-9]?[0-9]):([0-5][0-9]):([0-5][0-9])([a-z]?)$|^([0-5]?[0-9]):([0-5][0-9])([a-z]?)$|^([0-5]?[0-9])([a-z]?)$/', $timeString, $matches))
        {
            return false;
        }

        $hours = 0;
        $minutes = 0;
        switch (count($matches))
        {
            case 5:
                $hours = intval($matches[1]);
                $minutes = intval($matches[2]);
                $seconds = intval($matches[3]);
                $prefix = $matches[4];
                break;
            case 8:
                $minutes = intval($matches[5]);
                $seconds = intval($matches[6]);
                $prefix = $matches[7];
                break;
            case 10:
                $seconds = intval($matches[8]);
                $prefix = $matches[9];
                break;
            default:
                return false;
        }

        $this->m_timeCS = $hours * 60 * 60 * 100 + $minutes * 60 * 100 + $seconds * 100;
        if ($prefix != "")
        {
            $this->m_timeCS += ord($prefix) - ord('a') + 1;
        }

        return true;
    }

    function set(int $timeCS)
    {
        $this->m_timeCS = $timeCS;
    }

    function get(): int
    {
        return $this->m_timeCS;
    }

    function getSeconds(): int
    {
        return intdiv($this->m_timeCS, 100);
    }

    function getAsString(bool $useHyphen = true, bool $postfix = true, bool $leadingZero = true, bool $unit = false): string
    {
        if ($this->m_timeCS == Time::INVALID_TIME)
        {
            if ($useHyphen)
            {
                return "---";
            }
            else
            {
                return "";
            }
        }

        $centiseconds = $this->m_timeCS % 100;
        $seconds = $this->getSeconds() % 60;
        $hours = intdiv($this->m_timeCS, 60 * 60 * 100);
        $minutes = intdiv($this->m_timeCS, 60 * 100) % 60;
        $unitString = " Std.";

        if ($leadingZero)
        {
            $timeString = sprintf("%02d:%02d:%02d", $hours, $minutes, $seconds);
        }
        else
        {
            if ($hours != 0)
            {
                $timeString = sprintf("%d:%02d:%02d", $hours, $minutes, $seconds);
            }
            else
            {
                $timeString = sprintf("%d:%02d", $minutes, $seconds);
                $unitString = " Min.";
            }
        }
        if ($centiseconds > 0 && $postfix)
        {
            if ($centiseconds <= 26)
            {
                $timeString.=chr($centiseconds - 1 + ord('a'));
            }
        }
        if ($unit)
        {
            $timeString.=$unitString;
        }
        return $timeString;
    }

    function isValid(): bool
    {
        return $this->m_timeCS != Time::INVALID_TIME;
    }

    const INVALID_TIME = PHP_INT_MAX;

    private $m_timeCS = Time::INVALID_TIME;
}
