<?php declare(strict_types=1);
include_once 'private/config.php';
include_once 'private/entry.php';

function writeDeleteEntry(Database $database, string $key, Entry &$oldEntry, string $username): bool
{
    $entry = $database->searchEntryByKey($key);
    if (!$entry->isValid())
    {
        echo "<p class='yellow'>Fehler: Keine Anmeldung mit Code \"" . $entry->getKey() . "\" vorhanden!<br><br></p>";
        return false;
    }

    $emptyEntry = new Entry();
    $emptyEntry->setKey($entry->getKey());
    $emptyEntry->setPreRegistration($entry->getPreRegistration());
    $emptyEntry->setCurrentDate();
    $emptyEntry->setUsername($username);
    $writeOk = $database->writeEntry($emptyEntry);

    if (!$writeOk)
    {
        echo "<p class='yellow'>Fehler: Eintrag konnte nicht in Datenbank geschrieben werden !!!<br><br></p>";
        return false;
    }

    $oldEntry = $entry;
    return true;
}
