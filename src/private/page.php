<?php declare(strict_types=1);

include_once "private/errorHandler.php"; // First line, to init error handling before anything else

include_once 'private/config.php';
include_once 'private/programMode.php';
include_once 'private/menu.php';
include_once 'private/messageBox.php';
include_once 'private/access.php';
include_once 'private/parameters.php';
include_once 'private/jsFunctions.php';
include_once 'private/loginMenuWidget.php';

class Page
{
    function __construct()
    {
        ob_start();
        $this->m_access = new Access();
        $this->m_passThroughParameter = new Parameters();

        $this->addPassThroughParameter("username");
        $this->addPassThroughParameter("password");
        $this->addPassThroughParameter("orderBy");
        $this->addPassThroughParameter("hiddenColumns");
        $this->addPassThroughParameter("filterText");
        $this->addPassThroughParameter("filterRun");

        $this->addPassThroughParameter("orderByShowList");
        $this->addPassThroughParameter("hiddenColumnsShowList");
        $this->addPassThroughParameter("filterTextShowList");
        $this->addPassThroughParameter("filterRunShowList");
    }

    function __destruct()
    {
        ob_end_flush();
    }

    function outputHeader(string $javaScript = "", string $style = "", bool $scrollable=true)
    {
        date_default_timezone_set('Europe/Berlin');
        $eventName = Config::Get()['event']['name'];

        echo "<!DOCTYPE html>";
        echo "<html lang=\"de\">";
        echo "<head>";
        echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">";
        echo "<title>$eventName</title>";

        echo "<meta name='viewport' content='width=device-width,height=device-height,initial-scale=1.0'/>";

        if ($javaScript != "")
        {
            echo "<script>$javaScript</script>";
        }
        echo "<script src='page.js'></script>";
        echo "<script src='messageBox.js'></script>";

        $style .= $this->getDefaultStyles();

        if ($style != "")
        {
            echo "<style> $style </style>";
        }

        echo "<link rel='stylesheet' href='res/fontawesome-free-5.8.2-web/css/solid.min.css'>";
        echo "<link rel='stylesheet' href='res/fontawesome-free-5.8.2-web/css/fontawesome.min.css'>";

        echo "</head>";

        echo "<body style='margin: 0; overflow-x: hidden;'>";
        $programMode = Config::Get()['program']['mode'];

        echo "<div id='staticPart'>";

        echo "<div style='position: relative; margin: 0; background-color: #803030; border-style:solid; border-width: 0 0 1px 0; border-color: black; width: 100%; box-shadow: 0 5px 6px rgba(0,0,0,0.5);'>";
        echo "<img alt='Lauftreff Unterkirnach' class='logoimage' src=\"res/lt.png\">";
        echo "<div class='logoContainer'>";
        echo "<img alt='racetool' src='res/racetoolLogo.svg' style='width: 100%;'>";
        echo "</div>";
        echo "</div>";

        if ($scrollable)
        {
            $this->beginScrollable(true);
        }

        echo "<div style='margin: 0 0.5em 0 0.5em'>";

        switch ($programMode)
        {
            case ProgramMode::Registration:
                {
                    $eventNameShort = Config::Get()['event']['nameShort'];
                    echo "<h1 style='font-size: 2.5em; '>Willkommen beim $eventNameShort !</h1>";
                    break;
                }
            case ProgramMode::Finished:
                {
                    echo "<h1>Ergebnisse $eventName</h1>";
                    break;
                }

            default:
                {
                    echo "<h1>Anmeldung $eventName</h1>";
                    break;
                }
        }

        $remainingTime = 0;
        switch ($programMode)
        {
            case ProgramMode::PreRegistration:
                {
                    $closingTimeString = Config::Get()['event']['closingTime'];
                    $closingTime = DateTime::createFromFormat("d.m.Y H:i:s", $closingTimeString);
                    if ($closingTime)
                    {
                        $currentTime = new DateTime();
                        $remainingTime = $closingTime->getTimestamp() - $currentTime->getTimestamp();

                        if ($remainingTime <= 0)
                        {
                            $this->outputPreRegistrationFinished();
                        }
                    }
                    else
                    {
                        echo "<p class='yellow'>Fehler: Zeitformat für Anmeldeschluss ungültig!</p>";
                    }
                    break;
                }
            case ProgramMode::PreRegistrationValidatedAndFinished:
                {
                    $this->outputPreRegistrationFinished();
                    break;
                }
            case ProgramMode::Registration:
                {
                    break;
                }
            case ProgramMode::Finished:
                {
                    break;
                }
            default:
                {
                    echo "<p class='yellow'>Fehler: Ungültiger Modus!</p>";
                }
        }

        return $remainingTime;
    }

    function beginScrollable(bool $scrollableHeader = false)
    {
        if (!$scrollableHeader)
        {
            echo "</div>";
        }
        echo "</div>";
        echo "<div id='scrollablePart'>";
        if (!$scrollableHeader)
        {
            echo "<div style='margin: 0 0.5em 0 0.5em;'>";
        }
    }

    function beginFooter()
    {
        echo "<div style='padding-bottom: 6em;'></div>";
        echo "</div>";
        echo "</div>";

        echo "<footer id='footerPart' style='z-index: 100;'>";

        echo "<div style='margin: 0; background-color: #803030; border-style:solid; border-width: 1px 0 0 0; border-color: black; box-shadow: 0 -5px 6px rgba(0,0,0,0.5);'>";
        echo "<div class='displayFlex' style='padding: 0.2em 0.0em 0.2em 0.5em; overflow-x: auto; align-items: center;' id='footer'>";

        Menu::OutputBackground();
    }

    function endFooter(Parameters $parameters = null)
    {
        if ($parameters == null)
        {
            $parameters = new Parameters();
        }
        $this->userField($parameters);

        echo "</div>";
        echo "</div>";
        echo "</footer>";

        echo "<script src='datalist-polyfill.min.js'></script>";
        echo "<script src='menu.js'></script>";
        echo "</body>";
        echo "</html>";
    }

    function outputButton(string $page, string $text, Parameters $parameters = null, string $id = "", bool $disabled = false, string $class = "button")
    {
        if ($parameters == null)
        {
            $parameters = new Parameters();
        }
        $this->outputButtonJS($this->getPostString($page, $parameters), $text, $id, $disabled, $class);
    }

    function outputBackButton(string $page, Parameters $parameters = null)
    {
        Menu::OutputButton("fa-backward", self::BackButtonText, $this->getPostString($page, $parameters), "back");
        Menu::OutputSpacer();
    }

    function outputButtonJS(string $post, string $text, string $id = "", bool $disabled = false, string $class = "button")
    {
        if ($disabled)
        {
            $disabledString = "disabled";
        }
        else
        {
            $disabledString = "";
        }
        echo "<button class='$class'";
        if ($id != "")
        {
            echo " id='$id'";
        }
        echo "$disabledString onClick=\"$post\"> $text </button>";
    }

    function beginForm(string $page, Parameters $parameters = null, string $class = "")
    {
        $post = $this->getPostString($page, $parameters, "this");
        echo "<form enctype='multipart/form-data' action=\"#\" onsubmit=\"$post;return false;\"";
        if ($class != "")
        {
            echo " class='$class'";
        }
        echo ">";
    }

    function endForm()
    {
        echo "</form>";
    }

    function outputUploadForm(string $id, string $extension, string $page, Parameters $parameters)
    {
        $parameters->add("MAX_FILE_SIZE", Config::Get()['files']['uploadLimit']);
        $post = $this->getPostString($page, $parameters, "this.parentNode");

        echo "<form enctype='multipart/form-data'>";
        echo "<input id='$id' name='userfile' type='file' accept='$extension' style='display:none;' onchange=\"$post\"></input>";
        echo "</form>";

        return "document.getElementById('$id').click()";
    }

    function outputErrorMessage(string $message, string $page, Parameters $parameters = null)
    {
        $javaScript = getJSFunction_post();
        $this->outputHeader($javaScript);
        echo "<p class='yellow'>$message</p>";
        $this->beginFooter();
        $this->outputBackButton($page, $parameters);
        $this->endFooter();
    }

    function outputWrongPasswordErrorMessage()
    {
        $this->outputErrorMessage("Falsches Passwort!", "index.php");
    }

    function getPostString(string $page, ?Parameters $parameters = null, string $form = ""): string
    {
        if ($parameters == null)
        {
            $parameters = new Parameters();
        }

        $parametersArray = array_merge($this->m_passThroughParameter->get(), $parameters->get());

        $string = "post('$page', {";
        foreach ($parametersArray as $name => $value)
        {
            $string .= "$name: $value,";
        }
        $string .= "}";
        if ($form)
        {
            $string .= ", $form";
        }

        $string .= ")";

        return $string;
    }

    function getPostFunction(string $page, ?Parameters $parameters = null, string $form = ""): string
    {
        return "function(){" . $this->getPostString($page, $parameters, $form) . "}";
    }

    protected function enableLoginButton()
    {
        $this->m_hasLoginButton = true;
    }

    private function addPassThroughParameter(string $parameter)
    {
        $this->m_passThroughParameter->passThrough($parameter);
    }

    private function outputPreRegistrationFinished()
    {
        $preRegistrationClosedText = Config::Get()['event']['preRegistrationClosedText'];

        echo "<p class='yellow'>Die Voranmeldung ist geschlossen.<br>";
        echo $preRegistrationClosedText;
        echo "</p>";
    }

    private function getDefaultStyles(): string
    {
        $styles = Menu::GetStyle();
        $styles .= MessageBox::GetStyle();
        $styles .= LoginMenuWidget::GetStyle();
        $styles .= <<<EOD

        /* Needed for datalist polyfill. */
        [hidden]
        {
    	   display: none;
        }

        .fullwidth
        {
            width: 100%;
            box-sizing:border-box;
        }

        .displayFlex
        {
            display: flex;
            display: -ms-flexbox;
            display: -webkit-flex;
        }

        .alignItemsCenter
        {
            align-items: center;
            -ms-flex-align:center;
            -webkit-align-items: center;
        }

        .flexWrap
        {
            flex-wrap: wrap;
            -ms-flex-wrap: wrap;
            -webkit-flex-wrap: wrap;
        }

        .flexFlowColumn
        {
            flex-flow: column;
            -ms-flex-direction: column;
            -webkit-flex-flow: column;
        }

        .flexAuto
        {
            flex: auto;
            -ms-flex: 1 1 auto;
            -webkit-flex: 1 1 auto;
        }

        .flexNone,
        .button
        {
            flex: 0 0 auto;
            -ms-flex: 0 0 auto;
            -webkit-flex:  0 0 auto;
        }

        .justifyContentCenter
        {
            justify-content: center;
            -ms-flex-pack: center;
            -webkit-box-pack: center;
        }

        .tableWithPadding
        {
            border-spacing:0em;
            /*border: 1px Black solid;*/
        }
        .tableWithPadding tr td {padding-left: 0em; font-size: 0.7em;}
        .tableWithPadding tr th {padding-left: 0em; font-size: 1.0em;}
        .tableWithPadding tr td,  .tableWithPadding tr th {padding-right: 2em;}
        .alignTableHeadLeft tr th {text-align:left;}

        h1
        {
        	font-weight: bold;
        	text-shadow: 4px 4px 3px rgba(0,0,0,0.5);
            font-size: 1.5em;
        }
        p,li,label,span {font-size: 0.9em;}
        .small {font-size: 0.7em;}
        .yellow {font-size: 0.9em; color: yellow;}
        .smallYellow {font-size: 0.7em; color: yellow;}
        .big {font-size: 1.0em; font-weight: bold;}

        .button
        {
            height: 2.5em;
            font-weight: bold;
            font-size: 0.9em; line-height: 1em;
            background: #fdfdfd;
            background: -moz-linear-gradient(top, #fdfdfd 0%, #bebebe 100%);
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#fdfdfd), color-stop(100%,#bebebe));
            background: -webkit-linear-gradient(top, #fdfdfd 0%,#bebebe 100%);
            background: -o-linear-gradient(top, #fdfdfd 0%,#bebebe 100%);
            background: -ms-linear-gradient(top, #fdfdfd 0%,#bebebe 100%);
            background: linear-gradient(to bottom, #fdfdfd 0%,#bebebe 100%);
            -webkit-border-radius: 10px; -moz-border-radius: 10px; border-radius: 10px;
            min-width: 40px;
            border: 0px solid white;
            margin-bottom: 0.5em;
            margin-right: 0.5em;
            box-shadow: 5px 5px 6px rgba(0,0,0,0.5);
            padding: 0.5em;
            position: relative;
            color: black;
        }

        .redButton
        {
            background: #ffc000;
            background: -moz-linear-gradient(top, #ffc000 0%, #ff7000 100%);
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffc000), color-stop(100%,#ff7000));
            background: -webkit-linear-gradient(top, #ffc000 0%,#ff7000 100%);
            background: -o-linear-gradient(top, #ffc000 0%,#ff7000 100%);
            background: -ms-linear-gradient(top, #ffc000 0%,#ff7000 100%);
            background: linear-gradient(to bottom, #ffc000 0%,#ff7000 100%);
        }

        .noPaddingBottom {padding-bottom: 0.0em !important;}
        .noPaddingRight {padding-right: 0.0em !important;}
        .noPadding {padding: 0.0em !important;}
        .noMarginBottom {margin-bottom: 0.0em !important;}
        .noMarginTop {margin-top: 0.0em !important;}
        .noMarginRight {margin-right: 0.0em !important;}
        .marginLeft {margin-left: 0.5em !important;}
        .marginRight {margin-right: 0.5em !important;}
        .marginBottom {margin-bottom: 0.5em !important;}
        .marginTop {margin-top: 0.5em !important;}

        .button:disabled
        {
            color: #777;
        }

        .button:active:hover:not(:disabled)
        {
            background: #bbb;
            box-shadow: inset 5px 5px 6px rgba(0,0,0,0.5);
            top: 2px;
            left: 2px;
        }

        .redButton:active:hover:not(:disabled)
        {
            background: #dfb000;
        }

        button.link
        {
            display: inline-block;
            position: relative;
            background-color: transparent;
            cursor: pointer;
            border: 0;
            padding: 0;
            color: #00f;
            text-decoration: underline;
            font: inherit;
        }

        .logoimage
        {
            width: 100%;
            max-width: 600px;
        }

        .logoContainer
        {
            position: absolute;
            right: 0;
            top: 0;
            margin-right: 4px;
            margin-top: 4px;
            width: 200px;
            height: 0;
            transition: all 0.5s;
            padding-bottom: calc(200px * 114 / 454);
        }

        /* 600px picture logo + 200px racetool logo + 4px margin + 20px reserve for scroll-bar */
        @media screen and (max-width: 824px) {
            .logoContainer
            {
                opacity: 0;
            }
        }
        html {
            -webkit-text-size-adjust: none; font-family: sans-serif;
            background: #D20B12;
            color: White;
            height: 100%;
        }
EOD;
        return $styles;
    }

    private function userField(Parameters $parameters)
    {
        Menu::OutputSpacer();
        Menu::OutputFiller();
        if ($this->m_access->hasAccess(AccessRight::Read))
        {
            $parameters->add("username", "");
            $parameters->add("password", "");
            $post = $this->getPostString("index.php", $parameters);
            $menuEntries = [
                new MenuEntry("Abmelden", $post, "logoff")
            ];
            Menu::OutputButtonWithMenu("fa-user-circle menuButtonOrange footerRightAligned", "Benutzer", $menuEntries, "user");
            echo "<span style='font-size: 0.7em; padding-right: 0.5em; color: orange; text-shadow: 4px 4px 2px rgba(0,0,0,0.2);'>". $this->m_access->getUsername() . " </span>";
        }
        else
        {
            if ($this->m_hasLoginButton)
            {
                Menu::OutputButtonWithMenuWidget("fa-user-circle footerRightAligned", "Login Administrativer Bereich", new LoginMenuWidget($this, $parameters), "login");
                if (isset($_POST["userLogin"]))
                {
                    MessageBox::OutputMessage("Benutzer oder Passwort falsch!");
                }
            }
        }
    }

    protected $m_access = null;

    private const BackButtonText = "Zurück";
    private $m_passThroughParameter = null;
    private $m_hasLoginButton = false;

}
