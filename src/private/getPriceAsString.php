<?php declare(strict_types=1);

include_once 'private/config.php';

function getPriceAsString(float $price): string
{
    if (intval($price) == $price)
    {
        return "€ $price,–";
    }
    else
    {
        return "€ ".number_format($price, 2, ",", "");
    }
}
