<?php  declare(strict_types=1);

include_once "private/Spyc/Spyc.php";

class Config
{
    private static $_instance = null;

    public static function GetInstance(): Config
    {
        if (null === self::$_instance)
        {
            self::$_instance = new self;
        }
        return self::$_instance;
    }

    public static function Get(): array
    {
        return self::GetInstance()->m_config;
    }

    public function writeSettings(array $newSettings)
    {
        if (file_exists(Config::SettingsConfigFileName))
        {
            $settingsConfig = self::ReadConfigurationFromFile(Config::SettingsConfigFileName);
        }
        else
        {
            $settingsConfig = array();
        }

        $mergedSettings = array_replace_recursive($settingsConfig, $newSettings);
        self::WriteConfigurationToFile(Config::SettingsConfigFileName, $mergedSettings);
    }

    private function __clone()
    {
    }

    private function __construct()
    {
        $configFiles = [
            "private/config.yaml",
            "private/testconfig.yaml",
            "private/sandbox1config.yaml",
            self::SettingsConfigFileName
        ];

        $this->m_config = self::ReadConfigurationFromFiles($configFiles);

        $this->checkValues();
    }

    private function ReadConfigurationFromFiles(array $files): array
    {
        $config = self::ReadConfigurationFromFile($files[0]);
        for ($i = 1; $i < count($files); $i++)
        {
            if (file_exists($files[$i]))
            {
                $configForReplacement = self::ReadConfigurationFromFile($files[$i]);
                $config = array_replace_recursive($config, $configForReplacement);
            }
        }
        return $config;
    }

    private function ReadConfigurationFromFile(string $file): array
    {
        return Spyc::YAMLLoad($file);
    }

    private function WriteConfigurationToFile(string $file, Array $config)
    {
        $indent=false;
        $wordwrap=0;
        $noOpeningDashes = true;
        $yaml = Spyc::YAMLDump($config, $indent, $wordwrap, $noOpeningDashes);
        file_put_contents($file, $yaml);
    }

    private function checkValues()
    {
        assert(is_bool($this->m_config['files']['backupDeletedDatabase']));
        assert(is_bool($this->m_config['features']['cupFlag']));
    }

    private $m_config;

    const SettingsConfigFileName = "private/settingsconfig.yaml";
}
