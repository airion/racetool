<?php declare(strict_types=1);

include_once 'private/filefunctions.php';
include_once 'private/time.php';
include_once 'private/chipIds.php';

class TimeData
{
    function __construct($chipId, Time $time)
    {
        $this->m_chipId = $chipId;
        $this->m_time = clone $time;
    }

    function getChipId(): string
    {
        return $this->m_chipId;
    }

    function getTime(): Time
    {
        return $this->m_time;
    }

    function getChipNumber(): int
    {
        return ChipIds::GetInstance()->getNumberForId($this->m_chipId);
    }

    function setChipNumberFitsRun()
    {
        $this->m_chipNumberFitsRun = true;
    }

    function getChipNumberFitsRun(): bool
    {
        return $this->m_chipNumberFitsRun;
    }

    function setDuplicateChipId()
    {
        $this->m_duplicateChipId = true;
    }

    function getDuplicateChipId(): bool
    {
        return $this->m_duplicateChipId;
    }

    function isOk(): bool
    {
        return $this->m_chipNumberFitsRun && !$this->m_duplicateChipId;
    }

    private $m_chipId = "";
    private $m_time;
    private $m_chipNumberFitsRun = false;
    private $m_duplicateChipId = false;
}

class TimeDataset
{
    function readFromFile(array $fields): bool
    {
        while (!empty($fields))
        {
            $chipId = array_shift($fields);
            $timeString = array_shift($fields);
            if ($chipId === null || $timeString === null || !is_numeric($timeString))
            {
                echo "<p class='yellow'>TimeDataset kann nicht gelesen werden!</p>";
                return false;
            }
            $time = new Time(intval($timeString));
            $this->addTimeData(new TimeData($chipId, $time));
        }
        return true;
    }

    function writeToFile($fp): bool
    {
        $string = $this->GetTypeString();
        foreach($this->m_timeData as $timeData)
        {
            $string .= ";".$timeData->getChipId();
            $string .= ";".strval($timeData->getTime()->get());
        }
        $string.="\n";
        return writeStringToFile($fp, $string);
    }

    function getTimeData(): array
    {
        return $this->m_timeData;
    }

    function addTimeData(TimeData $timeData)
    {
        $this->m_timeData[] = $timeData;
    }

    function clear()
    {
        $this->m_timeData = array();
    }

    static function GetTypeString(): string
    {
        return "TimeDataset";
    }

    private $m_timeData = array();
}
