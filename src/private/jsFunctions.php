<?php declare(strict_types=1);

function getJSFunction_post(): string
{
    // Parameters are prepended - needed for parameter MAX_FILE_SIZE for file uploads!
    $javaScript = <<<EOD
function post(path, params, form)
{
    var createForm = (form === undefined);
    if (createForm)
    {
        form = document.createElement("form");
        document.body.appendChild(form);
    }

    form.setAttribute("method", "post");
    form.setAttribute("action", path);

    for(var key in params)
    {
        if(params.hasOwnProperty(key))
        {
            if (form.elements[key] === undefined)
            {
                var hiddenField = document.createElement("input");
                hiddenField.setAttribute("type", "hidden");
                hiddenField.setAttribute("name", key);
                hiddenField.setAttribute("value", params[key]);
                form.insertBefore(hiddenField, form.firstChild);
            }
        }
    }

    var inputs = form.querySelectorAll('input');
    var numberInputs = inputs.length;
    for (var i = 0; i < numberInputs; i++)
    {
        inputs[i].disabled = false;
    }
    form.submit();
}
EOD;
    return $javaScript;
}

function getJSFunction_getElementValue(): string
{
    $javaScript = <<<EOD

function getElementValue(id)
{
   console.log("getElementValue name: " + id);
   var elements = document.getElementsByName(id);
   console.log(elements);

   if ((elements.length > 0))
   {
      radioButtonFound = false;
      for(var i = 0; i < elements.length; i++)
      {
          if(elements[i].type == 'radio')
          {
              radioButtonFound = true;
              if (elements[i].checked)
              {
                  return elements[i].value;
              }
          }
       }
       if (radioButtonFound)
       {
            return '';
       }
   }

   var element = document.getElementById(id);
   if (element != null)
   {
       if (element.type == 'checkbox')
       {
           if (element.checked)
           {
               return "1";
           }
           else
           {
           	   return "0";
           }
        }

        return element.value;
    }
    return "";
}
EOD;
    return $javaScript;
}
