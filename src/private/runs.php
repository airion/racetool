<?php declare(strict_types=1);

include_once 'private/config.php';
include_once 'private/agegroups.php';

class Run
{
    function __construct(string $name, string $nameShort, string $cosaName, string $startTime, int $competition, AgeGroups $ageGroups, int $index, bool $teamPlacing, int $certificatesMaxAgeGroupPlace)
    {
        $this->m_name = $name;
        $this->m_nameShort = $nameShort;
        $this->m_cosaName = $cosaName;
        $this->m_startTime = $startTime;
        $this->m_competition = $competition;
        $this->m_ageGroups = $ageGroups;
        $this->m_index = $index;
        $this->m_teamPlacing = $teamPlacing;
        $this->m_certificatesMaxAgeGroupPlace = $certificatesMaxAgeGroupPlace;
    }

    function getName(): string
    {
        return $this->m_name;
    }

    function getNameShort(): string
    {
        return $this->m_nameShort;
    }

    function getCosaName(): string
    {
        return $this->m_cosaName;
    }

    function getCompetition(): int
    {
        return $this->m_competition;
    }

    function getStartTime(): string
    {
        return $this->m_startTime;
    }

    function getAgeGroups(): AgeGroups
    {
        return $this->m_ageGroups;
    }

    function getAgeGroupByName(string $ageGroupName): ?AgeGroup
    {
        return $this->m_ageGroups->getAgeGroupByName($ageGroupName);
    }

    function getAgeGroupForYear(int $year): ?AgeGroup
    {
        return $this->m_ageGroups->getAgeGroupFromYear($year);
    }

    function getIndex(): int
    {
        return $this->m_index;
    }

    function getTeamPlacing(): bool
    {
        return $this->m_teamPlacing;
    }

    function getCertificatesMaxAgeGroupPlace(): int
    {
        return $this->m_certificatesMaxAgeGroupPlace;
    }

    private $m_name;
    private $m_nameShort;
    private $m_cosaName;
    private $m_competition;
    private $m_startTime;
    private $m_ageGroups;
    private $m_index;
    private $m_teamPlacing;
    private $m_certificatesMaxAgeGroupPlace;
}

class Runs
{
    private static $_instance = null;

    public static function GetInstance()
    {
        if (null === self::$_instance)
        {
            self::$_instance = new self;
        }
        return self::$_instance;
    }

    private function __clone() {}
    private function __construct()
    {
        $config = Config::Get();

        $configRuns = $config['runs'];

        foreach ($configRuns as $runIndex => $configRun)
        {
            assert($runIndex == count($this->m_runs));

            $runName = strval($configRun['name']);
            $runNameShort = strval($configRun['nameShort']);
            $runCosaName = strval($configRun['cosaName']);
            $runStartTime = strval($configRun['startTime']);
            $runCompetition = strval($configRun['competition']);
            $ageGroupsConfig = $configRun['ageGroups'];
            $teamPlacing = $configRun['teamPlacing'] == 1;
            if (isset($configRun['certificates']['maxAgeGroupPlace']))
            {
                $certificatesMaxAgeGroupPlace = $configRun['certificates']['maxAgeGroupPlace'];
            }
            else
            {
                $certificatesMaxAgeGroupPlace = PHP_INT_MAX;
            }

            $runAgeGroups = new AgeGroups($ageGroupsConfig);

            $competitionIndex = $this->getCompetitionIndex($runCompetition);
            if ($competitionIndex == self::INVALID_COMPETITION)
            {
                array_push($this->m_competitionNames, $runCompetition);
                $competitionIndex = count($this->m_competitionNames) - 1;
            }

            $this->m_runs[] = new Run($runName, $runNameShort, $runCosaName, $runStartTime, $competitionIndex, $runAgeGroups, $runIndex, $teamPlacing, $certificatesMaxAgeGroupPlace);
        }
    }

    function getRun($index): Run
    {
        return $this->m_runs[$index];
    }

    function getRuns(): array
    {
        return $this->m_runs;
    }

    function getNumberOfRuns(): int
    {
        return count($this->m_runs);
    }

    function getCompetitionNames(): array
    {
        return $this->m_competitionNames;
    }

    function getCompetition($index): string
    {
        return $this->m_competitionNames[$index];
    }

    function getCompetitionIndex(string $competitionNameToSearch): int
    {
        foreach ($this->m_competitionNames as $index => $competitionName)
        {
            if ($competitionNameToSearch == $competitionName)
            {
                return $index;
            }
        }
        return self::INVALID_COMPETITION;
    }

    function getMinYear(): int
    {
        $minYear = PHP_INT_MAX;
        foreach($this->m_runs as $run)
        {
            $minYear = min($minYear, $run->getAgeGroups()->getMinYear());
        }
        return $minYear;
    }

    function getMaxYear(): int
    {
        $maxYear = -PHP_INT_MAX;
        foreach($this->m_runs as $run)
        {
            $maxYear = max($maxYear, $run->getAgeGroups()->getMaxYear());
        }
        return $maxYear;
    }

    private $m_runs = array();
    private $m_competitionNames = array();

    const INVALID_RUN = -1;
    const INVALID_COMPETITION = -1;
}
