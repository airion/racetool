<?php declare(strict_types=1);

function getRegistrationLink(): string
{
    $link = $_SERVER['REQUEST_SCHEME']."://".$_SERVER["SERVER_NAME"];
    $path = dirname($_SERVER["SCRIPT_NAME"]);
    if ($path != "/")
    {
        $link .= $path;
    }
    return $link;
}
