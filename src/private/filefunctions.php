<?php declare(strict_types=1);

function writeStringToFile($fh, string $string): bool
{
    $bytesToWrite = strlen($string);
    while ($bytesToWrite > 0)
    {
        $written = fwrite($fh, $string);
        if ($written === false)
        {
            return false;
        }
        $bytesToWrite -= $written;
        if ($bytesToWrite > 0)
        {
            $string = substring($string, $written);
        }
    }
    return true;
}
