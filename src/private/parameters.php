<?php declare(strict_types=1);

class Parameters
{

    function add(string $name, $value)
    {
        $this->m_parameters[$name] = "'$value'";
    }

    function addJS(string $name, string $javaScript)
    {
        $this->m_parameters[$name] = $javaScript;
    }

    function passThrough(string $name)
    {
        if (isset($_POST[$name]))
        {
            $this->add($name, $_POST[$name]);
        }
    }

    function get(): array
    {
        return $this->m_parameters;
    }

    private $m_parameters = array();
}
