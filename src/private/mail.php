<?php declare(strict_types=1);
include_once 'private/config.php';

class Mail
{
    static function SendMail(string $to, string $subject , string $message, $additionalHeaders = "", $additionalParameters = "") : bool
    {
        $sendMailAddress = Config::Get()['mail']['sendAddress'];
        $additionalParameters = implode(" ", array($additionalParameters, "-f $sendMailAddress"));
        return mail($to, "=?UTF-8?B?" . base64_encode($subject) . "?=", $message, $additionalHeaders, $additionalParameters);
    }
}
