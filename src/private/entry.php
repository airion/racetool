<?php declare(strict_types=1);

include_once 'private/config.php';
include_once 'private/agegroups.php';
include_once 'private/filefunctions.php';
include_once 'private/time.php';
include_once 'private/entryAttribute.php';
include_once 'private/runs.php';
include_once 'private/post.php';

class Entry
{
    public function __construct()
    {
        $this->m_datasetTime = new Time(Time::INVALID_TIME);
        $this->m_manualTime = new Time(Time::INVALID_TIME);
    }

    public function setFromPost()
    {
        $this->setFirstname(Post::GetStringValueFromPost("firstname"));
        $this->setLastname(Post::GetStringValueFromPost("lastname"));
        $this->setYearFromString(self::FilterInput(Post::GetStringValueFromPost("year")));
        $this->m_gender = Post::GetStringValueFromPost("gender");
        $this->setTeam(Post::GetStringValueFromPost("team"));

        $competitionString = Post::GetStringValueFromPost("competition");
        $competition = Runs::INVALID_COMPETITION;

        if (is_numeric($competitionString))
        {
            $competition=intval($competitionString);
        }

        $ageGroupName = Post::GetStringValueFromPost("agegroup");

        if ($ageGroupName != "auto")
        {
            $this->setAgeGroupAndRunFromCompetitionAndAgeGroupName($competition, $ageGroupName);
        }
        else
        {
            $this->setAgeGroupAndRunFromCompetitionAndYear($competition, $this->m_year);
        }

        $this->m_email = self::FilterInput(Post::GetStringValueFromPost("email"));

        $this->setStartnrFromString(self::FilterInput(Post::GetStringValueFromPost("startnr")));
        $this->setChipnrFromString(self::FilterInput(Post::GetStringValueFromPost("chipnr")));

        $this->m_key = Post::GetStringValueFromPost("key");

        if (Post::GetStringValueFromPost("cup") == "on")
        {
            $this->m_cup = true;
        }

        $this->setManualTimeFromString(Post::GetStringValueFromPost("time"));
        $this->setCurrentDate();
    }

    public static function PassThroughParameters(Parameters $parameters)
    {
        $parameters->add("takeValuesFromPost", "1");
        $names = array("firstname", "lastname", "year", "gender", "team", "competition", "agegroup", "email", "startnr", "chipnr", "key", "cup", "time");
        foreach ($names as $name)
        {
            $parameters->passThrough($name);
        }
    }

    public function set(string $startnr, string $lastname, string $firstname, string $gender, string $team, string $year, bool $cup, int $competition, string $username)
    {
        $this->setStartnrFromString($startnr);

        $this->setLastname($lastname);
        $this->setFirstname($firstname);
        $this->setYearFromString($year);
        $this->m_gender = $gender;
        $this->setTeam($team);

        if ($competition != Runs::INVALID_COMPETITION)
        {
            $this->setAgeGroupAndRunFromCompetitionAndYear($competition, $this->m_year);
        }
        else
        {
            $this->setAgeGroupAndRunFromYear($this->m_year);
        }

        $this->m_cup = $cup;
        $this->setUsername($username);
        $this->setCurrentDate();
    }

    public function getFirstname(): string
    {
        return $this->m_firstname;
    }

    public function getLastname(): string
    {
        return $this->m_lastname;
    }

    public function getYear(): int
    {
        return $this->m_year;
    }

    public function getYearAsString(): string
    {
        if ($this->m_year != self::INVALID_YEAR)
        {
            return (string)$this->m_year;
        }
        else
        {
            return "";
        }
    }

    public function getGender(): string
    {
        return $this->m_gender;
    }

    public function getTeam(): string
    {
        return $this->m_team;
    }

    public function getTeamAsString(): string
    {
        if ($this->m_team != "")
        {
            return $this->m_team;
        }
        else
        {
            return "---";
        }
    }

    public function getRun(): int
    {
        return $this->m_run;
    }

    public function getRunAsString(): string
    {
        if ($this->m_run != Runs::INVALID_RUN)
        {
            return Runs::GetInstance()->getRun($this->m_run)->getName();
        }
        else
        {
            return "";
        }
    }

    public function getRunAsStringShort(): string
    {
        if ($this->m_run != Runs::INVALID_RUN)
        {
            return Runs::GetInstance()->getRun($this->m_run)->getNameShort();
        }
        else
        {
            return "";
        }
    }

    public function getRunCosaName(): string
    {
        if ($this->m_run != Runs::INVALID_RUN)
        {
            return Runs::GetInstance()->getRun($this->m_run)->getCosaName();
        }
        else
        {
            return "";
        }
    }

    public function getCupAsString(): string
    {
        if ($this->m_cup)
        {
            return "Cup";
        }
        else
        {
            return "-";
        }
    }

    public function getEmail(): string
    {
        return $this->m_email;
    }

    public function getRevision(): int
    {
        return $this->m_revision;
    }

    public function getStartnr(): int
    {
        return $this->m_startnr;
    }

    public function getChipnr(): int
    {
        return $this->m_chipnr;
    }

    public function getKey(): string
    {
        return $this->m_key;
    }

    public function getDate(): string
    {
        return $this->m_date;
    }

    public function getIndex(): int
    {
        return $this->m_index;
    }

    public function getUsername(): string
    {
        if ($this->m_username != "")
        {
            return $this->m_username;
        }
        else
        {
            return "---";
        }
    }

    public function getCup(): bool
    {
        return $this->m_cup;
    }

    public function getAgeGroup(): ?AgeGroup
    {
        return $this->m_ageGroup;
    }

    public function getAgeGroupAsString(): string
    {
        if ($this->m_ageGroup != null)
        {
            return strtoupper($this->m_gender).$this->m_ageGroup->getName();
        }
        else
        {
            return "";
        }
    }

    public function getCosaAgeGroupAsString(): string
    {
        if ($this->m_ageGroup != null)
        {
            return $this->m_ageGroup->getCosaName($this->m_gender);
        }
        else
        {
            return "";
        }
    }

    function getAgeGroupIsGenerated(): bool
    {
        return $this->m_ageGroupIsGenerated;
    }

    public function setPreRegistration(bool $preRegistration)
    {
        $this->m_preRegistration = $preRegistration;
    }

    public function getPreRegistration(): bool
    {
        return $this->m_preRegistration;
    }

    public function getPreRegistrationAsString(): string
    {
        if ($this->m_preRegistration)
        {
            return "VA";
        }
        else
        {
            return "-";
        }
    }

    public function setCurrentDate()
    {
        $timestamp = time();
        $this->m_date = date("d.m.Y", $timestamp) . " " . date("H", $timestamp) . ":" . date("i", $timestamp);
    }

    public function setDate(string $date)
    {
        $this->m_date = $date;
    }

    public function setUsername(string $username)
    {
        // TODO: Remove filter, when no more old database
        if (!strpos($username, ".") && !strpos($username, ":"))
        {
            $this->m_username = $username;
        }
    }

    public function setKey(string $key)
    {
        $this->m_key = $key;
    }

    public function setStartnr(int $startnr)
    {
        $this->m_startnr = $startnr;
    }

    public function setChipnr(int $chipnr)
    {
        $this->m_chipnr = $chipnr;
    }

    public function setStartnrFromString(string $startnr)
    {
        if (is_numeric($startnr))
        {
            $this->m_startnr = intval($startnr);
        }
        else
        {
            $this->m_startnr = self::INVALID_STARTNR;
        }
    }

    public function setChipnrFromString(string $chipnr)
    {
        if (is_numeric($chipnr))
        {
            $this->m_chipnr = intval($chipnr);
        }
        else
        {
            $this->m_chipnr = self::INVALID_CHIPNR;
        }
    }

    public function setIndex(int $index)
    {
        $this->m_index = $index;
    }

    public function setRevision(int $revision)
    {
        $this->m_revision = $revision;
    }

    public function setYearFromString(string $yearString)
    {
        if (is_numeric($yearString))
        {
            $this->m_year = intval($yearString);
        }
        else
        {
            $this->m_year = self::INVALID_YEAR;
        }
    }

    public function writeToFile($fp): bool
    {
        $string = $this->getEntryDataAsString()."\n";
        return writeStringToFile($fp, $string);
    }

    public function getEntryDataAsString(): string
    {
        $yearString = "";
        $runString = "";
        $startnrString = "";
        $chipnrString = "";
        $ageGroupString = "";
        $manualTimeString = "";
        $cupString = "";
        $preRegistrationString = "";

        if ($this->isValid())
        {
            if ($this->m_year != self::INVALID_YEAR)
            {
                $yearString = (string) $this->m_year;
            }

            if ($this->m_run != Runs::INVALID_RUN)
            {
                $runString = (string) $this->m_run;
            }

            if ($this->m_startnr != self::INVALID_STARTNR)
            {
                $startnrString = (string) $this->m_startnr;
            }

            if ($this->m_chipnr != self::INVALID_CHIPNR)
            {
                $chipnrString = (string) $this->m_chipnr;
            }

            if (!$this->m_ageGroupIsGenerated)
            {
                $ageGroupString = $this->getAgeGroupAsString();
            }

            $manualTimeString = $this->getManualTimeAsString();

            $cupString = (int) $this->m_cup;
        }
        $preRegistrationString = (int) $this->m_preRegistration;

        $typeString = $this->GetTypeString();
        return "$typeString;$this->m_firstname;$this->m_lastname;$yearString;$this->m_gender;$this->m_team;$runString;$this->m_email;$startnrString;$chipnrString;$this->m_key;$this->m_date;$this->m_username;$cupString;$ageGroupString;$preRegistrationString;$manualTimeString";
    }

    public function readFromFile(array $fields): bool
    {
        $numberField = count($fields);
        if ($numberField == 16)
        {
            $this->m_firstname = $fields[0];
            $this->m_lastname = $fields[1];
            $this->setYearFromString($fields[2]);
            $this->m_gender = $fields[3];
            $this->m_team = $fields[4];
            $this->setRunFromString($fields[5]);
            $this->m_email = $fields[6];
            $this->setStartnrFromString($fields[7]);
            $this->setChipnrFromString($fields[8]);
            $this->m_key = $fields[9];
            $this->m_date = $fields[10];
            $this->setUsername($fields[11]);
            $this->setCupFromString($fields[12]);

            $ageGroupName = $fields[13];
            $this->setPreRegistrationFromString($fields[14]);
            if ($ageGroupName != "")
            {
                $run = Runs::GetInstance()->getRun($this->m_run);
                if ($run != null)
                {
                    $ageGroup = $run->getAgeGroupByName(substr($ageGroupName, 1));
                    if ($ageGroup != null)
                    {
                        $this->m_ageGroup = $ageGroup;
                        $this->m_ageGroupIsGenerated = false;
                    }
                    else
                    {
                        echo "<p class='yellow'>Fehler: Altersklasse $ageGroupName ungültig !!!</p>";
                        assert(false);
                        return false;
                    }
                }
                else
                {
                    echo "<p class='yellow'>Fehler: Lauf ungültig !!!</p>";
                    assert(false);
                    return false;
                }
            }
            else
            {
                $this->generateAgeGroupFromYearAndRun();
            }

            $this->setManualTimeFromString($fields[15]);

            return true;
        }
        else
        {
            echo "<p class='yellow'>Fehler: Entry ungültig !!!</p>";
            assert(false);
            return false;
        }
    }

    public function isValid(): bool
    {
        // m_startnr, m_chipnr, m_email and m_team can be empty
        return $this->m_firstname != "" &&
               $this->m_lastname != "" &&
               $this->m_year != self::INVALID_YEAR &&
               $this->m_gender != "" &&
               $this->m_run != Runs::INVALID_RUN &&
               $this->m_key != "" &&
               $this->m_date != "";
    }

    public function isEqual($other): bool
    {
        return
            $this->m_firstname == $other->m_firstname &&
            $this->m_lastname  == $other->m_lastname &&
            $this->m_year      == $other->m_year &&
            $this->m_gender    == $other->m_gender &&
            $this->m_team      == $other->m_team &&
            $this->m_run       == $other->m_run;
    }

    public function isEqualForAllEditableAttributes($other): bool
    {
        return
            $this->isEqual($other) &&
            $this->m_email == $other->m_email &&
            $this->m_cup == $other->m_cup &&
            ($this->getStartnr() == $other->getStartnr()) &&
            ($this->getChipnr() == $other->getChipnr()) &&
            $this->getAgeGroupWhenNotGenerated() == $other->getAgeGroupWhenNotGenerated() &&
            $this->getManualTime()->get() == $other->getManualTime()->get() &&
            $this->m_useManualTime == $other->m_useManualTime;
    }

    public function getAgeGroupWhenNotGenerated(): ?AgeGroup
    {
        if ($this->m_ageGroupIsGenerated)
        {
            return null;
        }
        else
        {
            return $this->m_ageGroup;
        }
    }

    public function setDeleted()
    {
        $this->m_deleted = true;
    }

    public function isDeleted(): bool
    {
        return $this->m_deleted;
    }

    public function isFirstnameOk(): bool
    {
        return $this->m_firstname != "";
    }

    public function isLastnameOk(): bool
    {
        return $this->m_lastname != "";
    }

    public function isYearOk(): bool
    {
        $minRegistrationYear = Runs::GetInstance()->getMinYear();
        $maxRegistrationYear = Runs::GetInstance()->getMaxYear();
        return $this->m_year >= $minRegistrationYear && $this->m_year <= $maxRegistrationYear;
    }

    public function isGenderOk(): bool
    {
        return preg_match('/^[mw]$/', $this->m_gender) == 1;
    }

    public function isRunOk(): bool
    {
        return $this->m_run >= 0 && $this->m_run < Runs::GetInstance()->getNumberOfRuns();
    }

    public function isStartnrOk(): bool
    {
        $minStartNumber = Config::Get()['startNumbers']['min'];
        $maxStartNumber = Config::Get()['startNumbers']['max'];
        return
            ($this->m_startnr == self::INVALID_STARTNR) ||
            (($this->m_startnr >= $minStartNumber) && ($this->m_startnr <= $maxStartNumber));
    }

    public function isChipnrOk(): bool
    {
        $minChipNumber = Config::Get()['chipNumbers']['min'];
        $maxChipNumber = Config::Get()['chipNumbers']['max'];
        return
            ($this->m_chipnr == self::INVALID_CHIPNR) ||
            (($this->m_chipnr >= $minChipNumber) && ($this->m_chipnr <= $maxChipNumber));
    }

    public function isEmailOk(bool $allowEmptyEmail): bool
    {
        if ($allowEmptyEmail && $this->m_email == "" )
        {
            return true;
        }
        // RegEx begin
        $nonascii = "\x80-\xff"; // Non-ASCII-Chars are not allowed

        $nqtext = "[^\\\\$nonascii\015\012\"]";
        $qchar = "\\\\[^$nonascii]";

        $protocol = '(?:mailto:)';

        $normuser = '[a-zA-Z0-9][a-zA-Z0-9_.-]*';
        $quotedstring = "\"(?:$nqtext|$qchar)+\"";
        $user_part = "(?:$normuser|$quotedstring)";

        $dom_mainpart = '[a-zA-Z0-9][a-zA-Z0-9._-]*\\.';
        $dom_subpart = '(?:[a-zA-Z0-9][a-zA-Z0-9._-]*\\.)*';
        $dom_tldpart = '[a-zA-Z]{2,5}';
        $domain_part = "$dom_subpart$dom_mainpart$dom_tldpart";

        $regex = "$protocol?$user_part\@$domain_part";
        // RegEx end

        return preg_match("/^$regex$/", $this->m_email) == 1;
    }

    public function getStartnrAsString(): string
    {
        if ($this->m_startnr == self::INVALID_STARTNR)
        {
            return "---";
        }
        else
        {
            return (string) $this->m_startnr;
        }
    }

    public function getChipnrAsString(): string
    {
        if ($this->m_chipnr == self::INVALID_CHIPNR)
        {
            return "---";
        }
        else
        {
            return (string) $this->m_chipnr;
        }
    }

    public function setDatasetTime(Time $time)
    {
        $this->m_datasetTime = clone $time;
    }

    public function getDatasetTime(): Time
    {
        return $this->m_datasetTime;
    }

    public function getManualTime(): Time
    {
        return $this->m_manualTime;
    }

    public function getTime(): Time
    {
        if ($this->m_useManualTime)
        {
            return $this->m_manualTime;
        }
        else
        {
            return $this->m_datasetTime;
        }
    }

    public function getTimeAsString(): string
    {
        $timeString = $this->getTime()->getAsString();
        if ($this->m_useManualTime)
        {
            $timeString .= " (M)";
        }
        return $timeString;
    }

    public function getManualTimeAsString(): string
    {
        if ($this->m_useManualTime)
        {
            return $this->m_manualTime->getAsString();
        }
        else
        {
            return "";
        }
    }

    public function isManualTimeOk(): bool
    {
        return $this->m_manualTimeOk;
    }

    function setPlacing(int $placing)
    {
        $this->m_placing = $placing;
    }

    function getPlacing(): int
    {
        return $this->m_placing;
    }

    function getPlacingAsString(): string
    {
        return self::ConvertPlacingToString($this->m_placing);
    }

    function setPlacingMW(int $placing)
    {
        $this->m_placingMW = $placing;
    }

    function getPlacingMW(): int
    {
        return $this->m_placingMW;
    }

    function getPlacingMWAsString(): string
    {
        return self::ConvertPlacingToString($this->m_placingMW);
    }

    function setPlacingAgeGroup(int $placing)
    {
        $this->m_placingAgeGroup = $placing;
    }

    function getPlacingAgeGroup(): int
    {
        return $this->m_placingAgeGroup;
    }

    function getPlacingAgeGroupAsString(): string
    {
        return self::ConvertPlacingToString($this->m_placingAgeGroup);
    }

    function setPlacingTeam(int $placing)
    {
        $this->m_placingTeam = $placing;
    }

    function getPlacingTeam(): int
    {
        return $this->m_placingTeam;
    }

    function getPlacingTeamAsString(): string
    {
        return self::ConvertPlacingToString($this->m_placingTeam);
    }


    function getLastnameFirstname(): string
    {
        return $this->m_lastname . ", " . $this->m_firstname;
    }

    static function GetTypeString(): string
    {
        return "Entry";
    }

    static function GetAttributeName(int $attribute): string
    {
        static $attributeNames = array(
            EntryAttribute::Index => "Index",
            EntryAttribute::Lastname => "Nachname",
            EntryAttribute::Firstname => "Vorname",
            EntryAttribute::Year => "Jahrgang",
            EntryAttribute::Gender => "Geschlecht",
            EntryAttribute::Team => "Verein oder Ort",
            EntryAttribute::Run => "Wettbewerb",
            EntryAttribute::Email => "E-Mail",
            EntryAttribute::Startnr => "Startnr.",
            EntryAttribute::Chipnr => "Chipnr.",
            EntryAttribute::Code => "Code",
            EntryAttribute::Revision => "Revision",
            EntryAttribute::Date => "Datum",
            EntryAttribute::Username => "Benutzer",
            EntryAttribute::Cup => "Cup",
            EntryAttribute::AgeGroup => "Altersklasse",
            EntryAttribute::PreRegistration => "Voranmeldung",
            EntryAttribute::Time => "Zeit",
            EntryAttribute::Placing => "Platz",
            EntryAttribute::PlacingMW => "Platz MW",
            EntryAttribute::PlacingAgeGroup => "Platz AK",
            EntryAttribute::PlacingTeam => "Platz Mannschaft",
        );

        return $attributeNames[$attribute];
    }

    static function GetAttributeShortName(int $attribute): string
    {
        static $attributeShortNames = array(
            EntryAttribute::Index => "Index",
            EntryAttribute::Lastname => "Nachname",
            EntryAttribute::Firstname => "Vorname",
            EntryAttribute::Year => "Jg.",
            EntryAttribute::Gender => "M/W",
            EntryAttribute::Team => "Verein/Ort",
            EntryAttribute::Run => "Wettb.",
            EntryAttribute::Email => "E-Mail",
            EntryAttribute::Startnr => "Stnr.",
            EntryAttribute::Chipnr => "Chnr.",
            EntryAttribute::Code => "Code",
            EntryAttribute::Revision => "Revision",
            EntryAttribute::Date => "Datum",
            EntryAttribute::Username => "Benutzer",
            EntryAttribute::Cup => "Cup",
            EntryAttribute::AgeGroup => "AK",
            EntryAttribute::PreRegistration => "VA",
            EntryAttribute::Time => "Zeit",
            EntryAttribute::Placing => "Platz",
            EntryAttribute::PlacingMW => "Platz MW",
            EntryAttribute::PlacingAgeGroup => "Platz AK",
            EntryAttribute::PlacingTeam => "Platz Ma.",
        );

        return $attributeShortNames[$attribute];
    }

    private function setFirstname(string $firstname)
    {
        $this->m_firstname = self::FilterInput($firstname);
    }

    private function setLastname(string $lastname)
    {
        $this->m_lastname = self::FilterInput($lastname);
    }

    private function setTeam(string $team)
    {
        $this->m_team = self::FilterInput($team);
    }

    private function generateAgeGroupFromYearAndRun()
    {
        if ($this->m_year != self::INVALID_YEAR && $this->m_run != Runs::INVALID_RUN)
        {
            $ageGroup = Runs::GetInstance()->getRun($this->m_run)->getAgeGroups()->getAgeGroupFromYear($this->m_year);
            if ($ageGroup != null)
            {
                $this->m_ageGroup = $ageGroup;
                $this->m_ageGroupIsGenerated = true;
            }
        }
    }

    private function setAgeGroupAndRunFromCompetitionAndAgeGroupName(int $competition, string $ageGroupName)
    {
        $runs = Runs::GetInstance()->getRuns();
        foreach ($runs as $run)
        {
            if ($run->getCompetition() == $competition)
            {
                $ageGroup = $run->getAgeGroupByName($ageGroupName);
                if ($ageGroup != null)
                {
                    $this->m_ageGroup = $ageGroup;
                    $this->m_ageGroupIsGenerated = false;
                    $this->m_run = $run->getIndex();
                    return;
                }
            }
        }
    }

    private function setAgeGroupAndRunFromCompetitionAndYear(int $competition, int $year)
    {
        $runs = Runs::GetInstance()->getRuns();
        foreach ($runs as $run)
        {
            if ($run->getCompetition() == $competition)
            {
                $ageGroup = $run->getAgeGroupForYear($year);
                if ($ageGroup != null)
                {
                    $this->m_ageGroup = $ageGroup;
                    $this->m_ageGroupIsGenerated = true;
                    $this->m_run = $run->getIndex();
                    return;
                }
            }
        }
    }

    private function setAgeGroupAndRunFromYear(int $year)
    {
        $runs = Runs::GetInstance()->getRuns();
        foreach ($runs as $run)
        {
            $ageGroup = $run->getAgeGroupForYear($year);
            if ($ageGroup != null)
            {
                $this->m_ageGroup = $ageGroup;
                $this->m_ageGroupIsGenerated = true;
                $this->m_run = $run->getIndex();
                return;
            }
        }
    }

    private function setRunFromString(string $runString)
    {
        if (is_numeric($runString))
        {
            $this->m_run = intval($runString);
        }
        else
        {
            $this->m_run = Runs::INVALID_RUN;
        }
    }

    private function setCupFromString(string $cupString)
    {
        if (is_numeric($cupString))
        {
            $this->m_cup = intval($cupString) == 1;
        }
        else
        {
        	$this->m_cup = false;
        }
    }

    private function setPreRegistrationFromString(string $preRegistrationString)
    {
        if (is_numeric($preRegistrationString))
        {
            $this->m_preRegistration = intval($preRegistrationString) == 1;
        }
        else
        {
            $this->m_preRegistration = false;
        }
    }

    private static function FilterInput(string $input): string
    {
        // Filter new line, return, form feed, tabulator, semicolon away
        $result = preg_replace("/\n|\r|\f|\t|;/", "", $input);

        // Filter multiple spaces away
        $result = preg_replace('/\s+/', ' ', $result);

        // Remove spaces at beginning and end
        return trim($result);
    }

    private function setManualTimeFromString(string $timeString)
    {
        if ($timeString == "")
        {
            $this->m_manualTimeOk = true;
            $this->m_useManualTime = false;
            $this->m_manualTime->set(Time::INVALID_TIME);
        }
        else
        {
            if ($timeString == "---")
            {
                $this->m_manualTimeOk = true;
                $this->m_useManualTime = true;
                $this->m_manualTime->set(Time::INVALID_TIME);
            }
            else
            {
                $this->m_manualTimeOk = $this->m_manualTime->setFromString($timeString);
                $this->m_useManualTime = $this->m_manualTime->isValid();
            }
        }
    }

    private static function ConvertPlacingToString(int $placing): string
    {
        if ($placing != self::INVALID_PLACING)
        {
            return (string)$placing;
        }
        else
        {
            return "---";
        }
    }

    const INVALID_YEAR = -1;
    const INVALID_STARTNR = -1;
    const INVALID_CHIPNR = -1;
    const INVALID_PLACING = -1;

    private $m_firstname = "";
    private $m_lastname = "";
    private $m_year = self::INVALID_YEAR;
    private $m_gender = "";
    private $m_team = "";
    private $m_run = Runs::INVALID_RUN;
    private $m_email = "";
    private $m_cup = false;
    private $m_startnr = self::INVALID_STARTNR;
    private $m_chipnr = self::INVALID_CHIPNR;
    private $m_key = "";
    private $m_date = "";
    private $m_deleted = false;
    private $m_index  = 0;
    private $m_revision = 0;
    private $m_username = "";
    private $m_ageGroup = null;
    private $m_ageGroupIsGenerated = true;
    private $m_preRegistration = false;
    private $m_manualTime = null;
    private $m_manualTimeOk = true;
    private $m_useManualTime = false;
    private $m_datasetTime = null;
    private $m_placing = self::INVALID_PLACING;
    private $m_placingMW = self::INVALID_PLACING;
    private $m_placingAgeGroup = self::INVALID_PLACING;
    private $m_placingTeam = self::INVALID_PLACING;

}
