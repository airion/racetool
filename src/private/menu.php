<?php declare(strict_types = 1);

class MenuEntry
{
    function __construct(string $title, string $post, string $id = "", bool $enabled = true, string $hoverText = "")
    {
        $this->m_title = $title;
        $this->m_post = $post;
        $this->m_id = $id;
        $this->m_enabled = $enabled;
        $this->m_hoverText = $hoverText;
    }

    function getTitle(): string
    {
        return $this->m_title;
    }

    function getPost(): string
    {
        return $this->m_post;
    }

    function getEnabled(): bool
    {
        return $this->m_enabled;
    }

    function getId(): string
    {
        return $this->m_id;
    }

    function getHoverText(): string
    {
        return $this->m_hoverText;
    }

    private $m_title;
    private $m_post;
    private $m_enabled;
    private $m_id;
    private $m_hoverText;

};

interface IMenuWidget
{
    function output();
}

class MenuEntriesWidget implements IMenuWidget
{
    function __construct(array $menuEntries)
    {
        $this->m_menuEntries = $menuEntries;
    }

    function output()
    {
        echo "<div class='menuContent'>";
        foreach($this->m_menuEntries as $menuEntry)
        {
            $entryPost = "hideOpenDropdown();".$menuEntry->getPost();
            $entryTitle = $menuEntry->getTitle();
            $disabledString = $menuEntry->getEnabled() ? "" : "disabled";
            $id = $menuEntry->getId();
            $idString = $id != "" ? "id='$id'" : "";
            $entryHoverText = $menuEntry->getHoverText();
            echo "<button title='$entryHoverText' onclick=\"$entryPost\" onMouseDown=\"event.preventDefault();\" $idString $disabledString>$entryTitle</button>";
        }
        echo "</div>";
    }
    private $m_menuEntries = [];
}

class Menu
{
    static function OutputButton(string $icon, string $title, string $post, string $id = "", bool $enabled = true)
    {
        self::OutputButtonInternal($icon, $title, $post, null, $enabled, $id);
    }

    static function OutputButtonWithMenu(string $icon, string $title, array $menuEntries, string $id = "", bool $enabled = true)
    {
        self::OutputButtonWithMenuWidget($icon, $title, new MenuEntriesWidget($menuEntries), $id, $enabled);
    }

    static function OutputButtonWithMenuWidget(string $icon, string $title, IMenuWidget $dialog, string $id = "", bool $enabled = true)
    {
        self::OutputButtonInternal($icon, $title, "buttonClicked(this)", $dialog, $enabled, $id);
    }

    static function OutputSpacer(float $space = 2.0)
    {
        echo "<div style='padding-right: $space" . "em;display: inline;'>&nbsp;</div>";
    }

    static function OutputFiller()
    {
        echo "<div class='flexAuto'></div>";
    }

    static function OutputBackground()
    {
        echo "<div class='menuBackground' id='menuBackground' onclick='hideOpenDropdown();'></div>";
    }

    private static function OutputButtonInternal(string $icon, string $title, string $post, ?IMenuWidget $menuWidget, bool $enabled, string $id)
    {
        echo "<div style='display: inline;'>"; // Needed for IE, otherwise buttons overlap when window to small
        $disabledString = $enabled ? "" : "disabled";
        $idString = $id != "" ? "id='$id'" : "";
        echo "<button title='$title' onclick=\"$post\" onMouseDown=\"event.preventDefault();\" class='menuButton fas $icon' $idString $disabledString></button>";

        if ($menuWidget != null)
        {
            $menuWidget->output();
        }
        echo "</div>";
    }

    static function GetStyle(): string
    {
        $style = <<<EOD
        .menuButton
        {
            color: white;
            background-color: transparent;
            position: relative; /* Needed for IE-10 */
            z-index: 100;
            vertical-align: middle;
            text-shadow: 4px 4px 2px rgba(0,0,0,0.2);
            font-size: 1.8em;
            cursor: pointer;
            border: none;
            padding: 0.2em 0.2em 0.2em 0em;
            margin-right: 0.3em;
        }

        .menuButton:hover
        {
            color: yellow;
        }

        .menuButton:disabled
        {
            color: grey;
            cursor: default;
        }

        .menuButtonRed
        {
            color: red;
        }

        .menuButtonOrange
        {
            color: orange;
        }

        .menuBackground
        {
            display: none;
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: rgba(0,0,0,0.4);
            z-index: 99;
        }

        .menuContent
        {
          display: none;
          position: fixed;
          background-color: #702020;
          box-shadow: 0px 0px 6px 2px rgba(0,0,0,0.9);
          z-index: 100;
          text-color: #400;
          padding: 0.2em;
        }

        .menuContent button
        {
          color: white;
          padding: 4px 16px;
          display: block;
          cursor: pointer;
          background-color: transparent;
          border: none;
          width: 100%;
          text-align: left;
          white-space: nowrap;
        }

        .menuContent button:hover
        {
            background-color: #d40;
        }

        .menuContent button:disabled
        {
            color: grey;
            cursor: default;
        }
EOD;

        return $style;
    }
}
