<?php declare(strict_types = 1);

include_once 'private/page.php';
include_once 'private/registrationFields.php';
include_once 'private/entry.php';
include_once "private/runs.php";
include_once 'private/jsFunctions.php';
include_once 'private/outputCompetitionTable.php';

class PreRegistrationPage extends Page
{
    function __construct()
    {
        parent::__construct();
        $this->enableLoginButton();

        $javaScript = "";

        $closingTimeString = Config::Get()['event']['closingTime'];
        $closingTime = DateTime::createFromFormat("d.m.Y H:i:s", $closingTimeString);
        $timeStamp = $closingTime->getTimestamp();

        $currentTime = new DateTime();
        $remainingTime = $timeStamp - $currentTime->getTimestamp();

        if ($remainingTime > 0)
        {
            $javaScript .= <<<EOD
            function updateRemainingTime()
            {
                var currentTimeStamp = Math.floor(Date.now() / 1000);

                var remainingTime = $timeStamp - currentTimeStamp;
                if (remainingTime < 0)
                {
                    remainingTime = 0;
                    location.reload();
                }
                var seconds = remainingTime;
                var minutes = Math.floor(remainingTime / 60);
                seconds -= minutes * 60;
                var hours = Math.floor(minutes / 60);
                minutes -= hours * 60;
                var days = Math.floor(hours / 24);
                hours -= days * 24;

                var timeString = days;

                if (days == 1)
                {
                    timeString += " Tag ";
                }
                else
                {
                    timeString += " Tagen ";
                }
                timeString += hours + " Std " + minutes + " Min " + seconds + " Sek";

                var remainingTimeSpan = document.getElementById('remainingTimeSpan');

                remainingTimeSpan.innerHTML = timeString;
            }
            window.onload = updateRemainingTime;
            window.setInterval(updateRemainingTime, 1000);
EOD;
        }

        $programMode = Config::Get()['program']['mode'];

        $javaScript .= getJSFunction_post();
        $javaScript .= getJSFunction_getElementValue();

        $style = <<<EOD
                .infoTable
                {
                    border-spacing:0em;
                }
                .infoTable tr td {padding-left: 0em; font-size: 0.7em;}
                .infoTable tr th {padding-left: 0em; font-size: 1.0em;}
                .infoTable tr td,  .infoTable tr th {padding-right: 2em;}

EOD;

        $style .= getRegistrationFormStyle();

        $remainingTime = $this->outputHeader($javaScript, $style);

        if ($remainingTime > 0)
        {
            $this->preRegistrationForm();
        }
        else
        {
            $this->outputAnouncementLink();
        }

        if ($programMode == ProgramMode::Finished)
        {
            $this->finishedInfo();
        }

        $this->beginFooter();

        if (($programMode == ProgramMode::PreRegistration) || ($programMode == ProgramMode::PreRegistrationValidatedAndFinished) || ($programMode == ProgramMode::Finished))
        {
            Menu::OutputButton("fa-users", "Teilnehmerstand", $this->getPostString("showlist.php"), "showList");
        }

        if (($programMode == ProgramMode::PreRegistration) && ($remainingTime > 0))
        {
            Menu::OutputButton("fa-user-edit", "Anmeldung ändern", $this->getPostString("editEntryInputKey.php"), "changeRegistration");
        }

        if ($this->m_access->hasAccess(AccessRight::Read))
        {
            Menu::OutputSpacer();
            Menu::OutputButton("fa-door-open", "Administrativer Bereich", $this->getPostString("admin.php"), "admin");
        }
        $this->endFooter();
    }

    function outputAnouncementText()
    {
        $anouncementText = Config::Get()['event']['anouncementText'];
        if ($anouncementText != "")
        {
            echo "$anouncementText<br>";
        }
    }

    function outputAnouncementLink()
    {
        $anouncementLink = Config::Get()['event']['anouncementLink'];
        $announcementName = Config::Get()['event']['announcementName'];
        echo "<a href='$anouncementLink'>$announcementName</a>";
    }

    function preRegistrationForm()
    {
        $eventNameShort = Config::Get()['event']['nameShort'];

        switch (Config::Get()['event']['nameGenus'])
        {
            case "n":
            case "m":
                $article = "zum";
                break;
            case "f":
                $article = "zur";
                break;
        }

        echo "<p style='color:yellow'>";
        echo "<span>Anmeldeschluss in </span>";
        echo "<span id='remainingTimeSpan'></span>";
        echo "<span> ...</span>";
        echo "</p>";

        echo "<p>";
        $this->outputAnouncementText();
        $this->outputAnouncementLink();
        echo "</p>";

        echo "<div class='displayFlex flexWrap' style='align-items: flex-start;'>";
        outputCompetitionTable();
        outputPriceTable(true);
        echo "</div>";

        $entry = new Entry();

        $registrationFieldsEnabled = Config::Get()['program']['mode'] == ProgramMode::PreRegistration;
        $showCup = false;
        $showEmail = true;
        $showStartnr = false;
        $showChipnr = false;
        $showAgeGroups = false;
        $showTime = false;
        $checkYearRestrictive = true;
        $database = new Database();
        $database->close();

        $parameters = new Parameters();

        outputRegistrationFields($this, $entry, $parameters, "addEntry.php", $showCup, $showEmail, $showStartnr, $showChipnr, $showAgeGroups, $showTime, $registrationFieldsEnabled,
        $checkYearRestrictive, $database);
    }

    function finishedInfo()
    {
        $eventNameShort = Config::Get()['event']['nameShort'];

        switch (Config::Get()['event']['nameGenus'])
        {
            case "n":
            case "m":
                $article = "des";
                $eventNameShort .= "s";
                break;
            case "f":
                $article = "der";
                break;
        }
        $post = $this->getPostString("showlist.php");
        echo "<p style='color:yellow'>Herzlichen Dank an alle Teilnehmer $article $eventNameShort! Die Laufergebnisse finden Sie unter: ";
        echo "<button type='button' class='link' onclick=\"$post\">Teilnehmerstand</button>";
        echo "<br></p>";
    }
}
