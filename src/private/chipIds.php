<?php declare(strict_types=1);

include_once 'private/config.php';

class ChipIds
{
    private static $_instance = null;

    public static function GetInstance(): ChipIds
    {
        if (null === self::$_instance)
        {
            self::$_instance = new self;
        }
        return self::$_instance;
    }

    private function __clone() {}
    private function __construct()
    {
        $fileName = "res/chiplist.csv";
        $f = @fopen($fileName, "r");
        if (!$f)
        {
            echo "<p class='yellow'>Konnte Datei mit Chip Ids $fileName nicht öffnen !</p>";
            return;
        }
        while (!feof($f))
        {
            $line = fgets($f);
            if ($line)
            {
                $line = str_replace(array("\r", "\n"), '', $line);
                $fields = explode(";", $line);
                $numberField = count($fields);
                if ($numberField == 2)
                {
                    if (is_numeric($fields[0]))
                    {
                        $number = intval($fields[0]);
                        $id = $fields[1];

                        $this->m_idsByNumber[$number] = $id;
                        $this->m_numbersById[$id] = $number;
                    }
                }
            }
        }
        $this->checkIfConsistent();
    }

    public function isOk(): bool
    {
        return $this->m_ok;
    }

    private function checkIfConsistent()
    {
        $minChipNumber = Config::Get()['chipNumbers']['min'];
        $maxChipNumber = Config::Get()['chipNumbers']['max'];

        $usedIds = array();
        for ($i = $minChipNumber; $i <= $maxChipNumber; $i++)
        {
            if (!array_key_exists($i, $this->m_idsByNumber))
            {
                echo "<p class='yellow'>Fehlende Chip Id für Chip mit Nummer $i!</p>";
                return;
            }
            $id = $this->m_idsByNumber[$i];
            if (array_key_exists($id, $usedIds))
            {
                echo "<p class='yellow'>Chip Id $id wird mehrfach benutzt!</p>";
                return;
            }

            $usedIds[$id] = 1;
        }
        $this->m_ok = true;
    }

    function getIdForNumber(int $number): string
    {
        if (array_key_exists($number, $this->m_idsByNumber))
        {
            return $this->m_idsByNumber[$number];
        }
        else
        {
            return "";
        }
    }

    function getNumberForId(string $id): int
    {
        if (array_key_exists($id, $this->m_numbersById))
        {
            return $this->m_numbersById[$id];
        }
        else
        {
            return Entry::INVALID_CHIPNR;
        }
    }

    private $m_idsByNumber = array();
    private $m_numbersById = array();
    private $m_ok = false;
}
