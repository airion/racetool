<?php declare(strict_types=1);

include_once "private/errorHandler.php"; // First line, to init error handling before anything else

class Post
{
    static function GetIntValueFromPost(string $name, int $defaultValue = 0): int
    {
        if (isset($_POST[$name]))
        {
            return intval($_POST[$name]);
        }
        else
        {
            return $defaultValue;
        }
    }

    static function GetStringValueFromPost(string $name, string $defaultValue = ""): string
    {
        if (isset($_POST[$name]))
        {
            return $_POST[$name];
        }
        else
        {
            return $defaultValue;
        }
    }

    static function GetBoolValueFromPost(string $name, bool $defaultValue = false): bool
    {
        if (isset($_POST[$name]))
        {
            return boolval($_POST[$name]);
        }
        else
        {
            return $defaultValue;
        }
    }
}
