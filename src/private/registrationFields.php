<?php declare(strict_types=1);

include_once 'private/page.php';
include_once 'private/agegroups.php';
include_once 'private/runs.php';
include_once 'private/convertStringToHTML.php';
include_once 'private/getTeamList.php';
include_once 'private/database.php';
include_once 'private/programMode.php';
include_once 'private/post.php';
include_once 'private/getMinMaxYear.php';

function getRegistrationFormStyle(): string
{
    if (Config::Get()['program']['mode'] == ProgramMode::Registration)
    {
        $maxWidth = "700px";
        $radioButtonAlign = "baseline";
    }
    else
    {
        $maxWidth = "500px";
        $radioButtonAlign = "top";
    }

    $style = <<<EOD
    .registrationForm
    {
        border-spacing:0em;
        max-width: $maxWidth;
        margin: 0;
        padding: 0;
        /*border: 1px solid Black;*/
    }
    .registrationForm th, .registrationForm td
    {
        padding-bottom: 0.5em;
        /*border: 1px solid Black;*/
    }
    .registrationForm th {padding-right: 1em; text-align: left; white-space: nowrap;}
    .registrationForm input[type="radio"]{vertical-align: $radioButtonAlign;}

    /* The switch - the box around the slider */
    .switch
    {
        position: relative;
        display: inline-block;
        width: 30px;
        height: 17px;
        vertical-align: middle;
    }

    /* Hide default HTML checkbox */
    .switch input {display:none;}

    /* The slider */
    .slider
    {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
        border-radius: 17px;
        background-color: #505050;
    }

    .slider:before
    {
        position: absolute;
        content: "";
        height: 13px;
        width: 13px;
        left: 2px;
        bottom: 2px;
        background-color: grey;
        -webkit-transition: .4s;
        transition: .4s;
        border-radius: 50%;
    }

    input:checked + .slider
    {
        background-color: orange;
    }

    input:focus + .slider
    {
        box-shadow: 0 0 1px orange;
    }

    input:checked + .slider:before
    {
        -webkit-transform: translateX(13px);
        -ms-transform: translateX(13px);
        transform: translateX(13px);
        background-color: white;
    }
EOD;
    return $style;
}

function getRegistrationFormJS(): string
{
    $js = <<<EOD
function editStartNrButtonClicked()
{
    var button = document.getElementById("editStartNrButton");
    var active = button.checked;
    var inputField = document.getElementById("startnr");
    inputField.disabled = !active;
}

function editChipNrButtonClicked()
{
    var button = document.getElementById("editChipNrButton");
    var active = button.checked;
    var inputField = document.getElementById("chipnr");
    inputField.disabled = !active;
}

function resetButtons()
{
    var startNrButton = document.getElementById("editStartNrButton");
    if (startNrButton != null)
    {
        startNrButton.checked = false;
        var startNrInputField = document.getElementById("startnr");
        if (startNrInputField != null)
        {
            startNrInputField.disabled = true;
        }
    }

    var chipNrButton = document.getElementById("editChipNrButton");
    if (chipNrButton != null)
    {
        chipNrButton.checked = false;
        var chipNrInputField = document.getElementById("chipnr");
        if (chipNrInputField != null)
        {
            chipNrInputField.disabled = true;
        }
    }
}

window.onload = resetButtons;

EOD;
    return $js;
}

function sortAgeGroups(AgeGroup $a, AgeGroup $b): int
{
    return $a->getMinAge() - $b->getMinAge();
}

function getAllAgeGroupNames(): array
{
    $runs = Runs::GetInstance()->getRuns();
    $ageGroups = array();
    foreach ($runs as $run)
    {
        foreach ($run->getAgeGroups()->getAgeGroups() as $ageGroup)
        {
            array_push($ageGroups, $ageGroup);
        }
    }
    usort($ageGroups, "sortAgeGroups");
    $ageGroupNames = array();
    foreach($ageGroups as $ageGroup)
    {
        $ageGroupName = $ageGroup->getName();
        $ageGroupNames[$ageGroupName] = 1;
    }

    return array_keys($ageGroupNames);
}

function outputRegistrationFields(Page $page, Entry $entry, Parameters $parameters, string $pageToOpen, bool $showCup, bool $showEmail, bool $showStartnr, bool $showChipnr, bool $showAgeGroups, bool $showTime, bool $enabled, bool $checkYearRestrictive, Database $database)
{
    $minRegistrationYear = getMinYear($checkYearRestrictive);
    $maxRegistrationYear = getMaxYear($checkYearRestrictive);

    $maxCharacter = 60;

    $disabled = "";
    if (!$enabled)
    {
        $disabled = 'disabled';
    }

    $takeValuesFromPost = Post::GetBoolValueFromPost("takeValuesFromPost");

    $competitions = Runs::GetInstance()->getCompetitionNames();

    if ($takeValuesFromPost)
    {
        $firstnameValue = Post::GetStringValueFromPost("firstname");
        $lastnameValue = Post::GetStringValueFromPost("lastname");
        $competitionValue = Post::GetIntValueFromPost("competition", RUNS::INVALID_COMPETITION);
        $yearValue = Post::GetStringValueFromPost("year");
        $ageGroupValue = Post::GetStringValueFromPost("agegroup", "auto");
        $genderValue = Post::GetStringValueFromPost("gender");
        $teamValue = Post::GetStringValueFromPost("team");
        $emailValue = Post::GetStringValueFromPost("email");
        $startnrValue = Post::GetStringValueFromPost("startnr");
        $chipnrValue = Post::GetStringValueFromPost("chipnr");
        $cupValue = Post::GetStringValueFromPost("cup");
        $timeValue = Post::GetStringValueFromPost("time");
    }
    else
    {
        $firstnameValue = $entry->getFirstname();
        ///
        $lastnameValue = $entry->getLastname();
        ///
        $currentRun = $entry->getRun();
        if ($currentRun != Runs::INVALID_RUN)
        {
            $competitionValue = Runs::GetInstance()->getRun($currentRun)->getCompetition();
        }
        else
        {
            if (count($competitions) == 1)
            {
                $competitionValue = 0;
            }
            else
            {
                $competitionValue = RUNS::INVALID_COMPETITION;
            }
        }
        ///
        $yearValue = $entry->getYearAsString();
        ///
        $ageGroupValue = "auto";
        if (!$entry->getAgeGroupIsGenerated())
        {
            $ageGroup = $entry->getAgeGroup();
            if ($ageGroup != null)
            {
                $ageGroupValue = $ageGroup->getName();
            }
        }
        ///
        $genderValue = $entry->getGender();
        ///
        $teamValue = $entry->getTeam();
        ///
        $emailValue = $entry->getEmail();
        ///
        $startNr = $entry->getStartnr();
        if ($startNr != Entry::INVALID_STARTNR)
        {
            $startnrValue = (string)$startNr;
        }
        else
        {
            $startnrValue = "";
        }
        ///
        $chipNr = $entry->getChipnr();
        if ($chipNr != Entry::INVALID_CHIPNR)
        {
            $chipnrValue = (string)$chipNr;
        }
        else
        {
            $chipnrValue = "";
        }
        ///
        $cupValue = $entry->getCup() ? "on" : "";
        ///
        $timeValue = $entry->getManualTimeAsString();
    }

    if (!$showAgeGroups)
    {
        $parameters->add("agegroup", $ageGroupValue);
    }

    if (!$showEmail)
    {
        $parameters->add("email", $emailValue);
    }

    if (!$showStartnr)
    {
        $parameters->add("startnr", $startnrValue);
    }

    if (!$showChipnr)
    {
        $parameters->add("chipnr", $chipnrValue);
    }

    if (!$showCup)
    {
        $parameters->add("cup", $cupValue);
    }

    if (!$showTime)
    {
        $parameters->add("time", $timeValue);
    }

    $page->beginForm($pageToOpen, $parameters);

    echo "<table class='registrationForm'>";

    echo "<tr>";
    echo "<th>Vorname</th>";
    echo "<td class='fullwidth'><input class='fullwidth' id='firstname' name='firstname' maxlength='$maxCharacter' value='" . convertStringToHTML($firstnameValue) . "' $disabled></td>";
    echo "</tr>";

    echo "<tr>";
    echo "<th>Nachname</th>";
    echo "<td class='fullwidth'><input class='fullwidth' id='lastname' name='lastname' maxlength='$maxCharacter' value='" . convertStringToHTML($lastnameValue) . "' $disabled></td>";
    echo "</tr>";

    echo "<tr>";
    echo "<th>Wettbewerb</th>";
    echo "<td class='fullwidth'><select class='fullwidth' id='competition' name='competition' $disabled>";

    echo "<option value='" . RUNS::INVALID_COMPETITION . "' disabled";
    if ($competitionValue == RUNS::INVALID_COMPETITION)
    {
        echo " selected";
    }
    echo "> -- Bitte auswählen -- </option>";
    foreach ($competitions as $index => $competitionName)
    {
        echo "<option value=\"$index\"";
        if ($index == $competitionValue)
        {
            echo " selected";
        }
        echo ">$competitionName</option>";
    }
    echo "</select>";
    echo "</td>";
    echo "</tr>";

    echo "<tr>";
    echo "<th>Jahrgang</th>";
    echo "<td class='fullwidth'><input class='fullwidth' id='year' name='year' type='number' min='$minRegistrationYear' max='$maxRegistrationYear' value='" . $yearValue . "' $disabled></td>";
    echo "</tr>";

    if ($showAgeGroups)
    {
        echo "<tr>";
        $ageGroupNames = getAllAgeGroupNames();
        echo "<th>Altersklasse</th>";
        echo "<td class='fullwidth'>";

        echo "<select id='agegroup' name='agegroup' class='fullwidth' $disabled> ";
        echo "<option value='auto'";
        if ($ageGroupValue == "auto")
        {
            echo ' selected="selected"';
        }
        echo ">-- automatisch --</option>";

        $gender = strtoupper($entry->getGender());
        foreach ($ageGroupNames as $ageGroupName)
        {
            echo "<option value='$ageGroupName'";
            if ($ageGroupName == $ageGroupValue)
            {
                echo ' selected="selected"';
            }
            echo ">" . $gender . $ageGroupName."</option>";
        }
        echo "</select></td>";
        echo "</tr>";
    }

    if ($genderValue == "m")
    {
        $checkedm = 'checked';
    }
    else
    {
        $checkedm = '';
    }

    if ($genderValue == "w")
    {
        $checkedw = 'checked';
    }
    else
    {
        $checkedw = '';
    }
    echo "<tr>";
    echo "<th>Geschlecht</th>";
    echo "<td>";
    echo "<input type='radio' id='male' name='gender' value='m' $checkedm $disabled/>";
    echo "<label for='male' style='margin-right: 1.0em;margin-left: 0.25em;'>männlich</label>";
    echo "<input type='radio' id='female' name='gender' value='w' $checkedw $disabled/>";
    echo "<label for='female' style='margin-right: 1.0em; margin-left: 0.25em;'>weiblich</label>";
    echo "</td>";
    echo "</tr>";

    echo "<tr>";
    echo "<th>Verein oder Ort</th>";
    // display: block needed for datalist polyfill
    echo "<td class='fullwidth'><input style='display: block;' type='text' class='fullwidth' id='team' name='team' maxlength='$maxCharacter' value='" . convertStringToHTML($teamValue) . "' list='teamList' autocomplete='off' $disabled>";

    echo "<datalist id='teamList'>";
    echo "<!--[if IE 9]><select disabled style='display:none' class='ie9_fix'><![endif]-->";
    $teamList = getTeamList($database);
    foreach ($teamList as $team)
    {
        if ($team != "")
        {
            $teamHTML = convertStringToHTML($team);
            echo "<option value='$teamHTML'>";
        }
    }
    echo "<!--[if IE 9]></select><![endif]-->";
    echo "</datalist>";

    echo "</td>";

    echo "</tr>";

    if ($showEmail)
    {
        echo "<tr>";
        echo "<th>E-Mail Adresse</th>";
        echo "<td class='fullwidth'><input class='fullwidth' id='email' name='email' type='email' maxlength='$maxCharacter'  value='" . convertStringToHTML($emailValue) . "' $disabled></td>";
        echo "</tr>";
    }

    if ($showStartnr)
    {
    	echo "<tr>";
        echo "<th>Startnr.</th>";
        $minStartnr = Config::Get()['startNumbers']['min'];
        $maxStartnr = Config::Get()['startNumbers']['max'];
        echo "<td class='fullwidth'>";
        echo "<div class='displayFlex alignItemsCenter'>";
        echo "<input class='flexAuto' id='startnr' name='startnr' type='number' min='$minStartnr' max='$maxStartnr' value='" . $startnrValue . "' disabled>";
        echo "<label id='editStartNrButtonLabel' class='switch' style='margin-left: 0.5em;'>";
        echo "<input type='checkbox' class='switch' id='editStartNrButton' onchange=\"editStartNrButtonClicked();\" $disabled>";
        echo "<span class='slider'> </span>";
        echo "</label>";
        echo "</div>";
        echo "</td>";
        echo "</tr>";
    }

    if ($showChipnr)
    {
        echo "<tr>";
        echo "<th>Chipnr.</th>";
        $minChipnr = Config::Get()['chipNumbers']['min'];
        $maxChipnr = Config::Get()['chipNumbers']['max'];
        echo "<td class='fullwidth'>";
        echo "<div class='displayFlex alignItemsCenter'>";
        echo "<input class='flexAuto' id='chipnr' name='chipnr' type='number' min='$minChipnr' max='$maxChipnr' value='" . $chipnrValue . "' disabled>";
        echo "<label id='editChipNrButtonLabel'class='switch' style='margin-left: 0.5em;'>";
        echo "<input type='checkbox' class='switch' id='editChipNrButton' onchange=\"editChipNrButtonClicked();\" $disabled>";
        echo "<span class='slider'> </span>";
        echo "</label>";
        echo "</div>";
        echo "</td>";
        echo "</tr>";
    }

    if ($showCup)
    {
        echo "<tr>";
        echo "<th>Cup Serienläufer</th>";
        echo "<td>";
        if ($cupValue == "on")
        {
            $cupString = "checked";
        }
        else
        {
            $cupString = "";
        }
        echo "<label style='vertical-align: middle; margin-right: 1.0em;'><input type='checkbox' id='cup' name='cup' $cupString $disabled></label>";
        echo "</td>";
        echo "</tr>";
    }

    if ($showTime)
    {
        echo "<tr>";
        echo "<th>Zeit</th>";
        echo "<td class='fullwidth'>";
        echo "<input class='fullwidth' id='time' name='time' maxlength='9'  value='" . $timeValue . "' $disabled title='( hh:mm:ss | h:mm:ss | mm:ss | m:ss | ss | s ) [a-z]\nDisqualifiziert:  ---'>";
        echo "</td>";
        echo "</tr>";
    }

    echo "<tr>";
    echo "<td colspan='2' class='fullwidth noPaddingBottom'>";
    echo "<button title='Anmeldedaten absenden' class='button noMarginRight' type='submit' style='float:right;' $disabled>Absenden <div class='fas fa-paper-plane'></div></button>";
    echo "</td>";
    echo "</tr>";

    echo "</table>";

    $page->endForm();
}
