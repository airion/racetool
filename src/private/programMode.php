<?php declare(strict_types=1);

class ProgramMode
{
    // Users, admin can enter/change pre registrations until end of pre-registration by time
    // Start numbers are dynamically updated and changed, but not written to database
    // Downloadable documents are marked as draft
    const PreRegistration = "PRE_REGISTRATION";

    // No more registrations/changes by user and admin
    // Downloadable documents are not marked as draft anymore
    // Online version is frozen, data tranferred to local pc, which continous in registration mode
    const PreRegistrationValidatedAndFinished = "PRE_REGISTRATION_VALIDATED_AND_FINISHED";

    // Offline mode for registrations at race day, protected by WLAN password
    // Enter new registrations, admin can change/delete registrations
    // Start numbers stored in database
    // New start numbers are automatically given: next free number
    const Registration = "REGISTRATION";

    // Event finished, showing competitors with results
    const Finished = "FINISHED";
};
