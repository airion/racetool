<?php declare(strict_types=1);
include_once 'private/entry.php';
include_once 'private/entrySorter.php';
include_once 'private/entries.php';
include_once 'private/team.php';
include_once 'private/runs.php';

class PlacingType
{
    const Placing = 0;
    const PlacingMW = 1;
    const PlacingAgeGroup = 2;
}

class Evaluation
{
    function __construct(array $entries)
    {
        $runs = Runs::GetInstance()->getNumberOfRuns();
        for ($run = 0; $run < $runs; $run++)
        {
            $this->m_teamResults[$run] = array("m" => array(), "w" => array());
        }

        Entries::RemoveEntriesWithInvalidTime($entries);

        $groups = Entries::SplitEntriesByRun($entries);
        foreach ($groups as $groupEntries)
        {
            assert(!empty($groupEntries));
            self::ComputePlacingForEntries($groupEntries, PlacingType::Placing);
        }

        $groups = Entries::SplitEntriesByRunAndGender($entries);
        foreach ($groups as $groupEntries)
        {
            assert(!empty($groupEntries));
            assert(self::CheckAllEntriesHaveSameRunAndGender($groupEntries));

            self::ComputePlacingForEntries($groupEntries, PlacingType::PlacingMW);
            self::ComputeAgeGroupPlacing($groupEntries);

            $runIndex = $groupEntries[0]->getRun();
            $run = Runs::GetInstance()->getRun($runIndex);
            if ($run->getTeamPlacing())
            {
                $this->computeTeamPlacing($groupEntries);
            }
        }
    }

    private static function CheckAllEntriesHaveSameRunAndGender(array $entries): bool
    {
        if (count($entries) < 2)
        {
            return true;
        }
        else
        {
            $firstEntry = null;
            foreach ($entries as $entry)
            {
                if ($firstEntry == null)
                {
                    $firstEntry = $entry;
                }
                else
                {
                    if ($entry->getRun() != $firstEntry->getRun() || $entry->getGender() != $firstEntry->getGender())
                    {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    private static function CheckAllEntriesHaveSameTeam(array $entries): bool
    {
        if (count($entries) < 2)
        {
            return true;
        }
        else
        {
            $firstEntry = null;
            foreach ($entries as $entry)
            {
                if ($firstEntry == null)
                {
                    $firstEntry = $entry;
                }
                else
                {
                    if ($entry->getTeam() != $firstEntry->getTeam())
                    {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    private static function ComputeAgeGroupPlacing(array $entries)
    {
        $groups = Entries::SplitEntriesByAgeGroups($entries);
        foreach ($groups as $groupEntries)
        {
            self::ComputePlacingForEntries($groupEntries, PlacingType::PlacingAgeGroup);
        }
    }

    private static function ComputePlacingForEntries(array $entries, int $placingType)
    {
        EntrySorter::Sort($entries, EntrySorter::OrderTimeLastnameIndex);
        $place = 1;
        $entryPlace = $place;
        $lastEntry = null;
        foreach ($entries as $entry)
        {
            if ($lastEntry != null)
            {
                if ($entry->getTime()->get() > $lastEntry->getTime()->get())
                {
                    $entryPlace = $place;
                }
            }

            switch ($placingType)
            {
                case PlacingType::Placing:
                    {
                        $entry->setPlacing($entryPlace);
                        break;
                    }
                case PlacingType::PlacingMW:
                    {
                        $entry->setPlacingMW($entryPlace);
                        break;
                    }
                case PlacingType::PlacingAgeGroup:
                    {
                        $entry->setPlacingAgeGroup($entryPlace);
                        break;
                    }
            }

            $lastEntry = $entry;
            $place++;
        }
    }

    private static function CmpTeamTime(Team $a, Team $b): int
    {
        $diffTime =  $a->getTime()->get() - $b->getTime()->get();
        if ($diffTime != 0)
        {
            return $diffTime;
        }

        $diffTimeFirst = $a->getEntry(0)->getTime()->get() - $b->getEntry(0)->getTime()->get();
        if ($diffTimeFirst != 0)
        {
            return $diffTimeFirst;
        }

        $diffTimeSecond = $a->getEntry(1)->getTime()->get() - $b->getEntry(1)->getTime()->get();
        if ($diffTimeSecond != 0)
        {
            return $diffTimeSecond;
        }

        $diffTimeThird = $a->getEntry(2)->getTime()->get() - $b->getEntry(2)->getTime()->get();
        return $diffTimeThird;
    }

    private function computeTeamPlacing(array $entries)
    {
        $groups = Entries::SplitEntriesByTeam($entries);
        $teams = array();
        foreach ($groups as $groupEntries)
        {
            $teams = array_merge($teams, $this->buildTeams($groupEntries));
        }
        usort($teams, "self::CmpTeamTime");
        $this->setTeamPlacing($teams);

        $place = 1;
        $teamPlace = $place;
        $lastTeam = null;
        foreach ($teams as $team)
        {
            if ($lastTeam != null)
            {
                if (self::CmpTeamTime($team, $lastTeam) > 0)
                {
                    $teamPlace = $place;
                }
            }

            $team->setPlacing($teamPlace);

            $lastTeam = $team;
            $place++;
        }
    }

    private function buildTeams(array $entries): array
    {
        assert(!empty($entries));
        assert(self::CheckAllEntriesHaveSameTeam($entries));

        $teams = array();

        if ($entries[0]->getTeam() != "")
        {
            EntrySorter::Sort($entries, EntrySorter::OrderTimeLastnameIndex);
            if (count($entries) >= 6)
            {
                $teamIndex = 1;
            }
            else
            {
                $teamIndex = 0;
            }
            reset($entries);
            do
            {
                $entry0 = current($entries);
                if (!$entry0)
                {
                    break;
                }
                $entry1 = next($entries);
                if (!$entry1)
                {
                    break;
                }
                $entry2 = next($entries);
                if (!$entry2)
                {
                    break;
                }
                $teams[] = new Team($entry0, $entry1, $entry2, $teamIndex);
                next($entries);
                $teamIndex++;
            }
            while (1);
        }
        return $teams;
    }

    private function setTeamPlacing(array $teams)
    {
        if (!empty($teams))
        {
            $firstTeam = $teams[0];
            $entry = $firstTeam->getEntry(0);
            $run = $entry->getRun();
            $gender = $entry->getGender();

            $this->m_teamResults[$run][$gender] = $teams;
        }
    }

    function getTeamPlacing(int $run, string $gender): array
    {
        return $this->m_teamResults[$run][$gender];
    }

    private $m_teamResults = array();
}
