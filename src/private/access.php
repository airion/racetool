<?php declare(strict_types=1);

include_once 'private/config.php';
include_once 'private/post.php';

class AccessRight
{
    const Read = 1;
    const ChangeEntry = 2;
    const DeleteImportExportDatabase = 3;
    const ChangeBlockerlist = 4;
    const ChangeSetting = 5;
    const AddRegistration = 6;
    const PrintLabel = 7;
    const ChangeTimeDataset = 8;
    const ImportCupData = 9;
};

class Access
{
    function __construct()
    {
        $userConfig = self::SearchUserFromConfig();

        if ($userConfig != null)
        {
            $permissions = $userConfig['permissions'];
            $this->m_username = $userConfig['name'];
        }
        else
        {
            $permissions = array();
        }

        $accessRightsTable =
        [
            [AccessRight::Read, "read"],
            [AccessRight::ChangeEntry, "changeEntry"],
            [AccessRight::DeleteImportExportDatabase, "deleteImportExportDatabase"],
            [AccessRight::ChangeBlockerlist, "changeBlockerlist"],
            [AccessRight::ChangeSetting, "changeSetting"],
            [AccessRight::AddRegistration, "addRegistration"],
            [AccessRight::PrintLabel, "printLabel"],
            [AccessRight::ChangeTimeDataset, "changeTimeDataset"],
            [AccessRight::ImportCupData, "importCupData"]
        ];

        foreach ($accessRightsTable as $right)
        {
            $this->m_accessRights[$right[0]] = self::IsPermissionSet($permissions, $right[1]);
        }
    }

    function hasAccess(int $accessRight): bool
    {
        return $this->m_accessRights[$accessRight];
    }

    function getUsername(): string
    {
        return $this->m_username;
    }

    private static function SearchUserFromConfig(): ?array
    {
        $username = Post::GetStringValueFromPost("username");
        $password = Post::GetStringValueFromPost("password");

        $users = Config::Get()['users'];
        foreach ($users as $user)
        {
            if ($user['name'] == $username && $user['password'] == $password)
            {
                return $user;
            }
        }
        return null;
    }

    private static function IsPermissionSet($array, $permission): bool
    {
        return isset($array[$permission]) && ($array[$permission] == true);
    }

    private $m_accessRights = array();
    private $m_username = "";
}
