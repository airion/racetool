<?php declare(strict_types = 1);

include_once 'private/database.php';

class CheckCosaCollidingTeamNames
{

    static function GetCollidingTeamNames(Database $database): array
    {
        $collidingTeamNames = array();

        $convertedTeamToTeams = array();
        $entries = $database->getEntries();
        foreach ($entries as $entry)
        {
            $team = $entry->getTeamAsString();
            $convertedTeam = CheckCosaCollidingTeamNames::GetConvertedTeamName($team);
            $convertedTeamToTeams[$convertedTeam][$team] = 1;
        }
        foreach ($convertedTeamToTeams as $convertedTeam => $teams)
        {
            if (count($teams) > 1)
            {
                $collidingTeamNames[$convertedTeam] = array_keys($teams);
            }
        }
        return $collidingTeamNames;
    }

    static function GetConvertedTeamName(string $team): string
    {
        return substr(mb_convert_encoding($team, "Windows-1252"), 0, 30);
    }

    static function OutputCollidingTeamNames(array $collidingTeamNames)
    {
        echo "<p class='yellow'>Kollidierende Namen bei Verein/Ort:</p>";
        echo "<ul>";
        foreach ($collidingTeamNames as $convertedTeam => $teams)
        {
            echo "<li>";
            $first = true;
            foreach ($teams as $team)
            {
                if (!$first)
                {
                    echo ", ";
                }
                echo $team;
                $first = false;
            }
            echo " => $convertedTeam</li>";
        }
        echo "</ul>";
    }
}
