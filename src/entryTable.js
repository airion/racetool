"use strict";

var NBSP = '\u00a0';
var PRE_REGISTRATION_MODE = "PRE_REGISTRATION";

var EntryAttribute_Index = 0;
var EntryAttribute_Lastname = 1;
var EntryAttribute_Firstname = 2;
var EntryAttribute_Year = 3;
var EntryAttribute_Gender = 4;
var EntryAttribute_Team = 5;
var EntryAttribute_Run = 6;
var EntryAttribute_Email = 7;
var EntryAttribute_Startnr = 8;
var EntryAttribute_Chipnr = 9;
var EntryAttribute_Code = 10;
var EntryAttribute_Revision = 11;
var EntryAttribute_Date = 12;
var EntryAttribute_Username = 13;
var EntryAttribute_Cup = 14;
var EntryAttribute_AgeGroup = 15;
var EntryAttribute_PreRegistration = 16;
var EntryAttribute_Time = 17;
var EntryAttribute_Placing = 18;
var EntryAttribute_PlacingMW = 19;
var EntryAttribute_PlacingAgeGroup = 20;
var EntryAttribute_PlacingTeam = 21;
var EntryAttribute_Last = 22;

// Caution: Has to match with table in entryAttribute.php !

function getColumnName(column)
{
    return document.settings.columnNames[column];
}

function sortEntries(entries, orderBy)
{
    var sortFunctions = [ cmpIndex, cmpLastname, cmpFirstname, cmpYear, cmpGender, cmpTeam, cmpRun, cmpEmail, cmpStartnr, cmpChipnr, cmpCode, cmpRevision,
            cmpIndex, cmpUsername, cmpCup, cmpAgeGroup, cmpPreRegistration, cmpTime, cmpPlacing, cmpPlacingMW, cmpPlacingAgeGroup, cmpPlacingTeam ];

    entries.sort(sortFunctions[orderBy]);
}

function cmpIndex(a, b)
{
    return a.index - b.index;
}

function cmpLastname(a, b)
{
    var res = a.lastname.localeCompare(b.lastname);
    if (res != 0)
    {
        return res;
    }
    return cmpIndex(a, b);
}

function cmpFirstname(a, b)
{
    var res = a.firstname.localeCompare(b.firstname);
    if (res != 0)
    {
        return res;
    }
    return cmpLastname(a, b);
}

function cmpYear(a, b)
{
    var res = a.year - b.year;
    if (res != 0)
    {
        return res;
    }
    return cmpLastname(a, b);
}

function cmpGender(a, b)
{
    var res = a.gender.localeCompare(b.gender);
    if (res != 0)
    {
        return res;
    }
    return cmpLastname(a, b);
}

function cmpTeam(a, b)
{
    var res = a.team.localeCompare(b.team);
    if (res != 0)
    {
        if (a.team == "")
        {
            return 1;
        }
        else if (b.team == "")
        {
            return -1;
        }
        else
        {
            return res;
        }
    }
    return cmpLastname(a, b);
}

function cmpRun(a, b)
{
    var res = a.run - b.run;
    if (res != 0)
    {
        return res;
    }
    return cmpIndex(a, b);
}

function cmpEmail(a, b)
{
    var res = a.email.localeCompare(b.email);
    if (res != 0)
    {
        return res;
    }
    return cmpLastname(a, b);
}

function cmpStartnr(a, b)
{
    var res = a.startnr - b.startnr;
    if (res != 0)
    {
        return res;
    }
    return cmpIndex(a, b);
}

function cmpChipnr(a, b)
{
    var res = a.chipnr - b.chipnr;
    if (res != 0)
    {
        return res;
    }
    return cmpIndex(a, b);
}

function cmpCode(a, b)
{
    var res = a.key.localeCompare(b.key);
    if (res != 0)
    {
        return res;
    }
    return cmpLastname(a, b);
}

function cmpRevision(a, b)
{
    var res = a.revision - b.revision;
    if (res != 0)
    {
        return res;
    }
    return cmpLastname(a, b);
}

function cmpUsername(a, b)
{
    var res = a.username.localeCompare(b.username);
    if (res != 0)
    {
        return res;
    }
    return cmpLastname(a, b);
}

function cmpCup(a, b)
{
    var res = a.cup - b.cup;
    if (res != 0)
    {
        return res;
    }
    return cmpLastname(a, b);
}

function cmpAgeGroup(a, b)
{
    var res = a.gender.localeCompare(b.gender);
    if (res != 0)
    {
        return res;
    }

    var res = a.agegroupMinAge - b.agegroupMinAge;
    if (res != 0)
    {
        return res;
    }
    return cmpLastname(a, b);
}

function cmpPreRegistration(a, b)
{
    var res = a.preregistration - b.preregistration;
    if (res != 0)
    {
        return res;
    }
    return cmpLastname(a, b);
}

function cmpTime(a, b)
{
    var res = a.timeCS - b.timeCS;
    if (res != 0)
    {
        return res;
    }
    return cmpLastname(a, b);
}

function cmpPlacing(a, b)
{
    var res = a.run - b.run;
    if (res != 0)
    {
        return res;
    }

    var res = convertPlacingForSorting(a.placing) - convertPlacingForSorting(b.placing);
    if (res != 0)
    {
        return res;
    }

    return cmpLastname(a, b);
}

function cmpPlacingMW(a, b)
{
    var res = a.run - b.run;
    if (res != 0)
    {
        return res;
    }

    var res = a.gender.localeCompare(b.gender);
    if (res != 0)
    {
        return res;
    }

    var res = convertPlacingForSorting(a.placingMW) - convertPlacingForSorting(b.placingMW);
    if (res != 0)
    {
        return res;
    }

    return cmpLastname(a, b);
}

function cmpPlacingAgeGroup(a, b)
{
    var res = a.run - b.run;
    if (res != 0)
    {
        return res;
    }

    var res = a.gender.localeCompare(b.gender);
    if (res != 0)
    {
        return res;
    }

    var res = convertPlacingForSorting(a.placingAgeGroup) - convertPlacingForSorting(b.placingAgeGroup);
    if (res != 0)
    {
        return res;
    }

    return cmpAgeGroup(a, b);
}

function cmpPlacingTeam(a, b)
{
    var res = a.run - b.run;
    if (res != 0)
    {
        return res;
    }

    var res = a.gender.localeCompare(b.gender);
    if (res != 0)
    {
        return res;
    }

    var res = convertPlacingForSorting(a.placingTeam) - convertPlacingForSorting(b.placingTeam);
    if (res != 0)
    {
        return res;
    }

    return cmpTime(a, b);
}

function convertPlacingForSorting(placing)
{
    if (placing < 0)
    {
        return Number.MAX_VALUE;
    } else
    {
        return placing;
    }
}

function getNumberAsString(number)
{
    if (number >= 0)
    {
        return number;
    } else
    {
        return "---";
    }
}

function Entry(data)
{
    var i = 0;
    this.lastname = data[i++];
    this.firstname = data[i++];
    this.year = data[i++];
    this.gender = data[i++];
    this.agegroup = data[i++];
    this.team = data[i++];
    this.run = data[i++];
    this.cup = data[i++];
    this.startnr = data[i++];
    this.chipnr = data[i++];
    this.email = data[i++];
    this.revision = data[i++];
    this.date = data[i++];
    this.username = data[i++];
    this.preregistration = data[i++];
    this.time = data[i++];
    this.placing = data[i++];
    this.placingMW = data[i++];
    this.placingAgeGroup = data[i++];
    this.placingTeam = data[i++];
    this.key = data[i++];
    this.index = data[i++];

    this.deleted = data[i++];
    this.agegroupIsGenerated = data[i++];

    this.agegroupMinAge = data[i++];
    this.timeCS = data[i++];

    this.post = data[i++];
}

Entry.prototype.getTeamAsString = function()
{
    if (this.team != "")
    {
        return this.team;
    }
    else
    {
        return "---";
    }
}


Entry.prototype.getStartnrAsString = function()
{
    return getNumberAsString(this.startnr);
}

Entry.prototype.getChipnrAsString = function()
{
    return getNumberAsString(this.chipnr);
}

Entry.prototype.getCupAsString = function()
{
    if (this.cup)
    {
        return "Cup";
    } else
    {
        return "-";
    }
}

Entry.prototype.getPreRegistrationAsString = function()
{
    if (this.preregistration)
    {
        return "VA";
    } else
    {
        return "-";
    }
}

Entry.prototype.getPlacingAsString = function()
{
    return getNumberAsString(this.placing);
}

Entry.prototype.getPlacingMWAsString = function()
{
    return getNumberAsString(this.placingMW);
}

Entry.prototype.getPlacingAgeGroupAsString = function()
{
    return getNumberAsString(this.placingAgeGroup);
}

Entry.prototype.getPlacingTeamAsString = function()
{
    return getNumberAsString(this.placingTeam);
}

Entry.prototype.getRunAsString = function()
{
    return document.settings.runs[this.run];
}

function isColumnVisible(column)
{
    var columnMask = 1 << column;
    return (document.settings.enabledColumns & columnMask) != 0 && (document.settings.hiddenColumns & columnMask) == 0;
}

function addTableHeader(column, row)
{
    if (isColumnVisible(column))
    {
        var th = document.createElement("th");
        makeElementStickyTop(th);

        if (column == document.settings.orderBy)
        {
            addClass(th, "nonSelectableHeader");
        } else
        {
            addClass(th, "selectableHeader");
            th.onclick = function()
            {
                selectColumn(column)
            };
        }

        var div0 = document.createElement("div");
        div0.className = "displayFlex";
        th.appendChild(div0);

        var div1 = document.createElement("div");
        div0.appendChild(div1);

        var text = document.createTextNode(getColumnName(column) + NBSP);
        div1.appendChild(text);

        var div2 = document.createElement("div");
        div0.appendChild(div2);

        var span = document.createElement("span");
        span.className = "fas fa-chevron-circle-down";
        if (column != document.settings.orderBy)
        {
            span.style.visibility = "hidden";
        }
        div2.appendChild(span);

        var div3 = document.createElement("div");
        div3.className = "flexAuto";
        div3.appendChild(document.createTextNode(NBSP));
        div0.appendChild(div3);
        row.appendChild(th);
    }
}

function buildTableHead(table)
{
    var thead = table.createTHead();
    var row = thead.insertRow(-1);

    addTableHeader(EntryAttribute_Lastname, row);
    addTableHeader(EntryAttribute_Firstname, row);
    addTableHeader(EntryAttribute_Year, row);
    addTableHeader(EntryAttribute_AgeGroup, row);
    addTableHeader(EntryAttribute_Team, row);
    addTableHeader(EntryAttribute_Run, row);
    addTableHeader(EntryAttribute_Cup, row);
    addTableHeader(EntryAttribute_Startnr, row);
    addTableHeader(EntryAttribute_Chipnr, row);
    addTableHeader(EntryAttribute_Email, row);
    addTableHeader(EntryAttribute_Revision, row);
    addTableHeader(EntryAttribute_Date, row);
    addTableHeader(EntryAttribute_Username, row);
    addTableHeader(EntryAttribute_PreRegistration, row);
    addTableHeader(EntryAttribute_Time, row);
    addTableHeader(EntryAttribute_Placing, row);
    addTableHeader(EntryAttribute_PlacingMW, row);
    addTableHeader(EntryAttribute_PlacingAgeGroup, row);
    addTableHeader(EntryAttribute_PlacingTeam, row);
    return row.children.length;
}

function includes(text, pattern)
{
    if (pattern.length == 0)
        return true;
    text = text.toLowerCase();

    var lsp = [ 0 ];
    for (var i = 1; i < pattern.length; i++)
    {
        var j = lsp[i - 1];
        while (j > 0 && pattern.charAt(i) != pattern.charAt(j))
            j = lsp[j - 1];
        if (pattern.charAt(i) == pattern.charAt(j))
            j++;
        lsp.push(j);
    }

    var j = 0;
    for (var i = 0; i < text.length; i++)
    {
        while (j > 0 && text.charAt(i) != pattern.charAt(j))
            j = lsp[j - 1];
        if (text.charAt(i) == pattern.charAt(j))
        {
            j++;
            if (j == pattern.length)
                return true;
        }
    }
    return false;
}

function equal(text, pattern)
{
    return text.toLowerCase() == pattern;
}

function filterEntry(entry, filterText, filterRun)
{
    if (filterRun >= 0 && entry.run != filterRun)
    {
        return false;
    }

    if (filterText == "")
    {
        return true;
    }

    if (isColumnVisible(EntryAttribute_Lastname) && includes(entry.lastname, filterText))
    {
        return true;
    }

    if (isColumnVisible(EntryAttribute_Firstname) && includes(entry.firstname, filterText))
    {
        return true;
    }

    if (isColumnVisible(EntryAttribute_Year) && equal(entry.year.toString(), filterText))
    {
        return true;
    }

    if (isColumnVisible(EntryAttribute_AgeGroup) && includes(entry.agegroup, filterText))
    {
        return true;
    }

    if (isColumnVisible(EntryAttribute_Team) && includes(entry.team, filterText))
    {
        return true;
    }

    if (isColumnVisible(EntryAttribute_Startnr) && equal(entry.startnr.toString(), filterText))
    {
        return true;
    }

    if (isColumnVisible(EntryAttribute_Chipnr) && equal(entry.chipnr.toString(), filterText))
    {
        return true;
    }

    if (isColumnVisible(EntryAttribute_Email) && includes(entry.email, filterText))
    {
        return true;
    }

    if (isColumnVisible(EntryAttribute_Cup) && equal(entry.getCupAsString(), filterText))
    {
        return true;
    }

    if (isColumnVisible(EntryAttribute_Username) && includes(entry.username, filterText))
    {
        return true;
    }

    return false;
}

function buildTableEntry(entry, body)
{
    var filterText = document.settings.filterText.toLowerCase();
    if (filterEntry(entry, filterText, document.settings.filterRun))
    {
        var lastEntry = body.lastEntry;
        if (lastEntry)
        {
            if ((body.splitRun && (lastEntry.run != entry.run)) || (body.splitGender && (lastEntry.gender != entry.gender)))
            {
                outputSplitter(body);
            }
        }

        var row = body.insertRow(-1);

        if (body.numberEntries & 1)
        {
            row.className = "oddRow";
        } else
        {
            row.className = "evenRow";
        }

        if (entry.post != "")
        {
            row.setAttribute("onclick", entry.post);
            row.className = row.className + " clickableRow";
        }

        var deleted = entry.deleted;

        addTableData(EntryAttribute_Lastname, row, entry.lastname, deleted, undefined, undefined, entry.key);
        addTableData(EntryAttribute_Firstname, row, entry.firstname, deleted);

        addTableData(EntryAttribute_Year, row, entry.year, deleted);

        var color = "";
        if (entry.agegroupIsGenerated)
        {
            color = "Grey";
        }
        addTableData(EntryAttribute_AgeGroup, row, entry.agegroup, deleted, true, color);
        addTableData(EntryAttribute_Team, row, entry.getTeamAsString(), deleted);
        addTableData(EntryAttribute_Run, row, entry.getRunAsString(), deleted, true);
        addTableData(EntryAttribute_Cup, row, entry.getCupAsString(), deleted);
        var color = "";
        if (entry.startnr < 0 && document.settings.programMode != PRE_REGISTRATION_MODE && !deleted)
        {
            color = "Red";
        }
        addTableData(EntryAttribute_Startnr, row, entry.getStartnrAsString(), deleted, false, color);

        var color = "";
        if (entry.chipnr < 0 && document.settings.programMode != PRE_REGISTRATION_MODE && !deleted)
        {
            color = "Red";
        }
        addTableData(EntryAttribute_Chipnr, row, entry.getChipnrAsString(), deleted, false, color);

        addTableData(EntryAttribute_Email, row, entry.email, deleted);

        addTableData(EntryAttribute_Revision, row, entry.revision, deleted);

        addTableData(EntryAttribute_Date, row, entry.date, deleted);

        addTableData(EntryAttribute_Username, row, entry.username, deleted);

        addTableData(EntryAttribute_PreRegistration, row, entry.getPreRegistrationAsString(), deleted);

        addTableData(EntryAttribute_Time, row, entry.time, deleted, true);

        addTableData(EntryAttribute_Placing, row, entry.getPlacingAsString(), deleted);

        addTableData(EntryAttribute_PlacingMW, row, entry.getPlacingMWAsString(), deleted);

        addTableData(EntryAttribute_PlacingAgeGroup, row, entry.getPlacingAgeGroupAsString(), deleted);

        addTableData(EntryAttribute_PlacingTeam, row, entry.getPlacingTeamAsString(), deleted);

        body.numberEntries++;
        body.lastEntry = entry;
    }
}

function addTableData(column, row, text, deleted, nowrap, color, id)
{
    if (isColumnVisible(column))
    {
        var cell = row.insertCell(-1);

        var text = document.createTextNode(text);

        if (deleted)
        {
            cell.style.textDecoration = "line-through";
        }

        if (nowrap)
        {
            cell.style.whiteSpace = "nowrap";
        }

        if (color !== undefined)
        {
            cell.style.color = color;
        }

        if (id !== undefined && id != "")
        {
            cell.id = id;
        }

        if (column == document.settings.orderBy)
        {
            cell.className = "sortColumn";
        }

        cell.appendChild(text);
    }
}

function selectColumn(column)
{
    document.settings.orderBy = column;
    buildTable();
}

function outputSplitter(body)
{
    var row = body.insertRow(-1);
    row.style.height = "0.6em";

    var cell = row.insertCell(-1);
    cell.className = "splitter";
    cell.colSpan = body.numberOfColumns;
}

function setupColumnButtons()
{
    for (var column = 0; column < EntryAttribute_Last; column++)
    {
        var columnMask = 1 << column;
        if (document.settings.enabledColumns & columnMask)
        {
            var button = document.getElementById("showColumnButton" + column);
            if (button)
            {
                var className = "entryTableShowColumnButton";
                if ((document.settings.hiddenColumns & columnMask) == 0)
                {
                    className = className + " entryTableShowColumnButtonHighLighted";
                }
                if (document.settings.orderBy == column)
                {
                    button.disabled = true;
                } else
                {
                    button.disabled = false;
                    className = className + " entryTableShowColumnButtonClickable";
                }
                button.className = className;
            }
        }
    }
}

function buildTable()
{
    var entryTableDiv = document.getElementById("entryTable");
    while (entryTableDiv.lastChild)
    {
        entryTableDiv.removeChild(entryTableDiv.lastChild);
    }

    setupColumnButtons();

    var div = document.createElement('div');

    entryTableDiv.appendChild(div);

    var table = document.createElement('table');
    table.className = "entryTable";

    div.appendChild(table);

    var numberOfColumns = buildTableHead(table);

    var body = document.createElement('tbody');
    table.appendChild(body);

    var orderBy = document.settings.orderBy;
    sortEntries(document.entries, orderBy);

    body.splitRun = (orderBy == EntryAttribute_PlacingMW) || (orderBy == EntryAttribute_PlacingAgeGroup) || (orderBy == EntryAttribute_PlacingTeam) || (orderBy == EntryAttribute_Placing);
    body.splitGender = (orderBy == EntryAttribute_PlacingMW) || (orderBy == EntryAttribute_PlacingAgeGroup) || (orderBy == EntryAttribute_PlacingTeam);
    body.numberOfColumns = numberOfColumns;
    body.numberEntries = 0;
    body.lastEntry = null;

    var n = document.entries.length;
    for (var i = 0; i < n; i++)
    {
        buildTableEntry(document.entries[i], body);
    }
}

function toggleColumnVisibility(column)
{
    document.settings.hiddenColumns ^= (1 << column);
    buildTable();
}

function filterTextInputChanged()
{
    document.settings.filterText = document.getElementById("filterTextInput").value;
    buildTable();
}


function filterRunChanged()
{
    document.settings.filterRun = document.getElementById("filterRun").value;
    buildTable();
}

function initEntryTable()
{
    var data = getData();
    document.entries = [];
    var entries = data["entries"];
    var n = entries.length;
    for (var i = 0; i < n; i++)
    {
        document.entries.push(new Entry(entries[i]));
    }
    document.settings = data["settings"];

    document.getElementById("filterTextInput").value = document.settings.filterText;
    document.getElementById("filterRun").value = document.settings.filterRun;
    buildTable();
}

initEntryTable();
