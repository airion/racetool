<?php declare(strict_types = 1);

include_once 'private/preRegistrationPage.php';
include_once 'private/registrationPage.php';
include_once 'private/config.php';
include_once 'private/programMode.php';

$programMode = Config::Get()['program']['mode'];
if ($programMode !=  ProgramMode::Registration)
{
    new PreRegistrationPage();
}
else
{
    new RegistrationPage();
}

?>