<?php declare(strict_types = 1);

include_once 'private/page.php';
include_once 'private/database.php';
include_once 'private/registrationFields.php';
include_once 'private/checkAndWriteEntry.php';
include_once 'private/writeDeleteEntry.php';
include_once 'private/entries.php';
include_once 'private/jsFunctions.php';
include_once 'private/convertStringToHTML.php';
include_once 'private/printLabel.php';

class AdminChangeEntryPage extends Page
{
    function __construct()
    {
        parent::__construct();

        if (isset($_POST["key"]))
        {
            $key = $_POST["key"];
        }
        else
        {
            $key = "";
        }

        if (isset($_POST["index"]))
        {
            $index = intval($_POST["index"]);
        }
        else
        {
            $index = self::INVALID_INDEX;
        }

        $javaScript = "";

        $javaScript .= getJSFunction_post();
        $javaScript .= getRegistrationFormJS();
        $javaScript .= getJSFunction_getElementValue();

        $style = "";

        $style .= <<<EOD
            .registrationTable  { font-size: 0.7em; border-collapse: collapse; border: 1px solid Black;}
            .head, .selectionHead { background: #505050; color: white; text-align: left; padding: 0.5em; padding-right: 1.0em; white-space: nowrap;}
            .head {border-right: 1px solid Black;}
            .selectionField { padding: 0.5em;}
            .field { padding: 0.5em; border-right: 1px solid Black; padding-right: 1.0em;}
            .oddRow, .oddRowSelectable { background: #dfdfdf; color: Black;}
            .evenRow, .evenRowSelectable { background: #ffffff; color: Black;}
            .oddRowSelectable:hover, .evenRowSelectable:hover { background: #0000ff; color: white; cursor: pointer;}
EOD;

        $style .= getRegistrationFormStyle();

        $this->outputHeader($javaScript, $style);
        $changeEntry = isset($_POST["changeEntry"]);
        $deleteEntry = isset($_POST["deleteEntry"]);

        if ($this->m_access->hasAccess(AccessRight::Read))
        {
            if ($changeEntry)
            {
                $database = new Database();
                $entry = new Entry();
                $entry->setFromPost();
                $entry->setUsername($this->m_access->getUsername());

                $allowAddWhenNoOldEntry = true;
                $allowEmptyEmail = true;
                $checkYearRestrictive = false;
                $writeOk = checkAndWriteEntry($database, $entry, $allowAddWhenNoOldEntry, $allowEmptyEmail, $checkYearRestrictive);
                if (!$database->close())
                {
                    $writeOk = false;
                }

                if (!$writeOk)
                {
                    $this->beginFooter();
                    $parameters = new Parameters();
                    Entry::PassThroughParameters($parameters);
                    $this->outputBackButton("adminChangeEntry.php", $parameters);
                    $this->endFooter();
                    return;
                }
            }
            else
            {
                if ($deleteEntry)
                {
                    $database = new Database();
                    $oldEntry = new Entry();
                    $writeOk = writeDeleteEntry($database, $key, $oldEntry, $this->m_access->getUsername());
                    if (!$database->close())
                    {
                        $writeOk = false;
                    }
                    if (!$writeOk)
                    {
                        $this->beginFooter();
                        $parameters = new Parameters();
                        $parameters->passThrough("key");
                        $this->outputBackButton("adminChangeEntry.php", $parameters);
                        $this->endFooter();
                        return;
                    }
                }
            }
            $database = new Database();
            $database->close();

            $this->printList($database, $key, $index);
            $this->beginFooter();

            $this->outputBackButton("admin.php");

            $isDeleted = !$database->searchEntryByKey($key)->isValid();
            $programMode = Config::Get()['program']['mode'];

            $parametersPrintLabel = new Parameters();
            $parametersPrintLabel->add("key", $key);
            $parametersPrintLabel->add("printLabel", 1);
            $enablePrintLabel = $this->m_access->hasAccess(AccessRight::PrintLabel) && !$isDeleted;

            $parametersCertificate = new Parameters();
            $parametersCertificate->add("key", $key);
            $enableDownloadCertificate = !$isDeleted;

            $menuEntries = [
                new MenuEntry("Label drucken", $this->getPostString("adminChangeEntry.php", $parametersPrintLabel), "", $enablePrintLabel),
                new MenuEntry("Urkunde generieren", $this->getPostString("downloadCertificateForEntrySettings.php", $parametersCertificate), "", $enableDownloadCertificate),
            ];
            Menu::OutputButtonWithMenu("fa-bars", "Label & Urkunde", $menuEntries, "");

            $parametersDelete = new Parameters();
            $parametersDelete->add("key", $key);
            $parametersDelete->add("deleteEntry", 1);
            $enableDelete = $this->m_access->hasAccess(AccessRight::ChangeEntry) && !$isDeleted && $programMode != ProgramMode::PreRegistrationValidatedAndFinished;
            $postClear = $this->getPostFunction("adminChangeEntry.php", $parametersDelete);
            $js = "openMessageBox('Teilnehmer wirklich löschen?', MessageBoxType_Question, $postClear)";
            Menu::OutputButton("fa-trash-alt menuButtonRed", "Eintrag löschen", $js , "delete", $enableDelete);

            $this->endFooter();
        }
        else
        {
            echo "<p class='yellow'>Falsches Passwort!</p>";
            $this->beginFooter();
            $this->outputBackButton("index.php");
            $this->endFooter();
        }
    }

    private function getLastNotDeleteEntryIndex(array $entries)
    {
        for ($i = count($entries) - 1; $i >= 0; $i--)
        {
            if (!$entries[$i]->isDeleted())
            {
                return $i;
            }
        }
    }

    private function outputTableData(string $string, bool $isDeletedEntry, bool $nowrap, string $color = "")
    {
        $styleString = "";

        if ($nowrap)
        {
            $styleString .= "white-space: nowrap;";
        }

        if ($color != "")
        {
            $styleString .= "color: $color;";
        }

        if ($styleString != "")
        {
            echo "<td class='field' style='$styleString'>";
        }
        else
        {
            echo "<td class='field'>";
        }

        if ($string != "")
        {
            if ($isDeletedEntry)
            {
                echo "<s>";
            }
            echo $string;
            if ($isDeletedEntry)
            {
                echo "</s>";
            }
        }
        echo "</td>";
    }

    private function outputTableHeader(int $attribute)
    {
        echo "<th class='head'>";
        echo Entry::GetAttributeShortName($attribute);
        echo "</th>";
    }

    private function printList(Database $database, string $key, int $index)
    {
        if ($this->m_access->hasAccess(AccessRight::ChangeEntry))
        {
            $showKey = " Eintrag $key";
        }
        else
        {
            $showKey = "";
        }

        echo "<p class='big'>Änderungshistorie$showKey</p>";
        echo "<p class='yellow'>";
        echo "Letzte Zeile ist aktueller Stand, Click übernimmt Zeile in Eingabefelder unten";
        echo "</p>";

        echo "<div style='overflow-x: auto;'>";
        echo "<table class='registrationTable'>";

        echo "<thead>";
        echo "<tr>";

        echo "<th class='selectionHead'></th>";

        $cupFlag = Config::Get()['features']['cupFlag'];

        $this->outputTableHeader(EntryAttribute::Lastname);
        $this->outputTableHeader(EntryAttribute::Firstname);
        $this->outputTableHeader(EntryAttribute::Year);
        $this->outputTableHeader(EntryAttribute::AgeGroup);
        $this->outputTableHeader(EntryAttribute::Team);
        $this->outputTableHeader(EntryAttribute::Run);
        if ($cupFlag)
        {
            $this->outputTableHeader(EntryAttribute::Cup);
        }
        $this->outputTableHeader(EntryAttribute::Startnr);
        $this->outputTableHeader(EntryAttribute::Chipnr);
        $this->outputTableHeader(EntryAttribute::Email);
        $this->outputTableHeader(EntryAttribute::Revision);
        $this->outputTableHeader(EntryAttribute::Date);
        $this->outputTableHeader(EntryAttribute::Username);
        $this->outputTableHeader(EntryAttribute::PreRegistration);
        $this->outputTableHeader(EntryAttribute::Time);

        echo "</tr>";
        echo "</thead>";

        echo "<tbody>";

        $entries = $database->getEntriesForKeyWithDeleted($key);
        $i = 0;

        if ($index == self::INVALID_INDEX)
        {
            $index = $this->getLastNotDeleteEntryIndex($entries);
        }

        foreach ($entries as $entry)
        {
            $id = ($i & 1) ? 'oddRow' : 'evenRow';
            if ($entry->isDeleted())
            {
                echo "<tr class='$id'>";
            }
            else
            {
                $id .= 'Selectable';
                $parameters = new Parameters();
                $parameters->add("key", $key);
                $parameters->add("index", $i);

                echo "<tr class='$id' onclick=\"";
                echo $this->getPostString("adminChangeEntry.php", $parameters);
                echo "\">";
            }

            echo "<td class='selectionField'>";
            if ($index == $i)
            {
                echo "<div class='fas fa-play'></div>";
            }
            echo "</td>";

            $this->outputTableData(convertStringToHTML($entry->getLastname()), $entry->isDeleted(), false);
            $this->outputTableData(convertStringToHTML($entry->getFirstname()), $entry->isDeleted(), false);
            $this->outputTableData($entry->getYearAsString(), $entry->isDeleted(), false);

            $color = "";
            if ($entry->getAgeGroupIsGenerated())
            {
                $color = "Grey";
            }
            $this->outputTableData($entry->getAgeGroupAsString(), $entry->isDeleted(), true, $color);

            $this->outputTableData(convertStringToHTML($entry->getTeamAsString()), $entry->isDeleted(), false);
            $this->outputTableData($entry->getRunAsString(), $entry->isDeleted(), true);
            if ($cupFlag)
            {
                $this->outputTableData($entry->getCupAsString(), $entry->isDeleted(), false);
            }

            $this->outputTableData($entry->getStartnrAsString(), $entry->isDeleted(), false);
            $this->outputTableData($entry->getChipnrAsString(), $entry->isDeleted(), false);

            $this->outputTableData($entry->getEmail(), $entry->isDeleted(), false);
            $this->outputTableData((string) $entry->getRevision(), $entry->isDeleted(), false);
            $this->outputTableData($entry->getDate(), $entry->isDeleted(), false);
            $this->outputTableData($entry->getUsername(), $entry->isDeleted(), false);
            $this->outputTableData($entry->getPreRegistrationAsString(), $entry->isDeleted(), false);
            $this->outputTableData($entry->getTimeAsString(), $entry->isDeleted(), true);

            echo "</tr>";
            $i++;
        }
        echo "</tbody>";
        echo "</table>";
        echo "</div>";
        $isCurrentlyDeleted = end($entries)->isDeleted();

        if ($isCurrentlyDeleted)
        {
            echo "<p class='big'> Eintrag wiederherstellen / ändern</p>";
        }
        else
        {
            echo "<p class='big'>Eintrag ändern</p>";
        }

        echo "<p class='yellow'>Hier ändern und mit Absenden neue Daten übernehmen</p>";

        $programMode = Config::Get()['program']['mode'];

        $registrationFieldsEnabled = $this->m_access->hasAccess(AccessRight::ChangeEntry) &&
                                     (($programMode == ProgramMode::PreRegistration) || ($programMode == ProgramMode::Registration));

        $showEmail = true;

        $showStartnr = true;
        $showChipnr = true;
        $showAgeGroups = true;
        $showTime = true;
        $checkYearRestrictive = false;

        $parameters = new Parameters();
        $parameters->add("key", $key);
        $parameters->add("changeEntry", 1);

        outputRegistrationFields($this, $entries[$index], $parameters, "adminChangeEntry.php", $cupFlag, $showEmail, $showStartnr, $showChipnr, $showAgeGroups, $showTime,
            $registrationFieldsEnabled, $checkYearRestrictive, $database);

        if (isset($_POST["printLabel"]) && !$isCurrentlyDeleted)
        {
            $entry = $entries[$this->getLastNotDeleteEntryIndex($entries)];
            printLabel($entry);
        }
    }

    const INVALID_INDEX = -1;

};

new AdminChangeEntryPage();

?>
