<?php declare(strict_types = 1);

include_once 'private/page.php';
include_once 'private/entries.php';
include_once 'private/database.php';
include_once 'private/jsFunctions.php';

class ImportCupDataImportFilePage extends Page
{
    function __construct()
    {
        parent::__construct();

        if ($this->m_access->hasAccess(AccessRight::ImportCupData))
        {
            if (isset($_POST['file']))
            {
                $file = $_POST['file'];
                $this->importFile($file);
            }
            else
            {
                $this->outputErrorMessage("Keine Datei für Import erhalten!", "admin.php");
            }
        }
        else
        {
            $this->outputWrongPasswordErrorMessage();
        }
    }

    function deleteFile($file)
    {
        if (file_exists($file))
        {
            unlink($file);
        }
    }

    function hasEqualEntry(array $entries, Entry $newEntry)
    {
        foreach ($entries as $entry)
        {
            if ($entry->isEqual($newEntry))
            {
                return true;
            }
        }
        return false;
    }

    function isStartnrFree(Database $database, array $entries, Entry $newEntry)
    {
        $startNr = $newEntry->getStartnr();

        if ($startNr != Entry::INVALID_STARTNR)
        {
            if ($database->isStartnrBlocked($startNr))
            {
                return false;
            }
            foreach ($entries as $existingEntry)
            {
                if ($existingEntry->getStartnr() == $startNr)
                {
                    return false;
                }
            }
        }
        return true;
    }

    function readAndCheckFile($file)
    {
        $fpImport = @fopen($file, "r");
        if ($fpImport)
        {
            $retval = $this->importData($fpImport);
            @fclose($fpImport);
            $this->deleteFile($file);
        }
        else
        {
            echo "<p class='yellow'>Datei konnte nicht geöffnet werden!</p>";
            $retval = false;
        }

        return $retval;
    }

    function importData($fpImport)
    {
        $database = new Database();

        $entries = $database->getEntries();

        $expectColumns = 6;
        $encoding = "Windows-1252";

        $newEntries = array();

        while (!feof($fpImport))
        {
            $line = fgets($fpImport);
            if ($line != false)
            {
                $line = str_replace(array(
                    "\r",
                    "\n"
                ), '', $line);
                $fields = explode(";", $line);

                $columns = count($fields);

                if ($columns != $expectColumns)
                {
                    echo "<p class='yellow'>Fehler: Falsche Anzahl Spalten!</p>";
                    return false;
                }

                for ($i = 0; $i < $columns; $i++)
                {
                    $fields[$i] = mb_convert_encoding($fields[$i], mb_internal_encoding(), $encoding);
                }

                $entry = new Entry();
                $cup = true;
                $competition = Runs::INVALID_COMPETITION;
                $entry->set($fields[0], $fields[1], $fields[2], $fields[3], $fields[4], $fields[5], $cup, $competition, $this->m_access->getUsername());

                if ($this->hasEqualEntry($entries, $entry))
                {
                    echo "<p class='yellow'>Identischer Eintrag bereits vorhanden!</p>";
                    return false;
                }

                if (!$this->isStartnrFree($database, $entries, $entry))
                {
                    echo "<p class='yellow'>Startnr bereits vergeben!</p>";
                    return false;
                }

                array_push($entries, $entry); // For hasEqualEntry/isStartnrFree check
                array_push($newEntries, $entry); // New entries to write to database
            }
        }

        foreach ($newEntries as $entry)
        {
            $key = $database->generateNewKey();
            $entry->setKey($key);
            $entry->setPreRegistration(true);
            $writeOk = $database->writeEntry($entry);

            if (!$writeOk)
            {
                echo "<p class='yellow'>Fehler: Eintrag konnte nicht in Datenbank geschrieben werden !!!</p>";
                return false;
            }
        }

        return true;
    }

    function importFile($file)
    {
        $javaScript = "";
        $style = "";

        $javaScript .= getJSFunction_post();

        $this->outputHeader($javaScript, $style);
        echo "<p class='big'>Cup Daten importieren</p>";

        if ($this->readAndCheckFile($file))
        {
            echo '<p class="yellow"> Alle Einträge wurden importiert.</p>';
        }
        else
        {
            echo '<p class="yellow">Daten können nicht importiert werden!</p>';
        }

        $this->beginFooter();
        $this->outputBackButton("admin.php");
        $this->endFooter();
    }
}

new ImportCupDataImportFilePage();

?>