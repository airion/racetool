<?php declare(strict_types = 1);

include_once 'private/page.php';
include_once 'private/entry.php';
include_once 'private/entrySorter.php';
include_once 'private/database.php';
include_once 'private/teamNumberGenerator.php';
include_once 'private/checkCosaCollidingTeamNames.php';

class DownloadCosaResultsPage extends Page
{

    function __construct()
    {
        parent::__construct();

        if ($this->m_access->hasAccess(AccessRight::Read))
        {
            $database = new Database();
            $database->close();

            $collidingTeamNames = CheckCosaCollidingTeamNames::GetCollidingTeamNames($database);
            if (!empty($collidingTeamNames))
            {
                $javaScript = getJSFunction_post();
                $this->outputHeader($javaScript);
                CheckCosaCollidingTeamNames::OutputCollidingTeamNames($collidingTeamNames);

                $this->beginFooter();
                $this->outputBackButton("admin.php");
                $this->endFooter();
            }
            else
            {
                $this->printList($database);
            }
        }
        else
        {
            $this->outputWrongPasswordErrorMessage();
        }
    }

    function outputLine(string $text)
    {
        echo mb_convert_encoding($text . "\r\n", "Windows-1252");
    }

    function printList(Database $database)
    {
        $teamNumberGenerator = new TeamNumberGenerator($database);

        $entries = $database->getEntries();
        Entries::RemoveEntriesWithInvalidTime($entries);
        EntrySorter::Sort($entries, EntrySorter::OrderStartnrIndex);

        $timeOfLastModification = $database->getTimeOfLastModificationAsString();
        $filename = "cosa_ergebnisdaten_" . $timeOfLastModification . ".csv";

        header("Content-Type: text/csv; charset=Windows-1252");
        header("Content-Disposition: attachment; filename=$filename");

        $this->outputLine(
            "Name;Vorname;Jahrgang;Zeit;Altersklasse;Verein;Wettbewerb;Wettbew-Name;Start-Nr.;Platz;Einlauf;Athleten-Nr.;Wertungs-Gr;Zeit ohne Text;m/w;Altersklasse-Kennz.;Wettbew-Nr.;LandesVerband;Vereins-Nr.;Mannsch.-Nr.;Starter_1;Starter_2;Starter_3;Starter_4;Starter_5");

        foreach ($entries as $entry)
        {
            $name = $entry->getLastname();
            $firstname = $entry->getFirstname();
            $year = $entry->getYearAsString();

            $time = '"' . $entry->getTime()->getAsString(false, false, false) . '"';
            $timeWithPrefix = '"' . $entry->getTime()->getAsString(false, false, false, true) . '"';

            $agegroup = $entry->getAgeGroupAsString();
            $team = $entry->getTeam(); // COSA exports "" when no team number, instead of the team name "---"
            $wettbNr = strval($entry->getRun() + 1) . "  ";

            $competition = $entry->getRunCosaName();

            $startnr = $entry->getStartnrAsString();
            $placingAgeGroup = $entry->getPlacingAgeGroupAsString();
            $placing = $entry->getPlacingAsString();
            $athletenNr = "";
            $wertGr = "";

            $gender = $entry->getGender();

            $agegroupCosa = $entry->getCosaAgeGroupAsString();

            $lv = "   ";
            $teamNumberString = $teamNumberGenerator->getNumberAsString($entry);
            if ($teamNumberString != "")
            {
                $vernr = str_pad($teamNumberString, 4, "0", STR_PAD_LEFT) . " ";
            }
            else
            {
                $vernr = "     ";
            }

            $this->outputLine(
                "$name;$firstname;$year;$timeWithPrefix;$agegroup;$team;$competition;$competition;$startnr;$placingAgeGroup;$placing;$athletenNr;$wertGr;$time;$gender;$agegroupCosa;$wettbNr;$lv;$vernr");
        }
    }
}

new DownloadCosaResultsPage();
?>