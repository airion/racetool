<?php declare(strict_types = 1);

include_once 'private/page.php';
include_once 'private/entry.php';
include_once 'private/entrySorter.php';
include_once 'private/database.php';
include_once 'private/PDFCreator/PDFCreator.php';
include_once 'private/addDraftOverlay.php';
include_once 'private/labeltype.php';
include_once 'private/runs.php';
include_once 'private/getPriceFromEntry.php';
include_once 'private/getPriceAsString.php';
include_once 'private/post.php';

class DownloadStickerPage extends Page
{

    function __construct()
    {
        parent::__construct();

        if ($this->m_access->hasAccess(AccessRight::Read))
        {
            $startIndex = Post::GetIntValueFromPost("index");
            $this->createLabel($startIndex);
        }
        else
        {
            $this->outputWrongPasswordErrorMessage();
        }
    }

    function getPositionOfLabel(int $index, LabelType $labelType, float $originX, float $originY, float &$x, float &$y)
    {
        $index = $index % ($labelType->getNumX() * $labelType->getNumY());
        $margin = $labelType->getMargin();
        $x = ($index % $labelType->getNumX()) * $labelType->getLabelWidth() + $originX + $margin;
        $y = intval($index / $labelType->getNumX()) * $labelType->getLabelHeight() + $originY + $margin;
    }

    function createLabel(int $labelIndexOnPage)
    {
        $labelConfig = Config::Get()['labels'];
        $labelType = new LabelType(floatval($labelConfig['pageWidth']), floatval($labelConfig['pageHeight']), intval($labelConfig['numX']),
            intval($labelConfig['numY']), floatval($labelConfig['margin']));

        $database = new Database();
        $database->close();

        $timeOfLastModification = $database->getTimeOfLastModificationAsString();
        $filename = "aufkleber_" . $timeOfLastModification . "_p" . ($labelIndexOnPage + 1) . ".pdf";

        $entries = $database->getEntries();

        EntrySorter::Sort($entries, EntrySorter::OrderStartnrIndex);

        $pdfCreator = new PDFCreator(0, 0, 0, 0);

        $pdfCreator->m_pdf->setPrintHeader(false);
        $pdfCreator->m_pdf->setPrintFooter(false);

        $font = new Font('dejavusans', 8);
        $pdfCreator->activateFont($font, true);

        $pdfCreator->doPageBreak();

        $originX = ($pdfCreator->getWidth() - $labelType->getPageWidth()) / 2;
        $originY = ($pdfCreator->getHeight() - $labelType->getPageHeight()) / 2;

        $pdfCreator->m_pdf->SetLineStyle(['dash' => '1', 'width' => 0.1]);


        $margin = $labelType->getMargin();
        $w = $labelType->getLabelWidth() - 2.0 * $margin;
        $h = $labelType->getLabelHeight() - 2.0 * $margin;

        $labelsPerPage = $labelType->getNumX() * $labelType->getNumY();

        foreach ($entries as $entry)
        {
            if ($labelIndexOnPage == $labelsPerPage)
            {
                $labelIndexOnPage = 0;
                $pdfCreator->doPageBreak();
            }

            $x = 0;
            $y = 0;
            $this->getPositionOfLabel($labelIndexOnPage, $labelType, $originX, $originY, $x, $y);

            $align = 'C';
            $valign = 'M';
            $boxHeight = $h / 3;

            $string = $entry->getFirstname() . " " . $entry->getLastname();
            $border = 'LRTB';
            $pdfCreator->outputTextInBox($x, $y, $w, $boxHeight, $string, $border, $align, $valign);
            $y += $boxHeight;

            $string = $entry->getTeamAsString();
            $border = 'LR';
            $pdfCreator->outputTextInBox($x, $y, $w, $boxHeight, $string, $border, $align, $valign);
            $y += $boxHeight;

            $run = Runs::GetInstance()->getRun($entry->getRun());
            $runNameShort = $run->getNameShort();
            $time = $run->getStartTime();
            $price = getPriceAsString(getPriceFromEntry($entry));
            $string = $entry->getYearAsString() .
                " • " . $entry->getAgeGroupAsString() .
                " • " . $runNameShort .
                " • " . $time .
                " • #" . $entry->getStartnrAsString() .
                " • C" . $entry->getChipnrAsString() .
                " • " . $price;
            if ($entry->getCup())
            {
                $string .= " • CUP";
            }
            $border = 'LRTB';
            $pdfCreator->outputTextInBox($x, $y, $w, $boxHeight, $string, $border, $align, $valign);

            $labelIndexOnPage++;
        }

        addDraftOverlay($pdfCreator->m_pdf);

        $pdfCreator->m_pdf->Output($filename, "D");
    }
}

new DownloadStickerPage();

?>