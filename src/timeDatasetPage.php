<?php declare(strict_types = 1);

include_once 'private/page.php';
include_once 'private/database.php';
include_once 'private/jsFunctions.php';
include_once 'private/runs.php';
include_once 'private/timeDataset.php';
include_once 'private/tabMenu.php';

class TimeDatasetPage extends Page
{
    function __construct()
    {
        parent::__construct();

        if ($this->m_access->hasAccess(AccessRight::Read))
        {
            $javaScript = getJSFunction_post();

            $style = self::GetStyle();
            $style .= TabMenu::GetStyle();

            $scrollable = false;
            $this->outputHeader($javaScript, $style, $scrollable);

            $runIndex = 0;
            if (isset($_POST['run']))
            {
                $runIndex = intval($_POST['run']);
            }
            $clear = false;
            if (isset($_POST['clear']))
            {
                $clear = intval($_POST['clear']) == 1;
            }
            $upload = false;
            if (isset($_POST['upload']))
            {
                $upload = intval($_POST['upload']) == 1;
            }

            if ($clear)
            {
                $this->clearTimeData($runIndex);
            }
            else if ($upload)
            {
                $this->uploadTimeData($runIndex);
            }

            $database = new Database();
            $database->close();
            $this->printList($database, $runIndex);
        }
        else
        {
            $this->outputWrongPasswordErrorMessage();
        }
    }

    function outputField($text, $ok, $alignRight)
    {
        echo "<td";

        if ($alignRight)
        {
            echo " style='text-align:right;'";
        }
        echo ">";

        if (!$ok)
        {
            echo "<s>";
        }
        echo $text;
        if (!$ok)
        {
            echo "</s>";
        }
        echo "</td>";
    }

    function printList(Database $database, int $runIndex)
    {
        echo "<p class='big'>Zeitnahme Datensatz</p>";

        $this->outputTabs($runIndex);

        $runName = Runs::GetInstance()->getRun($runIndex)->getName();

        $timeDataset = $database->getTimeDataset($runIndex);

        $this->beginScrollable();
        echo "<div class='overflowXAuto'>";
        echo "<table class='datasetTable'>";

        echo "<thead><tr>";
        echo "<th class='positionStickyTop'>Lfd. Nr.</th>";
        echo "<th class='positionStickyTop'>Chip Id</th>";
        echo "<th class='positionStickyTop'>Chip Nr.</th>";
        echo "<th class='positionStickyTop'>Start Nr.</th>";
        echo "<th class='positionStickyTop'>Zeit</th>";
        echo "<th class='positionStickyTop'>Check</th>";
        echo "</tr></thead>";
        echo "<tbody>";
        $line = 0;
        $timeDataArray = $timeDataset->getTimeData();

        $chipNumberToEntryMap = $database->getChipNumberToEntryMap();

        $number = 1;
        foreach ($timeDataArray as $timeData)
        {
            $check = "";

            $chipNumber = $timeData->getChipNumber();

            if (array_key_exists($chipNumber, $chipNumberToEntryMap))
            {
                $startNumber = $chipNumberToEntryMap[$chipNumber]->getStartNr();
            }
            else
            {
                $startNumber = Entry::INVALID_STARTNR;
            }

            if ($chipNumber != Entry::INVALID_CHIPNR)
            {
                if (!$timeData->getChipNumberFitsRun())
                {
                    $check .= "Chip nicht für $runName vergeben! ";
                }
            }
            else
            {
                $check .= "Falsche Chip Id! ";
            }

            if ($timeData->getDuplicateChipId())
            {
                $check .= "Mehrfachlistung! ";
            }

            $ok = $timeData->isOk();

            $id = ($line & 1) ? 'oddRow' : 'evenRow';
            echo "<tr class=$id>";

            if ($ok)
            {
                $this->outputField($number++, $ok, true);
            }
            else
            {
                $this->outputField("", $ok, true);
            }

            $this->outputField($timeData->getChipId(), $ok, false);

            if ($chipNumber != Entry::INVALID_CHIPNR)
            {
                $this->outputField($chipNumber, $ok, true);
            }
            else
            {
                $this->outputField("", $ok, true);
            }

            if ($startNumber != Entry::INVALID_STARTNR)
            {
                $this->outputField($startNumber, $ok, true);
            }
            else
            {
                $this->outputField("", $ok, true);
            }

            $this->outputField($timeData->getTime()->getAsString(), $ok, true);

            if (!$ok)
            {
                $this->outputField("<span style='color: Red; font-weight: bold;'>$check</span>", true, false);
            }
            else
            {
                $this->outputField("<span style='color: Green;'>Ok</span>", true, false);
            }

            echo "</tr>";
            $line++;
        }
        echo "</tbody>";

        echo "</table>";
        echo "</div>";

        $this->beginFooter();

        $this->outputBackButton("admin.php");

        if (empty($timeDataArray))
        {
            $enableImportButton = true;
            $enableDeleteButton = false;
        }
        else
        {
            $enableImportButton = false;
            $enableDeleteButton = true;
        }

        $programMode = Config::Get()['program']['mode'];
        if (!$this->m_access->hasAccess(AccessRight::ChangeTimeDataset) || ($programMode == ProgramMode::PreRegistrationValidatedAndFinished))
        {
            $enableImportButton = false;
            $enableDeleteButton = false;
        }

        $parameters = new Parameters();
        $parameters->add("run", $runIndex);
        $parameters->add("upload", "1");
        $post = $this->outputUploadForm("button-file", ".dat", "timeDatasetPage.php", $parameters);
        Menu::OutputButton("fa-file-upload", "Import", $post, "import", $enableImportButton);

        $parameters = new Parameters();
        $parameters->add("run", $runIndex);
        $parameters->add("clear", "1");
        $postClear = $this->getPostFunction("timeDatasetPage.php", $parameters);
        $js = "openMessageBox('Zeitnahme Datensatz wirklich löschen?', MessageBoxType_Question, $postClear)";
        Menu::OutputButton("fa-trash-alt menuButtonRed", "Löschen", $js, "delete", $enableDeleteButton);

        $this->endFooter();
    }

    function outputTabs(int $selectedTab)
    {
        $parameters = new Parameters();

        $tabMenu = new TabMenu($selectedTab);
        $tabMenu->begin();

        $runs = Runs::GetInstance()->getRuns();

        foreach ($runs as $runIndex => $run)
        {
            $runName = $run->getNameShort();
            $parameters = new Parameters();
            $parameters->add("run", $runIndex);
            $post = $this->getPostString("timeDatasetPage.php", $parameters);
            $tabMenu->addEntry($runName, $post, "timeDataset$runIndex");
        }
        $tabMenu->end();
    }

    function uploadTimeData(int $runIndex): bool
    {
        $database = new Database();
        if (!$this->m_access->hasAccess(AccessRight::ChangeTimeDataset))
        {
            MessageBox::OutputMessage("Benutzer hat keine Rechte für Änderungen von Zeitnahmedatensätzen!");
            return false;
        }

        if (!empty($database->getTimeDataset($runIndex)->getTimeData()))
        {
            MessageBox::OutputMessage("Zeitnahme Datensatz bereits vorhanden!");
            return false;
        }

        if (!isset($_FILES['userfile']['tmp_name']))
        {
            MessageBox::OutputMessage("Keine Datei für Import erhalten!");
            return false;
        }

        $uploadLimit = Config::Get()['files']['uploadLimit'];
        $size = intval($_FILES['userfile']['size']);

        if ($size > $uploadLimit)
        {
            MessageBox::OutputMessage("Fehler: Hochgeladene Datei ($size Bytes) überschreitet das Limit von $uploadLimit Bytes!");
            return false;
        }

        $fileName = $_FILES['userfile']['tmp_name'];
        $f = @fopen($fileName, "r");
        if (!$f)
        {
            MessageBox::OutputMessage("Datei konnte nicht geöffnet werden!");
            return false;
        }

        $retval = $this->uploadTimeDataFromFile($f, $runIndex, $database);
        @fclose($f);
        return $retval;
    }

    function uploadTimeDataFromFile($f, int $runIndex, Database $database): bool
    {
        $timeDataSet = array();

        $lineNumber = 1;
        while (!feof($f))
        {
            $line = fgets($f);
            if ($line != false)
            {
                $line = str_replace(array(
                    "\r",
                    "\n",
                    "\t",
                    " "
                ), '', $line);
                $fields = explode(";", $line);

                $columns = count($fields);
                if ($columns == 2 || $columns == 3)
                {
                    if (!($fields[0] == "Chip-Nr." && $fields[1] == "EinlaufzeitHH:MM:SS"))
                    {
                        if (strlen($fields[0]) != 8)
                        {
                            MessageBox::OutputMessage("Chip Id ist nicht achtstellig (Zeile $lineNumber)!");
                            return false;
                        }

                        if (!preg_match('/^[A-F0-9]+$/', $fields[0]))
                        {
                            MessageBox::OutputMessage("Chip Id beinhaltet ungültige Zeichen (Zeile $lineNumber)!");
                            return false;
                        }

                        $time = new Time(0);
                        if (!$time->setFromString($fields[1]))
                        {
                            MessageBox::OutputMessage("Zeit hat ungültiges Format (Zeile $lineNumber)!");
                            return false;
                        }

                        $timeData = new TimeData($fields[0], $time);
                        $timeDataSet[] = $timeData;
                    }
                }
                else
                {
                    if (!($columns == 1 && $fields[0] == ""))
                    {
                        MessageBox::OutputMessage("Zeile hat ungültige Spaltenanzahl (Zeile $lineNumber)!");
                        return false;
                    }
                }
            }
            $lineNumber++;
        }

        $this->removeUnneededPostfixesFromTimeData($timeDataSet);

        foreach ($timeDataSet as $timeData)
        {
            $database->addTimeData($runIndex, $timeData);
        }

        if (!$database->close())
        {
            MessageBox::OutputMessage("Zeitnahmedatensatz konnte nicht in Datenbank geschrieben werden!");
            return false;
        }
        return true;
    }

    function removeUnneededPostfixesFromTimeData(array $timeDataSet)
    {
        $timeMap = array();
        foreach ($timeDataSet as $timeData)
        {
            $timeInSeconds = $timeData->getTime()->getSeconds();
            if (!array_key_exists($timeInSeconds, $timeMap))
            {
                $timeMap[$timeInSeconds] = 1;
            }
            else
            {
                $timeMap[$timeInSeconds]++;
            }
        }

        foreach ($timeDataSet as $timeData)
        {
            $timeInSeconds = $timeData->getTime()->getSeconds();
            if ($timeMap[$timeInSeconds] == 1)
            {
                $timeData->getTime()->set($timeInSeconds * 100);
            }
        }
    }

    function clearTimeData(int $runIndex)
    {
        $database = new Database();
        if (!$this->m_access->hasAccess(AccessRight::ChangeTimeDataset))
        {
            MessageBox::OutputMessage("Benutzer hat keine Rechte für Änderungen von Zeitnahmedatensätzen!");
            return false;
        }

        $database->clearTimeData($runIndex);
        if (!$database->close())
        {
            MessageBox::OutputMessage("Zeitnahme Datensatz konnte nicht gelöscht werden!");
            return false;
        }

        return true;
    }

    static function GetStyle()
    {
        $style = <<<EOD
                .datasetTable
                {
                    font-size: 0.7em;
                    border-spacing: 0px;
                }
                td
                {
                    padding: 0.5em; padding-right: 1.0em;
                    box-shadow: inset -1px 0px Black;
                }
                td:first-child
                {
                    box-shadow: inset 1px 0 Black,
                                inset -1px 0px Black;
                }
                th
                {
                    background: #505050; color: White; text-align: left; padding: 0.5em;
                    padding-right: 1.0em;
                    text-align:center;
                    box-shadow: inset -1px 1px Black,
                                inset 0 -1px Black,
                                0px 2px 2px rgba(0, 0, 0, 0.5);
                }
                th:first-child
                {
                    box-shadow: inset -1px 1px Black,
                                inset +1px -1px Black,
                                0px 2px 2px rgba(0, 0, 0, 0.5);
                }
                tr:last-child td
                {
                    box-shadow: inset -1px -1px Black;
                }
                tr:last-child td:first-child
                {
                    box-shadow: inset 1px -1px Black,
                                inset -1px 0px Black;
                }
                .oddRow { background: #dfdfdf; color: Black;}
                .evenRow { background: #ffffff; color: Black;}
EOD;
        return $style;
    }
}

new TimeDatasetPage();

?>
