<?php declare(strict_types = 1);

include_once 'private/page.php';
include_once 'private/entry.php';
include_once 'private/entrySorter.php';
include_once 'private/database.php';
include_once 'private/PDFCreator/PDFCreator.php';
include_once 'private/PDFCreator/Font.php';
include_once 'private/addDraftOverlay.php';
include_once 'private/runs.php';
include_once 'private/config.php';
include_once 'private/post.php';

class DownloadCertificatesPage extends Page
{
    function __construct()
    {
        parent::__construct();

        if ($this->m_access->hasAccess(AccessRight::Read))
        {
            $this->createCertificates();
        }
        else
        {
            $this->outputWrongPasswordErrorMessage();
        }
    }

    function createCertificates()
    {
        $runs = Runs::GetInstance()->getRuns();
        $numberOfRuns = count($runs);
        $runList = array();
        for ($runIndex = 0; $runIndex < $numberOfRuns; $runIndex++)
        {
            $name = "run" . $runIndex . "List";
            $runList[$runIndex] = Post::GetBoolValueFromPost($name);
        }

        $teamList = Post::GetBoolValueFromPost("teamList");
        $team = Post::GetStringValueFromPost("team");

        $key = Post::GetStringValueFromPost("key");
        $certificateType = Post::GetStringValueFromPost("certificateType");
        $certificateTypeAgeGroup = $certificateType == "ageGroup";
        $certificateTypeSpecial = $certificateType == "special";
        $certificateText = Post::GetStringValueFromPost("certificateText");

        $database = new Database();
        $database->close();

        if ($key != "")
        {
            $entry = $database->searchEntryByKey($key);
        }
        else
        {
            $entry = null;
        }

        $timeOfLastModification = $database->getTimeOfLastModificationAsString();
        $filename = "urkunden_" . $timeOfLastModification;

        for ($runIndex = 0; $runIndex < $numberOfRuns; $runIndex++)
        {
            if ($runList[$runIndex])
            {
                $filename .= "_r". ($runIndex + 1);
            }
        }

        if ($teamList)
        {
            $filename .= "_" . self::GetTeamDescription($team);
        }

        if ($certificateTypeAgeGroup || $certificateTypeSpecial)
        {
            $filename .= "_" . self::GetEntryDescription($entry);
            if ($certificateTypeSpecial)
            {
                $filename .= "_" . self::GetCertificateTextDescription($certificateText);
            }
        }

        $filename .= ".pdf";

        $this->m_pdfCreator = new PDFCreator(0, 0, 0);
        $this->m_pdfCreator->m_pdf->setPrintHeader(false);
        $this->m_pdfCreator->m_pdf->setPrintFooter(false);

        if (($entry != null) && $entry->isValid())
        {
            if ($certificateTypeAgeGroup)
            {
                $this->createCertificateForEntry($entry);
            }

            if ($certificateTypeSpecial)
            {
                $this->createSpecialCertificateForEntry($entry, $certificateText);
            }
        }

        $entries = $database->getEntries();
        Entries::RemoveEntriesWithInvalidTime($entries);

        $groups = Entries::SplitEntriesByRun($entries);
        foreach ($groups as $groupEntries)
        {
            assert(!empty($groupEntries));
            $runIndex = $groupEntries[0]->getRun();
            if ($runList[$runIndex])
            {
                $this->createCertificatesForRun($groupEntries, Runs::GetInstance()->getRun($runIndex), $database);
            }
        }

        if ($teamList)
        {
            $groupEntries = array();
            foreach ($entries as $entry)
            {
                if ($entry->getTeam() == $team)
                {
                    $groupEntries[] = $entry;
                }
            }
            EntrySorter::Sort($groupEntries, EntrySorter::OrderTimeLastnameIndex);
            foreach ($groupEntries as $entry)
            {
                $this->createCertificateForEntry($entry);
            }
            $this->createTeamCertificatesForTeam($database, $team);
        }

        addDraftOverlay($this->m_pdfCreator->m_pdf);

        $this->m_pdfCreator->m_pdf->Output($filename, "D");
    }

    function createCertificatesForRun(array $entries, Run $run, Database $database)
    {
        $maxAgeGroupPlace = $run->getCertificatesMaxAgeGroupPlace();

        $groups = Entries::SplitEntriesByAgeGroupAndGender($entries);
        foreach ($groups as $groupEntries)
        {
            EntrySorter::Sort($groupEntries, EntrySorter::OrderTimeLastnameIndex);
            foreach ($groupEntries as $entry)
            {
                if ($entry->getPlacingAgeGroup() <= $maxAgeGroupPlace)
                {
                    $this->createCertificateForEntry($entry);
                }
            }
        }

        $this->createTeamCertificates($run, $database);
    }

    function createCertificate()
    {
        $this->m_pdfCreator->doPageBreak();
        $background = Config::Get()['certificates']['background'];
        if ($background != "")
        {
            $this->m_pdfCreator->m_pdf->Image($background, "0", "0", $this->m_pdfCreator->m_pdf->getPageWidth(), $this->m_pdfCreator->m_pdf->getPageHeight());
        }
        $this->m_pdfCreator->m_pdf->setPageMark();

        $this->m_align = "C";
        $this->m_border = "";
        $this->m_left = 0;
        $this->m_right = 0;
        $this->m_top = 0;
    }

    function addToCertificate(array $config, array $search, array $replace)
    {
        foreach ($config as $line)
        {
            $text = self::GetFromArray($line, 'text');
            $text = str_replace($search, $replace, $text);

            self::ChangeFromArray_Float($line, 'left', $this->m_left);
            self::ChangeFromArray_Float($line, 'right', $this->m_right);
            self::ChangeFromArray_Float($line, 'top', $this->m_top);
            self::ChangeFromArray_String($line, 'align', $this->m_align);
            self::ChangeFromArray_String($line, 'border', $this->m_border);

            $fontFamily = strval($line['font'][0]);
            $fontSize = $line['font'][1];
            $font = $this->m_pdfCreator->getOrCreateFont($fontFamily, $fontSize);
            $this->output($font, $text);
        }
    }

    static function GetCertificateObjectsForRun(array $templates, int $run): array
    {
        foreach ($templates as $template)
        {
            if (in_array($run, $template['runs'], true))
            {
                return self::GetArray($template, 'objects');
            }
        }
        return [];
    }

    static function GetArray(array $array, string $key): array
    {
        $value = $array[$key];
        if (is_array($value))
        {
            return $value;
        }
        else
        {
            return [];
        }
    }

    function createCertificateForEntry(Entry $entry)
    {
        $search = [];
        $replace = [];
        self::GetSearchAndReplaceForEntry($entry, $search, $replace);

        $this->createCertificate();

        $certificates = self::GetArray(Config::Get(),'certificates');
        $commonObjects = self::GetArray($certificates, 'common');
        $this->addToCertificate($commonObjects, $search, $replace);

        $templates = self::GetArray($certificates, 'single');
        $certificateObjects = self::GetCertificateObjectsForRun($templates, $entry->getRun());
        $this->addToCertificate($certificateObjects, $search, $replace);
    }

    function createSpecialCertificateForEntry(Entry $entry, string $text)
    {
        $search = array();
        $replace = array();
        self::GetSearchAndReplaceForEntry($entry, $search, $replace);
        $search[] = '$SPECIAL_STRING';
        $replace[] = $text;

        $this->createCertificate();

        $certificates = self::GetArray(Config::Get(),'certificates');
        $commonObjects = self::GetArray($certificates, 'common');
        $this->addToCertificate($commonObjects, $search, $replace);

        $templates = self::GetArray($certificates, 'special');
        $certificateObjects = self::GetCertificateObjectsForRun($templates, $entry->getRun());
        $this->addToCertificate($certificateObjects, $search, $replace);
    }

    function createTeamCertificate(Team $team)
    {
        $search = array();
        $replace = array();

        self::GetSearchAndReplaceForTeam($team, $search, $replace);

        $this->createCertificate();

        $certificates = self::GetArray(Config::Get(),'certificates');
        $commonObjects = self::GetArray($certificates, 'common');
        $this->addToCertificate($commonObjects, $search, $replace);

        $templates = self::GetArray($certificates, 'team');
        $certificateObjects = self::GetCertificateObjectsForRun($templates, $team->getEntry(0)->getRun());
        $this->addToCertificate($certificateObjects, $search, $replace);
    }

    function createTeamCertificates(Run $run, Database $database)
    {
        $this->createTeamCertificatesForGender($run, $database, "m");
        $this->createTeamCertificatesForGender($run, $database, "w");
    }

    function createTeamCertificatesForGender(Run $run, Database $database, string $gender)
    {
        $teams = $database->getTeamPlacing($run->getIndex(), $gender);
        foreach ($teams as $team)
        {
            if ($team->getPlacing() <= 3)
            {
                for ($i = 0; $i < 3; $i++)
                {
                    $this->createTeamCertificate($team);
                }
            }
        }
    }

    function createTeamCertificatesForTeam(Database $database, string $teamName)
    {
        $runs = Runs::GetInstance()->getRuns();
        foreach ($runs as $run)
        {
            $this->createTeamCertificatesForTeamAndGender($run, $database, "m", $teamName);
            $this->createTeamCertificatesForTeamAndGender($run, $database, "w", $teamName);
        }
    }

    function createTeamCertificatesForTeamAndGender(Run $run, Database $database, string $gender, string $teamName)
    {
        $teams = $database->getTeamPlacing($run->getIndex(), $gender);
        foreach ($teams as $team)
        {
            if ($team->getTeamNameWithoutNumber() == $teamName)
            {
                $this->createTeamCertificate($team);
            }
        }
    }

    function output(Font $font, string $string)
    {
        $this->m_pdfCreator->activateFont($font, true);

        $w = $this->m_pdfCreator->m_pdf->getPageWidth() - $this->m_right - $this->m_left;
        $h = $this->m_pdfCreator->getCellHeight();

        $lines = explode("\n", $string);
        $valign = 'M';
        foreach ($lines as $line)
        {
            $border =  $this->m_border;
            if ($border == "1")
            {
                $border = "all";
            }
            $this->m_pdfCreator->outputTextInBox($this->m_left, $this->m_top, $w, $h, $line, $border, $this->m_align, $valign);
            $this->m_top += $h;
        }
    }

    static function GetSearchAndReplaceForEntry(Entry $entry, array &$search, array &$replace)
    {
        $search = [
            '$RUN_NAME',
            '$FIRSTNAME',
            '$LASTNAME',
            '$TEAM',
            '$PLACING_AGEGROUP',
            '$PLACING_MW',
            '$PLACING',
            '$AGEGROUP',
            '$TIME'
        ];
        $replace = [
            $entry->getRunAsString(),
            $entry->getFirstname(),
            $entry->getLastname(),
            $entry->getTeamAsString(),
            $entry->getPlacingAgeGroupAsString(),
            $entry->getPlacingMWAsString(),
            $entry->getPlacingAsString(),
            $entry->getAgeGroupAsString(),
            $entry->getTime()->getAsString(true, false, false, true)
        ];
    }

    static function GetSearchAndReplaceForTeam(Team $team, array &$search, array &$replace)
    {
        $search = [
            '$RUN_NAME',
            '$TEAM',
            '$GENDER',
            '$PLACING_TEAM',
            '$TIME_TEAM'
        ];
        $replace = [
            $team->getEntry(0)->getRunAsString(),
            $team->getTeamName(),
            strtoupper($team->getEntry(0)->getGender()),
            $team->getPlacing(),
            $team->getTime()->getAsString(true, false, false, true)
        ];

        for ($i = 0; $i < 3; $i++)
        {
            $entry = $team->getEntry($i);
            $index = strval($i + 1);
            $search[] = '$FIRSTNAME[' . $index . ']';
            $replace[] = $entry->getFirstname();

            $search[] = '$LASTNAME[' . $index . ']';
            $replace[] = $entry->getLastname();

            $search[] = '$TIME[' . $index . ']';
            $replace[] = $entry->getTime()->getAsString(true, false, false, true);
        }
    }

    static function GetFromArray(array $array, string $key): string
    {
        if (array_key_exists($key, $array))
        {
            return strval($array[$key]);
        }
        else
        {
            return "";
        }
    }

    static function ChangeFromArray_String(array $array, string $key, string &$value)
    {
        if (array_key_exists($key, $array))
        {
            $value = strval($array[$key]);
        }
    }

    static function ChangeFromArray_Float(array $array, string $key, float &$value)
    {
        if (array_key_exists($key, $array))
        {
            $value = floatval($array[$key]);
        }
    }

    private static function GetTeamDescription(string $team): string
    {
        if ($team == "")
        {
            return "---";
        }
        return strtolower($team);
    }

    private static function GetCertificateTextDescription(string $certificateText): string
    {
        return strtolower($certificateText);
    }

    private static function GetEntryDescription(Entry $entry): string
    {
        return strtolower("s" . $entry->getStartnrAsString() . "_" . $entry->getFirstname() . "_" . $entry->getLastname());
    }

    private $m_pdfCreator;
    private $m_align = "";
    private $m_border = "";
    private $m_left = 0;
    private $m_right = 0;
    private $m_top = 0;
}

new DownloadCertificatesPage();

?>