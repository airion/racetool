<?php declare(strict_types = 1);

include_once 'private/page.php';
include_once 'private/jsFunctions.php';
include_once 'private/runs.php';

class DownloadListSettingsPage extends Page
{
    function __construct()
    {
        parent::__construct();

        if ($this->m_access->hasAccess(AccessRight::Read))
        {
            $javaScript = getJSFunction_post();
            $javaScript .= getJSFunction_getElementValue();

            $style = <<<EOD
            input[type="checkbox"]{vertical-align: bottom;}
            label{vertical-align: bottom;}
EOD;

            $this->outputHeader($javaScript, $style);

            $this->listSettings();

            $this->beginFooter();
            $this->outputBackButton("admin.php");

            $parameters = new Parameters();
            $parameters->addJS("nameList", "getElementValue('nameList')");
            $parameters->addJS("startNumberList", "getElementValue('startNumberList')");
            $parameters->addJS("teamLists", "getElementValue('teamLists')");
            $parameters->addJS("newPagePerTeam", "getElementValue('newPagePerTeam')");

            $runs = Runs::GetInstance()->getRuns();
            $numberOfRuns = count($runs);
            for ($runIndex = 0; $runIndex < $numberOfRuns; $runIndex++)
            {
                $name = "run" . $runIndex . "List";
                $parameters->addJS($name, "getElementValue('$name')");
            }

            if (Config::Get()['features']['cupFlag'])
            {
                $javaScript .= " cupList: getCheckBoxValue('cupList'),";
                $parameters->addJS("cupList", "getElementValue('cupList')");
            }

            Menu::OutputButton("fa-file-download", "Download (PDF)", $this->getPostString("downloadLists.php", $parameters), "download");
            $this->endFooter();
        }
        else
        {
            $this->outputWrongPasswordErrorMessage();
        }

    }

    function listSettings()
    {
        echo "<p class='big'>Teilnehmerlisten</p>";
        echo "<p class='yellow'>Gewünschte Listen auswählen</p>";

        echo "<p>";
        echo "<div><input id='nameList' type='checkbox' ><label for='nameList'> Namensliste</label></div>";
        echo "<div class='smallYellow'>(nach Name sortiert)</div>";
        echo "</p>";
        echo "<p>";
        echo "<div><input id='startNumberList' type='checkbox'><label for='startNumberList'> Startnummernliste</label></div>";
        echo "<div class='smallYellow'>(nach Startnummer sortiert)</div>";
        echo "</p>";
        echo "<p>";
        echo "<div><input id='teamLists' type='checkbox'><label for='teamLists'> Vereinslisten</label></div>";
        echo "<div><span>&nbsp;&nbsp;&nbsp;&nbsp;</span><input id='newPagePerTeam' type='checkbox'><label for='newPagePerTeam'> neue Seite pro Verein</label></div>";
        echo "<div class='smallYellow'>(nach Vereinsanme und Name sortiert)</div>";
        echo "</p>";

        echo "<p>";
        $runs = Runs::GetInstance()->getRuns();
        foreach ($runs as $runIndex => $run)
        {
            $runName = $run->getName();
            $id = "run" . $runIndex . "List";
            echo "<div><input id='$id' type='checkbox'><label for='$id'> Wettbewerb $runName</label></div>";
        }
        echo "<div class='smallYellow'>(nach Startnummer sortiert)</div>";
        echo "</p>";

        if (Config::Get()['features']['cupFlag'])
        {
            echo "<p>";
            echo "<div><input id='cupList' type='checkbox'><label for='cupList'> Cup-Liste</label></div>";
            echo "<div class='smallYellow'>(nach Name sortiert)</div>";
            echo "</p>";
        }
    }
}

new DownloadListSettingsPage();

?>
