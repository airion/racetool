<?php declare(strict_types = 1);

include_once 'private/page.php';
include_once 'private/entry.php';
include_once 'private/database.php';
include_once 'private/checkAndWriteEntry.php';
include_once 'private/convertStringToHTML.php';
include_once 'private/getRegistrationLink.php';
include_once 'private/getPriceFromEntry.php';
include_once 'private/getPriceAsString.php';
include_once 'private/jsFunctions.php';
include_once 'private/mail.php';

class AddEntryPage extends Page
{
    function __construct()
    {
        parent::__construct();

        $javaScript = getJSFunction_post();

        $registrationOpen = $this->outputHeader($javaScript) > 0;

        if ($registrationOpen)
        {
            $entry = new Entry();
            $entry->setFromPost();
            $entry->setUsername($this->m_access->getUsername());
            $entry->setPreRegistration(true);

            $isChange = $entry->getKey() != "";

            $database = new Database();
            $allowAddWhenNoOldEntry = false;
            $allowEmptyEmail = false;
            $checkYearRestrictive = true;
            $writeOk = checkAndWriteEntry($database, $entry, $allowAddWhenNoOldEntry, $allowEmptyEmail, $checkYearRestrictive);
            if (!$database->close())
            {
                $writeOk = false;
            }

            if ($writeOk)
            {
                $mailSend = $this->sendMail($entry, $isChange);
                $this->successfulWrittenMessage($entry, $isChange, $mailSend);

                $this->beginFooter();
                $this->outputBackButton("index.php");
                $this->endFooter();
            }
            else
            {
                $this->beginFooter();
                $parameters = new Parameters();
                Entry::PassThroughParameters($parameters);

                if ($isChange)
                {
                    $backPage = "editEntryInput.php";
                }
                else
                {
                    $backPage = "index.php";
                }
                $this->outputBackButton($backPage, $parameters);
                $this->endFooter();
            }
        }
        else
        {
            $this->beginFooter();
            $this->outputBackButton("index.php");
            $this->endFooter();
        }
    }

    function successfulWrittenMessage(Entry $entry, bool $isChange, bool $mailSend)
    {
        $eventName = Config::Get()['event']['name'];
        $address = convertStringToHTML($entry->getFirstname()) . " " . convertStringToHTML($entry->getLastname());

        switch (Config::Get()['event']['nameGenus'])
        {
            case "n":
                $article = "das";
                break;
            case "m":
                $article = "den";
                break;
            case "f":
                $article = "die";
                break;
        }

        echo "<p>Vielen Dank!</p>";
        echo "<p>";
        if ($isChange)
        {
            echo "Sie haben hiermit Ihre Anmeldung für $article $eventName für $address geändert.";
        }
        else
        {
            echo "Sie haben hiermit $address für $article $eventName angemeldet.";
        }

        if ($mailSend)
        {
            echo " Sie erhalten in Kürze eine Bestätigung per E-Mail an Ihre Adresse '" . convertStringToHTML($entry->getEmail()) . "'.";
            echo " Achtung: Beachten Sie unsere neue Absender Adresse '<span class='yellow'>" . Config::Get()['mail']['sendAddress'] . "</span>'.";
            echo "</p><p>Bitte überprüfen Sie ob alle Daten, insbesondere die Schreibweise des Vereinsnamens korrekt sind.";
        }

        echo " Sie können Ihre Anmeldung jederzeit noch ändern oder komplett stornieren. Benutzen Sie hierzu den ";
        echo "Button 'Anmeldung ändern' auf unserer Anmeldeseite und geben Sie den Code <span id='key' class='yellow'>" . $entry->getKey() .
            "</span> ein. ";
        echo "Der Änderungscode ist auch in der E-Mail enthalten.</p>";
    }

    function sendMail(Entry $entry, bool $isChange): bool
    {
        $sendMailAddress = Config::Get()['mail']['sendAddress'];

        if ($sendMailAddress == "")
        {
            return false;
        }

        $eventName = Config::Get()['event']['name'];

        $registrationLink = getRegistrationLink();
        $anouncementLink = $registrationLink . "/". Config::Get()['event']['anouncementLink'];

        $message = "<p>Sehr geehrte Damen und Herren!</p>";

        $firstname = $entry->getFirstname();
        $lastname = $entry->getLastname();

        $subject = "Anmeldung $eventName / $firstname $lastname";

        $firstname = convertStringToHTML($firstname);
        $lastname = convertStringToHTML($lastname);
        $competition = $entry->getRunAsString();
        $year = $entry->getYearAsString();
        $ageGroup = $entry->getAgeGroupAsString();
        $team = convertStringToHTML($entry->getTeamAsString());
        $key = $entry->getKey();
        $changeLink = "$registrationLink/editEntryInput.php?key=$key";
        $confirmationMailAddress = Config::Get()['mail']['confirmationAddress'];
        $run = Runs::GetInstance()->getRun($entry->getRun());
        $startTime = $run->getStartTime();
        $price = getPriceAsString(getPriceFromEntry($entry));

        if ($isChange)
        {
            $message .= "<p>Die Änderung Ihrer";
        }
        else
        {
            $message .= "<p>Ihre";
        }

        switch (Config::Get()['event']['nameGenus'])
        {
            case "n":
            case "m":
                $article = "den";
                break;
            case "f":
                $article = "die";
                break;
        }

        $message .= " Online-Anmeldung für $article $eventName für <b>$firstname $lastname</b> ist bei uns eingegangen. Wir haben die Anmeldung folgendermaßen registriert:</p>";

        $firstTableRow = "style='white-space: nowrap; padding-right: 1em;background-color:#C0C0C0;'";
        $secondTableRow = "style='background-color:#E0E0E0;'";

        $logoURL = "$registrationLink/res/racetoolLogoMonochrome.png";

        $message .= "<table>\n";
        $message .= "<tr><td $firstTableRow>Name</td> <td $secondTableRow><b>$firstname $lastname</b></td></tr>\n";
        $message .= "<tr><td $firstTableRow>Wettbewerb</td> <td $secondTableRow><b>$competition</b></td></tr>\n";
        $message .= "<tr><td $firstTableRow>Startzeit</td> <td $secondTableRow><b>$startTime</b></td></tr>\n";
        $message .= "<tr><td $firstTableRow>Startgebühr</td> <td $secondTableRow><b>$price</b></td></tr>\n";
        $message .= "<tr><td $firstTableRow>Jahrgang</td> <td $secondTableRow><b>$year</b></td></tr>\n";
        $message .= "<tr><td $firstTableRow>Altersklasse</td> <td $secondTableRow><b>$ageGroup</b></td></tr>\n";
        $message .= "<tr><td $firstTableRow>Verein/Ort</td> <td $secondTableRow><b>$team</b></td></tr>\n";
        $message .= "</table>";

        $message .= "<p>Weitere Info siehe <a href='$anouncementLink'>Ausschreibung</a>.</p>\n";

        $message .= "<p>Bitte überprüfen Sie ob alle Daten korrekt sind. \n";
        $message .= "<a href='$changeLink'>Hier</a> können Sie Ihre Anmeldung jederzeit noch ändern oder komplett stornieren. \n";
        $message .= "Alternativ benutzen Sie den Button 'Anmeldung ändern' auf unserer <a href='$registrationLink'>Anmeldeseite</a> und geben Sie den Code <b>$key</b> ein.</p>\n";

        $message .= "<p>Die aktuelle Anmeldeliste können Sie mit dem Button 'Teilnehmerstand' einsehen. Vergleichen Sie hier auch die genaue Schreibweise des\n";
        $message .= " Vereinsnamens mit anderen Anmeldungen für den gleichen Verein. Korrigieren Sie Ihre Schreibweise gegebenenfalls.</p>\n";

        $message .= "<p>Mit freundlichem Gruß<br>\n";
        $message .= "Lauftreff Unterkirnach</p>\n";

        $message .= "<img src='$logoURL' width='150' style='vertical-align: middle;'>\n";

        $headerFields = array(
            "From: $sendMailAddress",
            "MIME-Version: 1.0",
            "Content-Type: text/html;charset=utf-8"
        );
        if ($confirmationMailAddress != "")
        {
            array_push($headerFields, "Bcc: $confirmationMailAddress");
        }

        if (!Mail::SendMail($entry->getEmail(), $subject, $message, implode("\n", $headerFields)))
        {
            echo "<p class='yellow'>Fehler: Bestätigungs E-Mail konnte nicht an \"" . $entry->getEmail() . "\" versendet werden !<p>";
            return false;
        }
        return true;
    }
}

new AddEntryPage();

?>
