<?php declare(strict_types = 1);

include_once 'private/page.php';
include_once 'private/jsFunctions.php';

class DownloadStickerSettingsPage extends Page
{
    function __construct()
    {
        parent::__construct();

        if ($this->m_access->hasAccess(AccessRight::Read))
        {
            if (isset($_POST['index']))
            {
                $startIndex = intval($_POST['index']);
            }
            else
            {
                $startIndex = 0;
            }

            $javaScript = getJSFunction_post();
            $style = <<<EOD
            .gridButton
            {
                width: 100%; height: 100%;
                border: 1px solid black;
                background-color: white;
                border: 1px solid black;
                color: black;
                box-shadow: 5px 5px 6px rgba(0,0,0,0.5);
            }
            .filledGridButton
            {
                background-color: #e0e0e0;
            }
            .firstGridButton
            {
                background-color: #a0a0a0;
            }
            .gridButton:hover
            {
                background-color: orange;
                cursor: pointer;
            }
EOD;
            $this->outputHeader($javaScript, $style);

            $this->stickerSettings($startIndex);

            $this->beginFooter();
            $this->outputBackButton("admin.php");

            $parameters = new Parameters();
            $parameters->add("index", $startIndex);

            Menu::OutputButton("fa-file-download", "Download (PDF)", $this->getPostString("downloadSticker.php", $parameters), "download");

            $this->endFooter();
        }
        else
        {
            $this->outputWrongPasswordErrorMessage();
        }
    }

    function stickerSettings(int $startIndex)
    {
        echo "<p class='big'>Aufkleber für Startnummern</p>";
        echo "<p class='yellow'>Startposition erster Aufkleber auf Seite</p>";
        $labelConfig = Config::Get()['labels'];
        $numX = $labelConfig['numX'];
        $numY = $labelConfig['numY'];

        $aspect = ($labelConfig['pageWidth'] / $numX) / ($labelConfig['pageHeight'] / $numY);

        $index = 0;
        for ($y = 0; $y < $numY; $y++)
        {
            echo "<div>";
            $width = 100 / $numX;
            $height = $width / $aspect;
            for ($x = 0; $x < $numX; $x++)
            {
                if ($index < $startIndex)
                {
                    $filledGridButton = "";
                    $text = "&nbsp;";
                }
                else
                {
                    $text = $index + 1 - $startIndex . ".";
                    if ($index == $startIndex)
                    {
                        $filledGridButton = "firstGridButton";
                    }
                    else
                    {
                        $filledGridButton = "filledGridButton";
                    }
                }
                $width = 3.0;
                $height = $width / $aspect;
                echo "<div style='width:$width" . "em; height:$height" . "em; display: inline-block; margin: 0; background-color: white;'>";

                $parameters = new Parameters();
                $parameters->add("index", $index);
                $this->outputButton("downloadStickerSettings.php", $text, $parameters, "", false, "gridButton $filledGridButton");
                echo "</div>";
                $index++;
            }
            echo "</div>";
        }
    }
}

new DownloadStickerSettingsPage();

?>
