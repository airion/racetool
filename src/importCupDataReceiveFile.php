<?php declare(strict_types = 1);

include_once 'private/page.php';
include_once 'private/entries.php';
include_once 'private/database.php';
include_once 'private/jsFunctions.php';
include_once 'private/convertStringToHTML.php';
include_once 'private/filePathRegistry.php';

class ImportCupDataReceiveFilePage extends Page
{
    function __construct()
    {
        parent::__construct();

        if ($this->m_access->hasAccess(AccessRight::ImportCupData))
        {
            if (isset($_FILES['userfile']['tmp_name']))
            {
                $file = $_FILES['userfile']['tmp_name'];
                $this->importFile($file);
            }
            else
            {
                $this->outputErrorMessage("Keine Datei für Import erhalten!", "admin.php");
            }
        }
        else
        {
            $this->outputWrongPasswordErrorMessage();
        }
    }

    function cleanupTmpDir()
    {
        $tmpdir = FilePathRegistry::GetTmpDir();
        if ($tmpdir != "")
        {
            $files = scandir($tmpdir);
            $currentTime = time();
            foreach ($files as $file)
            {
                if (preg_match("/^tmpUpload.*$/", $file))
                {
                    $fileWithPath = $tmpdir . '/' . $file;
                    $fileModifiedTime = filemtime($fileWithPath);
                    $deltaTime = $currentTime - $fileModifiedTime;

                    if ($deltaTime > 5 * 60)
                    {
                        $this->deleteFile($fileWithPath);
                    }
                }
            }
        }
    }

    function deleteFile($file)
    {
        if (file_exists($file))
        {
            unlink($file);
        }
    }

    function copyFileToTmpDir($file)
    {
        $tmpdir = FilePathRegistry::GetTmpDir();
        $newFile = tempnam($tmpdir, "tmpUpload");
        if (!$newFile)
        {
            echo "<p class='yellow'>Fehler: Temporäre Datei konnte nicht erzeugt werden !</p>";
            return false;
        }
        if (!move_uploaded_file($file, $newFile))
        {
            echo "<p class='yellow'>Fehler: Verschieben der hochgeladenen Datei fehlgeschlagen !</p>";
            $this->deleteFile($newFile);
            return false;
        }
        return $newFile;
    }

    function hasEqualEntry(array $entries, Entry $newEntry)
    {
        foreach ($entries as $entry)
        {
            if ($entry->isEqual($newEntry))
            {
                return true;
            }
        }
        return false;
    }

    function isStartnrFree(Database $database, array $entries, Entry $newEntry)
    {
        $startNr = $newEntry->getStartnr();

        if ($startNr != Entry::INVALID_STARTNR)
        {
            if ($database->isStartnrBlocked($startNr))
            {
                return false;
            }
            foreach ($entries as $existingEntry)
            {
                if ($existingEntry->getStartnr() == $startNr)
                {
                    return false;
                }
            }
        }
        return true;
    }

    function readAndCheckFile($file)
    {
        $database = new Database();

        $this->cleanupTmpDir();
        // after Database(), so that lock is aquired and data is not removed for an
        // maybe existing process which still want's to import the data

        $entries = $database->getEntries();

        $expectColumns = 6;
        $encoding = "Windows-1252";

        echo "<div class='overflowXAuto'>";
        echo '<table>';
        echo '<thead>';
        echo '<tr>';
        echo '<th>#</th>';
        echo '<th>' . Entry::GetAttributeName(EntryAttribute::Startnr) . '</th>';
        echo '<th>' . Entry::GetAttributeName(EntryAttribute::Lastname) . '</th>';
        echo '<th>' . Entry::GetAttributeName(EntryAttribute::Firstname) . '</th>';
        echo '<th>' . Entry::GetAttributeName(EntryAttribute::Gender) . '</th>';
        echo '<th>' . Entry::GetAttributeName(EntryAttribute::Team) . '</th>';
        echo '<th>' . Entry::GetAttributeName(EntryAttribute::Year) . '</th>';

        echo '<th>Check</th>';
        echo '</tr>';
        echo '</thead>';
        echo '<tbody>';

        $fp = @fopen($file, "r");
        if (!$fp)
        {
            echo "<p class='yellow'>Datei konnte nicht geöffnet werden!</p>";
            return false;
        }
        $row = 1;
        $allValid = true;
        while (!feof($fp))
        {
            $id = ($row & 1) ? 'oddRow' : 'evenRow';
            $line = fgets($fp);
            if ($line != false)
            {
                $errorString = "";
                $line = str_replace(array(
                    "\r",
                    "\n"
                ), '', $line);
                $fields = explode(";", $line);

                $columns = count($fields);

                for ($i = 0; $i < $columns; $i++)
                {
                    $fields[$i] = mb_convert_encoding($fields[$i], mb_internal_encoding(), $encoding);
                }

                if ($columns > $expectColumns)
                {
                    $errorString .= ">$expectColumns Spalten! ";
                }
                if ($columns < $expectColumns)
                {
                    $errorString .= "<$expectColumns Spalten! ";
                }
                for ($i = $columns; $i < $expectColumns; $i++)
                {
                    $fields[$i] = "";
                }
                if ($columns > 0)
                {
                    $entry = new Entry();
                    $cup = true;
                    $competition = Runs::INVALID_COMPETITION;
                    $entry->set($fields[0], $fields[1], $fields[2], $fields[3], $fields[4], $fields[5], $cup, $competition, $this->m_access->getUsername());

                    $fieldOk = array();
                    $fieldOk[0] = ($fields[0] == "" || is_numeric($fields[0])) && $entry->isStartnrOk();

                    if (!$this->isStartnrFree($database, $entries, $entry))
                    {
                        $fieldOk[0] = false;
                        $errorString .= "Startnr bereits vergeben! ";
                    }

                    $fieldOk[1] = $entry->isLastnameOk();
                    $fieldOk[2] = $entry->isFirstnameOk();
                    $fieldOk[3] = $entry->isGenderOk();
                    $fieldOk[4] = true;
                    $fieldOk[5] = $entry->isYearOk();

                    echo "<tr class='$id'>";
                    echo "<td>$row</td>";
                    $allFieldsOk = true;
                    for ($col = 0; $col < $expectColumns; $col++)
                    {
                        echo '<td>';
                        if ($col < $columns)
                        {
                            if (!$fieldOk[$col])
                            {
                                $allFieldsOk = false;
                                echo "<span style='color: Red; font-weight: bold;'>";
                            }
                            if ($fields[$col] != "")
                            {
                                echo convertStringToHTML($fields[$col]);
                            }
                            else
                            {
                                if ($col != 0 && $col != 4)
                                {
                                    echo "Leer!";
                                }
                            }
                            if (!$fieldOk[$col])
                            {
                                echo "</span>";
                            }
                        }
                        else
                        {
                            // missing field
                            echo "<span style='color: Red; font-weight: bold;'>Fehlt!</span>";
                        }
                        echo '</td>';
                    }
                    if (!$allFieldsOk)
                    {
                        $errorString .= "Fehler in Feld! ";
                    }
                    echo '<td>';

                    if ($errorString == "")
                    {
                        if ($this->hasEqualEntry($entries, $entry))
                        {
                            $errorString = "Identischer Eintrag!";
                        }
                        else
                        {
                            array_push($entries, $entry);
                        }
                    }

                    if ($errorString != "")
                    {
                        $allValid = false;
                        echo "<span style='color: Red; font-weight: bold;'>$errorString</span>";
                    }
                    else
                    {
                        echo "<span style='color: Green;'>Ok</span>";
                    }
                    echo '</td>';
                    echo '</tr>';
                }
                $row++;
            }
        }

        echo '</tbody>';
        echo '</table>';
        echo '</div>';

        @fclose($fp);
        return $allValid;
    }

    function importFile($file)
    {
        $uploadLimit = Config::Get()['files']['uploadLimit'];
        $javaScript = "";
        $style = "";

        $style .= <<<EOD
            table { font-size: 0.75em; border-collapse: collapse; border: 1px solid Black;}
            th
            {
                background: #505050; color: white; text-align: left; padding: 0.5em; border-right: 1px solid Black;
                padding-right: 1.0em;
            }
            td
            {
                padding: 0.5em; border-right: 1px solid Black;
                padding-right: 1.0em;
            }
            .oddRow { background: #dfdfdf; color: Black;}
            .evenRow { background: #ffffff; color: Black;}
EOD;

        $javaScript .= getJSFunction_post();

        $scrollable = false;
        $this->outputHeader($javaScript, $style, $scrollable);
        echo "<p class='big'>Cup Daten importieren</p>";
        $this->beginScrollable();

        $size = $_FILES['userfile']['size'];

        $importButton = false;

        if ($size <= $uploadLimit)
        {
            $newFile = $this->copyFileToTmpDir($file);
            if ($newFile)
            {
                if ($this->readAndCheckFile($newFile))
                {
                    $importButton = true;
                }
                else
                {
                    MessageBox::OutputMessage("Fehlerhafte Einträge, Daten können nicht importiert werden.");
                    $this->deleteFile($newFile);
                }
            }
            else
            {
                MessageBox::OutputMessage("Fehler: Datei konnte nicht kopiert werden!");
            }
        }
        else
        {
            MessageBox::OutputMessage("Fehler: Hochgeladene Datei ($size Bytes) überschreitet das Limit von $uploadLimit Bytes !");
        }

        $this->beginFooter();
        $this->outputBackButton("admin.php");

        if ($importButton)
        {
            $parameters = new Parameters();
            $parameters->add("file", $newFile);
            Menu::OutputButton("fa-edit", "Einträge in Datenbank übernehmen", $this->getPostString("importCupDataImportFile.php", $parameters), "confirm");
        }
        $this->endFooter();
    }
}

new ImportCupDataReceiveFilePage();

?>