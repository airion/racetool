"use strict";

var currentScrollMode = -1;

var modeAllScrollable = 0;
var modeStaticAndScrollable = 1;

var lastWindowHeight = 0;

function hasClass(element, className)
{
    var classAttribute = element.getAttribute('class');
    return classAttribute && (classAttribute.indexOf(className) > -1);
}

function addClass(element, className)
{
    if (element.classList)
    {
        element.classList.add(className);
    } else if (!hasClass(element, className))
    {
        element.setAttribute('class', element.getAttribute('class') + ' ' + className);
    }
}

function removeClass(element, className)
{
    if (element.classList)
    {
        element.classList.remove(className);
    } else if (hasClass(element, className))
    {
        element.setAttribute('class', element.getAttribute('class').replace(className, ' '));
    }
}

function isStyleSupported(property, value)
{
    if ('CSS' in window && 'supports' in window.CSS)
    {
        return window.CSS.supports(property, value);
    }
    var el = document.createElement('div');
    if (!(property in el.style))
    {
        return false;
    }
    el.style[property] = value;
    return el.style[property] === value;
}

function setElementStickyStyle(element)
{
    element.style.position = "relative"; // Needed, when sticky not supported, to have z-index working.
    element.style.position = "-webkit-sticky";
    element.style.position = "sticky";
    element.style.top =  "0px";
}

function setElementNotStickyStyle(element)
{
    element.style.position = "relative"; // Needed for z-index working.
    element.style.top =  "";
}

function makeElementStickyTop(element)
{
    addClass(element, "positionStickyTop");
    if (currentScrollMode == modeStaticAndScrollable)
    {
        setElementStickyStyle(element);
    }
    else
    {
        setElementNotStickyStyle(element);
    }
}

function setModeAllScrollable()
{
    if (currentScrollMode != modeAllScrollable)
    {
        var body = document.body;
        removeClass(body, "displayFlex");
        removeClass(body, "flexFlowColumn");
        body.style.height = "";

        // Makes the page always scrollable a bit - workaround for position fixed problem on mobile devices
        body.style.paddingBottom = "1em";
        body.style.minHeight = "100%";

        var staticPart = document.getElementById("staticPart");
        removeClass(staticPart, "flexNone");

        var scrollablePart = document.getElementById("scrollablePart");
        scrollablePart.style.overflow = "";
        removeClass(scrollablePart, "flexAuto");

        var footerPart = document.getElementById("footerPart");
        footerPart.style.position = "fixed";
        footerPart.style.width = "100%";
        footerPart.style.bottom = "0";

        var elements = document.getElementsByClassName("overflowXAuto");
        for (var i = 0; i < elements.length; i++)
        {
            elements[i].style.overflowX = "auto";
        }

        elements = document.getElementsByClassName("positionStickyTop");
        for (var i = 0; i < elements.length; i++)
        {
            setElementNotStickyStyle(elements[i]);
        }

        currentScrollMode = modeAllScrollable;
    }
}

function setModeStaticAndScrollable()
{
    if (currentScrollMode != modeStaticAndScrollable)
    {
        var body = document.body;

        addClass(body, "displayFlex");
        addClass(body, "flexFlowColumn");
        body.style.height = "100%";

        body.style.paddingBottom = "";
        body.style.minHeight = "";

        var staticPart = document.getElementById("staticPart");
        addClass(staticPart, "flexNone");

        var scrollablePart = document.getElementById("scrollablePart");
        scrollablePart.style.overflow = "auto";
        addClass(scrollablePart, "flexAuto");

        var footerPart = document.getElementById("footerPart");
        footerPart.style.position = "";
        footerPart.style.width = "";
        footerPart.style.bottom = "";

        var elements = document.getElementsByClassName("overflowXAuto");
        for (var i = 0; i < elements.length; i++)
        {
            elements[i].style.overflowX = "";
        }

        elements = document.getElementsByClassName("positionStickyTop");
        for (var i = 0; i < elements.length; i++)
        {
            setElementStickyStyle(elements[i]);
        }

        currentScrollMode = modeStaticAndScrollable;
    }
}

function setScrollMode()
{
    if (document.flexAvailable)
    {
        // Filter small changes, when browsers show/hide their address bars during scrolling.
        var relevantChange = Math.abs(window.innerHeight - lastWindowHeight) > 120;
        if (relevantChange)
        {
            lastWindowHeight = window.innerHeight;
        }

        if (lastWindowHeight > 700 && window.innerWidth > 700)
        {
            // Mode only for large screens (tablets, desktop).
            setModeStaticAndScrollable();
        }
        else
        {
            setModeAllScrollable();
        }
    }
}

function pageLoaded()
{
    document.flexAvailable = isStyleSupported("display", "flex") ||
        isStyleSupported("display", "-ms-flexbox") ||
        isStyleSupported("display", "-webkit-flex");

    // Needed for correct window size values, otherwise mobile browsers return large values, when the page contains unconstrained elements.
    setModeAllScrollable();

    setScrollMode();
    window.addEventListener('resize', setScrollMode);
}

window.addEventListener("load", pageLoaded);