<?php declare(strict_types = 1);

include_once 'private/showRegistrationsPage.php';
include_once 'private/database.php';

class ShowCompetitionsPage extends ShowRegistrationsPage
{
    function __construct()
    {
        $style = "";
        $style .= <<<EOD
    .tableStatistics
    {
        color: White;
        border-spacing:0em;
    }
    .tableStatistics td
    {
        font-size: 0.9em;
        padding-left: 0em;
    }
    .tableStatistics th
    {
        font-size: 0.9em;
        text-align: left;
    }
    .tableStatistics th:not(:first-child)
    {
        text-align: right;
        padding-left: 1em;
    }
    .tableStatistics td:not(:first-child)
    {
        font-weight: bold;
        text-align: right;
        color: Yellow;
    }
    .tableStatistics tr:last-child td
    {
        border-top: 1px solid white;
    }
EOD;

        $parameters = new Parameters();
        $activeTab = 1;
        parent::__construct($style, $parameters, $activeTab);
    }

    function outputContent()
    {
        $database = new Database();
        $database->close();

        $registrationsPerRun = $database->getNumberOfRegistrationsPerRun();
        $finisherPerRun = $database->getNumberOfFinisherPerRun();

        $this->beginScrollable();

        echo "<div class='overflowXAuto'>";
        echo "<table class='tableStatistics'>";
        echo "<tr>";
        echo "<th>Wettbewerb</th>";
        echo "<th>Meldungen</th>";
        echo "<th>Finisher</th>";
        echo "</tr>";
        $totalNumberRegistrations = 0;
        $totalNumberFinisher = 0;
        $numberOfRuns = Runs::GetInstance()->getNumberOfRuns();
        for ($run = 0; $run < $numberOfRuns; $run++)
        {
            $numberRegistrations = $registrationsPerRun[$run];
            $numberFinisher = $finisherPerRun[$run];
            echo "<tr>";
            echo "<td>";
            echo Runs::GetInstance()->getRun($run)->getName();
            echo "</td>";
            echo "<td>";
            echo $numberRegistrations;
            echo "</td>";
            echo "<td>";
            echo $numberFinisher;
            echo "</td>";
            echo "</tr>";
            $totalNumberRegistrations += $numberRegistrations;
            $totalNumberFinisher += $numberFinisher;
        }
        echo "<tr>";
        echo "<td>";
        echo "Gesamt";
        echo "</td>";
        echo "<td>";
        echo $totalNumberRegistrations;
        echo "</td>";
        echo "<td>";
        echo $totalNumberFinisher;
        echo "</td>";
        echo "</tr>";

        echo "</table>";
        echo "</div>";
    }
}

new ShowCompetitionsPage();

?>
