<?php declare(strict_types = 1);

include_once 'private/page.php';
include_once 'private/database.php';
include_once 'private/jsFunctions.php';

class ClearDatabasePage extends Page
{
    function __construct()
    {
        parent::__construct();

        if ($this->m_access->hasAccess(AccessRight::DeleteImportExportDatabase))
        {
            $javaScript = getJSFunction_post();
            $style = "";
            $this->outputHeader($javaScript, $style);

            echo "<p class='big'>Datenbank löschen</p>";

            $backupName = "";
            if ($this->clearDatabase($backupName))
            {
                echo "<p class='yellow'>Die Datenbank wurde gelöscht.</p>";
                if ($backupName != "")
                {
                    echo "<p >Backup der gelöschten Daten: $backupName</p>";
                }
                else
                {
                    echo "<p >Kein Backup</p>";
                }
            }
            else
            {
                echo "<p class='yellow'>Datenbank konnte nicht gelöscht werden!</p>";
            }

            $this->beginFooter();
            $this->outputBackButton("admin.php");
            $this->endFooter();
        }
        else
        {
            $this->outputWrongPasswordErrorMessage();
        }
    }

    function clearDatabase(string &$backupName): bool
    {
        $database = new Database();
        if (Config::Get()['files']['backupDeletedDatabase'])
        {
            if (!$database->backup($backupName))
            {
                $database->close();
                return false;
            }
        }
        else
        {
            $backupName = "";
        }

        $database->clear();
        return $database->close();
    }
}

new ClearDatabasePage();

?>
