<?php declare(strict_types = 1);

include_once 'private/page.php';
include_once 'private/jsFunctions.php';
include_once 'private/database.php';

class DownloadCertificateForEntrySettingsPage extends Page
{
    function __construct()
    {
        parent::__construct();

        if ($this->m_access->hasAccess(AccessRight::Read))
        {
            if (isset($_POST["key"]))
            {
                $key = $_POST["key"];
            }
            else
            {
                $key = "";
            }

            $javaScript = getJSFunction_post();
            $javaScript .= getJSFunction_getElementValue();
            $javaScript .= <<<EOD
            function ageGroupCertificateClicked()
            {
                var inputField = document.getElementById("certificateText");
                inputField.disabled = true;
            }

            function specialCertificateClicked()
            {
                var inputField = document.getElementById("certificateText");
                inputField.disabled = false;
            }
EOD;

            $style = <<<EOD
            input[type="checkbox"]{vertical-align: bottom;}
            label{vertical-align: bottom;}

            table
            {
                border-spacing:0em;
                max-width: 25em;
            }
EOD;

            $this->outputHeader($javaScript, $style);

            $this->listSettings($key);

            $this->beginFooter();
            $parameters = new Parameters();
            $parameters->add("key", $key);
            $this->outputBackButton("adminChangeEntry.php", $parameters);

            $parameters = new Parameters();
            $parameters->add("key", $key);
            $parameters->addJS("certificateType", "getElementValue('certificateType')");
            $parameters->addJS("certificateText", "getElementValue('certificateText')");
            Menu::OutputButton("fa-file-download", "Download (PDF)", $this->getPostString("downloadCertificates.php", $parameters), "download");

            $this->endFooter();
        }
        else
        {
            $this->outputWrongPasswordErrorMessage();
        }
    }

    function listSettings($key)
    {
        $database = new Database();
        $database->close();
        $entry = $database->searchEntryByKey($key);

        if ($entry->isValid())
        {
            $this->listSettingsForEntry($entry);
        }
        else
        {
            echo "<p class='big'>Urkunde für Eintrag $key</p>";
            echo "<p class='yellow'>Kein gültiger Eintrag gefunden !</p>";
        }
    }

    function listSettingsForEntry(Entry $entry)
    {
        $name = $entry->getFirstname() . " " . $entry->getLastname();
        echo "<p class='big'>Urkunde für $name</p>";
        echo "<p class='yellow'>Gewünschten Urkundentyp auswählen</p>";

        echo "<p>";
        echo "<div><input id='ageGroupCertificate' name='certificateType' value='ageGroup' type='radio' onchange='ageGroupCertificateClicked();' checked><label for='ageGroupCertificate'> Altersklassenurkunde</label></div>";
        echo "</p>";

        echo "<p>";
        echo "<div><input id='specialCertificate' name='certificateType' value='special' type='radio' onchange='specialCertificateClicked();'><label for='specialCertificate'> Sonderurkunde</label></div>";

        echo "<table>";
        echo "<tr>";
        echo "<td style='padding-left: 2em; padding-right: 1em;'>";
        echo "Text";
        echo "</td>";
        echo "<td class='fullwidth'>";
        echo "<input id='certificateText' class='fullwidth' disabled>";
        echo "</td>";
        echo "</table>";

        echo "</p>";
    }

}

new DownloadCertificateForEntrySettingsPage();

?>
