<?php

declare(strict_types=1);

include_once 'private/page.php';
include_once 'private/database.php';
include_once 'private/entries.php';
include_once 'private/jsFunctions.php';
include_once 'private/messageBox.php';

class BlockerListPage extends Page
{
    function __construct()
    {
        parent::__construct();

        if ($this->m_access->hasAccess(AccessRight::ChangeBlockerlist))
        {
            $javaScript = getJSFunction_post();
            $javaScript .= getJSFunction_getElementValue();

            $style = <<<EOD

            .blockerTable
            {
                margin-bottom: 0.5em;
                padding: 0em;
                margin-left: 0em;
                border-collapse: collapse;
                width: 100%;
                border: 1px solid Black;
                font-size: 0.7em;
            }
            .blockerTable td
            {
                padding: 0.5em;
                width: 100%;
            }
            .blockerTable button
            {
                margin-bottom: 0em;
                height: 1.5em;
                white-space: nowrap;
            }
            .oddRow { background: #dfdfdf; color: Black;}
            .evenRow { background: #ffffff; color: Black;}
EOD;

            $this->outputHeader($javaScript, $style);
            $database = new Database();

            if (isset($_POST["addStartNumberBlocker"]))
            {
                if ($this->getStartNumberRange($_POST["addStartNumberBlocker"], $from, $to))
                {
                    for ($startnumber = $from; $startnumber <= $to; $startnumber++)
                    {
                        if (!$database->isStartnrBlocked($startnumber))
                        {
                            $database->addBlockedStartNumber($startnumber);
                        }
                    }
                }
            }

            if (isset($_POST["deleteStartNumberBlocker"]))
            {
                if ($this->getStartNumberRange($_POST["deleteStartNumberBlocker"], $from, $to))
                {
                    for ($startnumber = $from; $startnumber <= $to; $startnumber++)
                    {
                        if ($database->isStartnrBlocked($startnumber))
                        {
                            $database->removeBlockedStartNumber($startnumber);
                        }
                    }
                }
            }

            if (isset($_POST["addChipNumberBlocker"]))
            {
                if ($this->getChipNumberRange($_POST["addChipNumberBlocker"], $from, $to))
                {
                    for ($chipnumber = $from; $chipnumber <= $to; $chipnumber++)
                    {
                        if (!$database->isChipnrBlocked($chipnumber))
                        {
                            $database->addBlockedChipNumber($chipnumber);
                        }
                    }
                }
            }

            if (isset($_POST["deleteChipNumberBlocker"]))
            {
                if ($this->getChipNumberRange($_POST["deleteChipNumberBlocker"], $from, $to))
                {
                    for ($chipnumber = $from; $chipnumber <= $to; $chipnumber++)
                    {
                        if ($database->isChipnrBlocked($chipnumber))
                        {
                            $database->removeBlockedChipNumber($chipnumber);
                        }
                    }
                }
            }

            if (!$database->close())
            {
                echo "<p class='yellow'>Fehler: Blocker konnte nicht in Datenbank geschrieben werden !!!</p>";
            }

            $this->printList($database);
            $this->beginFooter();
            $this->outputBackButton("admin.php");
            $this->endFooter();
        }
        else
        {
            $this->outputWrongPasswordErrorMessage();
        }
    }

    function getStartNumberRange(string $input, &$from, &$to): bool
    {
        if (preg_match('/^([0-9]+)-([0-9]+)$/', $input, $matches))
        {
            $from = intval($matches[1]);
            $to = intval($matches[2]);
            return $this->checkIfStartNumberRangeValid($from, $to);
        }
        else
        if (preg_match('/^([0-9]+)$/', $input, $matches))
        {
            $from = intval($matches[1]);
            $to = $from;
            return $this->checkIfStartNumberRangeValid($from, $to);
        }
        MessageBox::OutputMessage("Eingabe ungültig! Gültiges Format: <Nummer> | <Nummer>-<Nummer>");
        return false;
    }

    function checkIfStartNumberRangeValid($from, $to): bool
    {
        $minStartNumber = Config::Get()['startNumbers']['min'];
        $maxStartNumber = Config::Get()['startNumbers']['max'];
        if (
            $from < $minStartNumber || $from > $maxStartNumber
            || $to < $minStartNumber || $to > $maxStartNumber
        )
        {
            MessageBox::OutputMessage("Startnummer muss im Bereich $minStartNumber - $maxStartNumber liegen!");
            return false;
        }

        if ($from > $to)
        {
            MessageBox::OutputMessage("Bei Bereichseingabe muss die erste Startnummer kleiner, gleich der zweiten Startnummer sein!");
            return false;
        }
        return true;
    }

    function getChipNumberRange(string $input, &$from, &$to): bool
    {
        if (preg_match('/^([0-9]+)-([0-9]+)$/', $input, $matches))
        {
            $from = intval($matches[1]);
            $to = intval($matches[2]);
            return $this->checkIfChipNumberRangeValid($from, $to);
        }
        else
        if (preg_match('/^([0-9]+)$/', $input, $matches))
        {
            $from = intval($matches[1]);
            $to = $from;
            return $this->checkIfChipNumberRangeValid($from, $to);
        }
        MessageBox::OutputMessage("Eingabe ungültig! Gültiges Format: <Nummer> | <Nummer>-<Nummer>");
        return false;
    }

    function checkIfChipNumberRangeValid($from, $to): bool
    {
        $minChipNumber = Config::Get()['chipNumbers']['min'];
        $maxChipNumber = Config::Get()['chipNumbers']['max'];
        if (
            $from < $minChipNumber || $from > $maxChipNumber
            || $to < $minChipNumber || $to > $maxChipNumber
        )
        {
            MessageBox::OutputMessage("Chipnummer muss im Bereich $minChipNumber - $maxChipNumber liegen!");
            return false;
        }

        if ($from > $to)
        {
            MessageBox::OutputMessage("Bei Bereichseingabe muss die erste Chipnummer kleiner, gleich der zweiten Chipnummer sein!");
            return false;
        }
        return true;
    }

    function printStartNumbers(Database $database)
    {
        echo "<p class='yellow noMarginTop'>Startnummer</p>";

        $minStartNumber = Config::Get()['startNumbers']['min'];
        $maxStartNumber = Config::Get()['startNumbers']['max'];

        $numbers = $database->getBlockedStartNumbers();

        echo "<div style='white-space:nowrap;'>";
        echo "<input class='marginRight marginBottom' style='width: 5em;' id='startNumber' name='startNumber' value=''>";

        $parameters = new Parameters();
        $parameters->addJS("addStartNumberBlocker", "getElementValue('startNumber')");
        $this->outputButton("blockerList.php", "Hinzufügen", $parameters);

        $parameters = new Parameters();
        $parameters->addJS("deleteStartNumberBlocker", "getElementValue('startNumber')");
        $this->outputButton("blockerList.php", "Löschen ✘", $parameters, "", false, "button redButton noMarginRight");
        echo "</div>";

        echo "<table class='blockerTable'>";
        $i = 0;
        foreach ($numbers as $number)
        {
            $id = ($i & 1) ? 'oddRow' : 'evenRow';
            echo "<tr class='$id'>";
            echo "<td>$number</td>";
            echo "</tr>";
            $i++;
        }
        echo "</table>";
    }

    function printChipNumbers(Database $database)
    {
        echo "<p class='yellow noMarginTop'>Chipnummer</p>";

        $minChipNumber = Config::Get()['chipNumbers']['min'];
        $maxChipNumber = Config::Get()['chipNumbers']['max'];

        $numbers = $database->getBlockedChipNumbers();

        echo "<div style='white-space:nowrap;'>";
        echo "<input class='marginRight marginBottom' style='width: 5em;' id='chipNumber' name='chipNumber' value=''>";

        $parameters = new Parameters();
        $parameters->addJS("addChipNumberBlocker", "getElementValue('chipNumber')");
        $this->outputButton("blockerList.php", "Hinzufügen", $parameters);

        $parameters = new Parameters();
        $parameters->addJS("deleteChipNumberBlocker", "getElementValue('chipNumber')");
        $this->outputButton("blockerList.php", "Löschen ✘", $parameters, "", false, "button redButton noMarginRight");
        echo "</div>";

        echo "<table class='blockerTable'>";
        $i = 0;
        foreach ($numbers as $number)
        {
            $id = ($i & 1) ? 'oddRow' : 'evenRow';
            echo "<tr class='$id'>";
            echo "<td>$number</td>";
            echo "</tr>";
            $i++;
        }
        echo "</table>";
    }

    function printList(Database $database)
    {
        echo "<p class='big'>Blockerliste</p>";
        echo "<div class='displayFlex flexWrap'>";
        echo "<div style='margin-right: 7em;'>";
        $this->printStartNumbers($database);
        echo "</div>";
        echo "<div class='margin-right margin-bottom'>";
        $this->printChipNumbers($database);
        echo "</div>";
        echo "</div>";
    }
}

new BlockerListPage();
