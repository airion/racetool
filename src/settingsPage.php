<?php declare(strict_types = 1);

include_once 'private/page.php';
include_once 'private/jsFunctions.php';

class SettingsPage extends Page
{
    function __construct()
    {
        parent::__construct();

        if ($this->m_access->hasAccess(AccessRight::ChangeSetting))
        {

            $javaScript = "";
            $javaScript .= getJSFunction_post();
            $javaScript .= getJSFunction_getElementValue();
            $style = "";
            $this->outputHeader($javaScript, $style);

            echo "<p class='big'>Einstellungen</p>";
            echo "<p class='yellow'>Aktueller Programm Modus</p>";
            $this->programMode();

            $this->beginFooter();
            $this->outputBackButton("admin.php");

            $parameters = new Parameters();
            $parameters->addJS("newMode", "getElementValue('mode')");
            Menu::OutputButton("fa-edit", "Einstellungen übernehmen", $this->getPostString("changeMode.php", $parameters), "confirm");
            $this->endFooter();
        }
        else
        {
            $this->outputWrongPasswordErrorMessage();
        }
    }

    function programMode()
    {
        $programMode = Config::Get()['program']['mode'];

        echo "<select id='mode' class='marginRight marginBottom'>";

        echo '<option value="' . ProgramMode::PreRegistration . '"';
        if ($programMode == ProgramMode::PreRegistration)
        {
            echo " selected";
        }
        echo ">1. Voranmeldung";
        echo "</option>";

        echo '<option value="' . ProgramMode::PreRegistrationValidatedAndFinished . '"';
        if ($programMode == ProgramMode::PreRegistrationValidatedAndFinished)
        {
            echo " selected";
        }
        echo ">2. Voranmeldung validiert & abgeschlossen";
        echo "</option>";

        echo '<option value="' . ProgramMode::Registration . '"';
        if ($programMode == ProgramMode::Registration)
        {
            echo " selected";
        }
        echo ">3. Anmeldung";
        echo "</option>";

        echo '<option value="' . ProgramMode::Finished . '"';
        if ($programMode == ProgramMode::Finished)
        {
            echo " selected";
        }
        echo ">4. Abgeschlossen";
        echo "</option>";

        echo "</select>";
    }
}

new SettingsPage();

?>
