<?php declare(strict_types = 1);

include_once 'private/page.php';
include_once 'private/config.php';
include_once 'private/registrationFields.php';
include_once 'private/entry.php';
include_once 'private/database.php';
include_once 'private/jsFunctions.php';
include_once 'private/convertStringToHTML.php';

class EditEntryPage extends Page
{
    function __construct()
    {
        parent::__construct();

        $style = "";
        $style .= getRegistrationFormStyle();

        $javaScript = "";
        $javaScript .= getJSFunction_post();
        $javaScript .= getJSFunction_getElementValue();

        $key = "";
        if (isset($_POST["key"]))
        {
            $key = $_POST["key"];
        }
        else
        {
            if (isset($_GET["key"]))
            {
                $key = $_GET["key"];
            }
        }

        $parameters = new Parameters();
        $parameters->add("key", $key);

        if ($this->outputHeader($javaScript, $style) > 0)
        {
            $database = new Database();
            $database->close();

            $entry = $database->searchEntryByKey($key);
            if ($entry->isValid())
            {
                echo "<p class='big'>Anmeldedaten ändern</p>";
                $this->preRegistrationForm($entry, $database, $parameters);

                $this->beginFooter();
                $this->outputBackButton("index.php");

                $postClear = $this->getPostFunction("deleteEntry.php", $parameters);
                $js = "openMessageBox('Anmeldung wirklich stornieren?', MessageBoxType_Question, $postClear)";
                Menu::OutputButton("fa-trash-alt menuButtonRed", "Anmeldung stornieren", $js , "delete");
            }
            else
            {
                echo "<p class='yellow'>Keine Anmeldung mit Code \"" . convertStringToHTML($key) . "\" vorhanden!</p>";

                $this->beginFooter();
                $this->outputBackButton("editEntryInputKey.php");
            }
        }
        else
        {
            $this->beginFooter();
            $this->outputBackButton("index.php");
        }

        $this->endFooter($parameters);
    }

    function preRegistrationForm(Entry $entry, Database $database, Parameters $parameters)
    {
        $key = $entry->getKey();
        $registrationFieldsEnabled = Config::Get()['program']['mode'] == ProgramMode::PreRegistration;
        $showCup = false;
        $showEmail = true;
        $showStartnr = false;
        $showChipnr = false;
        $showAgeGroups = false;
        $showTime = false;
        $checkYearRestrictive = true;

        outputRegistrationFields($this, $entry, $parameters, "addEntry.php",  $showCup, $showEmail, $showStartnr, $showChipnr, $showAgeGroups, $showTime,
            $registrationFieldsEnabled, $checkYearRestrictive, $database);
    }
}

new EditEntryPage();

?>