"use strict";

var MessageBoxType_Message = 0;
var MessageBoxType_Question = 1;

function openMessageBox(text, type, yesFunction = function(){})
{
    if (type == MessageBoxType_Message)
    {
        var symbol = "fa-exclamation-circle";
        var button0Text = "Ok";
        var button1Text = "";
    }
    else
    {
        var symbol = "fa-question-circle";
        var button0Text = "Ja";
        var button1Text = "Nein";
    }

    var div0 = document.createElement("div");
    div0.className="messageBox displayFlex alignItemsCenter justifyContentCenter";
    document.body.appendChild(div0);

    var div1 = document.createElement("div");
    div0.appendChild(div1);

    var div2 = document.createElement("div");
    div2.className="displayFlex";
    div1.appendChild(div2);

    var span = document.createElement("span");

    span.className="fas " + symbol +" fa-3x";
    div2.appendChild(span);

    var p = document.createElement("p");
    p.appendChild(document.createTextNode(text));
    div2.appendChild(p);

    var div3 = document.createElement("div");
    div3.className="displayFlex justifyContentCenter";
    div1.appendChild(div3);

    var button0 = document.createElement("button");
    button0.className="button";
    button0.id="messageBoxOk";
    button0.appendChild(document.createTextNode(button0Text + " "));
    var buttonDiv = document.createElement("div");
    buttonDiv.className = "fas fa-check messageBoxGreen";
    button0.appendChild(buttonDiv);
    div3.appendChild(button0);
    button0.focus();

    if (button1Text != "")
    {
        var button1 = document.createElement("button");
        button1.className="button";
        button1.id="messageBoxCancel";
        var text = "Nein";
        button1.appendChild(document.createTextNode(button1Text + " "));
        var buttonDiv = document.createElement("div");
        buttonDiv.className = "fas fa-times messageBoxRed";
        button1.appendChild(buttonDiv);
        div3.appendChild(button1);
    }

    if (type == MessageBoxType_Message)
    {
        button0.onclick = function(){closeMessageBox(div0)};
    }

    if (type == MessageBoxType_Question)
    {
        button0.onclick = function(){closeMessageBox(div0); yesFunction()};
        button1.onclick = function(){closeMessageBox(div0)};
    }
}

function closeMessageBox(messageBox)
{
    messageBox.remove();
}

