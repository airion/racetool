<?php declare(strict_types = 1);

include_once 'private/page.php';
include_once 'private/entry.php';
include_once 'private/database.php';

class DownloadDatabasePage extends Page
{
    function __construct()
    {
        parent::__construct();

        if ($this->m_access->hasAccess(AccessRight::DeleteImportExportDatabase))
        {
            $this->printList();
        }
        else
        {
            $this->outputWrongPasswordErrorMessage();
        }
    }

    function printList()
    {
        $database = new Database();

        $dbPath = Config::Get()['files']['dbName'];
        $pathParts = pathinfo($dbPath);

        $fname = $pathParts['filename'];
        $extension = $pathParts['extension'];

        $timeOfLastModification = $database->getTimeOfLastModificationAsString();
        $filename = $fname . "_" . $timeOfLastModification . "." . $extension;

        header("Content-Type: text/csv; charset=UTF-8");
        header("Content-Disposition: attachment; filename=$filename");

        $ok = $database->readfile();
        assert($ok);
    }
}

new DownloadDatabasePage();

?>