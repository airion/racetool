<?php declare(strict_types = 1);

include_once 'private/page.php';
include_once 'private/jsFunctions.php';
include_once 'private/runs.php';

class DownloadListSettingsPage extends Page
{
    function __construct()
    {
        parent::__construct();

        if ($this->m_access->hasAccess(AccessRight::Read))
        {
            $javaScript = getJSFunction_post();
            $javaScript .= getJSFunction_getElementValue();

            $style = <<<EOD
            input[type="checkbox"]{vertical-align: bottom;}
            label{vertical-align: bottom;}
EOD;
            $this->outputHeader($javaScript, $style);

            $this->listSettings();

            $this->beginFooter();

            $this->outputBackButton("admin.php");

            $parameters = new Parameters();
            $parameters->addJS('overview', "getElementValue('overview')");
            $runs = Runs::GetInstance()->getRuns();
            $numberOfRuns = count($runs);
            for ($runIndex = 0; $runIndex < $numberOfRuns; $runIndex++)
            {
                $name = "run" . $runIndex . "List";
                $parameters->addJS($name, "getElementValue('$name')");
            }

            Menu::OutputButton("fa-file-download", "Download (PDF)", $this->getPostString("downloadResults.php", $parameters), "download");
            $this->endFooter();
        }
        else
        {
            $this->outputWrongPasswordErrorMessage();
        }
    }

    function listSettings()
    {
        echo "<p class='big'>Ergebnislisten</p>";
        echo "<p class='yellow'>Gewünschte Listen auswählen</p>";

        echo "<p>";
        echo "<div><input id='overview' type='checkbox'><label for='overview'>Übersicht</label></div>";
        echo "<div class='smallYellow'>(Beteiligung Wettbewerbe / Vereine, Sieger MW)</div>";
        echo "</p>";

        $runs = Runs::GetInstance()->getRuns();
        foreach ($runs as $runIndex => $run)
        {
            echo "<p>";
            $runName = $run->getName();
            $id = "run" . $runIndex . "List";
            echo "<div><input id='$id' type='checkbox'><label for='$id'> Wettbewerb $runName</label></div>";
            $description = "(Einlaufliste, Altersklassen Wertung";
            if ($run->getTeamPlacing())
            {
                $description .= ", Mannschaftswertung";
            }
            $description .= ")";
            echo "<div class='smallYellow'>$description</div>";
            echo "</p>";
        }
    }
}

new DownloadListSettingsPage();

?>
