<?php declare(strict_types = 1);

include_once 'private/page.php';
include_once 'private/jsFunctions.php';
include_once 'private/runs.php';
include_once 'private/database.php';
include_once 'private/getTeamList.php';

class DownloadCertificatesSettingsPage extends Page
{
    function __construct()
    {
        parent::__construct();

        if ($this->m_access->hasAccess(AccessRight::Read))
        {
            $javaScript = getJSFunction_post();
            $javaScript .= getJSFunction_getElementValue();

            $javaScript .=  <<<EOD
            function teamListClicked()
            {
                var checked = getElementValue('teamList');
                var inputField = document.getElementById("team");
                inputField.disabled = (checked == 0);
            }
EOD;

            $style = <<<EOD
            input[type="checkbox"]{vertical-align: bottom;}
            label{vertical-align: bottom;}
EOD;
            $this->outputHeader($javaScript, $style);

            $this->listSettings();

            $this->beginFooter();
            $this->outputBackButton("admin.php");

            $parameters = new Parameters();

            $runs = Runs::GetInstance()->getRuns();
            $numberOfRuns = count($runs);
            for ($runIndex = 0; $runIndex < $numberOfRuns; $runIndex++)
            {
                $name = "run" . $runIndex . "List";
                $parameters->addJS($name, "getElementValue('$name')");
            }
            $parameters->addJS("teamList", "getElementValue('teamList')");
            $parameters->addJS("team", "getElementValue('team')");

            Menu::OutputButton("fa-file-download", "Download (PDF)", $this->getPostString("downloadCertificates.php", $parameters), "download");
            $this->endFooter();
        }
        else
        {
            $this->outputWrongPasswordErrorMessage();
        }
    }

    function listSettings()
    {
        echo "<p class='big'>Urkunden</p>";
        echo "<p class='yellow'>Gewünschte Urkunden auswählen</p>";

        $runs = Runs::GetInstance()->getRuns();

        $database = new Database();
        $database->close();
        $teams = getTeamList($database);

        foreach ($runs as $runIndex => $run)
        {
            echo "<p>";
            $runName = $run->getName();
            $id = "run" . $runIndex . "List";
            echo "<div><input id='$id' type='checkbox'><label for='$id'> Wettbewerb $runName</label></div>";
            $maxAgeGroupPlace = $run->getCertificatesMaxAgeGroupPlace();

            $certificateDescription = "(Altersklassenurkunden: ";
            if ($maxAgeGroupPlace == PHP_INT_MAX)
            {
                $certificateDescription .= "Alle";
            }
            else
            {
                $certificateDescription .= "Platz 1 - $maxAgeGroupPlace";
            }
            if ($run->getTeamPlacing())
            {
                $certificateDescription .= ", 3x Mannschaftsurkunden: Platz 1 - 3";
            }
            $certificateDescription .= ")";
            echo "<div class='smallYellow'>$certificateDescription</div>";
            echo "</p>";
        }
        echo "</br>";
        echo "<p>";
        echo "<div><input id='teamList' type='checkbox' onchange='teamListClicked();'><label for='teamList'> Für bestimmten Verein</label></div>";

        echo "<span>&nbsp;&nbsp;&nbsp;&nbsp;</span><select id='team' disabled>";
        echo "<option value='' selected disabled>-- Bitte auswählen --</option>";

        foreach ($teams as $team)
        {
            $teamString = $team != "" ? $team : "---";
            echo "<option value='$team'>$teamString</option>";
        }
        echo "</select>";

        echo "<div class='smallYellow'>(Alle Altersklassen- und Mannschaftsurkunden für einen Verein)</div>";
        echo "</p>";
    }
}

new DownloadCertificatesSettingsPage();

?>
