<?php declare(strict_types = 1);

include_once 'private/page.php';
include_once 'private/entry.php';
include_once 'private/entrySorter.php';
include_once 'private/database.php';
include_once 'private/chipIds.php';
include_once 'private/teamNumberGenerator.php';
include_once 'private/checkCosaCollidingTeamNames.php';

class DownloadCosaPage extends Page
{

    function __construct()
    {
        parent::__construct();

        if ($this->m_access->hasAccess(AccessRight::Read))
        {
            $database = new Database();
            $database->close();

            $collidingTeamNames = CheckCosaCollidingTeamNames::GetCollidingTeamNames($database);
            if (!empty($collidingTeamNames))
            {
                $javaScript = getJSFunction_post();
                $this->outputHeader($javaScript);
                CheckCosaCollidingTeamNames::OutputCollidingTeamNames($collidingTeamNames);

                $this->beginFooter();
                $this->outputBackButton("admin.php");
                $this->endFooter();
            }
            else
            {
                $this->printList($database);
            }
        }
        else
        {
            $this->outputWrongPasswordErrorMessage();
        }
    }

    function outputLine(string $text)
    {
        echo mb_convert_encoding($text . "\r\n", "Windows-1252");
    }

    function printList(Database $database)
    {
        $teamNumberGenerator = new TeamNumberGenerator($database);

        $entries = $database->getEntries();
        EntrySorter::Sort($entries, EntrySorter::OrderStartnrIndex);

        $timeOfLastModification = $database->getTimeOfLastModificationAsString();
        $filename = "cosa_teilnehmerliste_" . $timeOfLastModification . ".csv";

        $chipIds = ChipIds::GetInstance();
        if ($chipIds->isOk())
        {
            header("Content-Type: text/csv; charset=Windows-1252");
            header("Content-Disposition: attachment; filename=$filename");

            $this->outputLine("Chip-Nr.;Start-Nr.;Name;Vorname;Jg.;m/w;LV;Ver.Nr.;Verein/Ort;AK;Wettb.Nr.;Wert.Gr.");

            foreach ($entries as $entry)
            {
                $chipNr = $entry->getChipnr();
                if ($chipNr != Entry::INVALID_CHIPNR)
                {
                    $chipId = $chipIds->getIdForNumber($chipNr);
                }
                else
                {
                    $chipId = "---";
                }
                $startnrString = $entry->getStartnrAsString();
                $name = $entry->getLastname();
                $firstname = $entry->getFirstname();
                $year = $entry->getYearAsString();
                $gender = $entry->getGender();
                $lv = "";
                $team = $entry->getTeamAsString();
                $vernr = $teamNumberGenerator->getNumberAsString($entry);
                $ak = $entry->getAgeGroupAsString();
                $wettbNr = $entry->getRun() + 1;
                $wertGr = "";

                $this->outputLine("$chipId;$startnrString;$name;$firstname;$year;$gender;$lv;$vernr;$team;$ak;$wettbNr;$wertGr");
            }
        }
    }
}

new DownloadCosaPage();
?>