<?php declare(strict_types = 1);

include_once 'private/page.php';
include_once 'private/database.php';
include_once 'private/jsFunctions.php';

class UploadDatabaseReceiveFilePage extends Page
{
    function __construct()
    {
        parent::__construct();

        if ($this->m_access->hasAccess(AccessRight::DeleteImportExportDatabase))
        {
            if (isset($_FILES['userfile']['tmp_name']))
            {
                $fileName = $_FILES['userfile']['tmp_name'];
            }
            else
            {
                $fileName = "";
            }
            if ($fileName != "")
            {
                $this->importFile($fileName);
            }
            else
            {
                $this->outputErrorMessage("Keine Datei für Import erhalten!", "admin.php");
            }
        }
        else
        {
            $this->outputWrongPasswordErrorMessage();
        }
    }

    function openDatabaseAndImportData(string $importFilename): bool
    {
        $database = new Database();

        if (!$database->isEmpty())
        {
            echo "<p class='yellow'>Aktuelle Datenbank nicht leer!</p>";
            return false;
        }
        return $database->restoreAndClose($importFilename);
    }

    function importFile(string $fileName)
    {
        $uploadLimit = Config::Get()['files']['uploadLimit'];
        $javaScript = "";
        $style = "";
        $javaScript .= getJSFunction_post();

        $this->outputHeader($javaScript, $style);
        echo "<p class='big'>Datenbank hochladen</p>";

        $size = intval($_FILES['userfile']['size']);

        if ($size <= $uploadLimit)
        {
            if ($this->openDatabaseAndImportData($fileName))
            {
                echo '<p class="yellow">Datenbank wurde erfolgreich hochgeladen.</p>';
            }
            else
            {
                echo '<p class="yellow">Datenbank wurde nicht hochgeladen.</p>';
            }
        }
        else
        {
            echo "<p class='yellow'>Fehler: Hochgeladene Datei ($size Bytes) überschreitet das Limit von $uploadLimit Bytes !</p>";
        }

        $this->beginFooter();
        $this->outputBackButton("admin.php");
        $this->endFooter();
    }
}

new UploadDatabaseReceiveFilePage();

?>